<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewAppointmentForDoctor extends Mailable
{
    use Queueable, SerializesModels;
    public $doctor, $item;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($doctor, $item)
    {
        $this->doctor =  $doctor;
        $this->item =  $item;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.appointments.NewAppointment');
    }
}
