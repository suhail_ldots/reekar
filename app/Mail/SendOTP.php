<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendOTP extends Mailable
{
    use Queueable, SerializesModels;
    public $request, $otp;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request, $otp)
    {
        $this->request = $request;
        $this->otp = $otp;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Dont share otp with anyone')->markdown('emails.otp.send');
    }
}
