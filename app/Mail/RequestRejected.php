<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RequestRejected extends Mailable
{
    use Queueable, SerializesModels;

    public $user, $item;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $item)
    {
        $this->user =  $user;
        $this->item =  $item;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.CarAdditionRequestReject');
    }
}
