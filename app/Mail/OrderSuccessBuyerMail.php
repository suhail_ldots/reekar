<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderSuccessBuyerMail extends Mailable
{
    use Queueable, SerializesModels;
    public $order;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Your order have been successfully placed with Reekar')
        ->markdown('emails.orders.buyer')
        ->attach($this->order->order_invoice ? \Url('storage').'/invoice'.'/'.$this->order->order_invoice : '')
        ->attach(\Url('storage').'/agreement'.'/'.'VehicleRentingAgreement.docx');
    }
}
