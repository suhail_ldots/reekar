<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Masters\SubscriptionPackages;
use App\Models\Masters\Feature;
use App\Models\Masters\PlanFeature;

use App\Models\NursingHome;
use App\Models\Bookings;
use App\Models\Doctor;
use App\Models\DoctorNursingHomeRelation;
use App\User;
use DB;

class HomeController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getNursing(Request $request)
    {
        $radius = 100;

        $nursingHome = NursingHome::where('status', 1)->get(['user_id']);

        $query = User::query();

        $query->whereIn('id', $nursingHome);
        
        if($request->search_location != '' && $request->search_location != null){
            if ($request->user_lat != null && $request->user_lat != '' && $request->user_long != null && $request->user_long != '') {
                $coordinates = [
                    'latitude' => $request->user_lat,
                    'longitude' => $request->user_long,
                ];
            } else {
                $address = str_replace(" ", "+", $request->search_location);
                $json = file_get_contents("https://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&key=AIzaSyA2X23zOQ4hIRy3ZoHPWxSqnPRzjAdOoCc");
                $json = json_decode($json);

                $lat = count($json->{'results'}) ? $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'} : 0;
                $long = count($json->{'results'}) ? $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'} : 0;
                $coordinates = [
                    'latitude' => $lat,
                    'longitude' => $long,
                ];
            }
            $query->isWithinMaxDistance($coordinates, $radius);

            $query->orderBy('distance', 'ASC');

        }
        $items = $query->where('status', '1')->whereIn('role_id', ['3','4'])->paginate(20);

        $plans = SubscriptionPackages::where('status', 1)->get();
        // dd($items);
        return view('site_view.modules.home', [
            "nursing_home" => $items,
            "plans" => $plans,
            "doctors" => User::where(['role_id' => 3, 'status' => 1])->get(),
        ]);
        
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function searchNursing(Request $request)
    {
        /* $nursingHome = NursingHome::where('status', 1)->get();
        
        $nursingHome->doctor_nursing_r->doctor
        $nursingHome->user;
        $nursing = DoctorNursingHomeRelation::all(); */


        $radius = 300;

        $nursingHome = NursingHome::where('status', 1)->get(['user_id']);

        //Check user lat long for nursinghomes
        $checkUsers = User::whereIn('id', $nursingHome)->get();
        foreach($checkUsers as $checkUser){
            if($checkUser->lat == '' && $checkUser->address != ''){
                $new_address = str_replace(" ", "+", $checkUser->address);
                $json = file_get_contents("https://maps.google.com/maps/api/geocode/json?address=$new_address&sensor=false&key=AIzaSyA2X23zOQ4hIRy3ZoHPWxSqnPRzjAdOoCc");
                $json = json_decode($json);

                $lat = count($json->{'results'}) ? $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'} : 0;
                $long = count($json->{'results'}) ? $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'} : 0;
                
                //Update user with lat long
                User::where('id', $checkUser->id)->update(['lat' => $lat, 'long' => $long]);
            }
        }

        $query = User::query();

        $query->whereIn('id', $nursingHome)->whereIn('role_id', ['3','4']);
        
        if(($request->search_location != '' && $request->search_location != null) || ($request->user_lat != null && $request->user_lat != '' && $request->user_long != null && $request->user_long != '')){
            if ($request->user_lat != null && $request->user_lat != '' && $request->user_long != null && $request->user_long != '') {
                $coordinates = [
                    'latitude' => $request->user_lat,
                    'longitude' => $request->user_long,
                ];
            } else {
                //return "hello";
                $address = str_replace(" ", "+", $request->search_location);
                $json = file_get_contents("https://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&key=AIzaSyA2X23zOQ4hIRy3ZoHPWxSqnPRzjAdOoCc");
                $json = json_decode($json);

                $lat = count($json->{'results'}) ? $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'} : 0;
                $long = count($json->{'results'}) ? $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'} : 0;
                $coordinates = [
                    'latitude' => $lat,
                    'longitude' => $long,
                ];
            }
            
            $query->select("*", DB::raw("6371 * acos(cos(radians(" . $coordinates['latitude'] . "))
            * cos(radians(`lat`)) * cos(radians(`long`) - radians(" . $coordinates['longitude'] . "))
            + sin(radians(" .$coordinates['latitude']. ")) * sin(radians(`lat`))) AS distance"));

            $query->whereRaw(DB::raw("6371 * acos(cos(radians(" . $coordinates['latitude'] . "))
            * cos(radians(`lat`)) * cos(radians(`long`) - radians(" . $coordinates['longitude'] . "))
            + sin(radians(" .$coordinates['latitude']. ")) * sin(radians(`lat`)))") . '<= ?', [$radius]);

           // $query->having('distance', '<', $radius);
            $query->orderBy('distance', 'asc');
        }
       if($request->search_speciality != '' && $request->search_speciality != null){
            
            $query->whereHas('nursinghome', function($query1) use ($request){
                // $nursingHome->doctor_nursing_r->doctor
                $query1->whereHas('doctor_nursing_r', function($query2) use ($request){
                    $query2->whereHas('doctor', function($query3) use ($request){
                        $query3->where('speciality', $request->search_speciality);
                    });
                });
                
            });
       }
       if($request->search_text != '' && $request->search_text != null){
            $query->where('first_name', 'like', '%'.$request->search_text.'%');
       }
        $items = $query->where('status', '1')->paginate(20);
        // $items = $query->where('status', '1')->simplePaginate(20);
        //    return $items;
        $plans = SubscriptionPackages::where('status', 1)->get();
        
        if(request()->ajax()){
            return view('site_view.modules.searched_doctor_listing_body', [
                "nursing_home" => $items,                
            ]); 
        }      
        return view('site_view.modules.searched_doctor_listing', [
            "nursing_home" => $items,
            "plans" => $plans,
            "search_speciality" => $request->search_speciality,
            "search_text" => $request->search_text,
            'user_lat' => $request->user_lat,
            'user_long' => $request->user_long,
            'location' => $request->search_location,
        ]);       
    }
    public function featuredPlan(Request $request, $plan_id )
    {
        $plans = SubscriptionPackages::where(['status'=> 1, 'id'=>$plan_id])->first();
        
        return view('site_view.modules.featured_plan', [
            "item" => false,
            "plans" => $plans,
            "public_key" => config('api.stripe_p_key'),
        ]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function nursingDetails(Request $request)
    {  
        $user  =  User::find($request->id);
        if(!$user)
        return back()->with('message', "Sorry clinic not found!");

        if($user->role_id == 3)//single dr. clinic
        return view('site_view.modules.doctor_details', [
            "item" => $user,            
        ]);

        if($user->role_id == 4){
            //multi-speciality hospital
            $allDr = DoctorNursingHomeRelation::where(['nursing_home_id' => $user->nursing_home_id, 'status'=> 1])->get();
            return view('site_view.modules.multispeciality_clinic_details', [
                "item" => $user,
                "allDoctors" => $allDr,            
            ]);
        }
        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function doctorDetails(Request $request)
    {        
        return view('site_view.modules.nursing_details', [
            "item" => false,
            "plans" => false,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createAppointment(Request $request)
    {
        return view('site_view.modules.book_appointment', [
            "item" => false,
            "plans" => false,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function searchDoctor(Request $request)
    {
        if(isset($request->doctor)){
            $doctor = Doctor::find($request->doctor);    
            if(!$doctor){
                if (request()->ajax()) {
                    return response()->json([
                        'data' => '',
                    ]); 
                }
            }    
        } 
        
        $items = User::query();
        $items->where(['role_id' => 3, 'nursing_home_id' => $request->nursingHome])->with('doctor');

        if(isset($request->doctor)){
            $items->where('id', $doctor->user_id);
        }
         
        if(isset($request->speciality)){
            $items->whereHas('doctor', function($query) use ($request){
                $query->where('speciality', $request->speciality);
            });
        }        
        $newItem =  $items->orderBy('id', 'desc')->get();
        // $newItem =  $items->orderBy('id', 'desc')->paginate(mypagination());         
        if (request()->ajax()) {
            return response()->json([
                'data' => $newItem,
            ]); 
        }
        return view('admin.modules.doctors.list', [
            'items' => $newItem,
            'name' => $request->name,
            'email' => $request->email,
        ]);
    }
    /**
     * Display the specified resource for Doctor availability.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function searchDoctorAvailability(Request $request)
    {
        if(isset($request->doctor)){
            $doctor = Doctor::find($request->doctor);    
            if(!$doctor){
                if (request()->ajax()) {
                    return response()->json([
                        'data' => '',
                    ]); 
                }
            }    
        } 
        
        $items = User::query();
        $items->where(['role_id' => 3, 'nursing_home_id' => $request->nursingHome]);

        if(isset($request->doctor)){
            $items->where('id', $doctor->user_id);
        }

        if(isset($request->speciality)){
            $items->whereHas('doctor', function($query) use ($request){
                $query->where('speciality', $request->speciality);
            });
        }        
        $newItem =  $items->orderBy('id', 'desc')->first();
        // $newItem =  $items->orderBy('id', 'desc')->paginate(mypagination());         
        if (request()->ajax()) {
            return view('site_view.modules.doctor_availability', [
                'item' => $newItem,                
            ]);            
        }

        return view('admin.modules.doctors.list', [
            'items' => $newItem,
            'name' => $request->name,
            'email' => $request->email,
        ]);
    }
    /**
     * Cancel the Appointment.
     *
     * @return \Illuminate\Http\Response
     */
    public function bookingStatus($id, $status)
    {
        
        $item = Bookings::find($id);
        if (empty($item)) {
            return back()->with('message', 'Appointment not found!');
        }
        
        if ($status == 1) {
            $item->status = '0';
        } else {
            $item->status = '1';       
        }
        $item->save();
        $message = $item->status == 1 ? 'Appointment Confirmed.' : 'Appointment Canceled.';

        return back()->with('message', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
