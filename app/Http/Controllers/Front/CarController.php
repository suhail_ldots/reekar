<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Settings;
use App\Models\PrivacyPolicy;
use App\Models\TermAndConditions;
use App\Models\Cars;
use App\Models\Orders;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use PDF;
use Auth;
use Validator;

class CarController extends Controller
{

    /**
     * For Car lsit and car searching
     *
     * @param Request $request request body from client
     * @return responce data with http responce
     * */

    public function index(){
        
        if(Auth::check() && Auth::user()->role_id == '2'){
            
            return redirect()->route('customercar.create');
        }

        return view('front.customer_login');

    }
    /**
     * For term and conditions and privacy policy purpose
     *
     * @param Request $request request body from client
     * @return Json responce data with http responce code
     */
    public function termAndServiceAndPolicy(Request $request)
    {
            $validator = Validator::make($request->all(), [
                'content_for' => 'required|string|max:80|' . Rule::in(['privacy_policy', 'term_and_condition']),    
            ]);

            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 422);
            }

            if ($request->content_for == 'privacy_policy') {

                $item = PrivacyPolicy::first();
                $item = $item->privacy_content;

            }
            elseif( $request->content_for == 'term_and_condition' ){
                    
                $item = TermAndConditions::first();
                $item = $item->terms_content;
                
            }  
   
            return response()->json([
                   'content' => $item,
            ], 200);
    }

     
    /**
     * For searching cars
     *
     * @param Request $request request body from client
     * @return view page in responce
     */
    public function searchCar(Request $request)
    {  
        //get Current usd price
        //$jsoncurrency = currencyConverter();
        //return $request->all();
        //$userCurrency = $request->user_currency;
        $radius = 100;

        $this->validate($request, [
            'pickup_address' => 'required|max:255',
            'pickup_lat' => 'nullable|max:255',
            'pickup_long' => 'nullable|max:255',
            //'pickup_time' => 'required|date_format:H:i:s',
            'pickup_date' => 'required|after:' . date('Y-m-d H:i:s', strtotime('-1 hours')),
            //'drop_time' => 'nullable|date_format:H:i:s',
            'drop_date' => 'nullable|after:' . date('Y-m-d H:i:s', strtotime($request->pickup_date)),
            'brand_id' => 'nullable|array',
            'model_id' => 'nullable|array',
            // 'vehicle_type_id' => 'nullable|array',
            // 'engine_type_id' => 'nullable|array',
            'transmission_id' => 'nullable|array',
            'color_id' => 'nullable|array',
            'seat_id' => 'nullable|array',
            'brand_id.*' => 'nullable|numeric|exists:brands,id,deleted_at,NULL,status,1',
            'model_id.*' => 'nullable|numeric|exists:models,id,deleted_at,NULL,status,1',
            'vehicle_type_id' => 'nullable|numeric|exists:vehicle_types,id,deleted_at,NULL,status,1',
            // 'vehicle_type_id.*' => 'nullable|numeric|exists:vehicle_types,id,deleted_at,NULL,status,1',
            'engine_type_id' => 'nullable|numeric|exists:engine_types,id,deleted_at,NULL,status,1',
            'transmission_id.*' => 'nullable|numeric|exists:transmissions,id,deleted_at,NULL,status,1',
            'color_id.*' => 'nullable|numeric|exists:colors,id,deleted_at,NULL,status,1',
            'seat_id.*' => 'nullable|numeric|exists:seats,id,deleted_at,NULL,status,1',
            'year' => 'nullable|before:' . date('Y-m-d H:i:s') . '|date_format:Y',
            'currency' => 'nullable',
            'price_filter' => 'nullable|max:255',
            'price_per_day' => 'nullable|regex:/^\d+(\.\d{1,2})?$/|max:255',
            // 'price_per_day' => 'nullable|regex:/^[0-9]+(\.[0-9][0-9]?)?$/|max:255',
            // 'total_price' => 'nullable|regex:/^[0-9]+(\.[0-9][0-9]?)?$/|max:255',
            'total_price' => 'nullable|regex:/^\d+(\.\d{1,2})?$/|max:255',
            'ac' => 'nullable|' . Rule::in(['1', '0']),
            'gps' => 'nullable|' . Rule::in(['1', '0']),
            'ipod_interface' => 'nullable|' . Rule::in(['1', '0']),
            'sunroof' => 'nullable|' . Rule::in(['1', '0']),
            'child_seat' => 'nullable|' . Rule::in(['1', '0']),
            'electric_windows' => 'nullable|' . Rule::in(['1', '0']),
            'heated_seat' => 'nullable|' . Rule::in(['1', '0']),
            'panorma_roof' => 'nullable|' . Rule::in(['1', '0']),
            'prm_gauge' => 'nullable|' . Rule::in(['1', '0']),
            'abs' => 'nullable|' . Rule::in(['1', '0']),
            'traction_control' => 'nullable|' . Rule::in(['1', '0']),
            'audio_system' => 'nullable|' . Rule::in(['1', '0']),
            'sort_by' => 'nullable|' . Rule::in(['PPD_LTH', 'PPD_HTL', 'TP_LTH', 'TP_HTL']),
        ], [
            'pickup_address.required' => 'Pickup address is required',
            'pickup_address.max' => 'Pickup address should be maximum 255 characters long',
            'pickup_lat.max' => 'Pickup lat should be maximum 255 characters long',
            'pickup_long.max' => 'Pickup long should be maximum 255 characters long',
            'pickup_time.required' => 'Pickup time is required',
            'pickup_time.date_format' => 'Pickup time should be H:i:s format',
            'pickup_date.required' => 'Pickup date is required',
            'pickup_date.after' => 'Pickup date should be after today date',
            'drop_time.required' => 'Drop time is required',
            'drop_time.date_format' => 'Drop time should be H:i:s format',
            'drop_date.required' => 'Drop date is required',
            'drop_date.after' => 'Drop date should be after today date',
            'brand_id.required' => 'Brand id is required',
            'brand_id.numeric' => 'Brand id is invalid',
            'brand_id.exists' => 'Brand id is not found',
            'model_id.required' => 'Model id is required',
            'model_id.numeric' => 'Model id is invalid',
            'model_id.exists' => 'Model id is not found',
            'vehicle_type_id.required' => 'Vehicle type id is required',
            'vehicle_type_id.numeric' => 'Vehicle type id is invalid',
            'vehicle_type_id.exists' => 'Vehicle type id is not found',
            'engine_type_id.required' => 'Engine type id is required',
            'engine_type_id.numeric' => 'Engine type id is invalid',
            'engine_type_id.exists' => 'Engine type id is not found',
            'transmission_id.required' => 'Transmission id is required',
            'transmission_id.numeric' => 'Transmission id is invalid',
            'transmission_id.exists' => 'Transmission id is not found',
            'color_id.required' => 'Color id is required',
            'color_id.numeric' => 'Color id is invalid',
            'color_id.exists' => 'Color id is not found',
            'year.required' => 'Year is required',
            'year.before' => 'Year should be before today',
            'year.date_format' => 'Year format should be YYYY',
            'currency.required' => 'Currency is required',
            'price_per_day.required' => 'Price per day is required',
            'price_per_day.regex' => 'Price per day should be valid double value',
            'price_per_day.max' => 'Price per day is invalid',
            'total_price.required' => 'Total price is required',
            'total_price.regex' => 'Total price should be valid double value',
            'total_price.max' => 'Total price is invalid',
            'ac.required' => 'AC is required',
            'gps.required' => 'GPS is required',
            'ipod_interface.required' => 'IPod interface is required',
            'sunroof.required' => 'Sun roof is required',
            'child_seat.required' => 'Child seat is required',
            'electric_windows.required' => 'Electric windows is required',
            'heated_seat.required' => 'Heated seats is required',
            'panorma_roof.required' => 'Panaroma roof is required',
            'prm_gauge.required' => 'PRM Guage is required',
            'abs.required' => 'ABS is required',
            'traction_control.required' => 'Tranction control is required',
            'audio_system.required' => 'Audio system is required',
        ]);
       
        $query = Cars::query();
        $offset = 10;
        if ($request->offset != null && $request->offset != '') {
            $offset = $request->offset;
        }
        if ($request->pickup_address != null && $request->pickup_address != '') {
            if ($request->pickup_lat != null && $request->pickup_lat != '' && $request->pickup_long != null && $request->pickup_long != '') {
                $coordinates = [
                    'latitude' => $request->pickup_lat,
                    'longitude' => $request->pickup_long,
                ];
            } else {
                $address = str_replace(" ", "+", $request->pickup_address);
                $json = file_get_contents("https://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&key=AIzaSyA2X23zOQ4hIRy3ZoHPWxSqnPRzjAdOoCc");
                $json = json_decode($json);

                $lat = count($json->{'results'}) ? $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'} : 0;
                $long = count($json->{'results'}) ? $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'} : 0;
                $coordinates = [
                    'latitude' => $lat,
                    'longitude' => $long,
                ];
            }
            $query->isWithinMaxDistance($coordinates, $radius);
        }
        if ($request->pickup_date != null && $request->pickup_date != '') {
            $query->where('available_from_date', '<=', date('Y-m-d H:i:s', strtotime($request->pickup_date)))
                ->where('available_to_date', '>=', date('Y-m-d H:i:s', strtotime($request->pickup_date)));

            //if ($request->pickup_time != null && $request->pickup_time != '') {
                //$query->where('available_from_time', '<=', date('H:i:s', strtotime($request->pickup_date)));
                    //->where('available_to_time', '>=', date('H:i:s', strtotime($request->pickup_date)));
           // }
        }

        if ($request->drop_date != null && $request->drop_date != '') {
            $query->where('available_from_date', '<=', date('Y-m-d H:i:s', strtotime($request->drop_date)))
                ->where('available_to_date', '>=', date('Y-m-d H:i:s', strtotime($request->drop_date)));

            //if ($request->pickup_time != null && $request->pickup_time != '') {
                //$query
                //->where('available_from_time', '<=', date('H:i:s', strtotime($request->drop_date)))
                    //->where('available_to_time', '>=', date('H:i:s', strtotime($request->drop_date)));
            //}
        }

        if (!empty($request->brand_id)) {
            //$query->whereHas('brand', function ($query1) use ($request) {
                $query->whereIn('brand_id', $request->brand_id);
           // });
        }

        if (!empty($request->model_id)) {
            //$query->whereHas('model', function ($query1) use ($request) {
                $query->whereIn('model_id', $request->model_id);
            //});
        }

        if (!empty($request->vehicle_type_id)) {
            //$query->whereHas('vehicletype', function ($query1) use ($request) {
                 //$query->whereIn('vehicle_type_id', $request->vehicle_type_id);
                 $query->where('vehicle_type_id', $request->vehicle_type_id);
            //});
        }

        if (!empty($request->engine_type_id)) {
            //$query->whereHas('enginetype', function ($query1) use ($request) {
                // $query->whereIn('engine_type_id', $request->engine_type_id);
                $query->where('engine_type_id', $request->engine_type_id);
            //});
        }

        if (!empty($request->transmission_id)) {
            //$query->whereHas('transmission', function ($query1) use ($request) {
                $query->whereIn('transmission_id', $request->transmission_id);
            //});
        }

        if (!empty($request->color_id)) {
            //$query->whereHas('color', function ($query1) use ($request) {
                $query->whereIn('color_id', $request->color_id);
            //});
        }

        if (!empty($request->seat_id)) {
            //$query->whereHas('color', function ($query1) use ($request) {
                $query->whereIn('seat_id', $request->seat_id);
            //});
        }

        if ($request->year != null && $request->year != '') {
            $query->where('year', date('Y', strtotime($request->year)));
        }

        /* if ($request->price_per_day != null && $request->price_per_day != '') {
            $query->whereBetween('price_per_day', [($request->price_per_day - 10), ($request->price_per_day + 10)]);
        } */

        if ($request->total_price != null && $request->total_price != '') {
            $query->whereBetween('total_price', [($request->total_price - 10), ($request->total_price + 10)]);
        }

        if ($request->ac != null && $request->ac != '') {
            $query->where('ac', $request->ac);
        }

        if ($request->gps != null && $request->gps != '') {
            $query->where('gps', $request->gps);
        }

        if ($request->ipod_interface != null && $request->ipod_interface != '') {
            $query->where('ipod_interface', $request->ipod_interface);
        }

        if ($request->sunroof != null && $request->sunroof != '') {
            $query->where('sunroof', $request->sunroof);
        }

        if ($request->child_seat != null && $request->child_seat != '') {
            $query->where('child_seat', $request->child_seat);
        }

        if ($request->electric_windows != null && $request->electric_windows != '') {
            $query->where('electric_windows', $request->electric_windows);
        }

        if ($request->heated_seat != null && $request->heated_seat != '') {
            $query->where('heated_seat', $request->heated_seat);
        }

        if ($request->panorma_roof != null && $request->panorma_roof != '') {
            $query->where('panorma_roof', $request->panorma_roof);
        }

        if ($request->prm_gauge != null && $request->prm_gauge != '') {
            $query->where('prm_gauge', $request->prm_gauge);
        }

        if ($request->abs != null && $request->abs != '') {
            $query->where('abs', $request->abs);
        }

        if ($request->traction_control != null && $request->traction_control != '') {
            $query->where('traction_control', $request->traction_control);
        }

        if ($request->audio_system != null && $request->audio_system != '') {
            $query->where('audio_system', $request->audio_system);
        }

        if ($request->sort_by != null && $request->sort_by != '') {
            if ($request->sort_by == 'PPD_LTH') {
                $query->orderBy('price_per_day', 'ASC');
            } elseif ($request->sort_by == 'PPD_HTL') {
                $query->orderBy('price_per_day', 'DESC');
            } elseif ($request->sort_by == 'TP_LTH') {
                $query->orderBy('total_price', 'ASC');
            } elseif ($request->sort_by == 'TP_HTL') {
                $query->orderBy('total_price', 'DESC');
            }

        }else{

            $query->orderBy('distance', 'ASC');
        }
        
        $items = $query->where('status', '1')->where('listing_status', '1')->paginate(mypagination());
        /*
        //arvind sir
        if ($request->paginate == 1) {
            $items = $query->where('status', '1')->where('listing_status', '1')->paginate($offset);
        } else {
            $items = $query->where('status', '1')->where('listing_status', '1')->paginate(10);
        }

        $cars = [];
        $carsdetails = [];
        $features = [];
        $checked_items = $items->filter(function(Cars $car) use($request){
            return $car->getIsRentedOnDateAttribute($request->pickup_date, $request->drop_date) == false;
        });
        foreach ($checked_items as $item) {
            $features = [];
            $carsdetails['id'] = $item->id;
            $carsdetails['year'] = $item->year;
            $carsdetails['car_location'] = $item->car_location;
            $carsdetails['available_from_date'] = $item->available_from_date;
            $carsdetails['available_from_time'] = $item->available_from_time;
            $carsdetails['available_to_date'] = $item->available_to_date;
            $carsdetails['available_to_time'] = $item->available_to_time;
            $carsdetails['currency'] = $userCurrency;
            $carsdetails['price_per_day'] = getCurrencyFromUser('USD', $userCurrency, $item->currency, $item->price_per_day, $jsoncurrency);
            // $carsdetails['price_per_day'] = $item->price_per_day;
            $carsdetails['total_price'] = getCurrencyFromUser('USD', $userCurrency, $item->currency, $item->total_price, $jsoncurrency);
            $carsdetails['car_location_lat'] = $item->car_location_lat;
            $carsdetails['car_location_long'] = $item->car_location_long;
            $carsdetails['distance'] = $item->distance;
            $feature_count = 0;
            if ($item->ac) {
                if ($feature_count < 4) {
                    $feature_count++;
                    $features[] = 'ac';
                }
            }

            if ($item->gps) {
                if ($feature_count < 4) {
                    $feature_count++;
                    $features[] = 'gps';
                }
            }

            if ($item->ipod_interface) {
                if ($feature_count < 4) {
                    $feature_count++;
                    $features[] = 'ipod_interface';
                }
            }
            
            if ($item->sunroof) {
                if ($feature_count < 4) {
                    $feature_count++;
                    $features[] = 'sunroof';
                }
            }

            if ($item->child_seat) {
                if ($feature_count < 4) {
                    $feature_count++;
                    $features[] = 'child_seat';
                }
            }

            if ($item->electric_windows) {
                if ($feature_count < 4) {
                    $feature_count++;
                    $features[] = 'electric_windows';
                }
            }

            if ($item->heated_seat) {
                if ($feature_count < 4) {
                    $feature_count++;
                    $features[] = 'heated_seat';
                }
            }

            if ($item->panorma_roof) {
                if ($feature_count < 4) {
                    $feature_count++;
                    $features[] = 'panorma_roof';
                }
            }

            if ($item->prm_gauge) {
                if ($feature_count < 4) {
                    $feature_count++;
                    $features[] = 'prm_gauge';
                }
            }

            if ($item->abs) {
                if ($feature_count < 4) {
                    $feature_count++;
                    $features[] = 'abs';
                }
            }

            if ($item->traction_control) {
                if ($feature_count < 4) {
                    $feature_count++;
                    $features[] = 'traction_control';
                }
            }

            if ($item->audio_system) {
                if ($feature_count < 4) {
                    $feature_count++;
                    $features[] = 'audio_system';
                }
            }

            $carsdetails['brand'] = $item->brand->name;
            $carsdetails['model'] = $item->model->name;
            $carsdetails['color'] = $item->color->name;
            $carsdetails['seat'] = $item->seat ? $item->seat->name : null;
            $carsdetails['enginetype'] = $item->enginetype->name;
            $carsdetails['transmission'] = $item->transmission->name;
            $carsdetails['vehicletype'] = $item->vehicletype->name;
            $carsdetails['features'] = $features;
            $carsdetails['image'] = $item->image ? \Url('storage/app/images/cars/').'/'.$item->image->image : null;
            $cars[] = $carsdetails;
        }
        $cars['current_page'] = $items->currentPage();
        $cars['total_page'] = $items->lastPage();
        //return $cars;
        if ($request->paginate == 1) {
            return response()->json([
                'cars' => $cars,
                'current_page' => $items->currentPage(),
                'total_page' => $items->lastPage(),
            ], 200);
        }else{
            //end arvind sir
 */

        if ($request->ajax()) {
            return view('front.shop.body',[
                'cars' => $items,
                'from_date' => $request->pickup_date,
                'to_date' => $request->drop_date,
                'price_filter' => $request->price_filter,
                
            ]);
        }
        
        return view('front.shop.index',[
            'cars' => $items,
            'from_date' => $request->pickup_date,
            'to_date' => $request->drop_date,
            'pickup_address' => $request->pickup_address,
            'drop_address' => $request->drop_address,
            'vehicle_type' => $request->vehicle_type_id,
            'engine_type' => $request->engine_type_id,
            'price_filter' => $request->price_filter,
        ]);
            
        /* if ($request->paginate == 1) {
            return response()->json([
                'cars' => $cars,
                'current_page' => $items->currentPage(),
                'total_page' => $items->lastPage(),
            ], 200);
        }else{
        return response()->json([
            'cars' => $cars,
        ], 200); */
        //}
    }

    /**
     * For Nearest cars on map 
     *
     * @param Request $request request body from client
     * @return view page in responce
     */
    public function carsOnMap(Request $request)
    {  
       
        $radius = 7000;

        $this->validate($request, [            
            'lat' => 'nullable|max:255',
            'long' => 'nullable|max:255',
            
        ]);
       
        $query = Cars::query();
        
        $coordinates = [
            'latitude' => $request->lat,
            'longitude' => $request->long,
        ];
        $query->isWithinMaxDistance($coordinates, $radius);
       // $query->orderBy('distance', 'ASC');
        
        $items = $query->select('id','car_location_lat as lat', 'car_location_long as long')->where('status', '1')->where('listing_status', '1')->get();

        if ($request->ajax()) {
            return response()->json([
                'cars' => $items,
            ]);
        }                
    }

    /**
     * For car details
     *
     * @param Request $request request body from client
     * @return Json responce data with http responce code
     */
    public function carDetails(Request $request)
    {
        $this->validate($request, [
            'car_id' => 'required|numeric|exists:cars,id,deleted_at,NULL,status,1',
        ], [
            'car_id.required' => 'Car id is required',
            'car_id.numeric' => 'Car id is invalid',
            'car_id.max' => 'Car id is invalid',
        ]);
                
        //$userCurrency = $request->header('USER-LOCATION-CURRENCY');
        $car = Cars::where('id', $request->car_id)
        ->where('status', '1')
            ->first();

            $carsdetails = [];
            $features    = [];
            $item = $car;
            $carsdetails['id'] = $item->id;
            $carsdetails['year'] = $item->year;
            $carsdetails['car_location'] = $item->car_location;
            $carsdetails['available_from_date'] = $item->available_from_date;
            $carsdetails['available_from_time'] = $item->available_from_time;
            $carsdetails['available_to_date'] = $item->available_to_date;
            $carsdetails['available_to_time'] = $item->available_to_time;
            $carsdetails['currency'] =  $item->currency;
            $carsdetails['price_per_day'] =$item->price_per_day;
            $carsdetails['total_price'] = $item->total_price;
            $carsdetails['security_amount'] = $item->security_amount;
            $carsdetails['car_location_lat'] = $item->car_location_lat;
            $carsdetails['car_location_long'] = $item->car_location_long;
            if ($item->ac) {
                    $features[] = 'ac';
            }

            if ($item->gps) {
                    $features[] = 'gps';
            }

            if ($item->ipod_interface) {
                    $features[] = 'ipod_interface';
            }
            
            if ($item->sunroof) {
                    $features[] = 'sunroof';
            }

            if ($item->child_seat) {
                    $features[] = 'child_seat';
            }

            if ($item->electric_windows) {
                    $features[] = 'electric_windows';
            }

            if ($item->heated_seat) {
                    $features[] = 'heated_seat';
            }

            if ($item->panorma_roof) {
                    $features[] = 'panorma_roof';
            }

            if ($item->prm_gauge) {
                    $features[] = 'prm_gauge';
            }

            if ($item->abs) {
                    $features[] = 'abs';
            }

            if ($item->traction_control) {
                    $features[] = 'traction_control';
            }

            if ($item->audio_system) {
                    $features[] = 'audio_system';
            }
            if ($item->cruise_control) {
                $features[] = 'cruise_control';
            }

            $carsdetails['brand'] = $item->brand->name;
            $carsdetails['model'] = $item->model->name;
            $carsdetails['color'] = $item->color->name;
            $carsdetails['seat'] = $item->seat->name;
            $carsdetails['enginetype'] = $item->enginetype->name;
            $carsdetails['transmission'] = $item->transmission->name;
            $carsdetails['vehicletype'] = $item->vehicletype->name;
            $carsdetails['chassis_number'] = $item->chassis_number;
            $carsdetails['licensed'] = $item->is_licensed;
            $carsdetails['mileage'] = $item->mileage;
            $carsdetails['features'] = $features;
            $carsimages = [];
            if(count($item->images))
            foreach($item->images->where('status', '1') as $image){
                $carsimages[] = \Url('storage/app/images/cars/').'/'.$image->image;
            }
            $carsdetails['images'] = $carsimages;

        return view('front.bookings.car_details',[
            'car' => $carsdetails,
        ]);
       
    }

    public function currentLocation()
    {
         $data = getCurrentLocationData();
         return response()->json($data);
         /* return response()->json([
             'data' => $data
         ]); */
    }
    
}
