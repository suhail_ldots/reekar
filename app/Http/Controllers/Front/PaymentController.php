<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;

use App\Models\SubscriptionOrders;
use App\Models\Masters\SubscriptionPackages;
use App\Models\DoctorNursingHomeRelation;
use App\Models\NursingHome;
use App\Models\Doctor;
use App\Mail\NursingHomeSignup;
use App\Mail\SubscribedPackage;
use App\Mail\WelcomeToDoctorsLoop;
use Illuminate\Http\Request;

//strip payment
use Session;
use Stripe;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return $request->all();
        $email_regex = '/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix';
        $plan = SubscriptionPackages::find($request->package_id);

        //return $request->all();
        $this->validate($request, [
            'package_id' => 'required|numeric|exists:subscription_packages,id,deleted_at,NULL,status,1',
            'user_type' => 'required|numeric|exists:roles,id,deleted_at,NULL',
            'fname' => 'required|regex:/^[a-zA-Z ]+$/u|max:255',
            'lname' => 'required|regex:/^[a-zA-Z ]+$/u|max:255',
            'email' => 'required|email|max:255|unique:users|regex:'.$email_regex,
            'mobile' => 'nullable|string|max:20',
            'zipcode' => 'nullable|string|max:20',
            'address' => 'required|string|max:1000',   //text field in db        
            'user_lat' => 'nullable|string|max:30',
            'user_long' => 'nullable|string|max:30',
            'about_nursing'=>'required|string',
            'country' => 'required|numeric',
            'city' => 'nullable|string|max:255',
            // 'card_type' => 'nullable|string|max:255',
            // 'name_on_card' => 'required|regex:/^[a-zA-Z ]+$/u|max:255',
            // 'cvv_no' => 'required|numeric|digits-between:3,6',
            // 'card_no' => 'required|numeric|digits-between:10,20',
            // 'expiry_date' => 'required|date_format:Y-m-d|after:'.date('Y-m'),
            // 'amount'=>'nullable|numeric',           

        ],['user_type.numeric' =>'Account type is required'],
        [
            'fname'=>'first name',
            'lname'=>'last name',
            'phone'=>'mobile',
        ]);
        if($plan && $plan->price != 0 && $plan->price != ''){

            $this->validate($request, [            
                // 'card_type' => 'nullable|string|max:255',
                'name_on_card' => 'required|string|max:255',
                'cvv_no' => 'required|numeric|digits-between:3,6',
                'card_no' => 'required|numeric|digits-between:10,20',
                'card_exp_month' => 'required|date_format:m|after:'.date('m'),
                'card_exp_year' => 'required|date_format:Y|after_or_equal:'.date('Y'),
                // 'expiry_date' => 'required|date_format:Y-m-d|after:'.date('Y-m'),
                'amount'=>'required|numeric', 
            ]);           
        }
        
        if($plan && $plan->price != $request->amount ){

            return redirect()->back()->with('message', "Sorry, you can not subscribe this package right now. Please try again.");
        }

        $data = [];
        //Stripe validation
        if($plan->price != 0 && $plan->price != '') {
            $msg = "Sorry, payment can not complete now.";

            Stripe\Stripe::setApiKey(config('api.stripe_secret_key'));    

            $data = [];
            try {
                // Use Stripe's library to make requests...
                $data = Stripe\Charge::create ([
                    "amount" => $request->amount * 100,
                    "currency" => "usd",
                    "source" => $request->stripeToken,
                    "description" => "Test payment from Suhail Khan" 
                ]);
            } catch(\Stripe\Exception\CardException $e) {
                // Since it's a decline, \Stripe\Exception\CardException will be caught
            /*  echo 'Status is:' . $e->getHttpStatus() . '\n';
                echo 'Type is:' . $e->getError()->type . '\n';
                echo 'Code is:' . $e->getError()->code . '\n'; */
                // param is '' in this case
                /* echo 'Param is:' . $e->getError()->param . '\n'; */
            $msg = $e->getError()->message;
            } catch (\Stripe\Exception\RateLimitException $e) {
                // Too many requests made to the API too quickly
            } catch (\Stripe\Exception\InvalidRequestException $e) {
                // Invalid parameters were supplied to Stripe's API
            } catch (\Stripe\Exception\AuthenticationException $e) {
                // Authentication with Stripe's API failed
                // (maybe you changed API keys recently)
            } catch (\Stripe\Exception\ApiConnectionException $e) {
                // Network communication with Stripe failed
            } catch (\Stripe\Exception\ApiErrorException $e) {
                // Display a very generic error to the user, and maybe send
                // yourself an email
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe
            }
            
            if(!empty($data)){
                if($data->status == 'succeeded'){
                
                    $msg = "Payment Successfull.";

                }
                elseif($data->status == 'payment_failed')
                {
                    $msg = "Payment Faild.";
                }else{
                    $msg = "Payment can not complete now.";
                }
            }else{
                
                echo $msg ;
                exit();
            } 
            //dd($data);
            Session::flash('message', $msg); 
        }
        $rand = 'doctor@'.mt_rand('1111', 99999);
        //Stripe payment success
        $setuser = new User();

        $setuser->first_name = $request->fname;
        $setuser->last_name = $request->lname;
        $setuser->email = $request->email;
        $setuser->mobile = $request->mobile;
        $setuser->address = $request->address;
        $setuser->zipcode = $request->zipcode;
        $setuser->country = $request->country;
        $setuser->city = $request->city;
        $setuser->lat = $request->lat;
        $setuser->long = $request->long;
        $setuser->role_id = $request->user_type;
        $setuser->status = 1;
        $setuser->password = Hash::make($rand);

        if($setuser->save()){
            $payment = new SubscriptionOrders();//
            $payment->email = $request->email;
            // $payment->nursing_home_id = $request->nursing_home_id;
            $payment->booking_id = 'NH'.date('d').time().rand();
            $payment->paid_amount = $request->amount;
            $payment->grand_total = $request->amount;
            $payment->payment_gateway = 'Stripe';
            $payment->bank_ref_id    = $plan->price != 0 && $plan->price != '' ? $data->id :'notavailable' ;
            $payment->transaction_id = $plan->price != 0 && $plan->price != '' ? $data->balance_transaction :'notavailable' ;;
            $payment->card_no        = $plan->price != 0 && $plan->price != '' ? $request->card_no :'notavailable' ;
            $payment->exp_date       = $plan->price != 0 && $plan->price != '' ? $request->card_exp_year.'-'.$request->card_exp_month.'-'.'00' :'notavailable' ; 
            $payment->card_type	     = ($plan->price != 0 && $plan->price != '') && $data->payment_method_details->type.' '.($data->payment_method_details->type == 'card') ? $data->payment_method_details->card->brand .' '.$data->payment_method_details->card->funding.' card':''; 
            $payment->order_status	 = 1;
            $payment->stripe_invoice   = $plan->price != 0 && $plan->price != '' ? $data->receipt_url :'notavailable' ;

            if($payment->save()){
                $nursing_home = new NursingHome();
                $nursing_home->user_id = $setuser->id; //from user table
                $nursing_home->subscription_package_id = $request->package_id;
                $nursing_home->organization_name	 = $request->fname.' '.$request->lname;
                $nursing_home->about_nursing_home	 = $request->about_nursing;
                // $nursing_home->registration_no	 = ;
                $nursing_home->status	 = 1;
                $nursing_home->save();

                //update subscription orders table and users table
                $setuser->nursing_home_id = $nursing_home->id ;
                $setuser->save();
                //update payment
                $payment->nursing_home_id = $nursing_home->id;
                $payment->save();
                
                if($request->user_type == 3){
                    $doctor = new Doctor();
                    $doctor->user_id = $setuser->id;
                    $doctor->save();

                    //Doctor Nursing Relation
                    $doctorNursingRelation = new doctorNursingHomeRelation();
                    $doctorNursingRelation->doctor_id = $doctor->id;
                    $doctorNursingRelation->nursing_home_id = $nursing_home->id;
                    $doctorNursingRelation->status = 1;
                    $doctorNursingRelation->save();
                }

                $setuser['plan_name'] = $plan->name;
                $setuser['temp_password'] = $rand;
                $featurelist = [];
                foreach($plan->features as $feature){
                    $fitem = getFeature_byid($feature->feature_id);
                    $featurelist[] = $fitem->count.' '. $fitem->name;
                }
                $setuser['plan_feature_name'] = $featurelist;
               
                try {
                    \Mail::to($request->email)->send(new WelcomeToDoctorsLoop($setuser));
                    //createlogs(ErrorLogs::POSITIIVE, $user_type, ErrorLogs::WELCOME_MAIL);
                } catch (\Exception $e) {
                    $email_error = $e; // Get error here
                    //createlogs(ErrorLogs::POSITIIVE, $user_type, ErrorLogs::WELCOME_MAIL_FAILED);
                }
                if ($request->user_type == 3) {
                    $usertype = 'Single Doctor Clinic';
                } elseif($request->user_type == 4) {
                    $usertype = 'Multispecialty Hospital';
                }
                
                try {
                    \Mail::to(User::where('role_id', 1)->value('email'))->send(new NursingHomeSignup($usertype));
                    // createlogs(ErrorLogs::POSITIIVE, ErrorLogs::POSITIIVE, "New .$usertype. Signup mail Notification sended to admin" );
                } catch (\Exception $e) {
                    $email_error = 'Failed to authenticate on SMTP server with username \"apikey\" using 2 possible authenticators. Authenticator LOGIN returned Expected response code 250 but got an empty response. Authenticator PLAIN returned Expected response code 250 but got an empty response.'; // Get error here
                    //createlogs(ErrorLogs::POSITIIVE, $user_type, "New .$user_type. Signup mail Notification can't send to admin");
                }
                //Assign temp password 
                \Session::put('random_password', $rand);

                return redirect()->route('payment.thankyou', $nursing_home->id);
            }else{
                User::where('id', $setuser->id)->delete();
                return redirect()->back()->with('message', "Sorry, you can not subscribe this package right now. Please try again.");

            }

        }else{
            return redirect()->back()->with('message', "Sorry, you can not subscribe this package right now. Please try again.");

        }



    }

    /**
     * Display the specified resource.
     *
     * @param  \App\r  $r
     * @return \Illuminate\Http\Response
     */
    public function paymentSuccess(Request $request, $nhid)
    {
        $password = \Session::get('random_password');
        $item = NursingHome::find($nhid);
        $plan = SubscriptionPackages::find($item->subscription_package_id);
        $payamount = SubscriptionOrders::where('nursing_home_id', $nhid)->where('order_status', 1)->first();
        $plan->amount = $payamount->paid_amount;
        $plan->txn_id = $payamount->transaction_id;
        $plan->card_type = $payamount->card_type;
        $plan->stripe_invoice = $payamount->stripe_invoice;
        \Session::forget('random_password');
        return view('site_view.modules.thank-you',[
                'plan' => $plan,
                'user' => User::where('nursing_home_id', $nhid)->first(),
                'password' => $password,
        ]);
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\r  $r
     * @return \Illuminate\Http\Response
     */
    public function show(r $r)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\r  $r
     * @return \Illuminate\Http\Response
     */
    public function edit(r $r)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\r  $r
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, r $r)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\r  $r
     * @return \Illuminate\Http\Response
     */
    public function destroy(r $r)
    {
        //
    }
}
