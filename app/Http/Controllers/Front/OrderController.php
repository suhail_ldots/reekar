<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Utility\PassportToken;
use App\Http\Utility\UtilityFunction;
use App\Mail\OrderSuccessSellerMail;
use App\Mail\OrderSuccessBuyerMail;
use App\User;
use App\Models\Cars;
use App\Models\Orders;
use App\Models\Settings;
use Illuminate\Http\Request;
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use Hash;
use Illuminate\Validation\Rule;
use Validator;
use Auth;
use DB;
use PDF;
use DateTime;
use DateTimeZone;
use Session;
use Stripe;

class OrderController extends Controller
{
    use UtilityFunction;
    //image upload path set for works images
    protected $imagePathDoc = 'images/orders/docs';
    protected $imageSizes = [
        'thumb' => [200, 200],
    ];
    protected $imageSizesMarkup = [
        'thumb-markup' => [50, 50],
    ];
    

     /**
     * For list the orders
     *
     * @param Request  request body from client
     * @return Json responce data with http responce code
     */
    public function fillCart(Request $request){

        isset($request->picking_address) ?  \Session::put('cart', $request->picking_address) : '' ;         
        isset($request->car_id) ?  \Session::put('car_id', $request->car_id) : '' ;         

        //die(\Session::get('cart'));
        if(!Auth::check()){
            return redirect()->route('front-customer-login')->with('message', "Sorry, you are not Authorized. Please Login or Signup before renting any car.");
        }

        return view('front.bookings.buyer');
    }
     
     
    /**
     * For list the orders
     *
     * @param Request $request request body from client
     * @return responce view page
     */

    public function list(Request $request)
    {
        $query = Orders::query();
        $query->where('user_id', \Auth::id())
        ->where('status', '1')
        ->where('order_status', '2')
        ->orderBy('created_at', 'Desc');
           $items = $query->paginate(10);

           $orders = [];
        $ordersdetails = [];

        //Get converted currency
        $jsoncurrency = currencyConverter();
        $userCurrency = $request->header('USER-LOCATION-CURRENCY');

           foreach ($items as $item) {
            $features = [];
            $ordersdetails['id'] = $item->id;
            $ordersdetails['car_id'] = $item->car_id;
            $ordersdetails['booking_id'] = $item->booking_id;
            $ordersdetails['booking_from_date'] = $item->booking_from_date;
            $ordersdetails['booking_to_date'] = $item->booking_to_date;
            $ordersdetails['grand_total'] = getCurrencyFromUser('USD', $userCurrency, $item->currency, $item->grand_total, $jsoncurrency);
            $ordersdetails['payment_method'] = $item->payment_method;
            $ordersdetails['updated_at'] = date('Y-m-d H:i:s', strtotime($item->updated_at));
            $ordersdetails['billing_first_name'] = $item->billing_first_name;
            $ordersdetails['billing_last_name'] = $item->billing_last_name;
            $ordersdetails['brand'] = $item->car->brand->name;
            $ordersdetails['model'] = $item->car->model->name;
            $ordersdetails['enginetype'] = $item->car->enginetype->name;
            $ordersdetails['vehicletype'] = $item->car->vehicletype->name;
            $ordersdetails['owner_name'] = $item->car->user->full_name;
            $ordersdetails['image'] = $item->car->image ? \Url('storage/app/images/cars/').'/'.$item->car->image->image : null;
            $orders[] = $ordersdetails;
        }

        return response()->json([
            'data' => $orders,
            'current_page' => $items->currentPage(),
            'total_page' => $items->lastPage(),
        ], 200);
    }

    /**
     * For list the orders
     *
     * @param Request $request request body from client
     * @return Json responce data with http responce code
     */
    public function listMyCarBooking(Request $request)
    {
        //return Cars::where('user_id', \Auth::id())->pluck('id');
        $query = Orders::query();
        $query->whereIn('car_id', Cars::where('user_id', \Auth::id())->pluck('id'))
        ->where('status', '1')
        ->orderBy('created_at', 'Desc');
           $items = $query->paginate(10);
           
        //Get converted currency
        $jsoncurrency = currencyConverter();
        $userCurrency = $request->header('USER-LOCATION-CURRENCY');

           $orders = [];
        $ordersdetails = [];
        
           foreach ($items as $item) {
            $ordersdetails['id'] = $item->id;
            $ordersdetails['car_id'] = $item->car_id;
            $ordersdetails['booking_id'] = $item->booking_id;
            $ordersdetails['booking_from_date'] = $item->booking_from_date;
            $ordersdetails['booking_to_date'] = $item->booking_to_date;
            $ordersdetails['grand_total'] = getCurrencyFromUser('USD', $userCurrency, $item->currency, $item->grand_total, $jsoncurrency);
            $ordersdetails['payment_method'] = $item->payment_method;
            $ordersdetails['updated_at'] = date('Y-m-d H:i:s', strtotime($item->updated_at));
            $ordersdetails['billing_first_name'] = $item->billing_first_name;
            $ordersdetails['billing_last_name'] = $item->billing_last_name;
            $ordersdetails['brand'] = $item->car->brand->name;
            $ordersdetails['model'] = $item->car->model->name;
            $ordersdetails['enginetype'] = $item->car->enginetype->name;
            $ordersdetails['vehicletype'] = $item->car->vehicletype->name;
            $ordersdetails['image'] = $item->car->image ? \Url('storage/app/images/cars/').'/'.$item->car->image->image : null;
            $ordersdetails['owner_name'] = $item->car->user->full_name;
            $orders[] = $ordersdetails;
        }
        
        return response()->json([
            'data' => $orders,
            'current_page' => $items->currentPage(),
            'total_page' => $items->lastPage(),
        ], 200);
    }

    /**
     * For get the orders details
     *
     * @param Request $request request body from client
     * @return http responce  
     */
    public function orderDetails(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'order_id' => 'required|numeric|exists:orders,id,deleted_at,NULL,status,1',
        ], [
            'order_id.required' => 'Order id is required',
            'order_id.numeric' => 'Order id should be numeric',
            'order_id.exists' => 'Order id not available in our record',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        //Get converted currency
        $jsoncurrency = currencyConverter();
        $userCurrency = $request->header('USER-LOCATION-CURRENCY');

        $query = Orders::query();
        
       // $query->where('user_id', \Auth::id())
       $query->where('id', $request->order_id)

        ->where('status', '1');
        $item = $query->orderBy('created_at', 'Desc')->first();
          
           $ordersdetails['id'] = $item->id;
            $ordersdetails['car_id'] = $item->car_id;
            $ordersdetails['booking_id'] = $item->booking_id;
            $ordersdetails['booking_from_date'] = $item->booking_from_date;
            $ordersdetails['booking_to_date'] = $item->booking_to_date;
            $ordersdetails['total_amount'] = getCurrencyFromUser('USD', $userCurrency, $item->currency, $item->total_amount, $jsoncurrency);
            $ordersdetails['security_amount'] = getCurrencyFromUser('USD', $userCurrency, $item->currency, $item->security_amount, $jsoncurrency);
            $ordersdetails['tax_amount'] = getCurrencyFromUser('USD', $userCurrency, $item->currency, $item->tax_amount, $jsoncurrency);
            $ordersdetails['grand_total'] = getCurrencyFromUser('USD', $userCurrency, $item->currency, $item->grand_total, $jsoncurrency);
            $ordersdetails['payment_method'] = $item->payment_method;
            $ordersdetails['bank_reference_id'] = $item->bank_reference_id;
            $ordersdetails['transaction_id'] = $item->transaction_id;
            $ordersdetails['billing_first_name'] = $item->billing_first_name;
            $ordersdetails['billing_last_name'] = $item->billing_last_name;
            $ordersdetails['billing_email'] = $item->billing_email;
            $ordersdetails['billing_mobile'] = $item->billing_mobile;
            $ordersdetails['billing_alternate_mobile'] = $item->billing_alternate_mobile;
            $ordersdetails['billing_address'] = $item->billing_address;
            $ordersdetails['pickup_location'] = $item->pickup_location;
            $ordersdetails['order_status'] = $item->order_status;
            $ordersdetails['updated_at'] = date('Y-m-d H:i:s', strtotime($item->updated_at));
            
            $ordersdetails['brand'] = $item->car->brand->name;
            $ordersdetails['model'] = $item->car->model->name;
            $ordersdetails['color'] = $item->car->color->name;
            $ordersdetails['vehicletype'] = $item->car->vehicletype->name;
            $ordersdetails['enginetype'] = $item->car->enginetype->name;
            $ordersdetails['image'] = $item->car->image ? \Url('storage/app/images/cars/').'/'.$item->car->image->image : null;
            $ordersdetails['owner_name'] = $item->car->user->full_name;
            $ordersdetails['contact_no'] = $item->car->user->country_code.' '.$item->car->user->mobile;
            $ordersdetails['invoice'] = $item->order_invoice ? (\Url('storage').'/invoice'.'/'.$item->order_invoice) : null;
        return response()->json([
            'order' => $ordersdetails,
        ], 200);
    }

    /**
     * For Order save 
     *
     * @param Request $request request body from client
     * @return HTTP responce data 
     */
    public function saveOrder(Request $request)
    { 
         
        
        $request['booking_from_date']= getUTCTimezoneDateTime($request->booking_from_date);
        $request['booking_to_date']= getUTCTimezoneDateTime($request->booking_to_date);
        $request['car_id']=  \Session::has('car_id') ? \Session::get('car_id') :'';
       
        if($request->car_id == ''){
          
            return redirect()->route('main')->with('message','Sorry, somthing went wrong plz try again');
        } 
        
        $email_regex = '/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix';
        $this->validate($request, [
            'car_id' => 'required|numeric|exists:cars,id,deleted_at,NULL,status,1',
            'booking_from_date' => 'required|date_format:Y-m-d H:i:s',
            'booking_to_date' => 'required|after:'.date('Y-m-d H:i:s', strtotime($request->booking_from_date)).'|date_format:Y-m-d H:i:s',
            /////'currency' => 'required',
           ////// 'total_amount' => 'required|max:255',
            // /* 'total_amount' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/|max:255', */
           ///// 'security_amount' => 'required|max:255',
            /////'tax_amount' => 'required|max:255', //By default it is 0
            /////'grand_total' => 'required|max:255',
            "file" => ["required","array","min:2","max:6"],
            'file.*' => 'required|file|mimes:jpeg,png,jpg,pdf,doc,docx|max:20480',
            'billing_first_name' => 'required|regex:/^[\pL\s\-]+$/u|max:50',
            'billing_last_name' => 'nullable|regex:/^[\pL\s\-]+$/u|max:25',
            'billing_email' => 'required|email|regex:'.$email_regex.'|max:255',
            'billing_mobile' => 'required',
            'billing_alternate_mobile' => 'nullable',
            'billing_address' => 'required|string|max:255',
            'pickup_location' => 'required|string|max:255',
            'payment_method' => 'required|string|max:250',
        ], [
            'car_id.required' => 'Car id is required',
            'car_id.numeric' => 'Car id is invalid',
            'car_id.max' => 'Car id is invalid',
            // 'booking_from_date.required' => 'Booking from date is required',
            // 'booking_from_date.before' => 'Booking from date should be after today',
            // 'booking_from_date.date_format' => 'Booking from date format should be Y-m-d H:i:s',
            // 'booking_to_date.required' => 'Booking to date is required',
            // 'booking_to_date.before' => 'Booking to date should be after today',
            // 'booking_to_date.date_format' => 'Booking to date format should be Y-m-d H:i:s',
            'currency.required' => 'Currency is required',
            'total_amount.required' => 'Total amount is required',
            'total_amount.regex' => 'Total amount should be valid double value',
            'total_amount.max' => 'Total amount is invalid',
            'security_amount.required' => 'Security amount is required',
            'security_amount.regex' => 'Security amount should be valid double value',
            'security_amount.max' => 'Security amount is invalid',
            'tax_amount.required' => 'Tax amount is required',
            'tax_amount.regex' => 'Tax amount should be valid double value',
            'tax_amount.max' => 'Tax amount is invalid',
            'grand_total.required' => 'Grand total is required',
            'grand_total.regex' => 'Grand total should be valid double value',
            'grand_total.max' => 'Grand total is invalid',
            'billing_first_name.required' => 'Billing first Name is required',
            'billing_first_name.regex' => 'Billing first Name is invalid',
            'billing_first_name.max' => 'Billing first Name should be maximum 50 characters long',
            'billing_last_name.required' => 'Billing last Name is required',
            'billing_last_name.regex' => 'Billing last Name is invalid',
            'billing_last_name.max' => 'Billing last Name should be maximum 25 characters long',
            'billing_email.required' => 'Billing email is required',
            'billing_email.email' => 'Billing email is invalid',
            'billing_email.regex' => 'Billing email is invalid',
            'billing_mobile.required' => 'Billing mobile no. is required',
            'billing_mobile.numeric' => 'Billing mobile no. should be numeric',
            'billing_mobile.digits_between' => 'Billing mobile no. minimum 4 and maximum 15 digits long',
            'billing_alternate_mobile.required' => 'Billing alternate mobile no. is required',
            'billing_alternate_mobile.numeric' => 'Billing alternate mobile no. should be numeric',
            'billing_alternate_mobile.digits_between' => 'Billing alternate mobile no. minimum 4 and maximum 15 digits long',
            'billing_address.required' => 'Billing address is required',
            'billing_address.string' => 'Billing address is invalid',
            'billing_address.max' => 'Billing address should be max 255 characters longs',
            'pickup_location.required' => 'Pickup location is required',
            'pickup_location.string' => 'Pickup location is invalid',
            'pickup_location.max' => 'Pickup location should be max 255 characters longs',
        ]);
        
        /* if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        } */
        
        //Converted Currency
        $jsoncurrency = currencyConverter();
        
        $userCurrency = \Session::has('current_user_currency') ? \Session::get('current_user_currency') : 'USD';
        $current_car = Cars::find($request->car_id);
        $duration =  date_diff(date_create($request->booking_from_date), date_create($request->booking_to_date)); 
        
        $duration = $duration->format("%a");
        //die($duration);
        // die($request->booking_from_date.' '.$request->booking_to_date);

        if(($request->available_from_date != $request->available_to_date) && $duration == 0){
            $duration = 1;
        }
        //Calculate total of perday amount 
        $current_car_perday_total_amount = $current_car->price_per_day * $duration;
        $current_car_security_amount = $current_car->security_amount;
        $current_car_grand_total = $current_car_perday_total_amount + $current_car_security_amount;
        
        $booking_id = 'RKR' . date('Ymd') . mt_rand();
        
        $order = new Orders();
        $order->user_id = \Auth::id();
        $order->car_id = $request->car_id;
        $order->booking_id = $booking_id;
        $order->booking_from_date = date('Y-m-d H:i:s', strtotime($request->booking_from_date));
        $order->booking_to_date = date('Y-m-d H:i:s', strtotime($request->booking_to_date));
        $order->currency =  $userCurrency;
        /* $order->paid_amount = $request->grand_total;
        $order->total_amount = $request->total_amount;
        $order->security_amount = $request->security_amount;
        $order->tax_amount = $request->tax_amount;
        $order->grand_total = $request->grand_total; */
        
        $order->paid_amount = getCurrencyFromUser('USD', $userCurrency, $current_car->currency, $current_car_grand_total, $jsoncurrency);
        $order->total_amount = getCurrencyFromUser('USD', $userCurrency, $current_car->currency, $current_car_perday_total_amount, $jsoncurrency);
        $order->security_amount = getCurrencyFromUser('USD', $userCurrency, $current_car->currency, $current_car_security_amount, $jsoncurrency);
        $order->tax_amount = getCurrencyFromUser('USD',$userCurrency, $current_car->currency, '0', $jsoncurrency);
        $order->grand_total = getCurrencyFromUser('USD', $userCurrency, $current_car->currency,  $current_car_grand_total, $jsoncurrency);
        $order->billing_first_name = $request->billing_first_name;
        $order->billing_last_name = $request->billing_last_name;
        $order->billing_email = $request->billing_email;
        $order->billing_mobile = $request->billing_mobile;
        $order->billing_alternate_mobile = $request->billing_alternate_mobile;
        $order->billing_address = $request->billing_address;
        $order->pickup_location = $request->pickup_location;

        if ($request->hasFile('file')) {
                
            $files = $request->file('file');
            $file_array = [];
            foreach($files as $file){

                if ($file->getClientOriginalExtension() == 'jpeg' ||
                $file->getClientOriginalExtension() == 'png' ||
                $file->getClientOriginalExtension() == 'jpg') {
                    $file_new = $this->uploadImage($file, $this->imagePathDoc, $this->imageSizes);
                } else {
                    $file_new = $this->uploadDocs($file, $this->imagePathDoc);
                }

            // $img = Orders::where('booking_id', $request->booking_id)->first();
            $file_array[] = $file_new;
            }
            $order->doc = json_encode($file_array);
            $order->save();

        }else{
            $order->save();
        } 
        
        if( base64_decode($request->payment_method) == 'stripe'){

            return view('front.bookings.stripe',[
                'public_key' => config('api.stripe_p_key'),
                'booking_id' => $order->booking_id,
                'grand_total_usd' => getCurrencyFromUser('USD', 'USD', $current_car->currency, $current_car_grand_total, $jsoncurrency),
            ]);
        }
        elseif( base64_decode($request->payment_method ) == 'paypal'){
 
            //return back()->with('message', "Sorry, This payment method is under process please try with other method.");
            return view('front.bookings.paypal',[
                'clientId' => config('api.sandbox_clientId'),
                'booking_id' => $order->booking_id,
                'grand_total_usd' => getCurrencyFromUser('USD', 'USD', $current_car->currency, $current_car_grand_total, $jsoncurrency),
            ]);

        }else{
            return back()->with('message', 'Sorry, selected payment method is invalid.');
        }        

    }

    
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function PayNow()
    {
        return view('front.stripe');
    } */
  
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripePost(Request $request)
    {
        $msg = "Sorry, payment can not complete now.";
        //return $request->all();
        $email_regex = '/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix';
        

        $validator = Validator::make($request->all(), [
            'bookingid' => 'required|exists:orders,booking_id,deleted_at,NULL,status,1,order_status,1,user_id,'.Auth::user()->id,
            'amount'     => 'required|integer',
            ////'payment_method' => 'required|'.Rule::in(['Paypal', 'Checkout']),
            ////'bank_reference_id' => 'nullable|string|max:255',
           /// 'transaction_id' => 'nullable|string|max:255',
            //'time_zone' => 'required|string|max:255',
        ], [
            'bookingid.required' => 'Boking id is required',
            // 'payment_method.required' => 'Payment method is required',
            // 'bank_reference_id.required' => 'Bank reference id is required',
            // 'bank_reference_id.string' => 'Bank reference id is invalid',
            // 'bank_reference_id.max' => 'Bank reference id should be max 255 characters longs',
            // 'transaction_id.required' => 'Transaction id is required',
            // 'transaction_id.string' => 'Transaction id is invalid',
            // 'transaction_id.max' => 'Transaction id should be max 255 characters longs',
        ]);
        if ($validator->fails()) {
            // dd($validator->errors());
            // return response()->json(['error' => $validator->errors()], 422);
            return redirect()->route('main')->with('message', 'Sorry, this Car has been booked by other customer, please try again with same or other Car.'); 
        }
        //dd("dfss"); 
        Stripe\Stripe::setApiKey(config('api.stripe_secret_key'));
        
        $data = [];
        try {
            // Use Stripe's library to make requests...
            $data = Stripe\Charge::create ([
                "amount" => $request->amount * 100,
                "currency" => "usd",
                "source" => $request->stripeToken,
                "description" => "Test payment from Suhail Khan" 
            ]);
          } catch(\Stripe\Exception\CardException $e) {
            // Since it's a decline, \Stripe\Exception\CardException will be caught
           /*  echo 'Status is:' . $e->getHttpStatus() . '\n';
            echo 'Type is:' . $e->getError()->type . '\n';
            echo 'Code is:' . $e->getError()->code . '\n'; */
            // param is '' in this case
            /* echo 'Param is:' . $e->getError()->param . '\n'; */
           $msg = $e->getError()->message;
          } catch (\Stripe\Exception\RateLimitException $e) {
            // Too many requests made to the API too quickly
          } catch (\Stripe\Exception\InvalidRequestException $e) {
            // Invalid parameters were supplied to Stripe's API
          } catch (\Stripe\Exception\AuthenticationException $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
          } catch (\Stripe\Exception\ApiConnectionException $e) {
            // Network communication with Stripe failed
          } catch (\Stripe\Exception\ApiErrorException $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
          } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
          }
         
        if(!empty($data)){
            if($data->status == 'succeeded'){
              
                $msg = "Payment Successfull.";

            }
            elseif($data->status == 'payment_failed')
            {
                $msg = "Payment Faild.";
            }else{
                $msg = "Payment can not complete now.";
            }
        }else{
             
            echo $msg ;
            exit();
        } 
         //dd($data);
        Session::flash('message', $msg); 
        
        //Get converted currency
        /* $jsoncurrency = currencyConverter();
        $userCurrency = $request->header('USER-LOCATION-CURRENCY'); */

        $order = Orders::where('booking_id', $request->bookingid)->first();
        $order->payment_method    = $data->payment_method_details->type.' '.($data->payment_method_details->type == 'card') ? $data->payment_method_details->card->brand .' '.$data->payment_method_details->card->funding.' card':'';
        $order->bank_reference_id = $data->id;
        $order->transaction_id    = $data->balance_transaction;
        $order->stripe_invoice    = $data->receipt_url;
        $order->order_status      = 2;
        $order->save();
      // dd($order);
        $setting = Settings::first();

        $time_zone =  \Session::has('riders_admin_timezone') ?  \Session::get('riders_admin_timezone'):"Asia/Kolkata";
        
        if($time_zone != ''){
            $serverBokingFromDate = new DateTime($order->booking_from_date);
            $serverBokingFromDate->setTimezone(new DateTimeZone($time_zone));
            $bookingFrom = $serverBokingFromDate->format('Y-m-d H:i:s');
            
        }else{
            $bookingFrom = $order->booking_from_date ;
        }

        if($time_zone != ''){
            $serverBokingToDate = new DateTime($order->booking_to_date); 
            $serverBokingToDate->setTimezone(new DateTimeZone($time_zone));            
            $bookingTo = $serverBokingToDate->format('Y-m-d H:i:s');

        }else{
            $bookingTo = $order->booking_to_date ;
        }

        $pdf = PDF::loadView('order_invoice', compact('order', 'setting', 'bookingFrom','bookingTo'));
        $pdf->setPaper('a4', 'landscape');
         //return $pdf->download('OrderInvoice.pdf');
        
        //$storage_path =  \Url('storage').'/invoice';
        
        $storage_path =  storage_path().'/invoice';

        if(!is_dir($storage_path)){            
            mkdir($storage_path,'0777',true);
        }
       
        $filename = date('Ymd').time().'_myorder.pdf';

        $pdf->save($storage_path.'/'.$filename);
        //$display_path = $storage_path.'/'.$filename;
        $updated_order = Orders::find($order->id);
        $updated_order->order_invoice = $filename;
        $updated_order->save();

        \Mail::to($updated_order->user->email)->send(new OrderSuccessBuyerMail($updated_order));
        \Mail::to($updated_order->car->owner->email)->send(new OrderSuccessSellerMail($updated_order));
        
        $keys = $updated_order->car->owner->devices->pluck('device_token')->toArray();
       
        //send push notification to Owner
        $msg =  'Dear '.ucfirst($updated_order->car->owner->first_name) .' '.ucfirst($updated_order->car->owner->last_name).' you have a new booking.';
        $action = "Owner";

        foreach($keys as $key){
            fcm_notification($msg, 'Reekar New Booking', $key, $action); 
        }
        Session::flash('download.in.the.next.request',  $updated_order->order_invoice);
        
        return redirect()->route('main');
 
    }

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function paypalPost(Request $request)
    {  
        //dd($request);
        $this->validate($request, [
            'bookingid' => 'required|exists:orders,booking_id,deleted_at,NULL,status,1,order_status,1',//,user_id,'.Auth::user()->id,
            'amount'     => 'required|regex:/^\d+(\.\d{1,2})?$/',
             
        ], [
            'bookingid.required' => 'Boking id is required',
            
        ]);
        
        $order = Orders::where('booking_id', $request->bookingid)->first();
        $order->payment_method    = 'paypal';
        $order->bank_reference_id = $request->ref_id;
        $order->transaction_id    = $request->transaction_id;
        // $order->stripe_invoice    = $request->receipt_url;
        $order->order_status      = 2;
        $order->save();
      // dd($order);
        $setting = Settings::first();

        $time_zone =  \Session::has('riders_admin_timezone') ?  \Session::get('riders_admin_timezone'):"Asia/Kolkata";
        
        if($time_zone != ''){
            $serverBokingFromDate = new DateTime($order->booking_from_date);
            $serverBokingFromDate->setTimezone(new DateTimeZone($time_zone));
            $bookingFrom = $serverBokingFromDate->format('Y-m-d H:i:s');
            
        }else{
            $bookingFrom = $order->booking_from_date ;
        }

        if($time_zone != ''){
            $serverBokingToDate = new DateTime($order->booking_to_date); 
            $serverBokingToDate->setTimezone(new DateTimeZone($time_zone));            
            $bookingTo = $serverBokingToDate->format('Y-m-d H:i:s');

        }else{
            $bookingTo = $order->booking_to_date ;
        }

        $pdf = PDF::loadView('order_invoice', compact('order', 'setting', 'bookingFrom','bookingTo'));
        $pdf->setPaper('a4', 'landscape');
         //return $pdf->download('OrderInvoice.pdf');
        
        //$storage_path =  \Url('storage').'/invoice';
        
        $storage_path =  storage_path().'/invoice';

        if(!is_dir($storage_path)){            
            mkdir($storage_path,'0777',true);
        }
       
        $filename = date('Ymd').time().'_myorder.pdf';

        $pdf->save($storage_path.'/'.$filename);
        //$display_path = $storage_path.'/'.$filename;
        $updated_order = Orders::find($order->id);
        $updated_order->order_invoice = $filename;
        $updated_order->save();

        \Mail::to($updated_order->user->email)->send(new OrderSuccessBuyerMail($updated_order));
        \Mail::to($updated_order->car->owner->email)->send(new OrderSuccessSellerMail($updated_order));
        
        $keys = $updated_order->car->owner->devices->pluck('device_token')->toArray();
       
        //send push notification to Owner
        $msg =  'Dear '.ucfirst($updated_order->car->owner->first_name) .' '.ucfirst($updated_order->car->owner->last_name).' you have a new booking.';
        $action = "Owner";

        foreach($keys as $key){
            fcm_notification($msg, 'Reekar New Booking', $key, $action); 
        }
        Session::flash('download.in.the.next.request',  $updated_order->order_invoice);
        
        Session::flash('message', 'Payment Successfull.'); 
       
        return redirect()->route('main');

    }
    
    public function pdfDownload($image_url){
        
        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"" . basename($image_url) . "\"");
        readfile(\Url('storage').'/invoice'.'/'.$image_url); 
    }
}