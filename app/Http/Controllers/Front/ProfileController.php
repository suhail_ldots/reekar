<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Utility\PassportToken;
use App\Http\Utility\UtilityFunction;
use App\Models\Brands;
use App\Models\CarDocs;
use App\Models\CarImages;
use App\Models\Cars;
use App\Models\Colors;
use App\Models\EngineTypes;
use App\Models\Models;
use App\Models\PaymentMethods;
use App\Models\Transmissions;
use App\Models\VehicleTypes;
use App\Models\Seats;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class ProfileController extends Controller
{
    use UtilityFunction, PassportToken;
    //image upload path set for works images
    protected $imagePath = 'images/cars';
    protected $imageUserPath = 'images/users';
    protected $imagePathDoc = 'images/cars/docs';
    protected $imageSizes = [
        'thumb' => [200, 200],
    ];
    protected $imageSizesMarkup = [
        'thumb-markup' => [50, 50],
    ];

    /**
     * For authorized user profile information
     *
     * @param Request $request request body from client
     * @return Json responce data with http responce code
     */
    public function profile(Request $request)
    {
        $user = User::where('id', \Auth::id())->first();

        return response()->json([
            'profile' => User::where('id', \Auth::id())
                ->select('id', 'first_name', 'last_name', 'country_code', 'mobile', 'profile_pic', 'email', 'address')
                ->first(),
            'user_image_base_url' => \Url('storage/app/images/users/'),
        ], 200);
    }

    /**
     * For update the user profile
     *
     * @param Request $request request body from client
     * @return Json responce data with http responce code
     */
    public function updateProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|regex:/^[\pL\s\-]+$/u|max:50',
            'last_name' => 'nullable|alpha|max:25',
            'country_code' => 'required|max:10|' . Rule::in(array_column(json_decode(\Storage::disk('local')->get('data/country_code_json.json')), 'dial_code')),
            'mobile' => 'required|numeric|digits_between:4,15',
            'address' => 'required|string|max:255',
            'image' => 'nullable|image|mimes:jpeg,png,jpg|max:20480',
        ], [
            'first_name.required' => 'First Name is required',
            'first_name.regex' => 'First Name is invalid',
            'first_name.max' => 'First Name should be maximum 50 characters long',
            'last_name.required' => 'Last Name is required',
            'last_name.regex' => 'Last Name is invalid',
            'last_name.max' => 'Last Name should be maximum 25 characters long',
            'country_code.required' => 'Country code is required',
            'country_code.max' => 'Country code should be maximum 10 characters long',
            'mobile.required' => 'Phone no. is required',
            'mobile.numeric' => 'Phone no. should be numeric',
            'mobile.digits_between' => 'Phone no. minimum 4 and maximum 15 digits long',
            'address.required' => 'Address is required',
            'address.max' => 'Address should be maximum 255 characters long',
            'address.string' => 'Address is invalid',
            'image.image' => 'Profile image should be JPEG, PNG or JPG',
            'image.mimes' => 'Profile image should be JPEG, PNG or JPG',
            'image.max' => 'Profile image too large',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        $user = Auth::user();
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->country_code = $request->country_code;
        $user->mobile = $request->mobile;
        $user->address = $request->address;
        $user->save();

       
        $image_new = $user->profile_pic;
        if ($request->hasFile('image')) {

            $image = $request->file('image');
            $image_new = $this->uploadImage($image, $this->imageUserPath, $this->imageSizes);
            $this->ImageResize($image, $image_new, $this->imageSizesMarkup, $this->imageUserPath);
            $user->profile_pic = $image_new;
            $user->save();
        }

        return response()->json([
            'message' => 'Profile updated successfully',
            'image' => \Url('storage/app/images/users/').'/'.$image_new,
        ], 200);
    }

    /**
     * For update the user profile
     *
     * @param Request $request request body from client
     * @return Json responce data with http responce code
     */
    public function updateProfileImage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'required|image|mimes:jpeg,png,jpg|max:20480',
        ], [
            'image.image' => 'Profile image should be JPEG, PNG or JPG',
            'image.mimes' => 'Profile image should be JPEG, PNG or JPG',
            'image.max' => 'Profile image too large',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        $user = Auth::user();
        $image = $request->file('image');
        $image_new = '';
        if ($image) {
            $image_new = $this->uploadImage($image, $this->imageUserPath, $this->imageSizes);
            $this->ImageResize($image, $image_new, $this->imageSizesMarkup, $this->imageUserPath);
            $user->profile_pic = $image_new;
            $user->save();
        }

        return response()->json([
            'message' => 'Profile image updated successfully',
        ], 200);
    }

    /**
     * For register new user profile
     *
     * @param Request $request request body from client
     * @return Json responce data with http responce code
     */
    public function savePaymentMethod(Request $request)
    {
        $email_regex = '/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix';
        $validator = Validator::make($request->all(), [
            'payment_id' => 'nullable|numeric|exists:payment_methods,id,deleted_at,NULL,status,1,user_id,' . \Auth::id(),
            'type' => 'required|' . Rule::in(['Paypal', 'Checkout']),
            'first_name' => 'required_if:type,IBAN|regex:/^[\pL\s\-]+$/u|max:50',
            'last_name' => 'required_if:type,IBAN|regex:/^[\pL\s\-]+$/u|max:25',
            'email' => 'required_if:type,Paypal|email|regex:' . $email_regex . '|max:255',
            'iban' => 'required_if:type,IBAN|max:25',
            'bank_name' => 'required_if:type,IBAN|max:150',
            'bank_swift_code' => 'required_if:type,IBAN|max:150',
            'mobile' => 'required_if:type,IBAN|numeric|digits_between:4,15',
            'card_no' => 'required_if:type,CC|digits:16',
            'exp_date' => 'required_if:type,CC|max:20',
            'cvv' => 'required_if:type,CC|digits:3',
            'name_on_card' => 'required_if:type,CC|regex:/^[\pL\s\-]+$/u|max:50',
        ], [
            'type.required' => 'Type is required',
            'first_name.required_if' => 'First Name is required',
            'first_name.regex' => 'First Name is invalid',
            'first_name.max' => 'First Name should be maximum 50 characters long',
            'last_name.required_if' => 'Last Name is required',
            'last_name.regex' => 'Last Name is invalid',
            'last_name.max' => 'Last Name should be maximum 25 characters long',
            'email.required_if' => 'Email is required',
            'email.email' => 'Email is invalid',
            'email.regex' => 'Email is invalid',
            'iban.required_if' => 'IBAN is required',
            'iban.max' => 'IBAN should be maximum 25 characters long',
            'bank_name.required_if' => 'Bank name is required',
            'bank_name.max' => 'Bank name should be maximum 150 characters long',
            'bank_swift_code.required_if' => 'Bank swift code is required',
            'bank_swift_code.max' => 'Bank swift code should be maximum 150 characters long',
            'mobile.required_if' => 'Phone no. is required',
            'mobile.numeric' => 'Phone no. should be numeric',
            'mobile.digits_between' => 'Phone no. minimum 4 and maximum 15 digits long',
            'card_no.required_if' => 'Card no is required',
            'card_no.digits' => 'Card no is invalid',
            'exp_date.required_if' => 'Expiry date is required',
            'exp_date.max' => 'Expiry date is invalid',
            'cvv.required_if' => 'CVV is required',
            'cvv.digits' => 'CVV is invalid',
            'name_on_card.required_if' => 'Name on card is required',
            'name_on_card.regex' => 'Name on card is invalid',
            'name_on_card.max' => 'Name on card should be maximum 50 characters long',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        if ($request->payment_id) {
            $payment = PaymentMethods::find($request->payment_id);
        } else {
            $payment = new PaymentMethods();
        }

        $payment->user_id = \Auth::id();
        $payment->type = $request->type;
        $payment->first_name = $request->first_name;
        $payment->last_name = $request->last_name;
        $payment->email = $request->email;
        $payment->iban = $request->iban;
        $payment->bank_name = $request->bank_name;
        $payment->bank_swift_code = $request->bank_swift_code;
        $payment->mobile = $request->mobile;
        $payment->card_no = $request->card_no;
        $payment->exp_date = $request->exp_date;
        $payment->cvv = $request->cvv;
        $payment->name_on_card = $request->name_on_card;
        $payment->save();
        return response()->json([
            'message' => 'Payment method saved successfully',
        ], 200);
    }

    /**
     * For save car info
     *
     * @param Request $request request body from client
     * @return Json responce data with http responce code
     */
    public function saveCarInfo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'car_id' => 'nullable|numeric|exists:cars,id,deleted_at,NULL,status,1,user_id,' . \Auth::id(),
            'brand_id' => 'required|numeric|exists:brands,id,deleted_at,NULL,status,1',
            'model_id' => 'required|numeric|exists:models,id,deleted_at,NULL,status,1,brand_id,' . $request->brand_id,
            'vehicle_type_id' => 'required|numeric|exists:vehicle_types,id,deleted_at,NULL,status,1',
            'engine_type_id' => 'required|numeric|exists:engine_types,id,deleted_at,NULL,status,1',
            'transmission_id' => 'required|numeric|exists:transmissions,id,deleted_at,NULL,status,1',
            'color_id' => 'required|numeric|exists:colors,id,deleted_at,NULL,status,1',
            'seat_id' => 'required|numeric|exists:seats,id,deleted_at,NULL,status,1',
            'year' => 'required|integer|digits:4|min:1980|max:'.date('Y'), 
            'currency' => 'nullable',
            'price_per_day' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/|max:255',
            'total_price' => 'nullable|regex:/^[0-9]+(\.[0-9][0-9]?)?$/|max:255',
            'security_amount' => 'nullable|regex:/^[0-9]+(\.[0-9][0-9]?)?$/|max:255',
            'ac' => 'required|' . Rule::in(['1', '0']),
            'gps' => 'required|' . Rule::in(['1', '0']),
            'ipod_interface' => 'required|' . Rule::in(['1', '0']),
            'sunroof' => 'required|' . Rule::in(['1', '0']),
            'child_seat' => 'required|' . Rule::in(['1', '0']),
            'electric_windows' => 'required|' . Rule::in(['1', '0']),
            'heated_seat' => 'required|' . Rule::in(['1', '0']),
            'panorma_roof' => 'required|' . Rule::in(['1', '0']),
            'prm_gauge' => 'required|' . Rule::in(['1', '0']),
            'abs' => 'required|' . Rule::in(['1', '0']),
            'traction_control' => 'required|' . Rule::in(['1', '0']),
            'audio_system' => 'required|' . Rule::in(['1', '0']),
        ], [
            'brand_id.required' => 'Brand id is required',
            'brand_id.numeric' => 'Brand id is invalid',
            'brand_id.exists' => 'Brand id is not found',
            'model_id.required' => 'Model id is required',
            'model_id.numeric' => 'Model id is invalid',
            'model_id.exists' => 'Model id is not found',
            'vehicle_type_id.required' => 'Vehicle type id is required',
            'vehicle_type_id.numeric' => 'Vehicle type id is invalid',
            'vehicle_type_id.exists' => 'Vehicle type id is not found',
            'engine_type_id.required' => 'Engine type id is required',
            'engine_type_id.numeric' => 'Engine type id is invalid',
            'engine_type_id.exists' => 'Engine type id is not found',
            'transmission_id.required' => 'Transmission id is required',
            'transmission_id.numeric' => 'Transmission id is invalid',
            'transmission_id.exists' => 'Transmission id is not found',
            'color_id.required' => 'Color id is required',
            'color_id.numeric' => 'Color id is invalid',
            'color_id.exists' => 'Color id is not found',
            'seat_id.required' => 'Seat id is required',
            'seat_id.numeric' => 'Seat id is invalid',
            'seat_id.exists' => 'Seat id is not found',
            'year.required' => 'Year is required',
            'year.before' => 'Year should be before today',
            'year.date_format' => 'Year format should be YYYY',
            'currency.required' => 'Currency is required',
            'price_per_day.required' => 'Price per day is required',
            'price_per_day.regex' => 'Price per day should be valid double value',
            'price_per_day.max' => 'Price per day is invalid',
            'total_price.required' => 'Total price is required',
            'total_price.regex' => 'Total price should be valid double value',
            'total_price.max' => 'Total price is invalid',
            'security_amount.required' => 'Security amount is required',
            'security_amount.regex' => 'Security amount should be valid double value',
            'security_amount.max' => 'Security amount is invalid',
            'ac.required' => 'AC is required',
            'gps.required' => 'GPS is required',
            'ipod_interface.required' => 'IPod interface is required',
            'sunroof.required' => 'Sun roof is required',
            'child_seat.required' => 'Child seat is required',
            'electric_windows.required' => 'Electric windows is required',
            'heated_seat.required' => 'Heated seats is required',
            'panorma_roof.required' => 'Panaroma roof is required',
            'prm_gauge.required' => 'PRM Guage is required',
            'abs.required' => 'ABS is required',
            'traction_control.required' => 'Tranction control is required',
            'audio_system.required' => 'Audio system is required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        //Get converted currency
        $jsoncurrency = currencyConverter();
        $userCurrency = $request->header('USER-LOCATION-CURRENCY');


        if ($request->car_id) {
            $car = Cars::find($request->car_id);
        } else {
            $car = new Cars();
        }

        $car->user_id = \Auth::id();
        $car->brand_id = $request->brand_id;
        $car->model_id = $request->model_id;
        $car->vehicle_type_id = $request->vehicle_type_id;
        $car->engine_type_id = $request->engine_type_id;
        $car->transmission_id = $request->transmission_id;
        $car->color_id = $request->color_id;
        $car->seat_id = $request->seat_id;
        $car->year = $request->year;
        $car->currency = $userCurrency;
        $car->price_per_day = $request->price_per_day;
        $car->total_price = 0;//$request->total_price;
        $car->security_amount = $request->security_amount;
        // $car->security_amount = getCurrencyFromUser($userCurrency, 'USD', $request->security_amount, $jsoncurrency);
        $car->ac = $request->ac;
        $car->gps = $request->gps;
        $car->ipod_interface = $request->ipod_interface;
        $car->sunroof = $request->sunroof;
        $car->child_seat = $request->child_seat;
        $car->electric_windows = $request->electric_windows;
        $car->heated_seat = $request->heated_seat;
        $car->panorma_roof = $request->panorma_roof;
        $car->prm_gauge = $request->prm_gauge;
        $car->abs = $request->abs;
        $car->traction_control = $request->traction_control;
        $car->audio_system = $request->audio_system;
        $car->listing_status = 0; 
        $car->save();
        return response()->json([
            'car_id' => $car->id,
            'message' => 'Car Information saved successfully',
        ], 200);
    }

    /**
     * For save car location
     *
     * @param Request $request request body from client
     * @return Json responce data with http responce code
     */
    public function saveCarLocation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'car_id' => 'required|numeric|exists:cars,id,deleted_at,NULL,status,1,user_id,' . \Auth::id(),
            'car_location' => 'required|string|max:255',
            'car_location_lat' => 'required|string|max:255',
            'car_location_long' => 'required|string|max:255',
        ], [
            'car_id.required' => 'Car id is required',
            'car_id.numeric' => 'Car id should be numeric',
            'car_id.exists' => "Car id dosen't exist",
            'car_location.required' => 'Car location is required',
            'car_location.string' => 'Car location is invalid',
            'car_location.max' => 'Car location should be max 255 characters longs',
            'car_location_lat.required' => 'Car location lat is required',
            'car_location_lat.string' => 'Car location lat is invalid',
            'car_location_lat.max' => 'Car location lat should be max 255 characters longs',
            'car_location_long.required' => 'Car location long is required',
            'car_location_long.string' => 'Car location long is invalid',
            'car_location_long.max' => 'Car location long should be max 255 characters longs',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        $car = Cars::find($request->car_id);
        $car->car_location = $request->car_location;
        $car->car_location_lat = $request->car_location_lat;
        $car->car_location_long = $request->car_location_long;
        $car->save();
        return response()->json([
            'message' => 'Car location saved successfully',
        ], 200);
    }

    /**
     * For save car availability info
     *
     * @param Request $request request body from client
     * @return Json responce data with http responce code
     */
    public function saveCarAvailability(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'car_id' => 'required|numeric|exists:cars,id,deleted_at,NULL,status,1,user_id,' . \Auth::id(),
            'available_from_date' => 'required|date_format:Y-m-d H:i:s|after:' . date('Y-m-d H:i:s', strtotime('-1 hours')),
            'available_from_time' => 'nullable|date_format:H:i:s',
            'available_to_date' => 'required|date_format:Y-m-d H:i:s|after:' . date('Y-m-d H:i:s', strtotime($request->available_from_date)),
            'available_to_time' => 'nullable|date_format:H:i:s',
        ], [
            'car_id.required' => 'Car id is required',
            'car_id.numeric' => 'Car id should be numeric',
            'car_id.exists' => "Car id dosen't exist",
            'available_from_date.required' => 'Available from date is required',
            'available_from_date.after' => 'Available from date should be after today date',
            'available_from_date.date_format' => 'Available from date should be Y-m-d format',
            'available_from_time.required' => 'Available from time is required',
            'available_from_time.date_format' => 'Available from time should be H:i:s format',
            'available_to_date.required' => 'Available to date is required',
            'available_to_date.after' => 'Available to date should be after available from date',
            'available_to_date.date_format' => 'Available to date should be Y-m-d format',
            'available_to_time.required' => 'Available from time is required',
            'available_to_time.date_format' => 'Available from time should be H:i:s format',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        $car = Cars::find($request->car_id);
        $car->available_from_date = date('Y-m-d H:i:s', strtotime($request->available_from_date));
        $car->available_from_time = date('H:i:s', strtotime($request->available_from_date));
        $car->available_to_date = date('Y-m-d H:i:s', strtotime($request->available_to_date));
        $car->available_to_time = date('H:i:s', strtotime($request->available_to_date));
        $car->save();
        return response()->json([
            'message' => 'Car availability saved successfully',
        ], 200);
    }

    /**
     * For get all master data
     *
     * @param Request $request request body from client
     * @return Json responce data with http responce code
     */
    public function getMasterData(Request $request)
    {
        return response()->json([
            'filter' => [
                [
                    'name' => 'Brand',
                    'data' => Brands::with(['models' => function ($q) {
                        $q->select('id', 'brand_id', 'name')->where('status', '1');
                    }])->select('id', 'name')->where('status', '1')->get(),
                ],
                [
                    'name' => 'Model',
                    'data' => Models::select('id', 'brand_id', 'name')->where('status', '1')->get(),
                ],
                [
                    'name' => 'Color',
                    'data' => Colors::select('id', 'name')->where('status', '1')->get(),
                ],
                [
                    'name' => 'Engine Type',
                    'data' => EngineTypes::select('id', 'name')->where('status', '1')->get(),
                ],
                [
                    'name' => 'Transmission',
                    'data' => Transmissions::select('id', 'name')->where('status', '1')->get(),
                ],
                [
                    'name' => 'Vehicle Type',
                    'data' => VehicleTypes::select('id', 'name')->where('status', '1')->get(),
                ],
                [
                    'name' => 'Seats',
                    'data' => Seats::select('id', 'name')->where('status', '1')->get(),
                ],
            ],
        ], 200);
    }

    /**
     * For upload bulk images for car
     *
     * @param Request $request request body from client
     * @return Json responce data with http responce code
     */
    public function uploadBulkPhoto(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'car_id' => 'required|numeric|exists:cars,id,deleted_at,NULL,status,1,user_id,' . \Auth::id(),
            'images' => 'nullable|array',
        ], [
            'images.required' => 'At least one image is mandatory',
            'images.array' => 'Images invalid',
            'car_id.required' => 'Car id is required',
            'car_id.numeric' => 'Car id is invalid',
            'car_id.max' => 'Car id is invalid',
            'car_id.exists' => 'Car id is invalid',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }
        if($request->images && count($request->images)){
        foreach ($request->images as $image) {
            $validator = Validator::make($image, [
                'image' => 'nullable|image|mimes:jpeg,png,jpg|max:20480',
            ], [
                'image.required' => 'Image is required',
                'image.image' => 'Image should be JPEG, PNG or JPG',
                'image.mimes' => 'Image should be JPEG, PNG or JPG',
            ]);
            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 422);
            }
        }
    }

       /* if (CarImages::where('car_id', $request->car_id)->first()) {
            CarImages::where('car_id', $request->car_id)->delete();
        }*/

if($request->images && count($request->images)){
        foreach ($request->images as $key => $car_image) {
            $image = $request->file('images.' . $key . '.image');
            if ($image) {
                $image_new = $this->uploadImage($image, $this->imagePath, $this->imageSizes);

                $img = new CarImages();
                $img->car_id = $request->car_id;
                $img->image = $image_new;
                $img->save();
            }
        }
    }
        return response()->json([
            'message' => "Uploaded successfully",
        ], 200);
    }

    /**
     * For upload bulk docs for car
     *
     * @param Request $request request body from client
     * @return Json responce data with http responce code
     */
    public function uploadBulkDocs(Request $request)
    {
        //return \Auth::id();
        $validator = Validator::make($request->all(), [
            'car_id' => 'required|numeric|exists:cars,id,deleted_at,NULL,status,1,user_id,' . \Auth::id(),
            'docs' => 'nullable|array',
        ], [
            'docs.required' => 'At least one file is mandatory',
            'docs.array' => 'Docs invalid',
            'car_id.required' => 'Car id is required',
            'car_id.numeric' => 'Car id is invalid',
            'car_id.max' => 'Car id is invalid',
            'car_id.exists' => 'Car id is invalid',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }
        if($request->docs && count($request->docs)){
        foreach ($request->docs as $act_file) {
            $validator = Validator::make($act_file, [
                'file' => 'nullable|file|mimes:jpeg,png,jpg,pdf,doc,docx',
            ], [
                'file.required' => 'File is required',
            ]);
            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 422);
            }
        }
    }

        /*if (CarDocs::where('car_id', $request->car_id)->first()) {
            CarDocs::where('car_id', $request->car_id)->delete();
        }*/
if($request->docs && count($request->docs)){
        foreach ($request->docs as $key => $act_file) {
            $file = $request->file('docs.' . $key . '.file');
            if ($file) {
            	
                if ($file->getClientOriginalExtension() == 'jpeg' ||
                    $file->getClientOriginalExtension() == 'png' ||
                    $file->getClientOriginalExtension() == 'jpg') {
                    $file_new = $this->uploadImage($file, $this->imagePathDoc, $this->imageSizes);
                } else {
                    $file_new = $this->uploadDocs($file, $this->imagePathDoc);
                }

                $img = new CarDocs();
                $img->car_id = $request->car_id;
                $img->type = $file->getClientOriginalExtension();
                $img->doc = $file_new;
                $img->save();
            }
        }
    }
        return response()->json([
            'message' => "Uploaded successfully",
        ], 200);
    }

    /**
     * For list cars
     *
     * @param Request $request request body from client
     * @return Json responce data with http responce code
     */
    public function listCars(Request $request)
    {
        $items = Cars::where('user_id', \Auth::id())->where('status', '1')->orderBy('created_at', 'DESC')->paginate(10);

        $cars = [];
        $carsdetails = [];
        $features = [];

        //Get converted currency
        $jsoncurrency = currencyConverter();
        $userCurrency = $request->header('USER-LOCATION-CURRENCY');

        foreach ($items as $item) {
            $features = [];
            $carsdetails['id'] = $item->id;
            $carsdetails['year'] = $item->year;
            $carsdetails['car_location'] = $item->car_location;
            $carsdetails['available_from_date'] = $item->available_from_date;
            $carsdetails['available_from_time'] = $item->available_from_time;
            $carsdetails['available_to_date'] = $item->available_to_date;
            $carsdetails['available_to_time'] = $item->available_to_time;
            $carsdetails['currency'] = $userCurrency;
            $carsdetails['price_per_day'] = getCurrencyFromUser('USD', $userCurrency, $item->currency, $item->price_per_day, $jsoncurrency);
            $carsdetails['total_price'] = getCurrencyFromUser('USD', $userCurrency, $item->currency, $item->total_price, $jsoncurrency);
            $carsdetails['car_location_lat'] = $item->car_location_lat;
            $carsdetails['car_location_long'] = $item->car_location_long;
            $carsdetails['distance'] = $item->distance;
            $feature_count = 0;

            $carsdetails['brand'] = $item->brand->name;
            $carsdetails['model'] = $item->model->name;
            $carsdetails['color'] = $item->color->name;
            $carsdetails['seat'] = $item->seat->name;
            $carsdetails['enginetype'] = $item->enginetype->name;
            $carsdetails['transmission'] = $item->transmission->name;
            $carsdetails['vehicletype'] = $item->vehicletype->name;
            $carsdetails['features'] = $features;
            $carsdetails['reject_reason'] = null;
            $carsdetails['listing_status'] = $item->listing_status;
            if($item->listing_status == 2){
                $carsdetails['reject_reason'] = $item->reject_reason;
            }
            $carsdetails['image'] = $item->image ? \Url('storage/app/images/cars/').'/'.$item->image->image : null;
            $cars[] = $carsdetails;
        }

        if ($request->paginate == 1) {
            return response()->json([
                'cars' => $cars,
                'current_page' => $items->currentPage(),
                'total_page' => $items->lastPage(),
            ], 200);
        }else{
        return response()->json([
            'cars' => $cars,
        ], 200);
    }
    }

    /**
     * For car details
     *
     * @param Request $request request body from client
     * @return Json responce data with http responce code
     */
    public function carDetails(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'car_id' => 'required|numeric|exists:cars,id,deleted_at,NULL,status,1,user_id,'.\Auth::id(),
        ], [
            'car_id.required' => 'Car id is required',
            'car_id.numeric' => 'Car id is invalid',
            'car_id.max' => 'Car id is invalid',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        //Get converted currency
        $jsoncurrency = currencyConverter();
        $userCurency = $request->header('USER-LOCATION-CURRENCY');

        $car = Cars::where('id', $request->car_id)->first();
        $features = [];
            $carsdetails = [];
            $item = $car;
            $carsdetails['id'] = $item->id;
            $carsdetails['year'] = $item->year;
            $carsdetails['car_location'] = $item->car_location;
            $carsdetails['available_from_date'] = $item->available_from_date;
            $carsdetails['available_from_time'] = $item->available_from_time;
            $carsdetails['available_to_date'] = $item->available_to_date;
            $carsdetails['available_to_time'] = $item->available_to_time;
            $carsdetails['currency'] = $userCurency;
            $carsdetails['price_per_day'] = getCurrencyFromUser('USD', $userCurency, $item->currency, $item->price_per_day, $jsoncurrency);
            $carsdetails['total_price'] = getCurrencyFromUser('USD', $userCurency, $item->currency, $item->total_price, $jsoncurrency);
            $carsdetails['security_amount'] = getCurrencyFromUser('USD', $userCurency, $item->currency, $item->security_amount, $jsoncurrency);
            $carsdetails['car_location_lat'] = $item->car_location_lat;
            $carsdetails['car_location_long'] = $item->car_location_long;
            if ($item->ac) {
                    $features[] = 'ac';
            }

            if ($item->gps) {
                    $features[] = 'gps';
            }

            if ($item->ipod_interface) {
                    $features[] = 'ipod_interface';
            }
            
            if ($item->sunroof) {
                    $features[] = 'sunroof';
            }

            if ($item->child_seat) {
                    $features[] = 'child_seat';
            }

            if ($item->electric_windows) {
                    $features[] = 'electric_windows';
            }

            if ($item->heated_seat) {
                    $features[] = 'heated_seat';
            }

            if ($item->panorma_roof) {
                    $features[] = 'panorma_roof';
            }

            if ($item->prm_gauge) {
                    $features[] = 'prm_gauge';
            }

            if ($item->abs) {
                    $features[] = 'abs';
            }

            if ($item->traction_control) {
                    $features[] = 'traction_control';
            }

            if ($item->audio_system) {
                    $features[] = 'audio_system';
            }

            $carsdetails['brand_detail'] = $item->brand->only(['name', 'id']);
            $carsdetails['model_detail'] = $item->model->only(['name', 'id']);
            $carsdetails['color_detail'] = $item->color->only(['name', 'id']);
            $carsdetails['seat_detail'] = $item->seat->only(['name', 'id']);
            $carsdetails['enginetype_detail'] = $item->enginetype->only(['name', 'id']);
            $carsdetails['transmission_detail'] = $item->transmission->only(['name', 'id']);
            $carsdetails['vehicletype_detail'] = $item->vehicletype->only(['name', 'id']);
            $carsdetails['features'] = $features;
            $carsdetails['reject_reason'] = null;
            $carsdetails['listing_status'] = $item->listing_status;
            $carsdetails['is_rented'] = $item->is_rented;
            if($item->listing_status == 2){
                $carsdetails['reject_reason'] = $item->reject_reason;
            }
            $carsimages = [];
            $carsdocs = [];
             $carsdocs1 = [];
            if(count($item->images))
            foreach($item->images->where('status', '1') as $image => $val ){
                $carsimages[$image]['id'] = $val->id;
                $carsimages[$image]['image'] = \Url('storage/app/images/cars/').'/'.$val->image;
               
            }
            if(count($item->docs))
            foreach($item->docs->where('status', '1') as $doc => $val1){
                $carsdocs[$doc]['id'] = $val1->id;
                $carsdocs[$doc]['doc'] = \Url('storage/app/images/cars/docs/').'/'.$val1->doc;
            }
            $carsdetails['car_images'] = $carsimages;
            $carsdetails['car_docs'] = $carsdocs;

        return response()->json([
            'car' => $carsdetails,
        ], 200);
    }

    /**
     * For car details
     *
     * @param Request $request request body from client
     * @return Json responce data with http responce code
     */
    public function deleteCar(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'car_id' => 'required|numeric|exists:cars,id,deleted_at,NULL,status,1,user_id,'.\Auth::id(),
        ], [
            'car_id.required' => 'Car id is required',
            'car_id.numeric' => 'Car id is invalid',
            'car_id.max' => 'Car id is invalid',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        $car = Cars::where('id', $request->car_id)
        ->first();

        if($car->is_rented){
            return response()->json([
                'message' => "You can't delete this car as its rented by a renter",
            ], 201);
        }else{
            Cars::where('id', $request->car_id)->delete();
            return response()->json([
                'message' => "Deleted successfully",
            ], 200);
        }  
        
    }

   /**
     * For car image file deletion 
     *
     * @param Request $request request body from client
     * @return Json responce data with http responce code
     */
    public function deleteCarFile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'car_id' => 'required|numeric|exists:cars,id,deleted_at,NULL,status,1,user_id,'.\Auth::id(),
            'file_type' => 'required|string|'. Rule::in(['car_image', 'car_docs']),
            'file_id' => 'required|digits_between:1,11',
        ], [
            'car_id.required' => 'Car id is required',
            'car_id.numeric' => 'Car id is invalid',
            'car_id.max' => 'Car id is invalid',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        $car = Cars::where('id', $request->car_id)
        ->first();
        $carimage = '';
        $file1 = '';
        
        if($request->file_type == 'car_image'){
            $file1 = "Image";
            
            $ImgCheck = CarImages::where('id', $request->file_id)->where('car_id', $request->car_id)->first();

            if ($ImgCheck->image != '') { 
                $fileName = storage_path('app/images/cars/'.$ImgCheck->image);
               // return storage_path('app/images/cars/'.$fileName);
                if(file_exists($fileName)){
                    unlink($fileName);
                } 
                $carimage = $ImgCheck->delete();                
            }
            
        }
        elseif ($request->file_type == 'car_docs') {
            $file1 = "Document";

            $DocCheck = CarDocs::where('car_id', $request->car_id)
            ->where('id', $request->file_id)->first();
            
            if ($DocCheck->doc != '') {
                $fileName = storage_path('app/images/cars/docs/'.$DocCheck->doc);

                if(file_exists($fileName)){
                    unlink($fileName);
                }

                $carimage = $DocCheck->delete();
          
            }
        
        }        
        if($carimage != '')
        { 
            return response()->json([ 'message' => $file1." deleted successfully" ], 200);
        }  
        return response()->json([
            'message' => "You can't delete this car ".$file1,
        ], 201);
    } 

}
