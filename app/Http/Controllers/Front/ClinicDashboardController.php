<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Masters\SubscriptionPackages;
use App\Models\Masters\Feature;
use App\Models\Masters\PlanFeature;


class ClinicDashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('clinics.modules.nusingHome.nursing-dashboard');        
        
        $plans = SubscriptionPackages::where('status', 1)->get();
        // $plans = SubscriptionPackages::with('features')->get();
        // $plans = PlanFeature::with('features')->get();
        
        // $item = Products::where('status', 1)->get();
        // $relation = PlanFeature::whereHas('features', function ($plans) {
        //     $plans->where('status', 1);
        //     $plans->orderBy('name', 'desc');
        // })->get();

        // dd($plans[0]->features->myfeatures);
        return view('site_view.modules.home', [
            "item" => false,
            "plans" => $plans,
        ]);
        
    }
    public function featuredPlan(Request $request, $plan_id )
    {
        $plans = SubscriptionPackages::where(['status'=> 1, 'id'=>$plan_id])->first();
        // $plans = SubscriptionPackages::with('features')->get();
        // $plans = PlanFeature::with('features')->get();
        
        // $item = Products::where('status', 1)->get();
        // $relation = PlanFeature::whereHas('features', function ($plans) {
        //     $plans->where('status', 1);
        //     $plans->orderBy('name', 'desc');
        // })->get();

        // dd($plans[0]->features->myfeatures);
        return view('site_view.modules.featured_plan', [
            "item" => false,
            "plans" => $plans,
        ]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
