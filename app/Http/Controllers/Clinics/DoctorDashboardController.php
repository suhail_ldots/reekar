<?php

namespace App\Http\Controllers\Clinics;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Masters\SubscriptionPackages;
use App\Models\Masters\Feature;
use App\Models\Masters\PlanFeature;
use App\Models\Doctor;
use App\Models\Bookings;
use App\Models\GalleryImages;
use App\Models\NursingHome;
use Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

use App\User;


class DoctorDashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('clinics.modules.doctors.doctor-dashboard');
        
        $plans = SubscriptionPackages::where('status', 1)->get();
        // $plans = SubscriptionPackages::with('features')->get();
        // $plans = PlanFeature::with('features')->get();
        
        // $item = Products::where('status', 1)->get();
        // $relation = PlanFeature::whereHas('features', function ($plans) {
        //     $plans->where('status', 1);
        //     $plans->orderBy('name', 'desc');
        // })->get();

        // dd($plans[0]->features->myfeatures);
        return view('site_view.modules.home', [
            "item" => false,
            "plans" => $plans,
        ]);
        
    }
    public function profileView()
    {
        $doctor  = Doctor::where('user_id', Auth::user()->id)->first();

        return view('clinics.modules.doctors.profile', [
            'doctor' => $doctor,
        ]);
    }

    public function profileUpdate(Request $request)
    { 
        //   return $request->all();
        $email_regex = '/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix';
        $request->validate([
            'email' => 'required|string|email|regex:'.$email_regex.'|max:255|unique:users,email,' . $request->id . ',id',
            'reg_no' => 'required|string|max:50',
            'speciality' => 'required|string|max:255',
            'degree' => 'required|string|max:255',
            'experience' => 'nullable|integer|max:100',
            'past_experiences' => 'nullable|string|max:255',            
            'about_doc' => 'required|string',
            'facebook' => 'nullable|url|max:500',
            'twitter' => 'nullable|url|max:500',
            'linkedin' => 'nullable|url|max:500',
            'youtube' => 'nullable|url|max:500',
           
            'first_name' => 'required|string|max:255',
            'last_name' => 'nullable|string|max:255',
            'mobile' => 'required|string|max:20',            
           
            'password' => 'nullable|string|min:8|max:20|confirmed',
            'password_confirmation' => 'nullable|string|min:8|max:20',        
        ],[ 
            'password.min' => 'Your password must be more than 8 characters',
            'facebook.url' => 'Please use http:// Or https://',
            'twitter.url' => 'Please use http:// Or https://',
            'linkedin.url' => 'Please use http:// Or https://',
            'youtube.url' => 'Please use http:// Or https://',
    
        ],[
            'about_doc' => "About doctor"
        ]);

        $request->validate([
            'availability' => 'required',    
            'availability_from.*' => 'sometimes|nullable|date_format:H:i|required_with:availability.*',
            'availability_to.*' => 'sometimes|nullable|date_format:H:i|required_with:availability_from.*',
            'fees.*' => 'sometimes|nullable|integer|min:1|max:99999999999',
        ]);

        /*crete gallery images*/
        if(!empty($request->input('document'))){

            $newimageCount = count($request->input('document', []));
            $dbimageCount  = GalleryImages::where('nursing_home_id', Auth::user()->nursing_home_id)->count();
            $allowedImages = isset(Auth::user()->nursinghome->package->image_count) ? Auth::user()->nursinghome->package->image_count : '';
            $neededImages = $allowedImages - $dbimageCount;
            if($allowedImages == $dbimageCount){
                $msg = "Sorry, you can't upload more gallery images. You already have been used allowed space according to your plan. You can upload just ". $allowedImages .' images. Please remove extra images and try again';
                $this->validate($request, [               
                    'document' => 'nullable|array|max:'.$neededImages,
                ],['document.max'=>$msg]);
            
            } 
            elseif($allowedImages < $dbimageCount + $newimageCount ){
                
                $msg = "Sorry, According to your plan you can upload just ". $neededImages .' more images. Please remove extra images and try again.';
                $this->validate($request, [               
                    'document' => 'nullable|array|max:'.$neededImages,
                    // 'document' => 'nullable|string|min:8|max:20',
                ],['document.max'=>$msg]);
            } 
            elseif($allowedImages >= $dbimageCount + $newimageCount){             
                foreach ($request->input('document', []) as $file) {
                    $gallery = new GalleryImages();
                    $gallery->nursing_home_id = Auth::user()->nursing_home_id;
                    $gallery->file_url = $file;
                    $gallery->created_by = Auth::user()->id;
                    $gallery->save();
                }
            }            
        }
        /*end gallery images*/


        $data = $request->all();
        if (!empty($data['availability'])) {
            $keys = array_keys($data['availability']);
            $data2 = implode(",", $keys);
        } else {
            $data2 = '1,2,3,4,5,6,7';
        }
        $ava = array();
        if (!empty($data['availability'])) {
            foreach ($data['availability'] as $key => $value) {
                if (!empty($data['availability_from'][$value])) {
                    $ava_from = $data['availability_from'][$value];
                } else {
                    $ava_from = null;
                }

                if (!empty($data['availability_to'][$value])) {
                    $ava_to = $data['availability_to'][$value];
                } else {
                    $ava_to = null;
                }

                if (!empty($data['fees'][$value])) {
                    $fees = $data['fees'][$value];
                } else {
                    $fees = null;
                }

                $ava[$value] = [
                    'availability_from' => $ava_from,
                    'availability_to' => $ava_to,
                    'fees' => $fees,
                ];
            }
        }
        

        if (!empty($request->fees)) {
            $max = max($request->fees);
            $min = min($request->fees);
            if ($min == $max) {
                $data['fees'] = $max;
            } elseif ($min && $max) {
                $data['fees'] = $min . ' - ' . $max;
            } else {
                $data['fees'] = $max;
            }
        } else {
            $data['fees'] = (int) null;
        }
        
        if($request->id && $request->id != ''){
            $setuser = User::find($request->id);
            if(!$setuser){
                return back()->with('message', 'Sorry, User not found');  
            }         
        }

          

        $setuser->first_name = $request->first_name;
        $setuser->last_name = $request->last_name;
        $setuser->email = $request->email;
        $setuser->mobile = $request->mobile;
        $setuser->country = $request->country;
        $setuser->city = $request->city;
        $setuser->address = $request->address;
        $setuser->lat = $request->user_lat;
        $setuser->long = $request->user_long;
        if ($request->password != '' && $request->password != null) {
         $setuser->password =  Hash::make($request->password);           
        }

        if($setuser->save()){  //Save Dr.

            if(Auth::user()->nursinghome){
                //Update Nursing Home
                $nursing_home = NursingHome::where('user_id', Auth::user()->id)->first(); //from Nursing Home table
                
                if(!$nursing_home){
                  return back()->with('message', 'Sorry, nursing home not found.');                   
                }
                $nursing_home->organization_name	 = $request->first_name.' '.$request->last_name;
                $nursing_home->about_nursing_home	 = $request->about_nursing;
                $nursing_home->save();
                //NursingHome updating end
            }

            $doctor = Doctor::where('user_id', $setuser->id)->first();
            if(!$doctor){
                return back()->with('message', 'Sorry, Doctor not found');
            }
            $doctor->updated_by = Auth::user()->id;
           
            $doctor->reg_no = $request->reg_no;
            $doctor->department_id = $request->department;
            $doctor->speciality = $request->speciality;
            $doctor->degree = $request->degree;
            $doctor->experience = $request->experience;
            $doctor->past_experiences = $request->past_experiences;
            $doctor->fees = $data['fees'];//note
            $doctor->availability =  $ava;
            $doctor->working_days = $data2;//note
            $doctor->about_doc = $request->about_doc;            
            $doctor->facebook_url = $request->facebook;            
            $doctor->twitter_url = $request->twitter;            
            $doctor->linkedin_url = $request->linkedin;            
            $doctor->youtube_url = $request->youtube;            
            $doctor->save();
        }

        return redirect()->route('doctor.dashboard')->with('message', 'Details Updated.');
    }

    public function action(Request $request)
    {
       // return  $usrid = Auth::user()->id;
        $validation = Validator::make($request->all(), [
            'profile_image' => 'required|image|mimes:jpeg,png,jpg|max:8210',
        ], [], ['profile_image' => 'Image file']);
        if ($validation->passes()) {
            $image = $request->file('profile_image');
            $imageName = time() . rand() . '.' . $image->getClientOriginalExtension();
            
            $folderPath = 'storage/app/images/doctor/profile/';
            // $folderPath = 'public/profileImages';
            if(!file_exists($folderPath)) {
                mkdir($folderPath, 0777, true);
            }
                
            //$filePath = 'profile_images/' . $imageName;
            //$folderPath = public_path('assets/main/profileImages');
            
            $image->move($folderPath, $imageName);
            //file_put_contents($path, $image);
            $path = $imageName;
            $returnpath = url('/') . '/' . $folderPath . '/' . $imageName;
            //return $path;

        } else {
            return response()->json([
                'message' => $validation->errors()->all(),
                'uploaded_image' => '',
                'class_name' => 'alert-danger',
            ]);
        }
        $user = User::findOrFail(Auth::user()->id);
        $user->profile_pic = $path;
        
        
        if ($user->save()) {
            $doctor = Doctor::where('user_id', Auth::user()->id)->first();
            
            if($doctor){
                $doctor->profile_image = $path;    
                $doctor->save();
            }          

            return response()->json([
                'status' => 200,
                'message' => 'Image Uploaded Successfully',
                'uploaded_image' => '<img src="$returnpath" class="img-thumbnail" width="300" />',
                'class_name' => 'alert-success',
            ]);
        } else { 
            return response()->json([
                'message' => $validation->errors()->all(),
                'uploaded_image' => '',
                'class_name' => 'alert-danger',
            ]);
        }
    }
    /**
     * Perticular Doctors All appointment 
     */
    public function myAppointments(Request $request)
    {
        // dd(Auth::user()->id);
        $appointment = Bookings::where('doctor_id', Auth::user()->doctor->id)->orderBy('id', 'desc')->paginate(mypagination());
        //dd($appointment);
        if($request->ajax()){
            return view('clinics.modules.doctors.appointments.appointments_body', [
                "items" => $appointment,
                // "plans" => false,
            ]);
        }        
        return view('clinics.modules.doctors.appointments.appointments', [
            "items" => $appointment,
            // "plans" => false,
        ]);
        
    }
    /**
     * Perticular Doctors appointment details
     */
    public function appointmentDetails($id)
    {
        // dd(Auth::user()->id);
        $appointment = Bookings::find($id);
        if(!$appointment){
            return back()->with('failed', 'Appointment not found');
        }
                
        return view('clinics.modules.doctors.appointments.view', [
            "item" => $appointment->user,
            "appointment" => $appointment,
            // "plans" => false,
        ]);
       
    }
    /**
     * Perticular Doctors All patients 
     */
    public function myPatients(Request $request)
    {
        // dd(Auth::user()->id);
        $appointment = Bookings::where('doctor_id', Auth::user()->doctor->id)->get(['patient_id']);
                
        $items = User::whereIn('id', $appointment)->orderBy('id', 'desc')->paginate(mypagination());
        
        if($request->ajax()){
            return view('clinics.modules.doctors.patients.tbody', [
                "items" => $items,
               
            ]);
        }
        
        return view('clinics.modules.doctors.patients.list', [
            "items" => $items,
           
        ]);
        
    }
    /**
     * Patient search by Doctor
     *  */ 
    public function search(Request $request)
    {   
        $appointment = Bookings::where('doctor_id', Auth::user()->doctor->id)->get(['patient_id']);  
        $items = User::query();
        $items->where('role_id', 2)->whereIn('id', $appointment);

        if(isset($request->name)){
            $items->where('first_name','LIKE','%'.$request->name.'%');
        }
        if(isset($request->email)){
            $items->where('email','LIKE','%'.$request->email.'%');
        }
        if(isset($request->mobile)){
            $items->where('mobile','LIKE','%'.$request->mobile.'%');
        }
        $newItem =  $items->orderBy('id', 'desc')->paginate(mypagination());         

        if (request()->ajax()) {
            return view('clinics.modules.doctors.patients.tbody',[
                'items' => $newItem,
            ]); 
        }
        return view('clinics.modules.doctors.patients.list', [
            'items' => $newItem,
            'name' => $request->name,
            'email' => $request->email,
            'mobile' => $request->mobile,
        ]);
    }
    public function featuredPlan(Request $request, $plan_id )
    {
        $plans = SubscriptionPackages::where(['status'=> 1, 'id'=>$plan_id])->first();
        // $plans = SubscriptionPackages::with('features')->get();
        // $plans = PlanFeature::with('features')->get();
        
        // $item = Products::where('status', 1)->get();
        // $relation = PlanFeature::whereHas('features', function ($plans) {
        //     $plans->where('status', 1);
        //     $plans->orderBy('name', 'desc');
        // })->get();

        // dd($plans[0]->features->myfeatures);
        return view('site_view.modules.featured_plan', [
            "item" => false,
            "plans" => $plans,
        ]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
