<?php

namespace App\Http\Controllers\Clinics;

use App\Http\Controllers\Controller;
use App\Models\Doctor;
use App\Models\NursingHome;
use App\Models\DoctorNursingHomeRelation;
use App\Role;
use App\User;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class NursingDoctorController extends Controller
{
    public function index()
    {
        $items = User::where(['role_id' => 3, 'nursing_home_id' => Auth::user()->nursing_home_id])->paginate(mypagination());
        return view('clinics.modules.nursingHome.doctors.list',[
            'items' => $items,
        ]);
        // $users = DB::table('doctor')
        //       ->join('users', 'doctor.user_id', '=', 'users.id')
        //       ->join('doctor_nursing_home', 'doctor.id', '=', 'doctor_nursing_home.doctor_id')
        //       ->select('doctor.*', 'users.*', 'doctor_nursing_home.*')
        //       ->get();

        $user = Auth::guard('api')->user();
        if ($user->hasRole('SuperAdmin')) {
            return response()->json([
                'status' => 1,
                'message' => "Successfull",
                'data' => DB::table('doctor')
                    ->join('users', 'doctor.user_id', '=', 'users.id')
                    ->join('doctor_nursing_home', 'doctor.id', '=', 'doctor_nursing_home.doctor_id')
                    ->select('doctor.*', 'users.name', 'users.email', 'users.mobile', 'doctor_nursing_home.*')
                    ->get(),
            ], 200);
        } else {
            $nursing_home_id = getUserNhId($user->id);
            $role_id = getDocRoleId();
            $users = UserNursingHomeRelation::where('role_id', $role_id)->where('nursing_home_id', $nursing_home_id)->pluck('user_id')->toArray();

            $doctors = Doctor::whereIn('user_id', $users)->distinct('id')->get();
            foreach ($doctors as $key => $doctor) {
                $user_id = $doctor->user_id;
                $user = User::select('name', 'mobile', 'email')->where('id', $user_id)->first();
                //dd($user);
                $doc_nurs_data = DoctorNursingHomeRelation::where('doctor_id', $doctor->id)->where('nursing_home_id', $nursing_home_id)->first();
                $doctor = array_merge(array('doctor_id' => $doctor->id), $doctor->toArray(), $doc_nurs_data ? $doc_nurs_data->toArray() : array('inhouse_flag' => 0, 'working_days' => '', 'fees' => '', 'availability_from' => '', 'availability_to' => ''), $user->toArray());

                $doctors[$key] = $doctor;
            }
            return response()->json([
                'status' => 1,
                'message' => "Successfull",
                'data' => $doctors,
            ], 200);
        }

    }

    public function show($id)
    {
        $user = Auth::user();
        $nursing_home_id = $user->nursing_home_id;        
        
        $doctor = User::where(['id' => $id, 'nursing_home_id'=>$nursing_home_id])->first();
        
        return view('clinics.modules.nursingHome.doctors.view',[
            'item' => $doctor,
        ]);
    }
    /**
     * change Dr. status By Nursing admin
     * 
     */
    public function status($id, $status)
    {
        
        $item = User::find($id);

        if (empty($item)) {
            return redirect()->route('nursing-doctor.index')->with('message', 'Doctor not found!');
        }
        if (Auth::user()->role_id != 4) {
            return redirect()->route('nursing-doctor.index')->with('message', "Sorry, you don't have permission to access this user.");
        }
        
        if ($status == 1) {
            $item->status = '0';
        } else {
            $item->status = '1';
        }
        $item->save();
        $message = $item->status == 1 ? 'Doctor Activated.' : 'Doctor Deactivated.';

        return redirect()->back()->with('message', $message);
    }
    /**
     * Edit Dr. By Nursing admin
     * 
     */
    public function edit($id)
    {
        $items = User::where(['role_id' => 3, 'nursing_home_id' => Auth::user()->nursing_home_id])
        ->where('id', $id)->first();

        return view('clinics.modules.nursingHome.doctors.add_doctor',[
            'item' => $items,
        ]);
    }
    /**
     * Crete Dr. By Nursing admin
     * 
     */
    public function create()
    {
        return view('clinics.modules.nursingHome.doctors.add_doctor',[
            'item' => false,
        ]);
    }

    //Dr. Profile Store
    public function store(Request $request)
    {
        $email_regex = '/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix';
        $this->validate($request, [
            'reg_no' => 'required|string|max:50',
            'speciality' => 'required|string|max:255',
            'degree' => 'required|string|max:255',
            'experience' => 'nullable|integer|max:100',
            'past_experiences' => 'nullable|string|max:255',
            /* 'stamp_image' => 'nullable|mimes:jpeg,jpg,png',
            'bio' => 'nullable|string|max:50',
            'esign' => 'nullable|image|mimes:jpeg,jpg,png', */
            'profile_image' => 'nullable|image|mimes:jpeg,jpg,png',
            'about_doc' => 'required|string',
            'availability_from.morning.*' => 'sometimes|nullable|date_format:H:i|required_with:availability.*',
            'availability_to.morning.*' => 'sometimes|nullable|date_format:H:i|required_with:availability_from.*|after:availability_from.*',
            'fees.*' => 'sometimes|nullable|integer|min:1|max:99999999999',
            'email' => 'required|string|email|regex:'.$email_regex.'|max:255|unique:users,email,' . $request->id . ',id',
            'first_name' => 'required|string|max:255',
            'last_name' => 'nullable|string|max:255',
            'mobile' => 'required|string|max:20',
            // 'password' => 'required|string|min:8|confirmed',
            'availability' => 'required',
            //'profile_image' => 'nullable|string|max:255',

            'zipcode' => 'nullable|string|max:20',
            'address' => 'required|string|max:1000',   //text field in db        
            'user_lat' => 'nullable|string|max:30',
            'user_long' => 'nullable|string|max:30',
            'country' => 'required|numeric',
            'city' => 'nullable|string|max:255',
        ],[],[
            'about_doc' => "About doctor"
        ]);
        // ['availability_from.*.required_with' => 'This availability form field is required when respected day is checked',
        // 'availability_to.*.required_with'    => 'This availability to field is required if available from field for respected day is present'],
        // [
        //     'availability_from.*' => 'availability from',      
        // ]);
        if(!$request->id){
            $this->validate($request, [
               'password' => 'required|string|min:8|max:20|confirmed',
                'password_confirmation' => 'required|string|min:8|max:20',
            ],[
                'password.min' => 'Your password must be more than 8 characters',
                'nursing_home.required' => 'Please select doctor nursing home',
            ]);
        }

        $data = $request->all();
        if (!empty($data['availability'])) {
            $keys = array_keys($data['availability']);
            $data2 = implode(",", $keys);
        } else {
            $data2 = '1,2,3,4,5,6,7';
        }
        $ava = array();
        if (!empty($data['availability'])) {
            foreach ($data['availability'] as $key => $value) {
                if (!empty($data['availability_from'][$value])) {
                    $ava_from = $data['availability_from'][$value];
                } else {
                    $ava_from = null;
                }

                if (!empty($data['availability_to'][$value])) {
                    $ava_to = $data['availability_to'][$value];
                } else {
                    $ava_to = null;
                }

                if (!empty($data['fees'][$value])) {
                    $fees = $data['fees'][$value];
                } else {
                    $fees = null;
                }

                $ava[$value] = [
                    'availability_from' => $ava_from,
                    'availability_to' => $ava_to,
                    'fees' => $fees,
                ];
            }
        }

        

        // if (!$request->has('password')) {
        //     $data['password'] = 'password';
        // }

        if (!empty($request->fees)) {
            $max = max($request->fees);
            $min = min($request->fees);
            if ($min == $max) {
                $data['fees'] = $max;
            } elseif ($min && $max) {
                $data['fees'] = $min . ' - ' . $max;
            } else {
                $data['fees'] = $max;
            }
        } else {
            $data['fees'] = (int) null;
        }
        if($request->id && $request->id != ''){
            $setuser = User::find($request->id);
            $msg = 'Doctor updated successfully.';
            if(!$setuser){
                $setuser = new User();
                $setuser->role_id = 3;
                $setuser->status = 1;
                $setuser->password = Hash::make($request->password); 
                $msg = 'Doctor registerd successfully.';
            }
        }else{
            $setuser = new User();
            $setuser->role_id = 3;
            $setuser->status = 1;
            $setuser->password = Hash::make($request->password); 
            $msg = 'Doctor registerd successfully.';
        }
        
        if ($request->hasFile('profile_image')){
            $image = $request->file('profile_image');

            $imageName = time() . rand() . '.' . $image->getClientOriginalExtension();
            
            $folderPath = 'storage/app/images/doctor/profile/';
           // $folderPath = 'public/carDocs';

            if(!file_exists($folderPath)) {
                mkdir($folderPath, 0777, true);
            }

            $image->move($folderPath, $imageName);
            $path = $imageName; 
            $setuser->profile_pic = $path;
        }
        
        $setuser->first_name = $request->first_name;
        $setuser->last_name = $request->last_name;
        $setuser->email = $request->email;
        $setuser->mobile = $request->mobile;
        $setuser->address = $request->address;
        $setuser->zipcode = $request->zipcode;
        $setuser->country = $request->country;
        $setuser->city = $request->city;
        $setuser->lat = $request->lat;
        $setuser->long = $request->long;
        $setuser->nursing_home_id = Auth::user()->nursing_home_id;

        if($setuser->save()){  //Save Dr.
            $doctor = Doctor::where('user_id', $setuser->id)->first();
            if(!$doctor){
                $doctor = new Doctor();
                $doctor->created_by = Auth::user()->id;
            }else{
                $doctor->updated_by = Auth::user()->id;
            }
            $doctor->user_id = $setuser->id;
            $doctor->reg_no = $request->reg_no;
            $doctor->department_id = $request->department;
            $doctor->speciality = $request->speciality;
            $doctor->degree = $request->degree;
            $doctor->experience = $request->experience;
            $doctor->past_experiences = $request->past_experiences;
            $doctor->fees = $data['fees'];//note
            $doctor->availability =  $ava;
            $doctor->working_days = $data2;//note
            $doctor->about_doc = $request->about_doc;           
            $doctor->save();

        }
       
        $doctorNursing = DoctorNursingHomeRelation::where('doctor_id', $doctor->id)->where('nursing_home_id', Auth::user()->nursing_home_id)->first();
        
        if(!$doctorNursing){
            $doctorNursing = new DoctorNursingHomeRelation();
        }
        $doctorNursing->doctor_id = $doctor->id ;
        $doctorNursing->nursing_home_id = Auth::user()->nursing_home_id ;
        $doctorNursing->save();         
      
        return redirect()->route('nursing-doctor.index')->with('message',$msg);

        // return response()->json([
        //     'status' => 1,
        //     'message' => "Successfull",
        //     'data' => $doctor,
        // ], 201);
    }

    public function update(Request $request, $id)
    {
        $doctor = Doctor::findOrFail($id);
        $request->validate([
            'degree' => 'nullable|string',
            'experience' => 'nullable|integer',
            'stamp_image' => 'nullable|mimes:jpeg,jpg,png',
            'bio' => 'nullable|string|max:50',
            // 'fees' => 'required|integer|min:1|max:99999999999',
            'esign' => 'nullable|string',
            'profile_image' => 'nullable | mimes:jpeg,jpg,png | max:1000',
            'about_doc' => 'nullable|string|max:1000',
            'mobile'=>'nullable|numeric|digits:10',
            // 'availability_from' => 'nullable|date_format:H:i|required_with:availability_to',
            // 'availability_to' => 'nullable|date_format:H:i|required_with:availability_from',
            'availability_from.*' => 'sometimes|nullable|date_format:H:i|required_with:availability_to.*',
            'availability_to.*' => 'sometimes|nullable|date_format:H:i|required_with:availability_from.*',
            'fees.*' => 'sometimes|nullable|integer|min:1|max:99999999999',
            'inhouse_flag' => 'nullable|integer|min:0|max:1',
            'working_days' => 'nullable|string|max:13',
            'nursing_home_id' => 'required|integer|min:1|max:99999999999',
            'email' => 'required|string|email|max:255|unique:users,email,' . $doctor->user_id,

        ]);

        $data = $request->all();
        // $data['availability_from'] = \Request::input('availability_from', NULL);
        // $data['availability_to'] = \Request::input('availability_to', NULL);

        // if ($request->has('degree')) {
        //     $data['degree'] = json_encode($data['degree']);
        // }
        $data['updated_by'] = Auth::guard('api')->id();
        $doctor->update($data);

        $doctor_user = User::where('id', $doctor->user_id)->first();
        
        $doctor_user->name = $data['name'];
        $doctor_user->email = $data['email'];
        $doctor_user->mobile = $data['mobile'];
        $doctor_user->save();

        $ava = array();
        if (!empty($data['availability'])) {
            foreach ($data['availability'] as $key => $value) {
                if (!empty($data['availability_from'][$value])) {
                    $ava_from = $data['availability_from'][$value];
                } else {
                    $ava_from = null;
                }

                if (!empty($data['availability_to'][$value])) {
                    $ava_to = $data['availability_to'][$value];
                } else {
                    $ava_to = null;
                }

                if (!empty($data['fees'][$value])) {
                    $fees = $data['fees'][$value];
                } else {
                    $fees = null;
                }
                $ava[$value] = [
                    'availability_from' => $ava_from,
                    'availability_to' => $ava_to,
                    'fees' => $fees,
                ];
            }
        }
        $data['fees'] = array_filter($data['fees']);
        if (!empty($data['fees'])) {
            $max = max($data['fees']);
            $min = min($data['fees']);
            if ($min == $max) {
                $data['fees'] = $max;
            } elseif ($min && $max) {
                $data['fees'] = $min . ' - ' . $max;
            } else {
                $data['fees'] = $max;
            }
        } else {
            $data['fees'] = (int) null;
        }

        $data['availability'] = $ava;

        $nursing_home_id = $request->nursing_home_id;
        DoctorNursingHomeRelation::where('doctor_id', $id)->update(['inhouse_flag' => 0]);
        $doctor_nursing = DoctorNursingHomeRelation::where('doctor_id', $id)->where('nursing_home_id', $nursing_home_id)->first();
        $data5 = array();
        $data5['fees'] = $data['fees'];
        $data5['availability'] = $data['availability'];
        $data5['working_days'] = $data['working_days'];
        $doctor_nursing->update($data5);

        // $doctor['degree'] = json_decode($doctor['degree']);
        $doctor['fees'] = $data['fees'];
        // $doctor['availability_from'] = $data['availability_from'];
        // $doctor['availability_to'] =  $data['availability_to'];
        $doctor['availability'] = $ava;

        $doctor['working_days'] = $doctor_nursing['working_days'];

        $doctor['name'] = User::where('id', $doctor->user_id)->value('name');
        return response()->json([
            'status' => 1,
            'message' => "Successfull",
            'data' => $doctor,
        ], 200);
    }

    public function delete(Doctor $doctor)
    {
        //write code to delete from Doctor nursinghome
        $user = Auth::guard('api')->user();
        $nursing_home_id = getUserNhId($user->id);
        $doctor->delete();
        $role_id = getDocRoleId();
        UserNursingHomeRelation::where('role_id', $role_id)->where('nursing_home_id', $nursing_home_id)->where('user_id', $doctor->user_id)->delete();
        return response()->json(null, 204);
    }

    public function nhdoctors()
    {

        $user = Auth::guard('api')->user();
        $nursing_home_id = getUserNhId($user->id);
        $role_id = getDocRoleId();
        $users = UserNursingHomeRelation::where('role_id', $role_id)->where('nursing_home_id', $nursing_home_id)->pluck('user_id')->toArray();
        return Doctor::whereIn('user_id', $users)->distinct('id')->get();
    }

    public function docNursings()
    {
        $user = Auth::guard('api')->user();
        $user_id = $user->id;
        //dd($user_id);
        $role_id = getDocRoleId();
        $nursings = UserNursingHomeRelation::where('user_id', $user_id)->where('role_id', $role_id)->pluck('nursing_home_id');
        $nursings = NursingHome::whereIn('id', $nursings)->get();
        return response()->json([
            'status' => 1,
            'message' => "Successfull",
            'data' => $nursings,
        ], 200);
    }

    public function profile(Request $request)
    {
        $request->validate([
            'nursing_home_id' => 'required|integer|min:1|max:99999999999',
        ]);
        $user = Auth::guard('api')->user();
        $nursing_home_id = $request->nursing_home_id;
        $doctor = Doctor::where('user_id', $user->id)->first();
        $doc_nurs_data = DoctorNursingHomeRelation::where('doctor_id', $doctor->id)->where('nursing_home_id', $nursing_home_id)->first();
        $doctor['fees'] = optional($doc_nurs_data)->fees;
        $doctor['availability'] = optional($doc_nurs_data)->availability;
        //  $doctor['degree'] = json_decode($doctor['degree']);
        $doctor['working_days'] = $doc_nurs_data['working_days'];
        $doctor['name'] = User::where('id', $doctor->user_id)->value('name');
        return response()->json([
            'status' => 1,
            'message' => "Successfull",
            'data' => $doctor,
        ], 200);
    }
    public function nhdoctorsById(Request $request)
    {
        $request->validate([
            'nursing_home_id' => 'required|integer|min:1|max:99999999999',
        ]);

        $nursing_home_id = $request->nursing_home_id;
        $role_id = getDocRoleId();
        $users = UserNursingHomeRelation::where('role_id', $role_id)->where('nursing_home_id', $nursing_home_id)->pluck('user_id')->toArray();
        $doctors = Doctor::whereIn('user_id', $users)->distinct('id')->get();
        //dd($doctors);
        foreach ($doctors as $key => $doctor) {
            $user_id = $doctor->user_id;
            $user = User::select('name', 'mobile', 'email')->where('id', $user_id)->first();
            //dd($user);
            $doc_nurs_data = DoctorNursingHomeRelation::where('doctor_id', $doctor->id)->where('nursing_home_id', $nursing_home_id)->first();
            $doctor = array_merge(array('doctor_id' => $doctor->id), $doctor->toArray(), $doc_nurs_data ? $doc_nurs_data->toArray() : array('inhouse_flag' => 0, 'working_days' => '', 'fees' => '', 'availability_from' => '', 'availability_to' => ''), $user->toArray());

            $doctors[$key] = $doctor;
        }

        return response()->json([
            'status' => 1,
            'message' => "Successfull",
            'data' => $doctors,
        ], 200);

    }

    public function imageurl()
    {
        $user = Auth::guard('api')->user();
        $image_url = Doctor::where('user_id', $user->id)->value('profile_image');
        return response()->json([
            'status' => 1,
            'message' => "Successfull",
            'data' => $image_url,
        ], 200);
    }

    public function showPatDoc($id, $nhid)
    {
        $doctor = Doctor::findOrFail($id);
        $doc_nurs_data = DoctorNursingHomeRelation::where('doctor_id', $id)->where('nursing_home_id', $nhid)->first();
        $doctor['fees'] = optional($doc_nurs_data)->fees;
        $doctor['availability_from'] = optional($doc_nurs_data)->availability_from;
        $doctor['availability_to'] = optional($doc_nurs_data)->availability_to;
        $doctor['degree'] = $doctor['degree'];
        $doctor['working_days'] = $doc_nurs_data['working_days'];
        $doctor['name'] = User::where('id', $doctor->user_id)->value('name');
        return response()->json([
            'status' => 1,
            'message' => "Successfull",
            'data' => $doctor,
        ], 200);
    }
}
