<?php

namespace App\Http\Controllers\Clinics;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Masters\SubscriptionPackages;
use App\Models\Masters\Feature;
use App\Models\Masters\PlanFeature;
use Hash;
use App\Models\Bookings;
use App\Models\NursingHome;
use App\Models\GalleryImages;
use App\user;
use Auth;

class ClinicDashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $doctor = User::where(['role_id' => 3, 'nursing_home_id' => Auth::user()->nursinghome->id])->count();
        $appointment = Bookings::all();
       
        return view('clinics.modules.nursingHome.nursing-dashboard',[
            "appointments" => $appointment->where(['nursing_home_id' => Auth::user()->nursinghome->id, 'status' => 1])->count(),
            "patients"     => $appointment->where(['nursing_home_id' => Auth::user()->nursinghome->id, 'status' => 1])->groupBy('patient_id')->count(),
            "doctors"      => $doctor,
            "messages"     => '0',            
            "items"     => $appointment->where(['nursing_home_id' => Auth::user()->nursinghome->id, 'status' => 1]),
        ]);
        
        $plans = SubscriptionPackages::where('status', 1)->get();
        // $plans = SubscriptionPackages::with('features')->get();
        // $plans = PlanFeature::with('features')->get();
        
        // $item = Products::where('status', 1)->get();
        // $relation = PlanFeature::whereHas('features', function ($plans) {
        //     $plans->where('status', 1);
        //     $plans->orderBy('name', 'desc');
        // })->get();

        // dd($plans[0]->features->myfeatures);
        return view('site_view.modules.home', [
            "item" => false,
            "plans" => $plans,
        ]);
        
    }
    
    public function profileView()
    {
        $user  = User::where('id', Auth::user()->id)->first();

        return view('clinics.modules.nursingHome.profile', [
            'user' => $user,
        ]);
    }

    public function profileUpdate(Request $request)
    {
        $email_regex = '/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix';
               
        $this->validate($request, [
          
            'email' => 'required|string|email|regex:'.$email_regex.'|max:255|unique:users,email,' . $request->id . ',id',
            'first_name' => 'required|regex:/^[a-zA-Z ]+$/u|max:255',
            'last_name' => 'nullable|regex:/^[a-zA-Z ]+$/u|max:255',
                    
            'mobile' => 'required|string|max:25',             
            'zipcode' => 'nullable|string|max:20',
            'address' => 'required|string|max:1000',   //text field in db        
            'user_lat' => 'nullable|string|max:30',
            'user_long' => 'nullable|string|max:30',
            'country' => 'required|numeric',
            'country_code' => 'nullable|numeric',
            'city' => 'nullable|string|max:255',
            'about_nursing' => 'required|string',//text in db
            'password' => 'nullable|string|min:8|max:20|confirmed',
            'password_confirmation' => 'nullable|required_with:password|string|min:8|max:20',
        ],[
            'password.min' => 'Your password must be more than 8 characters',               
        ],['about_nursing' => 'About Nursing Home']);
        if(!$request->id){
            $this->validate($request, [               
                'password' => 'required|string|min:8|max:20|confirmed',
                'password_confirmation' => 'required|string|min:8|max:20',
            ]);

            $user = new User();

        }else {
            $user = User::find($request->id);
            $msg = "Profile details updated" ;
        }
        /*crete gallery images*/
        if(!empty($request->input('document'))){

            $newimageCount = count($request->input('document', []));
            $dbimageCount  = GalleryImages::where('nursing_home_id', Auth::user()->nursing_home_id)->count();
            $allowedImages = isset(Auth::user()->nursinghome->package->image_count) ? Auth::user()->nursinghome->package->image_count : '';
            $neededImages = $allowedImages - $dbimageCount;
            if($allowedImages == $dbimageCount){
                $msg = "Sorry, you can't upload more gallery images. You already have been used allowed space according to your plan. You can upload just ". $allowedImages .' images. Please remove extra images and try again';
                $this->validate($request, [               
                    'document' => 'nullable|array|max:'.$neededImages,
                ],['document.max'=>$msg]);
            
            } 
            elseif($allowedImages < $dbimageCount + $newimageCount ){
                
                $msg = "Sorry, According to your plan you can upload just ". $neededImages .' more images. Please remove extra images and try again.';
                $this->validate($request, [               
                    'document' => 'nullable|array|max:'.$neededImages,
                    // 'document' => 'nullable|string|min:8|max:20',
                ],['document.max'=>$msg]);
            } 
            elseif($allowedImages >= $dbimageCount + $newimageCount){             
                foreach ($request->input('document', []) as $file) {
                    $gallery = new GalleryImages();
                    $gallery->nursing_home_id = Auth::user()->nursing_home_id;
                    $gallery->file_url = $file;
                    $gallery->created_by = Auth::user()->id;
                    $gallery->save();
                }
            }            
        }        
        /* end gallery images*/
        
         //Update Nursing Home
         $nursing_home = NursingHome::where('user_id', Auth::user()->id)->first(); //from Nursing Home table
        
         if(!$nursing_home){
            \Session::flash('message', 'Sorry, nursing home not found.');
            return response()->json([
                'success' => false,
                'message' =>'nursing home not found',
                'redirect' => url()->previous(),
            ]);
         }
         $nursing_home->organization_name	 = $request->first_name.' '.$request->last_name;
         $nursing_home->about_nursing_home	 = $request->about_nursing;
         $nursing_home->save();
         //NursingHome updating end

        $user->first_name = $request->first_name;
        $user->last_name  = $request->last_name;
        $user->email          = $request->email;
        $user->mobile         = $request->mobile;
        
        $user->address        = $request->address;
        $user->lat            = $request->user_lat;
        $user->long           = $request->user_long;
        $user->zipcode        = $request->zipcode;
        $user->country_code   = $request->country_code;
        $user->country        = $request->country;
        $user->city           = $request->city;
        
        if($request->password != '' && $request->password != null){            
            $user->password = Hash::make($request->password);
        }  
        // return response()->json([
        //     'success' => false,
        //     'message' =>'hello',
        //     'redirect' => 'javascript:void(0)',
        // ]);      
        $user->save();

        \Session::flash('message', $msg);
        return response()->json([
            'success' => true,
            'message' => 'Success',
            'redirect' => route('nursing.dashboard'),
        ]);
        // return redirect()->route('nursing.dashboard')->with('message', 'Details Updated.');

    }


    /**
     * All dr.'s appointments of hospital 
     */
    public function nursingAppointments(Request $request)
    {
        // dd(Auth::user()->id);
        $appointment = Bookings::where('nursing_home_id', Auth::user()->nursing_home_id)->orderBy('id', 'desc')->paginate(mypagination());
        //dd($appointment);
        if($request->ajax()){
            return view('clinics.modules.nursingHome.appointments.appointments', [
                "items" => $appointment,
                // "plans" => false,
            ]);
        }
        
        return view('clinics.modules.nursingHome.appointments.appointments', [
            "items" => $appointment,
            // "plans" => false,
        ]);       
        
    }
    /**
     *All patient Of my Hospital 
     */
    public function myPatients(Request $request)
    {
        // dd(Auth::user()->id);
        $appointment = Bookings::where('nursing_home_id', Auth::user()->nursing_home_id)->get(['patient_id']);
                
        $items = User::whereIn('id', $appointment)->orderBy('id', 'desc')->paginate(mypagination());
        
        if($request->ajax()){
            return view('clinics.modules.nursingHome.patients.tbody', [
                "items" => $items,
               
            ]);
        }
        
        return view('clinics.modules.nursingHome.patients.list', [
            "items" => $items,
           
        ]);
        
    }
    /**
     * Patient search by Doctor
     *  */ 
    public function search(Request $request)
    {   
        $appointment = Bookings::where('nursing_home_id', Auth::user()->nursing_home_id)->get(['patient_id']);  
        $items = User::query();
        $items->where('role_id', 2)->whereIn('id', $appointment);

        if(isset($request->name)){
            $items->where('first_name','LIKE','%'.$request->name.'%');
        }
        if(isset($request->email)){
            $items->where('email','LIKE','%'.$request->email.'%');
        }
        if(isset($request->mobile)){
            $items->where('mobile','LIKE','%'.$request->mobile.'%');
        }
        $newItem =  $items->orderBy('id', 'desc')->paginate(mypagination());         

        if (request()->ajax()) {
            return view('clinics.modules.nursingHome.patients.tbody',[
                'items' => $newItem,
            ]); 
        }
        return view('clinics.modules.nursingHome.patients.list', [
            'items' => $newItem,
            'name' => $request->name,
            'email' => $request->email,
            'mobile' => $request->mobile,
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
