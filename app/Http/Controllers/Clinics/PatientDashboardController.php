<?php

namespace App\Http\Controllers\Clinics;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Masters\SubscriptionPackages;
use App\Models\Masters\Feature;
use App\Models\Masters\PlanFeature;
use App\Models\Doctor;
use App\Models\Bookings;
use Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Mail\AppointmentBooked;
use App\Mail\NewAppointmentForDoctor;


class PatientDashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('clinics.modules.patient.patient-dashboard');
        
        $plans = SubscriptionPackages::where('status', 1)->get();
        // $plans = SubscriptionPackages::with('features')->get();
        // $plans = PlanFeature::with('features')->get();
        
        // $item = Products::where('status', 1)->get();
        // $relation = PlanFeature::whereHas('features', function ($plans) {
        //     $plans->where('status', 1);
        //     $plans->orderBy('name', 'desc');
        // })->get();

        // dd($plans[0]->features->myfeatures);
        return view('site_view.modules.home', [
            "item" => false,
            "plans" => $plans,
        ]);
        
    }
    public function profileView()
    {
        $user  = User::where('id', Auth::user()->id)->first();

        return view('clinics.modules.patient.profile', [
            'user' => $user,
        ]);
    }

    public function profileUpdate(Request $request)
    {
        $email_regex = '/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix';
               
        $this->validate($request, [
          
            'email' => 'required|string|email|regex:'.$email_regex.'|max:255|unique:users,email,' . $request->id . ',id',
            'first_name' => 'required|regex:/^[a-zA-Z ]+$/u|max:255',
            'last_name' => 'nullable|regex:/^[a-zA-Z ]+$/u|max:255',
            'blood_group' => 'nullable|string|max:20',
            'gender' => 'required|string|max:20',
            'age' => 'required|integer|digits-between:1,3',            
            'mobile' => 'required|string|max:25',             
            'zipcode' => 'nullable|string|max:20',
            'address' => 'required|string|max:1000',   //text field in db        
            'user_lat' => 'nullable|string|max:30',
            'user_long' => 'nullable|string|max:30',
            'country' => 'required|numeric',
            'country_code' => 'nullable|numeric',
            'city' => 'nullable|string|max:255',
            'password' => 'nullable|string|min:8|max:20|confirmed',
            'password_confirmation' => 'nullable|required_with:password|string|min:8|max:20',
        ],[
            'password.min' => 'Your password must be more than 8 characters',               
        ]);
        if(!$request->id){
            $this->validate($request, [               
                'password' => 'required|string|min:8|max:20|confirmed',
                'password_confirmation' => 'required|string|min:8|max:20',
            ]);

            $user = new User();

        }else {
            $user = User::find($request->id);
            $msg = "Profile details updated" ;
        }
        
        $user->first_name = $request->first_name;
        $user->last_name  = $request->last_name;
        $user->email          = $request->email;
        $user->mobile         = $request->mobile;
        $user->gender         = $request->gender;
        $user->age            = $request->age;
        $user->blood_group    = $request->blood_group;
        $user->address        = $request->address;
        $user->lat            = $request->user_lat;
        $user->long           = $request->user_long;
        $user->zipcode        = $request->zipcode;
        $user->country_code   = $request->country_code;
        $user->country        = $request->country;
        $user->city           = $request->city;
        // $user->profile_pic    = $path;
        $user->email_verified_at = date('Y-m-d H:i:s');
        $user->role_id = 2;       
        
        if($request->password != '' && $request->password != null){            
            $user->password = Hash::make($request->password);
        }  
        // return response()->json([
        //     'success' => false,
        //     'message' =>'hello',
        //     'redirect' => 'javascript:void(0)',
        // ]);      
        $user->save();
        \Session::flash('message', $msg);
        return response()->json([
            'success' => true,
            'message' => 'Success',
            'redirect' => route('patient.dashboard'),
        ]);
        // return redirect()->route('patient.dashboard')->with('message', 'Details Updated.');

    }

    public function action(Request $request)
    {
       // return  $usrid = Auth::user()->id;
        $validation = Validator::make($request->all(), [
            'profile_image' => 'required|image|mimes:jpeg,png,jpg|max:8210',
        ], [], ['profile_image' => 'Image file']);
        if ($validation->passes()) {
            $image = $request->file('profile_image');
            $imageName = time() . rand() . '.' . $image->getClientOriginalExtension();
            
            $folderPath = 'storage/app/images/doctor/profile/';
            // $folderPath = 'public/profileImages';
            if(!file_exists($folderPath)) {
                mkdir($folderPath, 0777, true);
            }
                
            //$filePath = 'profile_images/' . $imageName;
            //$folderPath = public_path('assets/main/profileImages');
            
            $image->move($folderPath, $imageName);
            //file_put_contents($path, $image);
            $path = $imageName;
            $returnpath = url('/') . '/' . $folderPath . '/' . $imageName;
            //return $path;

        } else {
            return response()->json([
                'message' => $validation->errors()->all(),
                'uploaded_image' => '',
                'class_name' => 'alert-danger',
            ]);
        }
        $user = User::findOrFail(Auth::user()->id);
        $user->profile_pic = $path;
        
        
        if ($user->save()) {            
            return response()->json([
                'status' => 200,
                'message' => 'Image Uploaded Successfully',
                'uploaded_image' => '<img src="$returnpath" class="img-thumbnail" width="300" />',
                'class_name' => 'alert-success',
            ]);
        } else { 
            return response()->json([
                'message' => $validation->errors()->all(),
                'uploaded_image' => '',
                'class_name' => 'alert-danger',
            ]);
        }
    }
    /**
     * Perticular Doctors All appointment 
     */
    public function myAppointments(Request $request)
    {
        $appointment = Bookings::where('patient_id', Auth::user()->id)->orderBy('id', 'desc')->paginate(mypagination());
        //dd($appointment);
        if($request->ajax()){
            return view('clinics.modules.patient.appointments.appointments_body', [
                "items" => $appointment,
                // "plans" => false,
            ]);
        }
        
        return view('clinics.modules.patient.appointments.appointments', [
            "items" => $appointment,
            // "plans" => false,
        ]);
       
        
    }

    /**
     * Show the store data for creating a new resource.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bookAppointment(Request $request)
    {
        
        if(!Auth::check()){
            \Session::put('doctor_booking', route('nursing.list.details', $request->doctor_as_user));
            return redirect()->route('login');
        }

        $request->validate([
            'day' => 'required|integer|max:7',
            'speciality' => 'required|integer',
            'patient_disease' => 'required|string|max:255',           
        ],[],[
            'day.required' => "Appointment day is required"
        ]);

        $doctordata = Doctor::find($request->doctor);
        if(!$doctordata){
            return redirect()->back()->withInput()->with('failed', 'Sorry, selected doctor not found.');
        }
        
        $appointment = new Bookings();
        $appointment->patient_id = Auth::user()->id;
        $appointment->doctor_id = $request->doctor;
        $appointment->nursing_home_id = $request->nursing_home;
        $appointment->desease         = $request->patient_disease;
        $appointment->other_problems  = $request->other_problems ? $request->other_problems : '';
        $appointment->appointment_day = $request->day;
        $appointment->appointment_time = ($doctordata->availability[$request->day]['availability_from'].' - '.$doctordata->availability[$request->day]['availability_to']); ;
        $appointment->status           = 1; //active
        $appointment->save();

        $user = Auth::user();
        $doctor = $doctordata;

        try {
            \Mail::to($user)->send(new AppointmentBooked($doctor, $user));
           
        } catch (\Exception $e) {
            $email_error = 'Failed to authenticate on SMTP server with username \"apikey\" using 2 possible authenticators. Authenticator LOGIN returned Expected response code 250 but got an empty response. Authenticator PLAIN returned Expected response code 250 but got an empty response.'; // Get error here
          
        }
        try {
            \Mail::to($doctor->user->email)->send(new NewAppointmentForDoctor($doctor, $user));
        } catch (\Exception $e) {
            $email_error = 'Failed to authenticate on SMTP server with username \"apikey\" using 2 possible authenticators. Authenticator LOGIN returned Expected response code 250 but got an empty response. Authenticator PLAIN returned Expected response code 250 but got an empty response.'; // Get error here
            
        }
        return redirect()->route('main')->with('success', 'Appointment created successfully.');
       
    }
    
    /**
     * Cancel the Appointment.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function bookingStatus($id, $status)
    {        
        $item = Bookings::find($id);
        if (empty($item)) {
            return redirect()->route('patient.appointments')->with('message', 'Appointment not found!');
        }        
        if ($status == 1) {
            $item->status = '0';
        } else {
            $item->status = '1';       
        }
        $item->save();
        $message = $item->status == 1 ? 'Appointment Activated.' : 'Appointment Canceled.';

        return back()->with('message', $message);
    }
    */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
