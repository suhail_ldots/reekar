<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Socialite;
use App\Models\Role;
use App\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function authenticated(Request $request, $user)
    {  
        if(Auth::check()){
            //dd($request->tz);
            \Session::put('riders_admin_timezone', $request->tz);
            
            if(isset($request->form)){
                \Session::put('front_user', base64_decode($request->form));
            }
            

            if(Auth::user()->role_id == '2'){
                \Session::put('current_user_currency', $request->user_currency);
                \Session::put('currency_symbol', $request->user_currency_symbol);
            }
           
        }else{
            Auth::logout();
            return redirect()->back();
        }
        
    }

    public function socialLogin($social, Request $request)
    {
        \Session::put('riders_admin_timezone', $request->tz);
        \Session::put('front_user', $request->front_user);
        \Session::put('current_user_currency', $request->currency);
        \Session::put('currency_symbol', $request->currency_symbol);

        return Socialite::driver($social)->redirect();
    }

    public function siteFrontUserSignup(Request $request)
    {
        if(Auth::check()){             
            return resirect()->route('main')->with('message','You have already logged in.');           
        }else{
            return view('auth.customer_signup');
        }

        \Session::put('riders_admin_timezone', $request->tz);
        \Session::put('front_user', $request->front_user);
        \Session::put('current_user_currency', $request->currency);
        \Session::put('currency_symbol', $request->currency_symbol);

         
    }

    public function handleProviderCallback($social)
    {
        $userSocial = Socialite::driver($social)->user(); 
        //dd($userSocial);
	    if($userSocial->getEmail() == null){
		    return redirect()->back()->with('message', "Sorry, We didn't found your Email Id at this social site please login with other method.");
        }
        
      $user = User::where(['email' => $userSocial->getEmail()])->first();
      if($user){ 
          Auth::login($user); 
          return redirect()->action('Controller@index');
      }else{
        $role = Role::select('id')->where('name', 'User')->first();  
        
          $user = new User() ;         
            $user->first_name = ($userSocial->user['given_name'] != '' ) ? $userSocial->user['given_name'] : $userSocial->name ;
            $user->last_name = ($userSocial->user['family_name'] != '' ) ? $userSocial->user['family_name'] : NULL ;
            $user->email = $userSocial->getEmail();
            $user->profile_pic = $userSocial->avatar_original;
            $user->mobile = '';
            $user->password = \Hash::make(date('Ymd'));
            $user->role_id = $role->id;
            $item->email_verified_at = date('Y-m-d H:i:s');
            $user->save(); 

            Auth::login($user);
        return redirect()->action('Controller@index');
      }
  }

   /**
     *Site User login
     *
     * @param Request $request request body from client
     * @return responce view login page or Redirect to  User dashboard
     * */

    public function mysiteUser(Request $request){
        
        if(Auth::check() && Auth::user()->role_id == '2'){
            
            return redirect()->route('customercar.create')->with('Message', 'Login Successful.');
        }

        return view("front.customer_login");

    }
   /**
     *Site Front User login
     *
     * @param Request $request request body from client
     * @return responce view login page or Redirect to  home page with msg
     * */

    public function siteFrontUser(Request $request){
        
        if(Auth::check() && Auth::user()->role_id == '2'){
            
            return redirect()->route('main')->with('Message', 'Login Successful.');
        }

        return view("auth.front_customer_login");

    }

}
