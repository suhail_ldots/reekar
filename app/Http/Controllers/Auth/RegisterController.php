<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Mail\SendOTP;
use App\Mail\RegisterSuccessMail;
// use Illuminate\Support\Facades\Input;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    //protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /* public function __construct()
    {
        $this->middleware('guest');
    } */

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $email_regex = '/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix';
                  
        return Validator::make($data, [
          
            'email' => ['required', 'string', 'email','regex:'.$email_regex, 'max:255', 'unique:users'],
            'password' => 'required|string|min:8|max:20|confirmed',
            'first_name' => 'required|regex:/^[a-zA-Z ]+$/u|max:255',
            'last_name' => 'nullable|regex:/^[a-zA-Z ]+$/u|max:255',
            'blood_group' => 'nullable|string|max:3',
            'gender' => 'required|string|max:20',
            'age' => 'required|integer|digits-between:1,3',
            
            'profile_image' => 'nullable|image|mimes:jpeg,png,jpg|max:2048',
            'mobile' => 'required|string|max:25',             
            'zipcode' => 'nullable|string|max:20',
            'user_address' => 'required|string|max:1000',   //text field in db        
            'user_lat' => 'nullable|string|max:30',
            'user_long' => 'nullable|string|max:30',
            'country' => 'required|numeric',
            'city' => 'nullable|string|max:255',
        ]);
        
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        // if(\Session::has('user_email_otp')){
        //     $resetOtp =  \Session::get('user_email_otp');
        // }else{
        //     return redirect()->back()->with('message', 'Sorry, your OTP has been expired, Please generate.');
        // }
        // if($resetOtp != $data['otp']){
        //     return redirect()->back()->with('message', 'Sorry, you entered wrong OTP, Please try again with correct otp.');
        // }
       
        // if (Input::file('profile_image')->isValid()){
        //     $image = Input::file('profile_image');

        //     $imageName = time() . rand() . '.' . $image->getClientOriginalExtension();
            
        //     $folderPath = 'storage/app/images/doctor/profile/';
        //    // $folderPath = 'public/carDocs';

        //     if(!file_exists($folderPath)) {
        //         mkdir($folderPath, 0777, true);
        //     }

        //     $image->move($folderPath, $imageName);
        //     $path = $imageName; 
        //     // $data['profile_image'] = $path;
        // }else{
        //     $path = '';
        // }
        
        $user = new User();
        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        $user->email          = $data['email'];
        $user->mobile         = $data['mobile'];
        $user->gender         = $data['gender'];
        $user->age            = $data['age'];
        $user->blood_group    = $data['blood_group'];
        $user->address        = $data['user_address'];
        $user->lat            = $data['user_lat'];
        $user->long           = $data['user_long'];
        $user->zipcode        = $data['zipcode'];
        $user->country        = $data['country'];
        $user->city           = $data['city'];
        // $user->profile_pic    = $path;
        $user->email_verified_at = date('Y-m-d H:i:s');
        $user->role_id = 2;
        $user->status = 1;
        $user->password = Hash::make($data['password']);
        $user->save();
        
        \Session::put('user_signup', 'patient');

        try {
            \Mail::to($data['email'])->send(new RegisterSuccessMail($user));
            //createlogs(ErrorLogs::dr, $user_type, ErrorLogs::WELCOME_MAIL);
        } catch (\Exception $e) {
            $email_error = $e->getError()->message;; // Get error here
            //createlogs(ErrorLogs::dr, $user_type, ErrorLogs::WELCOME_MAIL_FAILED);
        }

        return $user;
       
    }

    
    public function userVerification(Request $request)
    {
        $this->validate($request,[
            'email' => 'required|string|email|max:255|unique:users,email',
        ]);

        $resetOtp = mt_rand('100000','999999');

        \Session::put('user_email_otp', $resetOtp);
        \Session::put('email', $request->email);
        \Mail::to($request->email)->send(new SendOTP($request, $resetOtp));

        return back()->with('message', 'We sent you an OTP at your mail id please check.') ;
    }
}
