<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Mail\SuccessfulPasswordChange;

use App\User;
use \App\Models\PaymentMethods;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dd("dfwf");
        return view('customer.modules.profile.view');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function store(Request $request)
    {
        $email_error = '';       
        $item = Auth::user();
            $this->validate($request, [
                'email' => 'required|string|email|max:255|unique:users,email,' . $request->id . ',id',
                'first_name' => 'required|regex:/^[a-zA-Z ]+$/u|max:255',
                'last_name' => 'required|regex:/^[a-zA-Z ]+$/u|max:255',
                'mobile' => 'required|string|max:25',
                'country_code' => 'required',              
                'address' => 'nullable|max:255',  
            ]); 

            $item = User::find($request->id);

            if ($request->password != '') {
                $this->validate($request, [
                    'password' => 'required|string|min:8|max:20|confirmed',
                    'password_confirmation' => 'required|string|min:8|max:20',
                ]);
                $item->password = Hash::make($request->password); 
            }
            $item->first_name = $request->first_name;
            $item->last_name = $request->last_name;           
            $item->email = $request->email;
            $item->mobile = $request->mobile; 
            $item->country_code = $request->country_code; 
            if($item->email_verified_at == '') { 
                $item->email_verified_at =  date('Y-m-d H:i:s');
            }           
            $item->address = $request->address; 
            $item->save();    
         
        \Session::flash('message', 'Profile Updated.');        
        return response()->json([
            'success' => true,
            'message' => 'Saved',
            'redirect' => route('customerprofile.index'),
        ]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('customer.modules.profile.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function paymentMethodCreate(Request $request)
    { 
        $payment = PaymentMethods::where('user_id', Auth::user()->id)->first();
        return view('customer.modules.payment.payment_create',[
            'item' => $payment
        ]);
    }

    /**
     * For register new user payment method
     *
     * @param Request $request request body from client
     * @return view response
     */
    public function paymentMethod(Request $request)
    {
        $email_regex = '/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix';
        $validator = Validator::make($request->all(), [
            'payment_id' => 'nullable|numeric|exists:payment_methods,id,deleted_at,NULL,status,1,user_id,' . \Auth::id(),
            'type' => 'required|' . Rule::in(['Paypal', 'IBAN', 'CC']),
            'first_name' => 'required_if:type,IBAN|regex:/^[\pL\s\-]+$/u|max:50',
            'last_name' => 'required_if:type,IBAN|regex:/^[\pL\s\-]+$/u|max:25',
            'email' => 'required_if:type,Paypal|email|regex:' . $email_regex . '|max:255',
            'iban' => 'required_if:type,IBAN|max:25',
            'bank_name' => 'required_if:type,IBAN|max:150',
            'bank_swift_code' => 'required_if:type,IBAN|max:150',
            'mobile' => 'required_if:type,IBAN|numeric|digits_between:4,15',
            'card_no' => 'required_if:type,CC|digits:16',
            'exp_date' => 'required_if:type,CC|max:20',
            'cvv' => 'required_if:type,CC|digits:3',
            'name_on_card' => 'required_if:type,CC|regex:/^[\pL\s\-]+$/u|max:50',
        ], [
            'type.required' => 'Type is required',
            'first_name.required_if' => 'First Name is required',
            'first_name.regex' => 'First Name is invalid',
            'first_name.max' => 'First Name should be maximum 50 characters long',
            'last_name.required_if' => 'Last Name is required',
            'last_name.regex' => 'Last Name is invalid',
            'last_name.max' => 'Last Name should be maximum 25 characters long',
            'email.required_if' => 'Email is required',
            'email.email' => 'Email is invalid',
            'email.regex' => 'Email is invalid',
            'iban.required_if' => 'IBAN is required',
            'iban.max' => 'IBAN should be maximum 25 characters long',
            'bank_name.required_if' => 'Bank name is required',
            'bank_name.max' => 'Bank name should be maximum 150 characters long',
            'bank_swift_code.required_if' => 'Bank swift code is required',
            'bank_swift_code.max' => 'Bank swift code should be maximum 150 characters long',
            'mobile.required_if' => 'Phone no. is required',
            'mobile.numeric' => 'Phone no. should be numeric',
            'mobile.digits_between' => 'Phone no. minimum 4 and maximum 15 digits long',
            'card_no.required_if' => 'Card no is required',
            'card_no.digits' => 'Card no is invalid',
            'exp_date.required_if' => 'Expiry date is required',
            'exp_date.max' => 'Expiry date is invalid',
            'cvv.required_if' => 'CVV is required',
            'cvv.digits' => 'CVV is invalid',
            'name_on_card.required_if' => 'Name on card is required',
            'name_on_card.regex' => 'Name on card is invalid',
            'name_on_card.max' => 'Name on card should be maximum 50 characters long',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        if ($request->payment_id) {
            $payment = PaymentMethods::find($request->payment_id);
        } else {
            $payment = new PaymentMethods();
        }

        $payment->user_id = \Auth::id();
        $payment->type = $request->type;
        $payment->first_name = $request->first_name;
        $payment->last_name = $request->last_name;
        $payment->email = $request->email;
        $payment->iban = $request->iban;
        $payment->bank_name = $request->bank_name;
        $payment->bank_swift_code = $request->bank_swift_code;
        $payment->mobile = $request->mobile;
        $payment->card_no = $request->card_no;
        $payment->exp_date = $request->exp_date;
        $payment->cvv = $request->cvv;
        $payment->name_on_card = $request->name_on_card;
        $payment->save();
        return response()->json([
            'message' => 'Payment method saved successfully',
        ], 200);
    }
    public function action(Request $request)
    {
        //return  $usrid = Auth::user()->id;
        $validation = Validator::make($request->all(), [
            'select_file' => 'required|image|mimes:jpeg,png,jpg|max:8210',
        ], [], ['select_file' => 'Image file']);
        if ($validation->passes()) {
            $image = $request->file('select_file');
            $imageName = time() . rand() . '.' . $image->getClientOriginalExtension();
            
            $folderPath = 'storage/app/images/users/';
            // $folderPath = 'public/profileImages';
            if(!file_exists($folderPath)) {
                mkdir($folderPath, 0777, true);
            }
           
            $image->move($folderPath, $imageName);
            //file_put_contents($path, $image); 
            $path = $imageName;
            $returnpath = url('/') . '/' . $folderPath . '/' . $imageName;
            //return $path;

        } else {
            return response()->json([
                'message' => $validation->errors()->all(),
                'uploaded_image' => '',
                'class_name' => 'alert-danger',
            ]);
        }

        if (isset($request->uid)) {
            $user = User::findOrFail($request->uid);
            $user->profile_pic = $path;
            $user->save(); 
            return response()->json([
                'status' => 200,
                'message' => 'Image Uploaded Successfully',
                'uploaded_image' => '<img src="$returnpath" class="img-thumbnail" width="300" />',
                'class_name' => 'alert-success',
            ]);
        } else { 
            return response()->json([
                'message' => $validation->errors()->all(),
                'uploaded_image' => '',
                'class_name' => 'alert-danger',
            ]);
        }
    }
}
