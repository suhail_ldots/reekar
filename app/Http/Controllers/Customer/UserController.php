<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Hash;
use Illuminate\Http\Request;
use DB;

class UserController extends Controller
{
    //showing listing
    public function index()
    {
        $items = User::where('role_id','!=', Auth::user()->role_id)->orderBy('first_name')->paginate(mypagination());
       
        if (request()->ajax()) {
            return view('admin.modules.users.tbody', compact('items'));
        }
        return view('admin.modules.users.list', [
            'items' => $items,
        ]);
    }

    public function search(Request $request)
    {     
        $items = User::query();
        $items->where('role_id', '!=', Auth::user()->role_id);

        if(isset($request->name)){
            $items->where('first_name','LIKE','%'.$request->name.'%');
        }
        if(isset($request->email)){
            $items->where('email','LIKE','%'.$request->email.'%');
        }
        $newItem =  $items->orderBy('first_name')->paginate(mypagination());         

        if (request()->ajax()) {
            return view('admin.modules.users.tbody',[
                'items' => $newItem,
            ]); 
        }
        return view('admin.modules.users.list', [
            'items' => $newItem,
            'name' => $request->name,
            'email' => $request->email,
        ]);
    }

    //add list
    public function create()
    {
        return view('admin.modules.users.add', [
            "item" => false,             
        ]);  
    }

    public function status($id, $status)
    {
        
        $item = User::find($id);
        if (empty($item)) {
            return redirect()->route('user.index')->with('message', 'User not found!');
        }
        
        if ($status == 1) {
            $item->status = '0';

            if (\DB::table('oauth_access_tokens')->where('user_id', $item->id)->first()) {
                foreach (\DB::table('oauth_access_tokens')->where('user_id', $item->id)->get() as $token) {
                    \DB::table('oauth_refresh_tokens')
                        ->where('access_token_id', $token->id)
                        ->update([
                            'revoked' => true,
                        ]);

                        $token = \DB::table('oauth_access_tokens')->where('id', $token->id)->update([
                            'revoked' => true,
                        ]);
                }
            }

        } else {
            $item->status = '1';
        }
        $item->save();
        $message = $item->status == 1 ? 'User Activated.' : 'User Deactivated.';

        return redirect()->back()->with('message', $message);
    }

    //strore data
    public function store(Request $request)
    { 
        if (Auth::user()->role_id != 1) {
            return redirect()->route('user.index')->with('message', 'Sorry, you are not allowed to create any user.');
        } 
        $this->validate($request, [
            'email' => 'required|string|email|max:255|unique:users,email,' . $request->id . ',id',
            'first_name' => 'required|regex:/^[a-zA-Z ]+$/u|max:255',
            'last_name' => 'nullable|regex:/^[a-zA-Z ]+$/u|max:255',
            //'b_group' => 'nullable|string|max:3',
            'country_code' => ['required'],
            'profile_img' => 'nullable|image|mimes:jpeg,png,jpg|max:20480',
            'mobile' => 'required|string|max:25',             
            'address' => 'nullable|max:255',  
        ],
        ['profile_img.max'=>"Profile image can not be greater than 2MB."],
        ['profile_img' => 'Profile Pic']            
        );
        
        if(!$request->id){
            $this->validate($request, [
                'password' => 'required|string|min:8|max:20|confirmed',
                'password_confirmation' => 'required|string|min:8|max:20',
            ],[
                'password.min' => 'Your password must be more than 8 characters',
            ]);
        }

        if ($request->id) {
            $item = User::find($request->id);

            if (empty($item)) {
                return redirect()->route('user.index')->with('message', 'User not found!');
            } 
          
            $item->first_name   = $request->first_name;
            $item->last_name    = $request->last_name;
            //$item->blood_group  = $request->b_group;
            $item->email        = $request->email;
            $item->mobile       = $request->mobile;
            $item->country_code = $request->country_code;
            $item->address      = $request->address;
            if($item->email_verified_at == '') { 
                $item->email_verified_at =  date('Y-m-d H:i:s');
            }
            
            $msg = 'User Updated.';

        } else {
            
            $item = new User();
            $item->first_name   = $request->first_name;
            $item->last_name    = $request->last_name;
           // $item->blood_group  = $request->b_group;
            $item->email        = $request->email;
            $item->mobile       = $request->mobile;
            $item->country_code = $request->country_code;
            $item->address      = $request->address;
            $item->role_id      = 2;
            $item->status       = 1;
            $item->status       = 1;
            $item->email_verified_at = date('Y-m-d H:i:s');
            $item->password     = Hash::make($request->password);

            $msg = 'User Added.';
        }
        if($request->hasFile('profile_img')){
            $image = $request->file('profile_img');
            $imageName = time() . rand() . '.' . $image->getClientOriginalExtension();
            
            if(!file_exists('public/profileImages')) {
                mkdir('public/profileImages', 0777, true);
            }
            $folderPath = 'public/profileImages';
            $image->move($folderPath, $imageName);
            $path = url('/') . '/' . $folderPath . '/' . $imageName;
            
            $item->profile_pic = $path;
        }

        $item->save(); 
        \Session::flash('message', $msg);
        return response()->json([
            'success' => true,
            'message' => 'Success',
            'redirect' => route('user.index'),
        ]);
    }

    //edit list
    public function edit($id)
    {
        $item = User::find($id); 
        return view('admin.modules.users.add', [
            "item" => $item,            
        ]);
    }

    //delete list
    public function delete($id)
    {  
        if (Auth::user()->role_id != 1) {
            return redirect()->route('user.index')->with('message', __('l.auth-error'));
        } 
        $item = User::find($id);

        if (empty($item)) {
            return redirect()->route('user.index')->with('message', 'User not found!');
        }  

        if (\DB::table('oauth_access_tokens')->where('user_id', $item->id)->first()) {
            foreach (\DB::table('oauth_access_tokens')->where('user_id', $item->id)->get() as $token) {
                \DB::table('oauth_refresh_tokens')
                    ->where('access_token_id', $token->id)
                    ->update([
                        'revoked' => true,
                    ]);

                    $token = \DB::table('oauth_access_tokens')->where('id', $token->id)->update([
                        'revoked' => true,
                    ]);
            }
        }

        $item->delete();

        return redirect()->back()->with('message', 'User Deleted');
    }

    public function show($id)
    { 
        $user = User::find($id);
        if (empty($user)) {
            return redirect()->route('user.index')->with('message', 'User not found!');
        } 
        return view('admin.modules.users.view', [
            "item" => $user,
        ]);
    }

}
