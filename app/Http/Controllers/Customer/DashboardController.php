<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Roles;

class DashboardController extends Controller
{
    
    public function index(){
        if(\Auth::check()){        
            return view('customer.dashboard',["item" => false]);
        }else{
            return redirect()->to('/customer/login');
        }
    }
}
