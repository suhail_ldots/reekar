<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Cars;
use App\Models\Brands;
use App\Models\Seats;
use App\Models\Colors;
use App\Models\EngineTypes;
use App\Models\VehicleTypes;
use App\Models\Models;
use App\Models\Transmissions;

use App\Models\CarDocs;
use App\Models\CarImages;
use Illuminate\Validation\Rule;
use App\Mail\RequestRejected;
use Illuminate\Support\Facades\Mail;
use Auth;
use App\User;
use Hash;
use Illuminate\Http\Request;

class CarController extends Controller
{
    //showing listing
    public function index()
    {
        $items = Cars::where('user_id', Auth::user()->id)->with(['model','user','color','enginetype','transmission','vehicletype'])->orderBy('id', 'DESC')->paginate(mypagination());
       
        if (request()->ajax()) {
            return view('customer.modules.cars.tbody', compact('items'));
        }
        return view('customer.modules.cars.list', [
            'items' => $items,
        ]);
    }

    public function search(Request $request)
    {     
        $items = Cars::query();
        
        /* if(isset($request->owner_name)){
            $items->whereHas('user', function($query) use ($request){
                $query->where('first_name', 'like', '%' . $request->owner_name . '%');
            });           
        } */
        if(isset($request->brand)){
            $items->whereHas('brand', function($query) use ($request){
                $query->where('name', 'like', '%' . $request->brand . '%');
            });           
        }
        if(isset($request->model)){
            $items->whereHas('model', function($query) use ($request){
                $query->where('name', 'like', '%' . $request->model . '%');
            });           
        }
        if(isset($request->color)){
            $items->whereHas('color', function($query) use ($request){
                $query->where('name', 'like', '%' . $request->color . '%');
            });           
        }
        if(isset($request->year)){
            $items->where('year','LIKE','%'.$request->year.'%');
        }
        if(isset($request->available_from_date)){
            $items->where('available_from_date','LIKE','%'.$request->available_from_date.'%');
        }
        if(isset($request->available_to_date)){
            $items->where('available_to_date','LIKE','%'.$request->available_to_date.'%');
        }
        $newItem =  $items->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->paginate(mypagination());         

        if (request()->ajax()) {
            return view('customer.modules.cars.tbody',[
                'items' => $newItem,
            ]); 
        }
        return view('customer.modules.cars.list', [
            'items' => $newItem,
            'name' => $request->owner_name,
            'model' => $request->model,
            'brand' => $request->brand,
            'color' => $request->color,
            'year' => $request->year,
            'available_from_date' => $request->available_from_date,
            'available_to_date' => $request->available_to_date,
        ]);
    }

    //add list
    public function create()
    {
        return view('customer.modules.cars.add', [
            "item" => false,             
            "ms_users" => \App\User::where(['status' => '1', 'role_id' => 2])->orderBy('first_name')->get(),             
            "ms_brands" => Brands::where('status', '1')->orderBy('name')->get(), 
            "ms_seats" => Seats::where('status', '1')->orderBy('name')->get(),             
            "ms_models" => Models::where('status' , '1')->orderBy('name')->get(),             
            "ms_vehicle_types" => VehicleTypes::where('status' , '1')->orderBy('name')->get(),             
            "ms_engine_types" => EngineTypes::where('status' , '1')->orderBy('name')->get(),             
            "ms_transmissions" => Transmissions::where('status' , '1')->orderBy('name')->get(),             
            "ms_colors" => Colors::where('status' , '1')->orderBy('name')->get(),             
        ]);  
    }

    public function status($id, $status)
    {        
        $item = Cars::where('user_id', Auth::user()->id)->find($id);
        if (empty($item)) {
            return redirect()->route('customercar.index')->with('message', 'Car not found!');
        }
        
        if ($status == 1) {
            $item->status = '0';
        } else {
            $item->status = '1';
        }
        $item->save();
        $message = $item->status == 1 ? 'Car Activated.' : 'Car Deactivated.';

        return redirect()->back()->with('message', $message);
    }


    public function listingStatus(Request $request) 
    {   
               
        /* return response()->json([
            'success' => false,
            'message' =>  $request->all(),
            'redirect' => 'javascript:void(0)',
        ]); */
        $id         = $request->carid;
        $liststatus = $request->listStatus;
        $item = Cars::find($id);
        if (empty($item)) {
            return redirect()->route('customercar.index')->with('message', 'Car not found!');
        }
        
        if ($liststatus == 'approve') {
            $item->listing_status = '1';
        }
        elseif($liststatus == 'reject'){
            $this->validate($request, [
                'reason' => 'required|string|max:250',
            ]);
            $item->listing_status = '2';
            $item->reject_reason = $request->reason; 
        }
        $item->save();
        if($item->listing_status == 1)
        $message = 'Car Approved.';

        if($item->listing_status == 2){

            $message =  'Car Rejected.';

            $user = User::find($item->user_id);
            Mail::to($user->email)->send(new RequestRejected($user, $item));
        } 
            

        if($item->listing_status == 0) 
            $message =  'Request Pending.';        

        return redirect()->back()->with('message', $message);
    }

    //store data
    public function store(Request $request)
    {  
        /* return response()->json([
            'success' => false,
            'message' => $request->all(),
            'redirect' => 'javascript:void(0)',
        ]); */
        if (Auth::user()->role_id != 2) {
            return redirect()->route('customercar.index')->with('message', 'Sorry, you are not allowed to create any Car.');
        }
        
        if(!$request->id){
            $this->validate($request, [
                "car_img" => ["required","array","min:1","max:6"],
                'car_img.*' => 'required|image|mimes:jpeg,png,jpg|min:3',
                "car_docs" => ["required","array","min:1","max:4"],
                'car_docs.*' => 'required|mimes:jpeg,png,jpg,pdf,doc,docx|min:1',
            ],
            ['car_img.required' => 'Car Image is required and must have at least 3 items.',
            'car_docs.required' => 'Car Doc is required and must have at least 1 item.',
            ],
            [ 'car_img' => 'Car Image', 
                'car_img.*' => 'Car Image', 
                'car_docs.*' => 'Car Doc',   
            ]);
        }
        
        
        $this->validate($request, [
            //'user' => 'required|integer',
            'brand' => 'required|integer',
            'color' => 'required|integer',
            'vehicle_type' => 'required|integer',
            'engine_type' => 'required|integer',
            'model' => 'required|integer',
            'transmission' => 'required|integer',
            'seat' => 'required|integer',
            'year' => 'required|integer|digits:4|min:1980|max:'.date('Y'), 
            /* 'car_img.*' => 'required|image|mimes:jpeg,png,jpg|min:3',
            'car_docs.*' => 'required|mimes:jpeg,png,jpg,pdf,doc,docx|min:1', */

            'available_from_date' => 'required|date_format:Y-m-d',             
            'available_from_time' => 'required|date_format:H:i',             
            'available_to_date' => 'required|date_format:Y-m-d|after:available_from_date',             
            'available_to_time' => 'required|date_format:H:i', 
            'car_location' => 'required|string|max:250',
            //'currency'      => 'required|string', 
            'price_per_day' => 'required|numeric', 
            'total_amount'  => 'nullable|numeric', 
            'security_amount' => 'nullable|numeric', 
          
            'chassis_number' => 'nullable|string|max:250',
            'mileage' => 'nullable|string|max:250',
            'licensed' => 'nullable|'. Rule::in(['yes']),
            'cruise_control' => 'nullable|'. Rule::in(['yes']),
        ],[],['year' => 'Model Year']);
          
       if ($request->id) {
            $item = Cars::find($request->id);

            if (empty($item)) {
                return redirect()->route('customercar.index')->with('message', 'Car not found!');
            } 
          
            $item->user_id              = Auth::user()->id;
            $item->brand_id             = $request->brand;
            $item->color_id             = $request->color;
            $item->model_id             = $request->model;
            $item->transmission_id      = $request->transmission;
            $item->vehicle_type_id      = $request->vehicle_type;
            $item->engine_type_id       = $request->engine_type;
            $item->seat_id              = $request->seat;
            $item->year                 = $request->year;
            $item->car_location         = $request->car_location;
            $item->available_from_date         = getUTCTimezoneDateTime($request->available_from_date.' '.$request->available_from_time);
            $item->available_from_time         = getUTCTimezoneTime($request->available_from_time);
            $item->available_to_date           = getUTCTimezoneDateTime($request->available_to_date.' '.$request->available_to_time);
            $item->available_to_time           = getUTCTimezoneTime($request->available_to_time);
            $item->price_per_day               = $request->price_per_day;
            $item->currency                    = 'AED';
            $item->total_price                 = ($request->total_amount != '') ? $request->total_amount :'0';
            $item->security_amount             = ($request->security_amount != '') ? $request->security_amount :'0';
            
            //CheckBoxes AC To Audio System          
            $item->ac                    = ($request->ac == 'yes') ? '1' : '0' ;
            $item->gps                    = ($request->gps == 'yes') ? '1' : '0' ;
            $item->ipod_interface         = ($request->ipod_interface == 'yes') ? '1' : '0' ;
            $item->sunroof	              = ($request->sunroof	 == 'yes') ? '1' : '0' ;
            $item->child_seat	          = ($request->child_seat	 == 'yes') ? '1' : '0' ;
            $item->electric_windows	      = ($request->electric_windows	 == 'yes') ? '1' : '0' ;
            $item->heated_seat	          = ($request->heated_seat	 == 'yes') ? '1' : '0' ;
            $item->panorma_roof	          = ($request->panorma_roof	 == 'yes') ? '1' : '0' ;
            $item->prm_gauge	          = ($request->prm_gauge	 == 'yes') ? '1' : '0' ;
            $item->abs	                  = ($request->abs	 == 'yes') ? '1' : '0' ;
            $item->traction_control	      = ($request->traction_control	 == 'yes') ? '1' : '0' ;
            $item->audio_system	          = ($request->audio_system	 == 'yes') ? '1' : '0' ;
            //addition
            $item->mileage = $request->mileage;
            $item->is_licensed = ($request->licensed == 'yes') ? '1' : '0' ;
            $item->cruise_control = ($request->cruise_control == 'yes') ? '1' : '0' ;
            $item->chassis_number = $request->chassis_number;
            $item->car_location_lat = $request->pickup_lat;
            $item->car_location_long = $request->pickup_long; 

            $msg = 'Car Updated.';

        } else { 
           
            $item = new Cars();
            $item->user_id              = Auth::user()->id;
            $item->brand_id             = $request->brand;
            $item->color_id             = $request->color;
            $item->model_id             = $request->model;
            $item->transmission_id      = $request->transmission;
            $item->vehicle_type_id      = $request->vehicle_type;
            $item->engine_type_id       = $request->engine_type;
            $item->seat_id              = $request->seat;
            $item->year                 = $request->year;
            $item->car_location         = $request->car_location;
            $item->available_from_date         = getUTCTimezoneDateTime($request->available_from_date.' '.$request->available_from_time);
            $item->available_from_time         = getUTCTimezoneTime($request->available_from_time);
            $item->available_to_date           = getUTCTimezoneDateTime($request->available_to_date.' '.$request->available_to_time);
            $item->available_to_time           = getUTCTimezoneTime($request->available_to_time);
            $item->price_per_day               = $request->price_per_day;
            $item->currency                    = 'AED';
            $item->total_price                 = ($request->total_amount != '') ? $request->total_amount :'0';
            $item->security_amount             = ($request->security_amount != '') ? $request->security_amount :'0';

            //CheckBoxes AC To Audio System          
            $item->ac                     = ($request->ac == 'yes') ? '1' : '0' ;
            $item->gps                    = ($request->gps == 'yes') ? '1' : '0' ;
            $item->ipod_interface         = ($request->ipod_interface == 'yes') ? '1' : '0' ;
            $item->sunroof	              = ($request->sunroof	 == 'yes') ? '1' : '0' ;
            $item->child_seat	          = ($request->child_seat	 == 'yes') ? '1' : '0' ;
            $item->electric_windows	      = ($request->electric_windows	 == 'yes') ? '1' : '0' ;
            $item->heated_seat	          = ($request->heated_seat	 == 'yes') ? '1' : '0' ;
            $item->panorma_roof	          = ($request->panorma_roof	 == 'yes') ? '1' : '0' ;
            $item->prm_gauge	          = ($request->prm_gauge	 == 'yes') ? '1' : '0' ;
            $item->abs	                  = ($request->abs	 == 'yes') ? '1' : '0' ;
            $item->traction_control	      = ($request->traction_control	 == 'yes') ? '1' : '0' ;
            $item->audio_system	          = ($request->audio_system	 == 'yes') ? '1' : '0' ;
            //addition
            $item->mileage = $request->mileage;
            $item->is_licensed = ($request->licensed == 'yes') ? '1' : '0' ;
            $item->cruise_control = ($request->cruise_control == 'yes') ? '1' : '0' ;
            $item->chassis_number = $request->chassis_number;
            $item->car_location_lat = $request->pickup_lat;
            $item->car_location_long = $request->pickup_long;

            $msg = 'Car Added.';
        }
        $item->save();

        if($request->hasFile('car_img')){

            $images = $request->file('car_img');

            foreach($images as $image){
                $carImages = new CarImages();
                
                $imageName = time() . rand() . '.' . $image->getClientOriginalExtension();
            
                $folderPath = 'storage/app/images/cars/';
                 //$folderPath = 'public/carImages';
                if(!file_exists($folderPath)) {
                    mkdir($folderPath, 0777, true);
                }
               
                $image->move($folderPath, $imageName);
               // $path = url('/') . '/' . $folderPath . '/' . $imageName; 
                $path = $imageName; 

                $carImages->image = $path;
                $carImages->car_id = $item->id;
                $carImages->save();
            }            
        }

        if($request->hasFile('car_docs')){

            $images = $request->file('car_docs');

            foreach($images as $image){
                $carDocs = new CarDocs();
                
                $imageName = time() . rand() . '.' . $image->getClientOriginalExtension();
            
                $folderPath = 'storage/app/images/cars/docs/';
               // $folderPath = 'public/carDocs';

                if(!file_exists($folderPath)) {
                    mkdir($folderPath, 0777, true);
                }
               
                $image->move($folderPath, $imageName);
                $path = $imageName; 

                $carDocs->doc = $path;
                $carDocs->car_id = $item->id;
                $carDocs->type = $image->getClientOriginalExtension();
                $carDocs->save();
            }
            
        }
        
        \Session::flash('message', $msg);
        return response()->json([
            'success' => true,
            'message' => 'Success',
            'redirect' => route('customercar.index'),
        ]);
    }

    //edit list
    public function edit($id)
    {
        $item = Cars::where('user_id', Auth::user()->id)->find($id);
        
        if($item->listing_status == 1){
            return redirect()->back()->with('message',"Sorry, you cant edit details of approved Car.");
        }
        return view('customer.modules.cars.add', [
            "item" => $item,
            "ms_users" => \App\User::where(['status' => '1', 'role_id' => 2])->orderBy('first_name')->get(),             
            "ms_brands" => Brands::where('status', '1')->orderBy('name')->get(),
            "ms_seats" => Seats::where('status', '1')->orderBy('name')->get(),              
            "ms_models" => Models::where('status' , '1')->orderBy('name')->get(),             
            "ms_vehicle_types" => VehicleTypes::where('status' , '1')->orderBy('name')->get(),             
            "ms_engine_types" => EngineTypes::where('status' , '1')->orderBy('name')->get(),             
            "ms_transmissions" => Transmissions::where('status' , '1')->orderBy('name')->get(),             
            "ms_colors" => Colors::where('status' , '1')->orderBy('name')->get(),             
        ]);
    }

    //delete list
    public function delete($id)
    {  
         
        if (Auth::user()->role_id != 2) {
            return redirect()->route('customercar.index')->with('message', __('l.auth-error'));
        } 
        $item = Cars::where('user_id', Auth::user()->id)->find($id);

        if (empty($item)) {
            return redirect()->route('customercar.index')->with('message', 'Car not found!');
        }  
        $item->delete();

        return redirect()->back()->with('message', 'Car Deleted');
    }

    public function show($id)
    { 
        $car = Cars::where('user_id', Auth::user()->id)->find($id);
        if (empty($car)) {
            return redirect()->route('customercar.index')->with('message', 'Car not found!');
        } 
        return view('customer.modules.cars.view', [
            "item" => $car,           
        ]);
    }

}
