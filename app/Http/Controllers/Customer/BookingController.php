<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Cars;
use App\Models\Orders;
use App\Models\Brands;
use App\Models\Colors;
use App\Models\EngineTypes;
use App\Models\VehicleTypes;
use App\Models\Models;
use App\Models\Transmissions;

use App\Models\CarDocs;
use App\Models\CarImages;
use Auth;
use Hash;
use Illuminate\Http\Request;

class BookingController extends Controller
{
    //showing listing
    public function index()
    {
        $mycar = Cars::where('user_id', Auth::user()->id)->get('id');
         
        $items = Orders::whereIn('car_id', $mycar)->with('car')->orderBy('id', 'DESC')->paginate(mypagination());
        
        if (request()->ajax()) {
            return view('customer.modules.bookings.tbody', compact('items'));
        }
        return view('customer.modules.bookings.list', [
            'items' => $items,
        ]);
    }

    public function search(Request $request)
    {     
        $mycar = Cars::where('user_id', Auth::user()->id)->get('id');
        $items = Orders::query();
        $items->whereIn('car_id', $mycar);

        if(isset($request->hired_by)){
            $items->whereHas('user', function($query) use ($request){
                $query->where('first_name', 'like', '%' . $request->hired_by . '%');
            });           
        }
        if(isset($request->brand)){
            $items->whereHas('car', function($query) use ($request){                
                $query->whereHas('brand', function($query1) use ($request){
                    $query1->where('name', 'like', '%' . $request->brand . '%');
                });               
            });           
        }
        if(isset($request->booking_id)){
            $items->where('booking_id','LIKE','%'.$request->booking_id.'%');
        }
        if(isset($request->order_date)){
            $items->where('updated_at','LIKE','%'.$request->order_date.'%');
        }
        $newItem =  $items->orderBy('id', 'DESC')->paginate(mypagination());         

        if (request()->ajax()) {
            return view('customer.modules.bookings.tbody',[
                'items' => $newItem,
            ]); 
        }
        return view('customer.modules.bookings.list', [
            'items' => $newItem,
            'name' => $request->hired_by,
            'model' => $request->model,
            'brand' => $request->brand,
            'booking_id' => $request->booking_id,
            'order_date' => $request->order_date,
            
        ]);
    }


    public function orderStatus($id, $order_status)
    {
        if (Auth::user()->role_id != 2) {
            return redirect()->route('mybooking.index')->with('message', __('l.auth-error'));
        }

        $item = Orders::find($id);

        if (empty($item)) {
            return redirect()->route('mybooking.index')->with('message', 'Booking not found!');
        }
        
        if ($order_status != 4) {
            $item->order_status = 4;
        }
         
        $item->save();
        $message = $item->order_status == 4 ? 'Booking Canceled.' : '';

        return redirect()->back()->with('message', $message);
    }

    public function show($id)
    { 
        $mycar = Cars::where('user_id', Auth::user()->id)->get('id');
        $Booking = Orders::where('id', $id)->whereIn('car_id', $mycar)->first();
        if (empty($Booking)) {
            return redirect()->route('mybooking.index')->with('message', 'Booking not found!');
        } 
        return view('customer.modules.bookings.view', [
            "item" => $Booking,           
        ]);
    }

}
