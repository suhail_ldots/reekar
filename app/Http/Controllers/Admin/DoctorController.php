<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use App\Models\NursingHome;
use App\Models\DoctorNursingHomerelation;
use App\Models\Doctor;
use App\Mail\WelcomeToDoctorsLoop;

use Auth;
use Hash;
use Illuminate\Http\Request;
use DB;

class DoctorController extends Controller
{
    //showing listing
    public function index()
    {
        if (Auth::user()->role_id != 1) {
            return redirect()->back()->with('message', "Sorry, you don't have permission to access this page.");
        }

        $items = User::where('role_id', 3)->orderBy('id', 'DESC')->paginate(mypagination());
       
        if (request()->ajax()) {
            return view('admin.modules.doctors.tbody', compact('items'));
        }
        return view('admin.modules.doctors.list', [
            'items' => $items,
        ]);
    }

    public function search(Request $request)
    {     
        $items = User::query();
        $items->where('role_id', 3);

        if(isset($request->name)){
            $items->where('first_name','LIKE','%'.$request->name.'%');
        }
        if(isset($request->email)){
            $items->where('email','LIKE','%'.$request->email.'%');
        }
        $newItem =  $items->orderBy('id', 'desc')->paginate(mypagination());         

        if (request()->ajax()) {
            return view('admin.modules.doctors.tbody',[
                'items' => $newItem,
            ]); 
        }
        return view('admin.modules.doctors.list', [
            'items' => $newItem,
            'name' => $request->name,
            'email' => $request->email,
        ]);
    }

    //add list
    public function create()
    {
        $nursinghome = NursingHome::query();
        $role = 4;
        $nursinghome->whereHas('user', function($query) use ($role){
            $query->where('role_id', $role)->where('status', 1);
        });
        $newnursinghome = $nursinghome->orderBy('id', 'desc')->get();
        //dd($newnursinghome);
        return view('admin.modules.doctors.add', [
            "item" => false,             
            "nursinghome" => $newnursinghome,             
        ]);  
    }

    public function status($id, $status)
    {
        
        $item = User::find($id);

        if (empty($item)) {
            return redirect()->route('doctor.index')->with('message', 'Doctor not found!');
        }
        if ($item->role_id != 3) {
            return redirect()->route('doctor.index')->with('message', "Sorry, you don't have permission to access this user.");
        }
        
        if ($status == 1) {
            $item->status = '0';

            if (\DB::table('oauth_access_tokens')->where('user_id', $item->id)->first()) {
                foreach (\DB::table('oauth_access_tokens')->where('user_id', $item->id)->get() as $token) {
                    \DB::table('oauth_refresh_tokens')
                        ->where('access_token_id', $token->id)
                        ->update([
                            'revoked' => true,
                        ]);

                        $token = \DB::table('oauth_access_tokens')->where('id', $token->id)->update([
                            'revoked' => true,
                        ]);
                }
            }

        } else {
            $item->status = '1';
        }
        $item->save();
        $message = $item->status == 1 ? 'Doctor Activated.' : 'Doctor Deactivated.';

        return redirect()->back()->with('message', $message);
    }

    //strore data
    public function store(Request $request)
    { 
       // dd("fgfdg");
        if (Auth::user()->role_id != 1) {
            return redirect()->route('doctor.index')->with('message', "Sorry, you don't have permission  to create any user.");
        } 
        $email_regex = '/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix';

        $this->validate($request, [
            'email' => 'required|string|email|regex:'.$email_regex.'|max:255|unique:users,email,' . $request->id . ',id',
            'first_name' => 'required|regex:/^[a-zA-Z ]+$/u|max:255',
            'last_name' => 'nullable|regex:/^[a-zA-Z ]+$/u|max:255',
            //'b_group' => 'nullable|string|max:3',
            'country_code' => ['required'],
            'mobile' => 'required|string|max:25', 
            'country' => 'required|integer',
            'speciality' => 'required|integer',
            'department' => 'required|integer',
            'degree' => 'required|string|max:200',
            'reg_no' => 'required|string|max:200',
            'nursing_home' => 'nullable|integer',

            'city' => 'nullable|max:255', 
            'address' => 'nullable|max:255', 
            'about_nursing' => 'required|string', 
            'profile_img' => 'nullable|image|mimes:jpeg,png,jpg|max:2048',   
            'password' => 'nullable|string|min:8|max:20|confirmed',
            'password_confirmation' => 'nullable|string|min:8|max:20',                     
        ],
        [
            'profile_img.max'=>"Profile image can not be greater than 2MB.",
            'nursing_home.required' => 'Please select doctor nursing home',
            'password.min' => 'Your password must be more than 8 characters',
        ],
        ['profile_img' => 'Profile imgage',
         'about_nursing'=> 'About doctor',
         'degree'=> 'Doctor qualification',
         'reg_no'=> 'Doctor license or doctor registration',
        ]              
        );
        
        if(!$request->id){
            $this->validate($request, [
                'nursing_home' => 'required|integer',
                'password' => 'required|string|min:8|max:20|confirmed',
                'password_confirmation' => 'required|string|min:8|max:20',
            ],[
                'password.min' => 'Your password must be more than 8 characters',
                'nursing_home.required' => 'Please select doctor nursing home',
            ]);
        }

        if ($request->id) {
            $item = User::find($request->id);

            if (empty($item)) {
                return redirect()->route('doctor.index')->with('message', 'Doctor not found!');
            } 
          
            $item->first_name   = $request->first_name;
            $item->last_name    = $request->last_name;
            //$item->blood_group  = $request->b_group;
            $item->email        = $request->email;
            $item->mobile       = $request->mobile;
            $item->country_code = $request->country_code;
            $item->country      = $request->country;
            $item->city         = $request->city;
            $item->lat          = $request->lat;
            $item->long         = $request->long;
            $item->address      = $request->address;
            
            if($request->nursing_home != Null && $request->nursing_home != ''){
                $item->nursing_home_id   = $request->nursing_home;    
            }
           
            if($item->email_verified_at == '') { 
                $item->email_verified_at =  date('Y-m-d H:i:s');
            }
            
            $msg = 'User Updated.';
            if($request->hasFile('profile_img')){
                $image = $request->file('profile_img');
                $imageName = time() . rand() . '.' . $image->getClientOriginalExtension();
                
                $folderPath = 'storage/app/images/doctor/profile';
                // $folderPath = 'public/profileImages';
                if(!file_exists($folderPath)) {
                    mkdir($folderPath, 0777, true);
                }
                $image->move($folderPath, $imageName);
                $path = $imageName;            
                
                $item->profile_pic = $path;
            }
            $item->save();
            $doctor = Doctor::where('user_id', $item->id)->first();
            $doctor->speciality = $request->speciality;
            $doctor->department_id = $request->department;
            $doctor->reg_no        = $request->reg_no;
            $doctor->degree        = $request->degree;
            $doctor->about_doc  = $request->about_nursing;
            $doctor->save();


        } else {            
            $item = new User();
            $item->first_name   = $request->first_name;
            $item->last_name    = $request->last_name;
           // $item->blood_group  = $request->b_group;
            $item->email        = $request->email;
            $item->mobile       = $request->mobile;
            $item->country_code = $request->country_code;
            $item->country      = $request->country;
            $item->city         = $request->city;
            $item->lat          = $request->lat;
            $item->long         = $request->long;
            $item->address      = $request->address;
            $item->role_id      = 3;
            $item->status       = 1;
            $item->email_verified_at = date('Y-m-d H:i:s');
            $item->password     = Hash::make($request->password);

            if($request->nursing_home != Null && $request->nursing_home != ''){
                $item->nursing_home_id   = $request->nursing_home;    
            }
                   

            $msg = 'User Added.';
        
        if($request->hasFile('profile_img')){
            $image = $request->file('profile_img');
            $imageName = time() . rand() . '.' . $image->getClientOriginalExtension();
            
            $folderPath = 'storage/app/images/doctor/profile';
            // $folderPath = 'public/profileImages';
            if(!file_exists($folderPath)) {
                mkdir($folderPath, 0777, true);
            }
            $image->move($folderPath, $imageName);
            $path = $imageName;            
            
            $item->profile_pic = $path;
        }
       
        if($item->save()){
            $doctor = new Doctor();
            $doctor->user_id = $item->id;
            $doctor->speciality = $request->speciality;
            $doctor->department_id = $request->department;
            $doctor->reg_no        = $request->reg_no;
            $doctor->degree        = $request->degree;
            $doctor->about_doc  = $request->about_nursing;
            $doctor->save();

            //Doctor Nursing Relation
            $doctorNursingRelation = new doctorNursingHomeRelation();
            $doctorNursingRelation->doctor_id = $doctor->id;
            $doctorNursingRelation->nursing_home_id = $item->nursing_home_id;
            $doctorNursingRelation->status = 1;
            if($doctorNursingRelation->save()){
                $item['plan_name'] = '';
                $item['temp_password'] = $request->password;
                $item['plan_feature_name'][] = '';
                
                try {
                    \Mail::to($request->email)->send(new WelcomeToDoctorsLoop($item));
                    
                } catch (\Exception $e) {
                    $email_error = $e; // Get error here
                    
                }
            }else{
                User::where('id', $item->id)->delete();
                Doctor::where('user_id', $item->id)->delete();
                return redirect()->back()->with('message', "Sorry, something went wrong. Please try again .");
        
            }
        }
        }

        \Session::flash('message', $msg);
        return response()->json([
            'success' => true,
            'message' => 'Success',
            'redirect' => route('doctor.index'),
        ]);
    }

    //edit list
    public function edit($id)
    {
        $item = User::find($id); 

        $nursinghome = NursingHome::query();
        $role = 4;
        $nursinghome->whereHas('user', function($query) use ($role){
            $query->where('role_id', $role)->where('status', 1);
        });
        $newnursinghome = $nursinghome->orderBy('id', 'desc')->get();
        return view('admin.modules.doctors.add', [
            "item" => $item,  
            "nursinghome" => $newnursinghome,             
        ]);
    }

    //delete list
    public function delete($id)
    {  
        if (Auth::user()->role_id != 1) {
            return redirect()->route('doctor.index')->with('message', __('l.auth-error'));
        } 
        $item = User::find($id);

        if (empty($item)) {
            return redirect()->route('doctor.index')->with('message', 'Doctor not found!');
        }  
        if ($item->role_id != 3) {
            return redirect()->route('doctor.index')->with('message', "Sorry, you don't have permission to access this user.");
        }
        if (\DB::table('oauth_access_tokens')->where('user_id', $item->id)->first()) {
            foreach (\DB::table('oauth_access_tokens')->where('user_id', $item->id)->get() as $token) {
                \DB::table('oauth_refresh_tokens')
                    ->where('access_token_id', $token->id)
                    ->update([
                        'revoked' => true,
                    ]);

                    $token = \DB::table('oauth_access_tokens')->where('id', $token->id)->update([
                        'revoked' => true,
                    ]);
            }
        }

        $item->delete();

        return redirect()->back()->with('message', 'Doctor Deleted');
    }

    public function show($id)
    { 
        $user = User::find($id);
        if (empty($user)) {
            return redirect()->route('doctor.index')->with('message', 'Doctor not found!');
        } 
        if ($user->role_id != 3) {
            return redirect()->route('doctor.index')->with('message', "Sorry, you don't have permission to access this user.");
        }
        return view('admin.modules.doctors.view', [
            "item" => $user,
        ]);
    }

}
