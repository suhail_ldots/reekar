<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Request;
use Illuminate\Validation\Rule;
use App\Models\Langs;
use URL;

class LangContentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.modules.masters.lang-translate-frame');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.modules.contents.home_page', [
            'item' => false,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'content_type' => 'required|'.Rule::in(['en','arabic']),
        ]);
        if($request->content_type == 'en'){
            
            //$url = URL::to("/").'/storage/app/data/country_code_json.json';

            ///$url = URL::to("/").'/resources/lang/en/l.php';
            $arr_data = include(resource_path('lang/en/l.php'));
            
            $arr_data = implode("\n", $arr_data);
// file_put_contents($file, $str);
            file_put_contents(resource_path('lang/en/l.php'), $arr_data);
            return $arr_data;
             $url = resource_path('lang/en/l.php');
             $data =  file_get_contents($url, true);

            // $data = json_encode($data);
            //$data = $data['Read'];
            $exp= $arr_data['Read'];
            $arr_data['Read'] = 'asdfsfsg fsdhfdsh dfshhsdf fds';
            $arr_data = json_encode($arr_data);
            $arr_data = '<?php return [' . $arr_data .']';
            $arr_data = json_decode($arr_data); 

            // file_put_contents(resource_path('lang/en/l.php'), $arr_data);
           // $url = require(resource_path('lang/en/l.php'));

        }elseif($request->content_type == 'arabic'){
            $url = URL::to("/").'/resources/lang/arabic/l.php';
            $url = URL::to("/").resource_path('view');
            $data =  file_get_contents($url);
        }
        // $url = URL::to("/").'/storage/app/data/country_code_json.json';
        //     $data =  file_get_contents($url);
        //     $data = json_decode($data);
        // if(count($data) >= 1){
        //     return $data;
        // }else{
        //     return false;
        // }  
        
    //    $arr_data = json_encode($arr_data);
    //    $arr_data = '<?php return [' . $arr_data .']';
       
        return response()->json([
            'success' => false,
            'status' => $data,
            'message' => $arr_data,
            // 'message' => json_decode($arr_data),
            //'message1' => $url,
            'redirect' => 'javascript:void(0)',
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
