<?php

namespace App\Http\Controllers\Admin\Masters;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Masters\Country;
use Illuminate\Support\Facades\Auth; 
use Validator;

class CountryController extends Controller
{
    public function index(){
        if (Auth::user()->role_id != 1) {
            return redirect()->route('countries.index')->with('message', 'Sorry, you are not allowed to create any Car.');
        }
        $countries = Country::paginate('20');

        if(request()->ajax()){
            return view('admin.modules.masters.region.countries.tbody', [
                'items' => $countries,
            ]);
        }
        return view('admin.modules.masters.region.countries.list', [
            'items' => $countries,
        ]);
    }

    public function create(){
        if (Auth::user()->role_id != 1) {
            return redirect()->route('countries.index')->with('message', 'Sorry, you are not allowed to create any countries.');
        } 
        return view('admin.modules.masters.region.countries.add', [
            "item" => false,
        ]);
    }

    public function edit($id){
        if (Auth::user()->role_id != 1) {
            return redirect()->route('countries.index')->with('message', 'Sorry, you are not allowed to create any countries.');
        }
        $countries = Country::find($id); 
        return view('admin.modules.masters.region.countries.add', [
            'item' => $countries,           
        ]);
    }

    public function store(Request $request){
       
        if (Auth::user()->role_id != 1) {
            return redirect()->route('countries.index')->with('message', 'Sorry, you are not allowed to create any countries.');
        }

        $this->validate($request, [
            'name' => 'required|string|unique:countries,name,'.($request->id ? $request->id : NULL).',id',
        ]);
  
        if ($request->id) {          
            $countries = Country::find($request->id);
            if (empty($countries)) {
                return redirect()->route('countries.index')->with('message', 'Country not found!');
            }            
            $countries->name = $request->name;
            $countries->save();  
            return redirect()->route('countries.index')->with('message', 'Country Updated.');
        } else {       
            $countries = new Country();
            $countries->name = $request->name;
            $countries->save();  
            return redirect()->route('countries.index')->with('message', 'Country Added.');
        }
        
    }

    public function delete($id){
        
        if (Auth::user()->role_id != 1) {
            return redirect()->route('countries.index')->with('message', __('l.auth-error'));
        } 
        $countries = Country::find($id); 

        if (empty($countries)) {
            return redirect()->route('countries.index')->with('message', 'Country not found!');
        }  
        $countries->delete();

        return redirect()->back()->with('message', 'Country Deleted');
    }

    public function search(Request $request)
    {   
        $items = Country::paginate('20');
        if(isset($request->name)){
            $items = Country::where('name','LIKE','%'.$request->name.'%')->paginate('20');
        }
        if (request()->ajax()) {
            return view('admin.modules.masters.region.countries.tbody',[
                'items' => $items,
            ]); 
        }
        return view('admin.modules.masters.region.countries.list', [
            'items' => $items,
            'name' => $request->name,
        ]);
    }
}
