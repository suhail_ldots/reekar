<?php

namespace App\Http\Controllers\Admin\Masters;
use App\Http\Controllers\Controller;
use App\Models\ErrorLogs;
use App\Models\Masters\SubscriptionPackages;
use App\Models\Masters\Feature;
use App\Models\Masters\PlanFeature;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class SubscriptionPlansController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $item = SubscriptionPackages::orderBy('id','desc')->paginate(mypagination());
        if (request()->ajax()) {
            return view('admin.modules.masters.subscriptionplans.tbody', [
                'items' => $item,
            ]);
        }
        return view('admin.modules.masters.subscriptionplans.list', [
            'items' => $item,
        ]);
    }

    public function search(Request $request)
    { 
        $item = SubscriptionPackages::query();
        if (isset($request->name)) {
            $item->where('name', 'like', '%' . $request->name . '%');
        }
       
        $newitem = $item->orderBy('id', 'desc')->paginate(mypagination());

        // $newitem->appends(Input::except('page'));

        if (request()->ajax()) {
           return view('admin.modules.masters.subscriptionplans.tbody', ['items' => $newitem]);
        }
        
        return view('admin.modules.masters.subscriptionplans.list', [
            'items' => $newitem,
            'name'  => $request->name,                 
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $features = Feature::all();
        return view('admin.modules.masters.subscriptionplans.add', [
            "item" => false,
            "features" => $features,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255|unique:subscription_packages,name,' . $request->id . ',id,deleted_at,NULL',
            'time_duration' => 'required|numeric|digits_between:2,4',
            'image_count' => 'required|numeric|digits_between:1,4',
            'price' => 'required|numeric|digits_between:1,10',
            'feature_id' =>'required|array|min:1,10',
        ]);

       // return $request->all();
        if ($request->id) {
            $item = SubscriptionPackages::find($request->id);
            $item->name = $request->name;
            $item->time_duration = $request->time_duration;
            $item->image_count = $request->image_count;
            $item->price = $request->price;
            $item->status = $request->status;
            $item->save();
            //update relation
            PlanFeature::where('subscription_package_id', $request->id)->delete();
          
            foreach($request->feature_id as $feature_id){
                $feature_map = new PlanFeature();
                $feature_map->feature_id              = $feature_id;
                $feature_map->subscription_package_id = $request->id;
                $feature_map->save();
            }

            $msg =  "Plan Updated.";
        } else {
            $item = new SubscriptionPackages();
            $item->name = $request->name;
            $item->time_duration = $request->time_duration;
            $item->image_count = $request->image_count;
            $item->price = $request->price;
            $item->status = $request->status;
            $item->save();
            //store relation
            
            foreach($request->feature_id as $feature_id){
                $feature_map = new PlanFeature();
                $feature_map->feature_id              = $feature_id;
                $feature_map->subscription_package_id = $item->id;
                $feature_map->save();
            }
            $msg =  "Plan Added.";
        }

        \Session::flash('message', $msg);
        return response()->json([
            'success' => true,
            'message' => __('l.plans_add'),
            'redirect' => route('plans.index'),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $features = Feature::all();
        $related_features = PlanFeature::where('subscription_package_id', $id)->get(['feature_id']);
       
        if( count($related_features) > 0 )
        foreach($related_features as $val){
           $assigned_features[] = $val->feature_id;
        }
        return view('admin.modules.masters.subscriptionplans.add', [
            "item" => SubscriptionPackages::find($id),
            "features" => $features,
            "assigned_features" => isset($assigned_features) ? $assigned_features :[],
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete($id)
    {
        SubscriptionPackages::find($id)->delete();
        createlogs(ErrorLogs::POSITIIVE, 'Subscription Plan', ErrorLogs::DELETE);
        return redirect()->back()->with('message', __('l.plan_delete'));
    }
}
