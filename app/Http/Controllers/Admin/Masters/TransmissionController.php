<?php

namespace App\Http\Controllers\Admin\Masters;
use App\Http\Controllers\Controller;
use App\Models\Transmissions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
// use App\Imports\BrandImport;
// use Maatwebsite\Excel\Facades\Excel;

class TransmissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Transmissions::orderBy('name')->paginate(mypagination());
        if (request()->ajax()) {
            return view('admin.modules.masters.transmission.tbody', compact('items'));

        }

        return view('admin.modules.masters.transmission.list', [
            'items' => $items,
        ]);
    }
    public function search(Request $request)
    {
        //return "dfsg";
        $item = Transmissions::query();
        if (isset($request->name)) {
            $item->where('name', 'like', '%' . $request->name . '%');
        }
       
        $newitem = $item->orderBy('name')->paginate(mypagination());

        $newitem->appends(request()->input())->links();

        if (request()->ajax()) {
           return view('admin.modules.masters.transmission.tbody', ['items' => $newitem]);
        }
        
        return view('admin.modules.masters.transmission.list', [
            'items' => $newitem,
            'name'  => $request->name,                 
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.modules.masters.transmission.add', [
            "item" => false,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $this->validate($request, [
            'name' => 'required|string|max:255|unique:transmissions,name,' . $request->id . ',id,deleted_at,NULL',
            ]);

        if ($request->id){
            $item = Transmissions::find($request->id);
            $msg =  "Transmission Updated";            
        } else {
            $item = new Transmissions();
            $msg =  "Transmission Added" ;            
        } 
        $item->name = $request->name;
        $item->save();
        \Session::flash('message', $msg);
        return response()->json([
            'success' => true,
            'message' =>  'success',
            'redirect' => route('transmission.index'),
        ]); 
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.modules.masters.transmission.add', [
            "item" => Transmissions::find($id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function status($id, $status)
    {
        $item = Transmissions::where('id', $id)->where('status', $status)->first();        
        if($item){
            $main_status =  ($status == 1) ? '0' : '1';

            $item->status = $main_status; 
            $item->save();
            $msg = ($main_status == 1) ? 'has been Activated.' : 'has been Deactivated.'; 
        }
        return redirect()->back()->with('message', __('l.transmission').' '.$msg);
    }

    public function delete($id)
    {
        Transmissions::find($id)->delete();
        return redirect()->back()->with('message', __('l.transmission').' '.__('l.delete'));
    }

}
