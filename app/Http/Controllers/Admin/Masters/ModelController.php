<?php

namespace App\Http\Controllers\Admin\Masters;
use App\Http\Controllers\Controller;
use App\Models\Models;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
// use App\Imports\BrandImport;
// use Maatwebsite\Excel\Facades\Excel;

class ModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $items = Models::whereHas('brand', function ($query){
        //     $query->whereNull('deleted_at');
        $items = Models::with('brand')->orderBy('name')->paginate(mypagination());

        if (request()->ajax()) {
            return view('admin.modules.masters.models.tbody', compact('items'));

        }

        return view('admin.modules.masters.models.list', [
            'items' => $items,
        ]);
    }
    //Car Model ASea
    public function search(Request $request)
    {
        //return "dfsg";
        $item = Models::query();
        if (isset($request->name)) {
            $item->where('name', 'like', '%' . $request->name . '%');
        }
        if (isset($request->brand_name)) {
            $item->whereHas('brand', function ($query) use ($request){
                    $query->where('name', 'like', '%' . $request->brand_name . '%');
            });
        }
       
        $newitem = $item->orderBy('name')->paginate(mypagination());

        $newitem->appends(request()->input())->links();

        if (request()->ajax()) {
           return view('admin.modules.masters.models.tbody', ['items' => $newitem]);
        }
        
        return view('admin.modules.masters.models.list', [
            'items' => $newitem,
            'name'  => $request->name,                 
            'brand_name'  => isset($request->brand_name) ? $request->brand_name : '',                 
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.modules.masters.models.add', [
            "item" => false,
        ]);
    }
    public function modelByBrand(Request $request)
    {
        if($request->id){
            $items = Models::where('brand_id', $request->id)->get();
            return json_encode([
                'status' => 1,
                'message' => 'success',
                'data' => $items,
            ]);
        }else{
            return json_encode([
                'status' => 0,
                'message' => 'please select brand',
                'data' => '',
            ]);
        }
        
        
        view('admin.modules.masters.models.add', [
            "item" => false,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $this->validate($request, [
            'name' => 'required|string|max:255|unique:models,name,' . $request->id . ',id,deleted_at,NULL',
            'brand_id' => 'required|numeric|digits_between:1,20',
        ],[],['brand_id' => 'Brand']);

        if ($request->id){
            $item = Models::find($request->id);
            $msg =  "Model Updated";            
        } else {
            $item = new Models();
            $msg =  "Model Added" ;            
        } 
        $item->name = $request->name;
        $item->brand_id = $request->brand_id;
        $item->save();
        \Session::flash('message', $msg);
        return response()->json([
            'success' => true,
            'message' =>  'success',
            'redirect' => route('model.index'),
        ]); 
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.modules.masters.models.add', [
            "item" => Models::find($id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function status($id, $status)
    {
        $item = Models::where('id', $id)->where('status', $status)->first();        
        if($item){
            $main_status =  ($status == 1) ? '0' : '1';

            $item->status = $main_status; 
            $item->save();

            $msg = ($main_status == 1) ? 'has been Activated.' : 'has been Deactivated.'; 
        }
        return redirect()->back()->with('message', __('l.model').' '.$msg);
    }

    public function delete($id)
    {
        Models::find($id)->delete();
        return redirect()->back()->with('message', __('l.model').' '.__('l.delete'));
    }

}
