<?php

namespace App\Http\Controllers\Admin\Masters;
use App\Http\Controllers\Controller;
use App\Models\Specialities;
use App\Models\Models;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
// use App\Imports\BrandImport;
// use Maatwebsite\Excel\Facades\Excel;

class SpecialityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items =Specialities::orderBy('name')->paginate(mypagination());
        //dd($items);
        if (request()->ajax()) {
            return view('admin.modules.masters.speciality.tbody', compact('items'));

        }

        return view('admin.modules.masters.speciality.list', [
            'items' => $items,
        ]);
    }
    public function search(Request $request)
    {
        //return "dfsg";
        $item =Specialities::query();
        if (isset($request->name)) {
            $item->where('name', 'like', '%' . $request->name . '%');
        }
       
        $newitem = $item->orderBy('name')->paginate(mypagination());

        $newitem->appends(request()->input())->links();

        if (request()->ajax()) {
           return view('admin.modules.masters.speciality.tbody', ['items' => $newitem]);
        }
        
        return view('admin.modules.masters.speciality.list', [
            'items' => $newitem,
            'name'  => $request->name,                 
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.modules.masters.speciality.add', [
            "item" => false,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
       /*  return response()->json([
            'success' => false,
            'message' => $request->all(),
            'redirect' => 'JavaScript:Void(0)',
        ]); */
                 

        if ($request->id){
            $this->validate($request, [
                'name' => 'required|string|max:255|unique:specialities,name,' .$request->id. ',id,deleted_at,NULL',
            ]);
            
        } else {
            $this->validate($request, [
                'name' => 'required|string|max:255|unique:specialities,name',
            ]);            
        } 

        if ($request->id){
            $item =  Specialities::find($request->id);
            $msg =  "Speciality Updated";
            
        } else {
            $item = new Specialities();
            $msg =  "Speciality Added" ;
            
        } 
        $item->name = $request->name;
        $item->save();
        \Session::flash('message', $msg);
        return redirect()->route('speciality.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.modules.masters.speciality.add', [
            "item" => Specialities::find($id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    public function status($id, $status)
    {
        $item = Specialities::where('id', $id)->where('status', $status)->first();        
        if($item){
            $main_status =  ($status == 1) ? '0' : '1';

            $item->status = $main_status; 
            $item->save();

            $msg = ($main_status == 1) ? 'has been Activated.' : 'has been Deactivated.'; 
        }
        return redirect()->back()->with('message', __('l.speciality').' '.$msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete($id)
    {
        $items = Specialities::find($id)->delete();
        
        return redirect()->back()->with('message', __('l.speciality').' '.__('l.delete'));
    }

    public function brand_upload()
    {
        return view('admin.modules.masters.speciality.import');
    }

    /* public function brand_import(Request $request)
    {

        $this->validate($request, [
            'file' => 'required|max:10000|mimes:xlsx',
        ]);
        $import = new BrandImport();
        //$import->onlySheets(0);
        Excel::import($import, request()->file('file'));
        return redirect()->back()->with('message', 'Speciality imported!');
    } */
}
