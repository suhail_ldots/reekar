<?php

namespace App\Http\Controllers\Admin\Masters;
use App\Http\Controllers\Controller;
use App\Models\Colors;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
// use App\Imports\BrandImport;
// use Maatwebsite\Excel\Facades\Excel;

class ColorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items =Colors::orderBy('name')->paginate(mypagination());
        if (request()->ajax()) {
            return view('admin.modules.masters.colors.tbody', compact('items'));

        }

        return view('admin.modules.masters.colors.list', [
            'items' => $items,
        ]);
    }
    public function search(Request $request)
    {
        //return "dfsg";
        $item = Colors::query();
        if (isset($request->name)) {
            $item->where('name', 'like', '%' . $request->name . '%');
        }
       
        $newitem = $item->orderBy('name')->paginate(mypagination());

        $newitem->appends(request()->input())->links();

        if (request()->ajax()) {
           return view('admin.modules.masters.colors.tbody', ['items' => $newitem]);
        }
        
        return view('admin.modules.masters.colors.list', [
            'items' => $newitem,
            'name'  => $request->name,                 
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.modules.masters.colors.add', [
            "item" => false,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'name' => 'required|string|max:255|unique:colors,name,' . $request->id . ',id,deleted_at,NULL',
            ]);

        if ($request->id){
            $item = Colors::find($request->id);
            $msg =  "Color Updated";
            
        } else {
            $item = new Colors();
            $msg =  "Color Added" ;
            
        } 
        $item->name = $request->name;
        $item->save();
        \Session::flash('message', $msg);
        return response()->json([
            'success' => true,
            'message' =>  $item,
            'redirect' => route('colors.index'),
        ]); 
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.modules.masters.colors.add', [
            "item" => Colors::find($id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete($id)
    {
        Colors::find($id)->delete();
        return redirect()->back()->with('message', __('l.color').' '.__('l.delete'));
    } 

    public function status($id, $status)
    {
        $item = Colors::where('id', $id)->where('status', $status)->first();        
        if($item){
            $main_status =  ($status == 1) ? '0' : '1';

            $item->status = $main_status; 
            $item->save();

            $msg = ($main_status == 1) ? 'has been Activated.' : 'has been Deactivated.'; 
        }
        return redirect()->back()->with('message', __('l.color').' '.$msg);
    }
    
}
