<?php

namespace App\Http\Controllers\Admin\Masters;

use App\Http\Controllers\Controller;
use App\Imports\BrandsImport;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use DB;

class ImportBrandsModelsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        return view('admin.modules.masters.import.import');
    }

    public function import(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|max:10000|mimes:xlsx,xls',
        ]);
        $import = new BrandsImport();
        //$import->onlySheets(0);
        Excel::import($import, request()->file('file'));

        /* $path = $request->file('file')->getRealPath();

        $data = Excel::load($path)->get();

        if($data->count() > 0){
            foreach($data->toArray() as $key => $value){
                foreach($value as $row){
                    $insert_data[] = array(
                        'name'  => $row['Brand_name'],                     
                    );
                }
            }
            if(!empty($insert_data)){
                DB::table('tbl_customer')->insert($insert_data);
            }
        } */
        return redirect()->back()->with('message', 'Brands and Models imported!');
    }
}
