<?php

namespace App\Http\Controllers\Admin\Masters;
use App\Http\Controllers\Controller;
use App\Models\Masters\Equipments;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
// use App\Imports\BrandImport;
// use Maatwebsite\Excel\Facades\Excel;

class EquipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Equipments::orderBy('name')->paginate(mypagination());
        //dd($items);
        if (request()->ajax()) {
            return view('admin.modules.masters.equipments.tbody', compact('items'));

        }

        return view('admin.modules.masters.equipments.list', [
            'items' => $items,
        ]);
    }
    public function search(Request $request)
    {
        //return "dfsg";
        $item = Equipments::query();
        if (isset($request->name)) {
            $item->where('name', 'like', '%' . $request->name . '%');
        }
       
        $newitem = $item->orderBy('name')->paginate(mypagination());

        $newitem->appends(request()->input())->links();

        if (request()->ajax()) {
           return view('admin.modules.masters.equipments.tbody', ['items' => $newitem]);
        }
        
        return view('admin.modules.masters.equipments.list', [
            'items' => $newitem,
            'name'  => $request->name,                 
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.modules.masters.equipments.add', [
            "item" => false,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
       /*  return response()->json([
            'success' => false,
            'message' => $request->all(),
            'redirect' => 'JavaScript:Void(0)',
        ]); */
          
        $this->validate($request, [
            'name' => 'required|string|max:255|unique:equipments,name,' . $request->id . ',id,deleted_at,NULL',
            ]);

        if ($request->id){
            $item =   Equipments::find($request->id);
            $msg =  "Equipment Updated";
            
        } else {
            $item = new Equipments();
            $msg =  "Equipment Added" ;
            
        } 
        $item->name = $request->name;
        $item->save();
        \Session::flash('message', $msg);
        return redirect()->route('equipments.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.modules.masters.equipments.add', [
            "item" =>  Equipments::find($id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    public function status($id, $status)
    {
        $item =  Equipments::where('id', $id)->where('status', $status)->first();        
        if($item){
            $main_status =  ($status == 1) ? '0' : '1';

            $item->status = $main_status; 
            $item->save();

            $msg = ($main_status == 1) ? 'has been Activated.' : 'has been Deactivated.'; 
        }
        return redirect()->back()->with('message','Equipment '.$msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete($id)
    {
        $items =  Equipments::find($id)->delete();
        
        return redirect()->back()->with('message', 'Equipment '.__('l.delete'));
    }

}
