<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Roles;
use App\User;


class DashboardController extends Controller
{
    
    public function index(){
        if(\Auth::check()){    
            if(\Auth::user()->role_id == 1){
                $user = User::all()->where('role_id', '!=', 2);
                return view('admin.dashboard',[                    
                    "doctors"  => $user->where('role_id', 3)->where('status', 1)->count(),
                    "clinics" => $user->where('role_id', 4)->where('status', 1)->count(),
                    "patients" => $user->where('role_id', 2)->where('status', 1)->count(),
                ]);
            }else{
                \Auth::logout();
                return redirect()->to('/login');
            }            
        }else{
            return redirect()->to('/login');
        }
    }
}
