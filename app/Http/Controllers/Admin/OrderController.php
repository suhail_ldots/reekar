<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Cars;
use App\Models\Orders;
use App\Models\Brands;
use App\Models\Colors;
use App\Models\EngineTypes;
use App\Models\VehicleTypes;
use App\Models\Models;
use App\Models\Transmissions;

use App\Models\CarDocs;
use App\Models\CarImages;
use Auth;
use Hash;
use Illuminate\Http\Request;
use App\Mail\OrderCanceledSellerMail;
use App\Mail\OrderCanceledBuyerMail;

class OrderController extends Controller
{
    //showing listing
    public function index()
    {
        $items = Orders::with(['car','user'])->orderBy('id', 'DESC')->paginate(mypagination());
       
        if (request()->ajax()) {
            return view('admin.modules.orders.tbody', compact('items'));
        }
        return view('admin.modules.orders.list', [
            'items' => $items,
        ]);
    }

    public function search(Request $request)
    {     
        $items = Orders::query();
        
        if(isset($request->hired_by)){
            $items->whereHas('user', function($query) use ($request){
                $query->where('first_name', 'like', '%' . $request->hired_by . '%');
            });           
        }
        if(isset($request->brand)){
            $items->whereHas('car', function($query) use ($request){                
                $query->whereHas('brand', function($query1) use ($request){
                    $query1->where('name', 'like', '%' . $request->brand . '%');
                });               
            });           
        }
        if(isset($request->booking_id)){
            $items->where('booking_id','LIKE','%'.$request->booking_id.'%');
        }
        if(isset($request->order_date)){
            $items->where('updated_at','LIKE','%'.$request->order_date.'%');
        }
        $newItem =  $items->orderBy('id', 'DESC')->paginate(mypagination());         

        if (request()->ajax()) {
            return view('admin.modules.orders.tbody',[
                'items' => $newItem,
            ]); 
        }
        return view('admin.modules.orders.list', [
            'items' => $newItem,
            'name' => $request->hired_by,
            'model' => $request->model,
            'brand' => $request->brand,
            'booking_id' => $request->booking_id,
            'order_date' => $request->order_date,
            
        ]);
    }

    //add list
    public function create()
    {
        return view('admin.modules.orders.add', [
            "item" => false,             
            "ms_users" => \App\User::where(['status' => '1', 'role_id' => 2])->orderBy('first_name')->get(),             
            "ms_brands" => Brands::where('status', '1')->orderBy('name')->get(),             
            "ms_models" => Models::where('status' , '1')->orderBy('name')->get(),             
            "ms_vehicle_types" => VehicleTypes::where('status' , '1')->orderBy('name')->get(),             
            "ms_engine_types" => EngineTypes::where('status' , '1')->orderBy('name')->get(),             
            "ms_transmissions" => Transmissions::where('status' , '1')->orderBy('name')->get(),             
            "ms_colors" => Colors::where('status' , '1')->orderBy('name')->get(),             
        ]);  
    }

    public function orderStatus($id, $order_status)
    {
        if (Auth::user()->role_id != 1) {
            return redirect()->route('order.index')->with('message', __('l.auth-error'));
        }

        $item = Orders::find($id);

        if (empty($item)) {
            return redirect()->route('order.index')->with('message', 'Order not found!');
        }
        
        if ($order_status != 4) {
            $item->order_status = 4;
        }
         
        $item->save();
        $message = $item->order_status == 4 ? 'Order Canceled.' : '';

        \Mail::to($item->user->email)->send(new OrderCanceledBuyerMail($item));
        \Mail::to($item->car->owner->email)->send(new OrderCanceledSellerMail($item));

        $keys = $item->car->owner->devices->pluck('device_token')->toArray();
        //send push notification to Owner
        $msg =  'Dear '.ucfirst($item->car->owner->first_name) .' '.ucfirst($item->car->owner->last_name).'your Booking with Booking Id- '.$item->booking_id.' has been canceled.';
        $action = "Owner";

        foreach($keys as $key){
            fcm_notification($msg, 'Reekar Booking Canceled', $key, $action); 
        }
        
        //send push notification to Buyer
        $keys1 = $item->user->devices->pluck('device_token')->toArray();
       
        $msg =  'Dear '.ucfirst($item->user->first_name) .' '.ucfirst($item->user->last_name).' your Booking with Booking Id- '.$item->booking_id.' has been canceled.';
        $action = "Buyer";

        foreach($keys1 as $key){
            fcm_notification($msg, 'Reekar Booking Canceled', $key, $action); 
        }

        return redirect()->back()->with('message', $message);
    }

    //edit list
   /*  public function edit($id)
    {
        $item = Cars::find($id); 
        return view('admin.modules.orders.add', [
            "item" => $item,
            "ms_users" => \App\User::where(['status' => '1', 'role_id' => 2])->orderBy('first_name')->get(),             
            "ms_brands" => Brands::where('status', '1')->orderBy('name')->get(),             
            "ms_models" => Models::where('status' , '1')->orderBy('name')->get(),             
            "ms_vehicle_types" => VehicleTypes::where('status' , '1')->orderBy('name')->get(),             
            "ms_engine_types" => EngineTypes::where('status' , '1')->orderBy('name')->get(),             
            "ms_transmissions" => Transmissions::where('status' , '1')->orderBy('name')->get(),             
            "ms_colors" => Colors::where('status' , '1')->orderBy('name')->get(),             
        ]);
    } */

    //delete list
    /* public function delete($id)
    {  
        if (Auth::user()->role_id != 1) {
            return redirect()->route('car.index')->with('message', __('l.auth-error'));
        } 
        $item = Cars::find($id);

        if (empty($item)) {
            return redirect()->route('car.index')->with('message', 'Car not found!');
        }  
        $item->delete();

        return redirect()->back()->with('message', 'Car Deleted');
    }
 */
    public function show($id)
    { 
        $Orders = Orders::find($id);
        if (empty($Orders)) {
            return redirect()->route('order.index')->with('message', 'Order not found!');
        } 
        return view('admin.modules.orders.view', [
            "item" => $Orders,           
        ]);
    }

}
