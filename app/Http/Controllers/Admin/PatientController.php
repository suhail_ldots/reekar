<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Hash;
use Illuminate\Http\Request;

use DB;

class PatientController extends Controller
{
    //showing listing
    public function index()
    {
        if (Auth::user()->role_id != 1) {
            return redirect()->back()->with('message', "Sorry, you don't have permission to access this page.");
        } 
        $items = User::where('role_id', 2)->orderBy('id', 'desc')->paginate(mypagination());
       
        if (request()->ajax()) {
            return view('admin.modules.patients.tbody', compact('items'));
        }
        return view('admin.modules.patients.list', [
            'items' => $items,
        ]);
    }

    public function search(Request $request)
    {     
        $items = User::query();
        $items->where('role_id', 2);

        if(isset($request->name)){
            $items->where('first_name','LIKE','%'.$request->name.'%');
        }
        if(isset($request->email)){
            $items->where('email','LIKE','%'.$request->email.'%');
        }
        $newItem =  $items->orderBy('id', 'desc')->paginate(mypagination());         

        if (request()->ajax()) {
            return view('admin.modules.patients.tbody',[
                'items' => $newItem,
            ]); 
        }
        return view('admin.modules.patients.list', [
            'items' => $newItem,
            'name' => $request->name,
            'email' => $request->email,
        ]);
    }

    //add list
    public function create()
    {
        return view('admin.modules.patients.add', [
            "item" => false,             
        ]);  
    }

    public function status($id, $status)
    {
        
        $item = User::find($id);

        if (empty($item)) {
            return redirect()->route('patient.index')->with('message', 'Patient not found!');
        }
        if ($item->role_id != 2) {
            return redirect()->route('patient.index')->with('message', "Sorry, you don't have permission to access this user.");
        }
        
        if ($status == 1) {
            $item->status = '0';

            if (\DB::table('oauth_access_tokens')->where('user_id', $item->id)->first()) {
                foreach (\DB::table('oauth_access_tokens')->where('user_id', $item->id)->get() as $token) {
                    \DB::table('oauth_refresh_tokens')
                        ->where('access_token_id', $token->id)
                        ->update([
                            'revoked' => true,
                        ]);

                        $token = \DB::table('oauth_access_tokens')->where('id', $token->id)->update([
                            'revoked' => true,
                        ]);
                }
            }

        } else {
            $item->status = '1';
        }
        $item->save();
        $message = $item->status == 1 ? 'User Activated.' : 'User Deactivated.';

        return redirect()->back()->with('message', $message);
    }

    //strore data
    public function store(Request $request)
    { 
        if (Auth::user()->role_id != 1) {
            return redirect()->route('patient.index')->with('message', "Sorry, you don't have permission  to create any user.");
        } 
        $email_regex = '/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix';

        $this->validate($request, [
            'email' => 'required|string|email|regex:'.$email_regex.'|max:255|unique:users,email,' . $request->id . ',id',
            'first_name' => 'required|regex:/^[a-zA-Z ]+$/u|max:255',
            'last_name' => 'nullable|regex:/^[a-zA-Z ]+$/u|max:255',
            'b_group' => 'nullable|string|max:12',
            'country_code' => 'required|string|max:6',
            'profile_img' => 'nullable|image|mimes:jpeg,png,jpg|max:2048',
            'mobile' => 'required|string|max:25',             
            'address' => 'required|max:255',  
           
            'gender' => 'required|string|max:20',
            'age' => 'required|integer|digits-between:1,3',                      
          
            'country' => 'required|numeric',
            'city' => 'nullable|string|max:255',
        ],
        ['profile_img.max'=>"Profile Pic can not be greater than 2MB."],
        [
            'profile_img' => 'Profile Pic',
            'b_group' => 'Blood Group',
        ]            
        );
        
        if(!$request->id){
            $this->validate($request, [
                'password' => 'required|string|min:8|max:20|confirmed',
                'password_confirmation' => 'required|string|min:8|max:20',
            ],[
                'password.min' => 'Your password must be more than 8 characters',
            ]);
        }

        if ($request->id) {
            $item = User::find($request->id);

            if (empty($item)) {
                return redirect()->route('patient.index')->with('message', 'Patient not found!');
            } 
            if($item->email_verified_at == '') { 
                $item->email_verified_at =  date('Y-m-d H:i:s');
            }     
            
            $msg = 'User Updated.';

        } else {            
            $item = new User();
            $item->role_id      = 2;
            $item->status       = 1;
            $item->email_verified_at = date('Y-m-d H:i:s');
            $item->password          = Hash::make($request->password);

            $msg = 'User Added.';
        }

        if($request->hasFile('profile_img')){
            $image = $request->file('profile_img');
            $imageName = time() . rand() . '.' . $image->getClientOriginalExtension();
            
            $folderPath = 'storage/app/images/doctor/profile';
            // $folderPath = 'public/profileImages';
            if(!file_exists($folderPath)) {
                mkdir($folderPath, 0777, true);
            }
            $image->move($folderPath, $imageName);
            $path = $imageName; 
            $item->profile_pic = $path;
        }

            $item->first_name   = $request->first_name;
            $item->last_name    = $request->last_name;
            //$item->blood_group  = $request->b_group;
            $item->email        = $request->email;
            $item->mobile       = $request->mobile;
            $item->country_code = $request->country_code;
            $item->address      = $request->address;
            $item->city         = $request->city;
            $item->country      = $request->country;
            $item->blood_group  = $request->b_group;
            $item->age          = $request->age;
            $item->gender       = $request->gender;
        $item->save(); 
        
        \Session::flash('message', $msg);
        return response()->json([
            'success' => true,
            'message' => 'Success',
            'redirect' => route('patient.index'),
        ]);
    }

    //edit list
    public function edit($id)
    {
        $item = User::find($id); 
        return view('admin.modules.patients.add', [
            "item" => $item,            
        ]);
    }

    //delete list
    public function delete($id)
    {  
        if (Auth::user()->role_id != 1) {
            return redirect()->route('patient.index')->with('message', __('l.auth-error'));
        } 
        $item = User::find($id);

        if (empty($item)) {
            return redirect()->route('patient.index')->with('message', 'Patient not found!');
        }  
        if ($item->role_id != 2) {
            return redirect()->route('patient.index')->with('message', "Sorry, you don't have permission to access this user.");
        }
        if (\DB::table('oauth_access_tokens')->where('user_id', $item->id)->first()) {
            foreach (\DB::table('oauth_access_tokens')->where('user_id', $item->id)->get() as $token) {
                \DB::table('oauth_refresh_tokens')
                    ->where('access_token_id', $token->id)
                    ->update([
                        'revoked' => true,
                    ]);

                    $token = \DB::table('oauth_access_tokens')->where('id', $token->id)->update([
                        'revoked' => true,
                    ]);
            }
        }

        $item->delete();

        return redirect()->back()->with('message', 'User Deleted');
    }

    public function show($id)
    { 
        $user = User::find($id);
        if (empty($user)) {
            return redirect()->route('patient.index')->with('message', 'Patient not found!');
        } 
        if ($user->role_id != 2) {
            return redirect()->route('patient.index')->with('message', "Sorry, you don't have permission to access this user.");
        }
        return view('admin.modules.patients.view', [
            "item" => $user,
        ]);
    }

}
