<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Cars;
use App\Models\Orders;
use App\Models\Brands;
use App\Models\Colors;
use App\Models\EngineTypes;
use App\Models\VehicleTypes;
use App\Models\Models;
use App\Models\Transmissions;

use App\Models\CarDocs;
use App\Models\CarImages;
use Auth;
use Hash;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    //showing listing
    public function index()
    {
        $items = Orders::where('order_status', 2)->with(['car','user'])->orderBy('id', 'DESC')->paginate(mypagination());
       
        if (request()->ajax()) {
            return view('admin.modules.account.tbody', compact('items'));
        }
        return view('admin.modules.account.list', [
            'items' => $items,
        ]);
    }

    public function search(Request $request)
    {     
        $items = Orders::query();
        
        if(isset($request->hired_by)){
            $items->whereHas('user', function($query) use ($request){
                $query->where('first_name', 'like', '%' . $request->hired_by . '%');
            });           
        }
        if(isset($request->brand)){
            $items->whereHas('car', function($query) use ($request){                
                $query->whereHas('brand', function($query1) use ($request){
                    $query1->where('name', 'like', '%' . $request->brand . '%');
                });               
            });           
        }
        if(isset($request->booking_id)){
            $items->where('booking_id','LIKE','%'.$request->booking_id.'%');
        }
        if(isset($request->order_date)){
            $items->where('updated_at','LIKE','%'.$request->order_date.'%');
        }
        $newItem =  $items->where('order_status', 2)->orderBy('id', 'DESC')->paginate(mypagination());         

        if (request()->ajax()) {
            return view('admin.modules.account.tbody',[
                'items' => $newItem,
            ]); 
        }
        return view('admin.modules.account.list', [
            'items' => $newItem,
            'name' => $request->hired_by,
            'model' => $request->model,
            'brand' => $request->brand,
            'booking_id' => $request->booking_id,
            'order_date' => $request->order_date,
            
        ]);
    }

    //add list
    public function create()
    {
        return view('admin.modules.account.add', [
            "item" => false,             
            "ms_users" => \App\User::where(['status' => '1', 'role_id' => 2])->orderBy('first_name')->get(),             
            "ms_brands" => Brands::where('status', '1')->orderBy('name')->get(),             
            "ms_models" => Models::where('status' , '1')->orderBy('name')->get(),             
            "ms_vehicle_types" => VehicleTypes::where('status' , '1')->orderBy('name')->get(),             
            "ms_engine_types" => EngineTypes::where('status' , '1')->orderBy('name')->get(),             
            "ms_transmissions" => Transmissions::where('status' , '1')->orderBy('name')->get(),             
            "ms_colors" => Colors::where('status' , '1')->orderBy('name')->get(),             
        ]);  
    }

    public function ownerPayment($id, $paid_status)
    {
        
        $item = Orders::find($id);
        if (empty($item)) {
            return redirect()->route('account.index')->with('message', 'Transaction not found!');
        }
        
        if ($paid_status == 1) {
            $item->paid_to_owner_status = '0';
        }
        elseif ($paid_status == 0) {
            $item->paid_to_owner_status = '1';
        }
        $item->save();
        $message = $item->paid_to_owner_status == 1 ? 'Payment Cleared.' : 'Payment Not Cleared.';

        return redirect()->back()->with('message', $message);
    }

    //edit list
   /*  public function edit($id)
    {
        $item = Cars::find($id); 
        return view('admin.modules.account.add', [
            "item" => $item,
            "ms_users" => \App\User::where(['status' => '1', 'role_id' => 2])->orderBy('first_name')->get(),             
            "ms_brands" => Brands::where('status', '1')->orderBy('name')->get(),             
            "ms_models" => Models::where('status' , '1')->orderBy('name')->get(),             
            "ms_vehicle_types" => VehicleTypes::where('status' , '1')->orderBy('name')->get(),             
            "ms_engine_types" => EngineTypes::where('status' , '1')->orderBy('name')->get(),             
            "ms_transmissions" => Transmissions::where('status' , '1')->orderBy('name')->get(),             
            "ms_colors" => Colors::where('status' , '1')->orderBy('name')->get(),             
        ]);
    } */

    //delete list
    /* public function delete($id)
    {  
        if (Auth::user()->role_id != 1) {
            return redirect()->route('car.index')->with('message', __('l.auth-error'));
        } 
        $item = Cars::find($id);

        if (empty($item)) {
            return redirect()->route('car.index')->with('message', 'Car not found!');
        }  
        $item->delete();

        return redirect()->back()->with('message', 'Car Deleted');
    }
 */
    public function show($id)
    { 
        $Orders = Orders::find($id);
        if (empty($Orders)) {
            return redirect()->route('order.index')->with('message', 'Order not found!');
        } 
        return view('admin.modules.account.view', [
            "item" => $Orders,           
        ]);
    }

}
