<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use App\Models\NursingHome;
use Auth;
use Hash;
use App\Models\SubscriptionOrders;
use App\Models\Masters\SubscriptionPackages;
use App\Models\DoctorNursingHomeRelation;
use App\Models\Doctor;
use App\Models\GalleryImages;
use App\Mail\NursingHomeSignup;
use App\Mail\SubscribedPackage;
use App\Mail\WelcomeToDoctorsLoop;
use Illuminate\Http\Request;
use DB;

class NursingHomeController extends Controller
{
    //showing listing
    public function index()
    {
        if (Auth::user()->role_id != 1) {
            return redirect()->back()->with('message', 'Sorry, you are not allowed to create any user.');
        }
        $nh = NursingHome::where('status', 1)->get(['user_id']);
        $items = User::whereIn('id', $nh)->orderBy('id', 'desc')->paginate(mypagination());
        // $items = NursingHome::orderBy('id', 'DESC')->paginate(mypagination());
       
        if (request()->ajax()) {
            return view('admin.modules.nursinghomes.tbody', compact('items'));
        }
        return view('admin.modules.nursinghomes.list', [
            'items' => $items,
        ]);
    }

    public function search(Request $request)
    {    
        $nh = NursingHome::where('status', 1)->get(['user_id']);

        $items = User::query();
        $items->whereIn('id', $nh);

        if(isset($request->name)){
            $items->where('first_name','LIKE','%'.$request->name.'%');
        }
        if(isset($request->email)){
            $items->where('email','LIKE','%'.$request->email.'%');
        }
        $newItem =  $items->orderBy('id', 'desc')->paginate(mypagination());         

        if (request()->ajax()) {
            return view('admin.modules.nursinghomes.tbody',[
                'items' => $newItem,
            ]); 
        }
        return view('admin.modules.nursinghomes.list', [
            'items' => $newItem,
            'name' => $request->name,
            'email' => $request->email,
        ]);
    }

    //add list
    public function create()
    {
        return view('admin.modules.nursinghomes.add', [
            "item" => false,             
        ]);  
    }

    public function status($id, $status)
    {
        
        $item = User::find($id);
        if (empty($item)) {
            return redirect()->back()->with('message', 'Nursing home not found!');
        }
        
        if ($status == 1) {
            $item->status = '0';

            if (\DB::table('oauth_access_tokens')->where('user_id', $item->id)->first()) {
                foreach (\DB::table('oauth_access_tokens')->where('user_id', $item->id)->get() as $token) {
                    \DB::table('oauth_refresh_tokens')
                        ->where('access_token_id', $token->id)
                        ->update([
                            'revoked' => true,
                        ]);

                        $token = \DB::table('oauth_access_tokens')->where('id', $token->id)->update([
                            'revoked' => true,
                        ]);
                }
            }

        } else {
            $item->status = '1';
        }
        $item->save();
        $message = $item->status == 1 ? 'Nursing Home Activated.' : 'Nursing Home Deactivated.';

        return redirect()->back()->with('message', $message);
    }

    //strore data
    public function store(Request $request)
    { 
       
        if (Auth::user()->role_id != 1) {
            return redirect()->route('user.index')->with('message', 'Sorry, you are not allowed to create any user.');
        }
        $plan = SubscriptionPackages::where('price', 0)->first();
       /*  return response()->json([
            'success' => FALSE,
            'message' => $plan,
            'redirect' => 'javasript:void(0)',
        ]); */
        $email_regex = '/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix';
        $this->validate($request, [
            'email' => 'required|string|email|regex:'.$email_regex.'|max:255|unique:users,email,' . $request->id . ',id',
            'first_name' => 'required|regex:/^[a-zA-Z ]+$/u|max:255',
            'last_name' => 'nullable|regex:/^[a-zA-Z ]+$/u|max:255',
            //'b_group' => 'nullable|string|max:3',
            'country_code' => ['required'],
            'country' => 'required|numeric',
            'user_type' => 'required|integer',
            'city' => 'nullable|max:200', 
            'about_nursing' => 'required|string', 
           
            'profile_img' => 'nullable|image|mimes:jpeg,png,jpg|max:2048',
            'mobile' => 'required|string|max:25',             
            'address' => 'nullable|max:255',  
        ],
        ['profile_img.max'=>"Profile image can not be greater than 2MB."],
        ['profile_img' => 'Logo',
        'user_type' => 'Account Type',
        'about_nursing' => 'About Nursing Home',
        ]            
        );
        
        if(!$request->id){
            $this->validate($request, [
                'password' => 'required|string|min:8|max:20|confirmed',
                'password_confirmation' => 'required|string|min:8|max:20',
            ],[
                'password.min' => 'Your password must be more than 8 characters',
            ]);
        }

        if ($request->id) {
            $item = User::find($request->id);

            if (empty($item)) {
                return redirect()->route('nursinghome.index')->with('message', 'Nursing home not found!');
            } 
          
            $item->first_name   = $request->first_name;
            $item->last_name    = $request->last_name;
            //$item->blood_group  = $request->b_group;
            $item->email        = $request->email;
            $item->mobile       = $request->mobile;
            $item->country_code = $request->country_code;
            $item->country = $request->country;
            $item->city = $request->city;
            $item->lat = $request->lat;
            $item->long = $request->long;
            $item->address      = $request->address;
            if($item->email_verified_at == '') { 
                $item->email_verified_at =  date('Y-m-d H:i:s');
            }
            
            $msg = 'Nursing Home Updated.';

            if($request->hasFile('profile_img')){
                $image = $request->file('profile_img');
                $imageName = time() . rand() . '.' . $image->getClientOriginalExtension();
                
                $folderPath = 'storage/app/images/doctor/profile/';
                // $folderPath = 'public/profileImages';
                if(!file_exists($folderPath)) {
                    mkdir($folderPath, 0777, true);
                }
                $image->move($folderPath, $imageName);
                $path = $imageName;            
                
                $item->profile_pic = $path;
            }
    
            $item->save(); 
            //Update Nursing Home
            $nursing_home = NursingHome::where('user_id', $item->id)->first(); //from Nursing Home table
            $nursing_home->organization_name	 = $request->first_name.' '.$request->last_name;
            $nursing_home->about_nursing_home	 = $request->about_nursing;
            $nursing_home->save();
            /*crete gallery images*/
            if(!empty($request->input('document')))
            foreach ($request->input('document', []) as $file) {
                $gallery = new GalleryImages();
                $gallery->nursing_home_id = $item->nursing_home_id;
                $gallery->file_url = $file;
                $gallery->created_by = Auth::user()->id;
                $gallery->save();

                // $project->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('document');
            }
            /* end gallery images*/

        } else {            
            $item = new User();
            $item->first_name   = $request->first_name;
            $item->last_name    = $request->last_name;
           // $item->blood_group  = $request->b_group;
            $item->email        = $request->email;
            $item->mobile       = $request->mobile;
            $item->country_code = $request->country_code;
            $item->address      = $request->address;
            $item->role_id      = $request->user_type;
            $item->status       = 1;
           
            $item->email_verified_at = date('Y-m-d H:i:s');
            $item->password     = Hash::make($request->password);
           
            $item->country = $request->country;
            $item->city = $request->city;
            $item->lat = $request->lat;
            $item->long = $request->long;

            if($request->hasFile('profile_img')){
                $image = $request->file('profile_img');
                $imageName = time() . rand() . '.' . $image->getClientOriginalExtension();
                
                $folderPath = 'storage/app/images/doctor/profile/';
                // $folderPath = 'public/profileImages';
                if(!file_exists($folderPath)) {
                    mkdir($folderPath, 0777, true);
                }
                $image->move($folderPath, $imageName);
                $path = $imageName;            
                
                $item->profile_pic = $path;
            }

            if($item->save()){
                $payment = new SubscriptionOrders();//
                $payment->email = $request->email;
                // $payment->nursing_home_id = $request->nursing_home_id;
                $payment->booking_id = 'NH'.date('d').time().rand();
                $payment->paid_amount = 0;
                $payment->grand_total = 0;
                $payment->payment_gateway = 'Stripe';
                $payment->bank_ref_id    = $plan->price != 0 && $plan->price != '' ? $data->id :'notavailable' ;
                $payment->transaction_id = $plan->price != 0 && $plan->price != '' ? $data->balance_transaction :'notavailable' ;;
                $payment->card_no        = $plan->price != 0 && $plan->price != '' ? $request->card_no :'notavailable' ;
                $payment->exp_date       = $plan->price != 0 && $plan->price != '' ? $request->card_exp_year.'-'.$request->card_exp_month.'-'.'00' :'notavailable' ; 
                $payment->card_type	     = ($plan->price != 0 && $plan->price != '') && $data->payment_method_details->type.' '.($data->payment_method_details->type == 'card') ? $data->payment_method_details->card->brand .' '.$data->payment_method_details->card->funding.' card':''; 
                $payment->order_status	 = 1;
                $payment->plan_exp	     = date('Y-m-d', strtotime('+1 month'));
                $payment->stripe_invoice   = $plan->price != 0 && $plan->price != '' ? $data->receipt_url :'notavailable' ;
    
                if($payment->save()){
                    $nursing_home = new NursingHome();
                    $nursing_home->user_id = $item->id; //from Nursing Home table
                    $nursing_home->subscription_package_id = $plan->id; //$request->package_id;
                    $nursing_home->organization_name	 = $request->first_name.' '.$request->last_name;
                    $nursing_home->about_nursing_home	 = $request->about_nursing;
                    // $nursing_home->registration_no	 = ;
                    $nursing_home->status	 = 1;
                    $nursing_home->save();
    
                    //update subscription orders table and users table
                    $item->nursing_home_id = $nursing_home->id ;
                    $item->save();
                    //update payment
                    $payment->nursing_home_id = $nursing_home->id;
                    $payment->save();
                    
                    if($request->user_type == 3){
                        $doctor = new Doctor();
                        $doctor->user_id = $item->id;
                        $doctor->save();
    
                        //Doctor Nursing Relation
                        $doctorNursingRelation = new DoctorNursingHomeRelation();
                        $doctorNursingRelation->doctor_id = $doctor->id;
                        $doctorNursingRelation->nursing_home_id = $nursing_home->id;
                        $doctorNursingRelation->status = 1;
                        $doctorNursingRelation->save();
                    }

                    /*crete gallery images*/
                    if(!empty($request->input('document')))
                    foreach ($request->input('document', []) as $file) {
                        $gallery = new GalleryImages();
                        $gallery->nursing_home_id = $nursing_home->id;
                        $gallery->file_url = $file;
                        $gallery->created_by = Auth::user()->id;
                        $gallery->save();

                        // $project->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('document');
                    }
                    /* end gallery images*/
                    $user['plan_name'] = $plan->name;
                    $user['temp_password'] = $request->password;
                    foreach($plan->features as $feature){
                        $fitem = getFeature_byid($feature->feature_id);
                        $user['plan_feature_name'][] = $fitem->count.' '. $fitem->name;
                    }
                    
                    try {
                        \Mail::to($request->email)->send(new WelcomeToDoctorsLoop($user));
                        //createlogs(ErrorLogs::POSITIIVE, $user_type, ErrorLogs::WELCOME_MAIL);
                    } catch (\Exception $e) {
                        $email_error = $e; // Get error here
                        //createlogs(ErrorLogs::POSITIIVE, $user_type, ErrorLogs::WELCOME_MAIL_FAILED);
                    }
                    if ($request->user_type == 3) {
                        $usertype = 'Single Doctor Clinic';
                    } elseif($request->user_type == 4) {
                        $usertype = 'Multispecialty Hospital';
                    }
    
                    try {
                        \Mail::to($maintainer->email)->send(new NursingHomeSignup($usertype));
                        // createlogs(ErrorLogs::POSITIIVE, ErrorLogs::POSITIIVE, "New .$usertype. Signup mail Notification sended to admin" );
                    } catch (\Exception $e) {
                        $email_error = 'Failed to authenticate on SMTP server with username \"apikey\" using 2 possible authenticators. Authenticator LOGIN returned Expected response code 250 but got an empty response. Authenticator PLAIN returned Expected response code 250 but got an empty response.'; // Get error here
                        //createlogs(ErrorLogs::POSITIIVE, $user_type, "New .$user_type. Signup mail Notification can't send to admin");
                    }
                   
                }else{
                    User::where('id', $item->id)->delete();
                    return redirect()->back()->with('message', "Sorry, you can not subscribe this package right now. Please try again.");
    
                }
    
            }else{
                return redirect()->back()->with('message', "Sorry, you can not subscribe this package right now. Please try again.");
    
            }
            $msg = 'Nursing Home Added.';
        }
        
        \Session::flash('message', $msg);

        return response()->json([
            'success' => true,
            'message' => 'Success',
            'redirect' => route('nursinghome.index'),
        ]);
    }

    //edit list
    public function edit($id)
    {
        $item = User::find($id);
        $item->user_type = $item->role_id ;
        return view('admin.modules.nursinghomes.add', [
            "item" => $item,            
        ]);
    }

    //delete list
    public function delete($id)
    {  
        if (Auth::user()->role_id != 1) {
            return redirect()->route('user.index')->with('message', __('l.auth-error'));
        } 
        $item = User::find($id);

        if (empty($item)) {
            return redirect()->route('user.index')->with('message', 'Nursing home not found!');
        }  

        if (\DB::table('oauth_access_tokens')->where('user_id', $item->id)->first()) {
            foreach (\DB::table('oauth_access_tokens')->where('user_id', $item->id)->get() as $token) {
                \DB::table('oauth_refresh_tokens')
                    ->where('access_token_id', $token->id)
                    ->update([
                        'revoked' => true,
                    ]);

                    $token = \DB::table('oauth_access_tokens')->where('id', $token->id)->update([
                        'revoked' => true,
                    ]);
            }
        }

        $item->delete();

        return redirect()->back()->with('message', 'Nursing Home Deleted');
    }

    public function show($id)
    { 
        $user = User::find($id);
        if (empty($user)) {
            return redirect()->route('user.index')->with('message', 'Nursing home not found!');
        }
        
        $nursing_home = DoctorNursingHomeRelation::where('nursing_home_id', $user->nursing_home_id)->get(['doctor_id']);
        
        $doctors = User::whereHas('doctor',function($query) use ($nursing_home){
            $query->whereIn('id', $nursing_home);
        });
        $alldoctors = $doctors->orderBy('id', 'desc')->paginate(mypagination()); 
        // dd($alldoctors);
        if (request()->ajax()) {
            return view('admin.modules.nursinghomes.view_tbody',[
                "items" => $alldoctors,
            ]);
        }
        return view('admin.modules.nursinghomes.view', [
            "item" => $user,
            "items" => $alldoctors,
        ]);
    }
    public function createGalleryImage(Request $request){
        //Temprary Store Images on server
        $path = storage_path('gallery/uploads');

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $file = $request->file('file');

        $name = uniqid() . '_' . trim(str_replace(' ','', $file->getClientOriginalName()));

        $file->move($path, $name);

        return response()->json([
            'name'          => $name,
            'original_name' => $file->getClientOriginalName(),
        ]);
    }

    function fetch(Request $request)
    {
        //  $images = \File::allFiles(storage_path('tmp/uploads'));
        //  $images = \File::allFiles(public_path('images'));
        $images = GalleryImages::all()->where('nursing_home_id', $request->nhid);
        $output = '<div class="row">';
        if(!empty($images))
        foreach($images as $image)
        {
        $output .= '
        <div class="col-md-3" style="margin-bottom:16px;" align="center">
                    <img src="'.asset('storage/gallery/uploads').'/'. $image->file_url.'" class="img-thumbnail" width="175" height="175" style="height:175px;" />
                                    <button type="button" class="btn btn-link remove_image" id="'.$image->id.'">Remove</button>
                </div>
        ';
        /* $output .= '
        <div class="col-md-2" style="margin-bottom:16px;" align="center">
                    <img src="'.storage_path('tmp/uploads').'/'. $image->getFilename().'" class="img-thumbnail" width="175" height="175" style="height:175px;" />
                    <button type="button" class="btn btn-link remove_image" id="'.$image->getFilename().'">Remove</button>
                </div>
        '; */
        }
        $output .= '</div>';
        return  count($images) > 0 ? $output  : '';
    }

    function deleteImage(Request $request)
    {
        if($request->get('name'))
        {
            GalleryImages::find($request->get('name'))->delete();

            return "success";
        }else{
            return "failed";
        }
     /*    if($request->get('name'))
        {
        \File::delete(public_path('images/' . $request->get('name')));
        } */
    }

}
