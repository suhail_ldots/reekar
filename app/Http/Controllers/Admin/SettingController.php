<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use App\Models\Settings;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dd("dfwf");
        $items = Settings::first();
        return view('admin.modules.setting.add',[
            'item' => $items,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function store(Request $request)
    {
         
            $this->validate($request, [
                'logo'     => 'nullable|image|mimes:jpeg,png,jpg|max:8210',
                'fav_icon' => 'nullable|image|mimes:jpeg,png,jpg|max:1024',
                'email'    => 'required|string|email|max:255',
                'data_per_page' => 'required|numeric|max:50',
                'contact_person' => 'required|regex:/^[a-zA-Z ]+$/u|max:255',
                'company_name' => 'required|string|max:255',
                'contact_no' => 'required|string|max:25',
                'alternate_contact_no' => 'nullable|string|max:25',
                'country_code' => 'required|string|max:20',              
                'company_full_address' => 'required|max:400',  
            ]); 

            $item = Settings::find($request->id);             
            $item->data_per_page = $request->data_per_page;
            $item->company_name = $request->company_name;           
            $item->logo         = $request->logo;           
            $item->fav_icon     = $request->fav_icon;           
            $item->email        = $request->email;
            $item->contact_person = $request->contact_person; 
            $item->country_code   = $request->country_code; 
            $item->contact_no     = $request->contact_no; 
            $item->alternate_contact_no = $request->alternate_contact_no; 
            $item->company_full_address = $request->company_full_address;
            
            if ($request->hasFile('logo')) {
                $image = $request->file('logo');
                $imageName = time() . rand() . '.logo' . $image->getClientOriginalExtension();
                
                $folderPath = 'public/SettingImages';
                if(!file_exists($folderPath)) {
                    mkdir($folderPath, 0777, true);
                } 
                //$filePath = 'Setting_images/' . $imageName;
                //$folderPath = public_path('assets/main/SettingImages');
               
                $image->move($folderPath, $imageName);                
                $path = $folderPath . '/' . $imageName;
                //return $path;
                $item->logo =  $path;
                //fetching path will be url('/').'/'
            }
            if ($request->hasFile('fav_icon')) {
                $image = $request->file('fav_icon');
                $imageName = time() . rand() . '.fav_icon' . $image->getClientOriginalExtension();
                
                $folderPath = 'public/SettingImages';
                if(!file_exists($folderPath)) {
                    mkdir($folderPath, 0777, true);
                } 
                //$filePath = 'Setting_images/' . $imageName;
                //$folderPath = public_path('assets/main/SettingImages');
               
                $image->move($folderPath, $imageName);                
                $path = $folderPath . '/' . $imageName;
                //return $path;
                $item->fav_icon =  $path;
                //fetching path will be url('/').'/'
            }
            
            $item->save();    
         
        \Session::flash('message', 'Setting Updated.');        
        return response()->json([
            'success' => true,
            'message' => 'Saved',
            'redirect' => route('settings.index'),
        ]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.modules.setting.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function action(Request $request)
    {
        //return  $usrid = Auth::user()->id;
        $validation = Validator::make($request->all(), [
            'select_file' => 'required|image|mimes:jpeg,png,jpg|max:8210',
        ], [], ['select_file' => 'Image file']);
        if ($validation->passes()) {
            $image = $request->file('select_file');
            $imageName = time() . rand() . '.' . $image->getClientOriginalExtension();
            
            if(!file_exists('public/SettingImages')) {
                mkdir('public/SettingImages', 0777, true);
            }
             
            //$filePath = 'Setting_images/' . $imageName;
            //$folderPath = public_path('assets/main/SettingImages');
            $folderPath = 'public/SettingImages';
            $image->move($folderPath, $imageName);
            //file_put_contents($path, $image);
            $path = url('/') . '/' . $folderPath . '/' . $imageName;
            //return $path;

        } else {
            return response()->json([
                'message' => $validation->errors()->all(),
                'uploaded_image' => '',
                'class_name' => 'alert-danger',
            ]);
        }

        if (isset($request->uid)) {
            $user = User::findOrFail($request->uid);
            $user->Setting_pic = $path;
            $user->save(); 
            return response()->json([
                'status' => 200,
                'message' => 'Image Uploaded Successfully',
                'uploaded_image' => '<img src="$path" class="img-thumbnail" width="300" />',
                'class_name' => 'alert-success',
            ]);
        } else { 
            return response()->json([
                'message' => $validation->errors()->all(),
                'uploaded_image' => '',
                'class_name' => 'alert-danger',
            ]);
        }
    }
}
