<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /* public function __construct()
    {
        $this->middleware('verified');
    } */

    public function index(){
        if(Auth::check()){
          
            //dd(\Session::get('riders_admin_timezone'));
            $user = Auth::user();
            /* Auth::logout();
            return back(); */
            //dd($user);
            if(Auth::user()->role_id == 1){
               
                return redirect()->route('admin.panel');
                //return view('admin.dashboard');

            }else if(Auth::user()->role_id == 3){               
                return redirect()->route('doctor.profile.view')->with('message', 'Login Successful.');
                // return redirect()->route('doctor.dashboard')->with('message', 'Login Successful.');
                
                /* if(\Session::has('user_email_otp'))
                {                    
                    \Session::forget('user_email_otp');
                    return redirect()->route('main')->with('message', "Registered Successfully.");
                }
                return view('customer.dashboard'); */

            }elseif(Auth::user()->role_id == 4 ){ 
                
                return redirect()->route('nursing.dashboard')->with('message', 'Login Successful.');            
            
            }elseif(Auth::user()->role_id == 2 ){ 
                //patient
                if(\Session::has('user_signup') && \Session::get('user_signup') == 'patient'){
                    \Session::forget('user_signup');
                    \Session::flash('message', "Registered Successfully.");
                    return response()->json([
                        'success' => true,
                        'redirect' => route('patient.dashboard'),
                         //'message' =>  $item,
                    ]);
                }elseif(\Session::has('doctor_booking')){

                    $sessionRoute = \Session::get('doctor_booking');
                    \Session::forget('doctor_booking');
                    return redirect()->to($sessionRoute);            
                
                }else{
                    return redirect()->route('patient.dashboard')->with('message', 'Login Successful.');            
                }
                 
            
            }else{   
                Auth::logout();
                return redirect()->back()->with('message', 'Sorry, You are not authorized to access this page.');
            }

        }else { 
            return redirect()->to('/');
        }
    }
}
