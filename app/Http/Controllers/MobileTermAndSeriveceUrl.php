<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\TermAndConditions;
use \App\Models\PrivacyPolicy;

class MobileTermAndSeriveceUrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function termAndService(Request $request, $url)
    {
        if($url == "privacy-policy"){

            $item =  PrivacyPolicy::first();

            $items = $item->privacy_content ? $item->privacy_content : false;
            
        }elseif($url == "terms-and-condition"){

            $item =  TermAndConditions::first();
            
            $items = $item->terms_content ? $item->terms_content : false;
        
        }else{
            $items = '';
        }

        return view('static-privacy-termandcondition', compact('items'));
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function webtermAndService(Request $request, $url)
    {
        if($url == "privacy-policy"){

            $item =  PrivacyPolicy::first();

            $items = $item->privacy_content ? $item->privacy_content : false;
            
        }elseif($url == "terms-and-condition"){

            $item =  TermAndConditions::first();
            
            $items = $item->terms_content ? $item->terms_content : false;
        
        }else{
            $items = '';
        }

        return view('front.privacy-policy', compact('items'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
