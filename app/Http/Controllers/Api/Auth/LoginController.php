<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Utility\PassportToken;
use App\Http\Utility\UtilityFunction;
use App\Mail\SendOTP;
use App\Models\LoginTrack;
use App\Mail\RegisterSuccessMail;
use App\User;
use Auth;
use DB;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class LoginController extends Controller
{
    use PassportToken;
    use UtilityFunction;
    protected $imagePath = 'images/users';
    protected $imageSizes = [
        'thumb' => [200,200],
    ];

    /**
     * For register new user profile
     *
     * @param Request $request request body from client
     * @return Json responce data with http responce code
     */
    public function register(Request $request)
    {
        $email_regex = '/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix';
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|regex:/^[\pL\s\-]+$/u|max:50',
            'last_name' => 'nullable|regex:/^[\pL\s\-]+$/u|max:25',
            'email' => 'required|email|regex:' . $email_regex . '|max:255|unique:users,email,NULL,id,deleted_at,NULL,email_verified_at,!NULL',
            'country_code' => 'required|max:10|' . Rule::in(array_column(json_decode(\Storage::disk('local')->get('data/country_code_json.json')), 'dial_code')),
            'mobile' => 'required|numeric|digits_between:4,15',
            'password' => 'required|min:8|max:16|confirmed',
            'myip' => 'required',
            'password_confirmation' => 'required|max:16',
            //'device_type' => 'required|max:255|'.Rule::in(['Android', 'IOS']),
        ], [
            'first_name.required' => 'First Name is required',
            'myip.required' => 'IP Address is required',
            'first_name.regex' => 'First Name is invalid',
            'first_name.max' => 'First Name should be maximum 50 characters long',
            'last_name.required' => 'Last Name is required',
            'last_name.regex' => 'Last Name is invalid',
            'last_name.max' => 'Last Name should be maximum 25 characters long',
            'email.required' => 'Email is required',
            'email.email' => 'Email is invalid',
            'email.regex' => 'Email is invalid',
            'country_code.required' => 'Country code is required',
            'country_code.max' => 'Country code should be maximum 10 characters long',
            'mobile.required' => 'Phone no. is required',
            'mobile.numeric' => 'Phone no. should be numeric',
            'mobile.digits_between' => 'Phone no. minimum 4 and maximum 15 digits long',
            'password.required' => 'Password is required',
            'password.min' => 'Password should be minimum 8 characters long',
            'password.max' => 'Password should be maximum 16 characters long',
            'password.confirmed' => 'Password not matched',
            'password_confirmation.required' => 'Confirm password is required',
            'password_confirmation.max' => 'Current password should be maximum 16 characters long',
            //'device_type.required' => 'Device type is required',
            //'device_type.max' => 'Device type should be maximum 255 characters long',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        $otp = '';
            for ($i = 0; $i < 4; $i++) {
                $otp .= mt_rand(0, 9);
            }
            //->where('email_verified_at', null)
           /* $user = User::where('email', $request->email)->first();
        if ($user) {
            if($user->email_verified_at == null){
                $user->email_verified_at = date('Y-m-d H:i:s') ;
                $user->save();  
            }*/
        if(User::where('email', $request->email)->where('email_verified_at', null)->first()){
            $user = User::where('email', $request->email)->where('email_verified_at', null)->first();
        } else {
            $user = new User();
        }
        $user->role_id = 2;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->country_code = $request->country_code;
        $user->mobile = $request->mobile;
        $user->password = Hash::make($request->password);
        //$user->device_type = $request->device_type;
        $user->otp = $otp;
        $user->save();

        \Mail::to($user->email)->send(new SendOTP($request, $user->otp));
        $result_currency = getCurrentLocationData( $request->myip );

        return response()->json([
            'message' => 'OTP sent successfully',
            'user_currency' => $result_currency->geoplugin_currencyCode,
            'geoplugin_currencySymbol' => $result_currency->geoplugin_currencySymbol,
        ], 200);
    }

    /**
     * For social register
     *
     * @param Request $request request body from client
     * @return Json responce data with http responce code
     */
    public function socialRegister(Request $request)
    {
        $email_regex = '/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix';
        $validator = Validator::make($request->all(), [
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:50',
            'email' => 'required|email|regex:' . $email_regex . '|max:255',
            'myip' => 'required|max:255',
            'device_type' => 'required|max:255',
            'device_token' => 'nullable|max:255',
            'country_code' => 'nullable|max:10|' . Rule::in(array_column(json_decode(\Storage::disk('local')->get('data/country_code_json.json')), 'dial_code')),
            'mobile' => 'nullable|numeric|digits_between:4,15',
            'image' => 'nullable',
        ], [
            'name.required' => 'Name is required',
            'myip.required' => 'IP Address is required',
            'name.regex' => 'Name is invalid',
            'name.max' => 'Name should be maximum 50 characters long',
            'email.required' => 'Email is required',
            'email.email' => 'Email is invalid',
            'email.regex' => 'Email is invalid',
            'device_type.required' => 'Device type required',
            'device_type.max' => 'Device type is invalid',
            'device_token.max' => 'Device token is invalid',
            'country_code.required' => 'Country code is required',
            'country_code.max' => 'Country code should be maximum 10 characters long',
            'mobile.required' => 'Phone no. is required',
            'mobile.numeric' => 'Phone no. should be numeric',
            'mobile.digits_between' => 'Phone no. minimum 4 and maximum 15 digits long',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        if (User::where('email', $request->email)->first()) {
            $user = User::where('email', $request->email)->first();
            if(empty($user->mobile)){
                $user->mobile = $request->mobile ; 
                $user->country_code = $request->country_code;
            }
            $user->email_verified_at = date('Y-m-d H:i:s');
            $user->save();


        } else {
            $user = new User();
            $user->role_id = 2;
            $user->first_name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($this->generateRandomString(8));
            $user->device_type = $request->device_type;
            $user->country_code = $request->country_code;
            $user->mobile = $request->mobile;
            $user->email_verified_at = date('Y-m-d H:i:s');
            $user->save();

            \Mail::to($user->email)->send(new RegisterSuccessMail($user));
        }
        if(!$user->profile_pic){
                $image_new = "";
            if($request->image) {
                $image_new = $this->uploadImageFromURL($request->image, $this->imagePath, $this->imageSizes);
                $user->profile_pic = $image_new;
                $user->save();
            }
            }
        if (LoginTrack::where('device_token', $request->device_token)->first()) {
            $login = LoginTrack::where('device_token', $request->device_token)->first();
        } else {
            $login = new LoginTrack();
        }
        $login->user_id = $user->id;
        $login->device_type = $request->device_type;
        $login->device_token = $request->device_token;
        $login->save();

        $response = $this->getBearerTokenByUser($user, 1, true);
        $access_code = json_decode((string) $response->getBody(), true)['access_token'];
        $result_currency = getCurrentLocationData( $request->myip );
        return response()->json([
            'message' => 'Logged in successfully',
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'has_mobile' => $user->mobile ? 1 : 0,
            'has_country_code' => $user->country_code ? 1 : 0,
            'user_image' => $user->profile_pic ? \Url('storage/app/images/users/').'/'.$user->profile_pic : null,
            'access_code' => $access_code,     
            'user_currency' => $result_currency->geoplugin_currencyCode,
            'geoplugin_currencySymbol' => $result_currency->geoplugin_currencySymbol,
        ], 200);

    }

    /**
     * For change password
     *
     * @param Request $request request body from client
     * @return Json responce data with http responce code
     */
    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|min:8|max:16|confirmed',
            'password_confirmation' => 'required|max:16',
        ], [
            'password.required' => 'Password is required',
            'password.min' => 'Password should be minimum 8 characters long',
            'password.max' => 'Password should be maximum 16 characters long',
            'password.confirmed' => 'Password not matched',
            'password_confirmation.required' => 'Confirm password is required',
            'password_confirmation.max' => 'Current password should be maximum 16 characters long',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        $user = \Auth::user();
        $user->password = Hash::make($request->password);
        $user->save();

        return response()->json([
            'message' => 'Password changed successfully',
        ], 200);
    }

    /**
     * For resend otp request
     *
     * @param Request $request request body from client
     * @return Json responce data with http responce code
     */
    public function resend(Request $request)
    {
        $email_regex = '/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix';
        $validator = Validator::make($request->all(), [
            'email' => 'required|regex:' . $email_regex . '|min:4|max:50|exists:users,email,deleted_at,NULL,status,1',
        ], [
            'email.required' => 'Email is required',
            'email.regex' => 'Email is invalid',
            'email.exists' => "Email dosen't exist in our system",
            'email.min' => 'Email should be minimum 4 characters long',
            'email.max' => 'Email should be maximum 50 characters long',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        $user = User::where('email', $request->email)->where('status', '1')->first();
        if ($user) {
            if (!$user->otp) {
                $otp = '';
                for ($i = 0; $i < 4; $i++) {
                    $otp .= mt_rand(0, 9);
                }
                $user->otp = $otp;
                $user->save();
            }
            \Mail::to($user->email)->send(new SendOTP($request, $user->otp));
            return response()->json([
                'message' => 'OTP sent successfully',
            ], 200);
        } else {
            return response()->json([
                'message' => 'Something went wrong',
            ], 201);
        }
    }

    /**
     * For verify otp request
     *
     * @param Request $request request body from client
     * @return Json responce data with http responce code
     */
    public function otpVerify(Request $request)
    {
        $email_regex = '/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix';
        $validator = Validator::make($request->all(), [
            'email' => 'required|regex:' . $email_regex . '|min:4|max:50|exists:users,email,deleted_at,NULL,status,1',
            'otp' => 'required|digits:4',
            'device_type' => 'required|max:255',
            'device_token' => 'nullable|max:255',
        ], [
            'email.required' => 'Email is required',
            'email.regex' => 'Email is invalid',
            'email.min' => 'Email should be minimum 4 characters long',
            'email.max' => 'Email should be maximum 50 characters long',
            'otp.required' => 'OTP is required',
            'otp.digits_between' => 'OTP is invalid',
            'device_type.required' => 'Device type required',
            'device_type.max' => 'Device type is invalid',
            'device_token.max' => 'Device token is invalid',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        $user = User::where('email', $request->email)->where('status', '1')->first();
        $user_register = $user->email_verified_at == null ? true : false;
        if ($user->otp == $request->otp) {
            $user->otp = null;
            $user->email_verified_at = date('Y-m-d H:i:s');
            $user->save();

            if (LoginTrack::where('device_token', $request->device_token)->first()) {
                $login = LoginTrack::where('device_token', $request->device_token)->first();
            } else {
                $login = new LoginTrack();
            }
            $login->user_id = $user->id;
            $login->device_type = $request->device_type;
            $login->device_token = $request->device_token;
            $login->save();

            if($user_register){
                \Mail::to($user->email)->send(new RegisterSuccessMail($user));
            }

            $response = $this->getBearerTokenByUser($user, 1, true);
            $access_code = json_decode((string) $response->getBody(), true)['access_token'];
            return response()->json([
                'message' => 'OTP matched',
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'user_image' => $user->profile_pic ? \Url('storage/app/images/users/').'/'.$user->profile_pic : null,
                'access_code' => $access_code,
            ], 200);
        } else {
            return response()->json([
                'message' => 'Wrong OTP',
            ], 201);
        }
    }

    /**
     * For login request using passport
     *
     * @param Request $request request body from client
     * @return Json responce data with http responce code
     */
    public function login(Request $request)
    {
        $email_regex = '/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix';
        $validator = Validator::make($request->all(), [
            'email' => 'required|regex:' . $email_regex . '|min:4|max:50',
            'password' => 'required|min:8|max:16',
            'myip' => 'required|max:255',
            'device_type' => 'required|max:255',
            'device_token' => 'nullable|max:255',
        ], [
            'email.required' => 'Email is required',
            'myip.required' => 'IP Address is required',
            'email.regex' => 'Email is invalid',
            'email.min' => 'Email should be minimum 4 characters long',
            'email.max' => 'Email should be maximum 50 characters long',
            'password.required' => 'Password is required',
            'password.min' => 'Password should be minimum 8 characters long',
            'password.max' => 'Password should be maximum 16 characters long',
            'device_type.required' => 'Device type required',
            'device_type.max' => 'Device type is invalid',
            'device_token.max' => 'Device token is invalid',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }
        

        if (\Auth::attempt(['email' => $request->email, 'password' => $request->password]) && User::where('email', $request->email)->where('role_id', 2)->where('email_verified_at', '!=', null)->first()) {
            if(\Auth::user()->status != '1'){
                return response()->json([
                    'message' => 'Account deactivated, Please contact to our customer support',
                ], 201);
            }
            if (LoginTrack::where('device_token', $request->device_token)->first()) {
                $login = LoginTrack::where('device_token', $request->device_token)->first();
            } else {
                $login = new LoginTrack();
            }
            $login->user_id = \Auth::id();
            $login->device_type = $request->device_type;
            $login->device_token = $request->device_token;
            $login->save();
            $response = $this->getBearerTokenByUser(\Auth::user(), 1, true);
            $access_code = json_decode((string) $response->getBody(), true)['access_token'];
            $user = \Auth::user();

            $result_currency = getCurrentLocationData( $request->myip );
            // return 
            return response()->json([
                'message' => 'Logged in Successfully',
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'user_image' => $user->profile_pic ? \Url('storage/app/images/users/').'/'.$user->profile_pic : null,
                'access_code' => $access_code,
                'user_currency' => $result_currency->geoplugin_currencyCode,
                'geoplugin_currencySymbol' => $result_currency->geoplugin_currencySymbol,
            ], 200);
        } else {
            return response()->json([
                'message' => 'Wrong credentials',
            ], 201);
        }
    }

    /**
     * For logout from passport
     *
     * @param Request $request request body from client header
     * @return Json responce data with http responce code
     */
    public function logout(Request $request)
    {
        $accessToken = $request->user()->token();

        if(LoginTrack::where('user_id', \Auth::id())->where('device_token', $request->device_token)->first())
        {
            LoginTrack::where('user_id', \Auth::id())->where('device_token', $request->device_token)->delete();
        }

        DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update([
                'revoked' => true,
            ]);
        $accessToken->revoke();
        return response()->json([
            'message' => 'Logged out',
        ], 200);
    }

    /**
     * For check email availablity
     *
     * @param Request $request request body from client header
     * @return Json responce data with http responce code
     */
    public function checkEmail(Request $request)
    {
        $email_regex = '/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix';
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|regex:' . $email_regex . '|max:255',
        ], [
            'email.required' => 'Email is required',
            'email.email' => 'Email is invalid',
            'email.regex' => 'Email is invalid',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }
        if (!User::where('email', $request->email)->where('email_verified_at', null)->where('status', '1')->first()) {
            return response()->json([
                'message' => 'Available',
            ], 200);
        } else {
            return response()->json([
                'message' => 'Unavailable',
            ], 201);
        }

    }

    public function generateRandomString($length)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}
