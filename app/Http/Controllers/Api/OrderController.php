<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Utility\PassportToken;
use App\Http\Utility\UtilityFunction;
use App\Mail\OrderSuccessSellerMail;
use App\Mail\OrderSuccessBuyerMail;
use App\User;
use App\Models\Cars;
use App\Models\Orders;
use App\Models\Settings;
use Illuminate\Http\Request;
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use Hash;
use Illuminate\Validation\Rule;
use Validator;
use Auth;
use DB;
use PDF;
use DateTime;
use DateTimeZone;

class OrderController extends Controller
{
    use UtilityFunction;
    //image upload path set for works images
    protected $imagePathDoc = 'images/orders/docs';
    protected $imageSizes = [
        'thumb' => [200, 200],
    ];
    protected $imageSizesMarkup = [
        'thumb-markup' => [50, 50],
    ];
    
     
    /**
     * For list the orders
     *
     * @param Request $request request body from client
     * @return Json responce data with http responce code
     */

    public function list(Request $request)
    {
        $query = Orders::query();
        $query->where('user_id', \Auth::id())
        ->where('status', '1')
        ->where('order_status', '2')
        ->orderBy('created_at', 'Desc');
           $items = $query->paginate(10);

           $orders = [];
        $ordersdetails = [];

        //Get converted currency
        $jsoncurrency = currencyConverter();
        $userCurrency = $request->header('USER-LOCATION-CURRENCY');

           foreach ($items as $item) {
            $features = [];
            $ordersdetails['id'] = $item->id;
            $ordersdetails['car_id'] = $item->car_id;
            $ordersdetails['booking_id'] = $item->booking_id;
            $ordersdetails['booking_from_date'] = $item->booking_from_date;
            $ordersdetails['booking_to_date'] = $item->booking_to_date;
            $ordersdetails['grand_total'] = getCurrencyFromUser('USD', $userCurrency, $item->currency, $item->grand_total, $jsoncurrency);
            $ordersdetails['payment_method'] = $item->payment_method;
            $ordersdetails['updated_at'] = date('Y-m-d H:i:s', strtotime($item->updated_at));
            $ordersdetails['billing_first_name'] = $item->billing_first_name;
            $ordersdetails['billing_last_name'] = $item->billing_last_name;
            $ordersdetails['brand'] = $item->car->brand->name;
            $ordersdetails['model'] = $item->car->model->name;
            $ordersdetails['enginetype'] = $item->car->enginetype->name;
            $ordersdetails['vehicletype'] = $item->car->vehicletype->name;
            $ordersdetails['owner_name'] = isset($item->car->user->full_name) ? $item->car->user->full_nam : '' ;
            $ordersdetails['image'] = $item->car->image ? \Url('storage/app/images/cars/').'/'.$item->car->image->image : null;
            $orders[] = $ordersdetails;
        }

        return response()->json([
            'data' => $orders,
            'current_page' => $items->currentPage(),
            'total_page' => $items->lastPage(),
        ], 200);
    }

    /**
     * For list the orders
     *
     * @param Request $request request body from client
     * @return Json responce data with http responce code
     */
    public function listMyCarBooking(Request $request)
    {
        //return Cars::where('user_id', \Auth::id())->pluck('id');
        $query = Orders::query();
        $query->whereIn('car_id', Cars::where('user_id', \Auth::id())->pluck('id'))
        ->where('status', '1')
        ->orderBy('created_at', 'Desc');
           $items = $query->paginate(10);
           
        //Get converted currency
        $jsoncurrency = currencyConverter();
        $userCurrency = $request->header('USER-LOCATION-CURRENCY');

           $orders = [];
        $ordersdetails = [];
        
           foreach ($items as $item) {
            $ordersdetails['id'] = $item->id;
            $ordersdetails['car_id'] = $item->car_id;
            $ordersdetails['booking_id'] = $item->booking_id;
            $ordersdetails['booking_from_date'] = $item->booking_from_date;
            $ordersdetails['booking_to_date'] = $item->booking_to_date;
            $ordersdetails['grand_total'] = getCurrencyFromUser('USD', $userCurrency, $item->currency, $item->grand_total, $jsoncurrency);
            $ordersdetails['payment_method'] = $item->payment_method;
            $ordersdetails['updated_at'] = date('Y-m-d H:i:s', strtotime($item->updated_at));
            $ordersdetails['billing_first_name'] = $item->billing_first_name;
            $ordersdetails['billing_last_name'] = $item->billing_last_name;
            $ordersdetails['brand'] = $item->car->brand->name;
            $ordersdetails['model'] = $item->car->model->name;
            $ordersdetails['enginetype'] = $item->car->enginetype->name;
            $ordersdetails['vehicletype'] = $item->car->vehicletype->name;
            $ordersdetails['image'] = $item->car->image ? \Url('storage/app/images/cars/').'/'.$item->car->image->image : null;
            $ordersdetails['owner_name'] = isset($item->car->user->full_name) ? $item->car->user->full_nam : '' ;
            $orders[] = $ordersdetails;
        }

        return response()->json([
            'data' => $orders,
            'current_page' => $items->currentPage(),
            'total_page' => $items->lastPage(),
        ], 200);
    }

    /**
     * For get the orders details
     *
     * @param Request $request request body from client
     * @return Json responce data with http responce code
     */
    public function orderDetails(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'order_id' => 'required|numeric|exists:orders,id,deleted_at,NULL,status,1',
        ], [
            'order_id.required' => 'Order id is required',
            'order_id.numeric' => 'Order id should be numeric',
            'order_id.exists' => 'Order id not available in our record',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        //Get converted currency
        $jsoncurrency = currencyConverter();
        $userCurrency = $request->header('USER-LOCATION-CURRENCY');

        $query = Orders::query();
        
       // $query->where('user_id', \Auth::id())
       $query->where('id', $request->order_id)

        ->where('status', '1');
        $item = $query->orderBy('created_at', 'Desc')->first();
          
           $ordersdetails['id'] = $item->id;
            $ordersdetails['car_id'] = $item->car_id;
            $ordersdetails['booking_id'] = $item->booking_id;
            $ordersdetails['booking_from_date'] = $item->booking_from_date;
            $ordersdetails['booking_to_date'] = $item->booking_to_date;
            $ordersdetails['total_amount'] = getCurrencyFromUser('USD', $userCurrency, $item->currency, $item->total_amount, $jsoncurrency);
            $ordersdetails['security_amount'] = getCurrencyFromUser('USD', $userCurrency, $item->currency, $item->security_amount, $jsoncurrency);
            $ordersdetails['tax_amount'] = getCurrencyFromUser('USD', $userCurrency, $item->currency, $item->tax_amount, $jsoncurrency);
            $ordersdetails['grand_total'] = getCurrencyFromUser('USD', $userCurrency, $item->currency, $item->grand_total, $jsoncurrency);
            $ordersdetails['payment_method'] = $item->payment_method;
            $ordersdetails['bank_reference_id'] = $item->bank_reference_id;
            $ordersdetails['transaction_id'] = $item->transaction_id;
            $ordersdetails['billing_first_name'] = $item->billing_first_name;
            $ordersdetails['billing_last_name'] = $item->billing_last_name;
            $ordersdetails['billing_email'] = $item->billing_email;
            $ordersdetails['billing_mobile'] = $item->billing_mobile;
            $ordersdetails['billing_alternate_mobile'] = $item->billing_alternate_mobile;
            $ordersdetails['billing_address'] = $item->billing_address;
            $ordersdetails['pickup_location'] = $item->pickup_location;
            $ordersdetails['order_status'] = $item->order_status;
            $ordersdetails['updated_at'] = date('Y-m-d H:i:s', strtotime($item->updated_at));
            
            $ordersdetails['brand'] = $item->car->brand->name;
            $ordersdetails['model'] = $item->car->model->name;
            $ordersdetails['color'] = $item->car->color->name;
            $ordersdetails['vehicletype'] = $item->car->vehicletype->name;
            $ordersdetails['enginetype'] = $item->car->enginetype->name;
            $ordersdetails['image'] = $item->car->image ? \Url('storage/app/images/cars/').'/'.$item->car->image->image : null;
            $ordersdetails['owner_name'] = isset($item->car->user->full_name) ? $item->car->user->full_nam : '' ;
            $ordersdetails['contact_no'] = $item->car->user->country_code.' '.$item->car->user->mobile;
            $ordersdetails['invoice'] = $item->order_invoice ? (\Url('storage').'/invoice'.'/'.$item->order_invoice) : null;
        return response()->json([
            'order' => $ordersdetails,
        ], 200);
    }

    /**
     * For searching cars
     *
     * @param Request $request request body from client
     * @return Json responce data with http responce code
     */
    public function saveOrder(Request $request)
    {
        $email_regex = '/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix';
        $validator = Validator::make($request->all(), [
            'car_id' => 'required|numeric|exists:cars,id,deleted_at,NULL,status,1',
            'booking_from_date' => 'required|after:'.date('Y-m-d H:i:s', strtotime('-1 hours')).'|date_format:Y-m-d H:i:s',
            'booking_to_date' => 'required|after:'.date('Y-m-d H:i:s', strtotime($request->booking_from_date)).'|date_format:Y-m-d H:i:s',
            'currency' => 'required',
            'total_amount' => 'required|max:255',
            // 'total_amount' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/|max:255',
            'security_amount' => 'required|max:255',
            'tax_amount' => 'required|max:255',
            'grand_total' => 'required|max:255',
            'billing_first_name' => 'required|regex:/^[\pL\s\-]+$/u|max:50',
            'billing_last_name' => 'nullable|regex:/^[\pL\s\-]+$/u|max:25',
            'billing_email' => 'required|email|regex:'.$email_regex.'|max:255',
            'billing_mobile' => 'required',
            'billing_alternate_mobile' => 'nullable',
            'billing_address' => 'required|string|max:255',
            'pickup_location' => 'required|string|max:255',
        ], [
            'car_id.required' => 'Car id is required',
            'car_id.numeric' => 'Car id is invalid',
            'car_id.max' => 'Car id is invalid',
            'booking_from_date.required' => 'Booking from date is required',
            'booking_from_date.before' => 'Booking from date should be after today',
            'booking_from_date.date_format' => 'Booking from date format should be Y-m-d H:i:s',
            'booking_to_date.required' => 'Booking to date is required',
            'booking_to_date.before' => 'Booking to date should be after today',
            'booking_to_date.date_format' => 'Booking to date format should be Y-m-d H:i:s',
            'currency.required' => 'Currency is required',
            'total_amount.required' => 'Total amount is required',
            'total_amount.regex' => 'Total amount should be valid double value',
            'total_amount.max' => 'Total amount is invalid',
            'security_amount.required' => 'Security amount is required',
            'security_amount.regex' => 'Security amount should be valid double value',
            'security_amount.max' => 'Security amount is invalid',
            'tax_amount.required' => 'Tax amount is required',
            'tax_amount.regex' => 'Tax amount should be valid double value',
            'tax_amount.max' => 'Tax amount is invalid',
            'grand_total.required' => 'Grand total is required',
            'grand_total.regex' => 'Grand total should be valid double value',
            'grand_total.max' => 'Grand total is invalid',
            'billing_first_name.required' => 'Billing first Name is required',
            'billing_first_name.regex' => 'Billing first Name is invalid',
            'billing_first_name.max' => 'Billing first Name should be maximum 50 characters long',
            'billing_last_name.required' => 'Billing last Name is required',
            'billing_last_name.regex' => 'Billing last Name is invalid',
            'billing_last_name.max' => 'Billing last Name should be maximum 25 characters long',
            'billing_email.required' => 'Billing email is required',
            'billing_email.email' => 'Billing email is invalid',
            'billing_email.regex' => 'Billing email is invalid',
            'billing_mobile.required' => 'Billing mobile no. is required',
            'billing_mobile.numeric' => 'Billing mobile no. should be numeric',
            'billing_mobile.digits_between' => 'Billing mobile no. minimum 4 and maximum 15 digits long',
            'billing_alternate_mobile.required' => 'Billing alternate mobile no. is required',
            'billing_alternate_mobile.numeric' => 'Billing alternate mobile no. should be numeric',
            'billing_alternate_mobile.digits_between' => 'Billing alternate mobile no. minimum 4 and maximum 15 digits long',
            'billing_address.required' => 'Billing address is required',
            'billing_address.string' => 'Billing address is invalid',
            'billing_address.max' => 'Billing address should be max 255 characters longs',
            'pickup_location.required' => 'Pickup location is required',
            'pickup_location.string' => 'Pickup location is invalid',
            'pickup_location.max' => 'Pickup location should be max 255 characters longs',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }
        //Converted Currency
        $jsoncurrency = currencyConverter();
        $userCurrency = $request->header('USER-LOCATION-CURRENCY');

        $booking_id = 'RKR' . date('Y') . mt_rand();
        $order = new Orders();
        $order->user_id = \Auth::id();
        $order->car_id = $request->car_id;
        $order->booking_id = $booking_id;
        $order->booking_from_date = date('Y-m-d H:i:s', strtotime($request->booking_from_date));
        $order->booking_to_date = date('Y-m-d H:i:s', strtotime($request->booking_to_date));
        $order->currency =  $userCurrency;
        $order->paid_amount = $request->grand_total;
        $order->total_amount = $request->total_amount;
        $order->security_amount = $request->security_amount;
        $order->tax_amount = $request->tax_amount;
        $order->grand_total = $request->grand_total;
        // $order->paid_amount = getCurrencyFromUser($userCurrency, 'USD', $request->grand_total, $jsoncurrency);
        // $order->total_amount = getCurrencyFromUser($userCurrency, 'USD', $request->total_amount, $jsoncurrency);
        // $order->security_amount = getCurrencyFromUser($userCurrency, 'USD', $request->security_amount, $jsoncurrency);
        // $order->tax_amount = getCurrencyFromUser($userCurrency, 'USD', $request->tax_amount, $jsoncurrency);
        // $order->grand_total = getCurrencyFromUser($userCurrency, 'USD', $request->grand_total, $jsoncurrency);
        $order->billing_first_name = $request->billing_first_name;
        $order->billing_last_name = $request->billing_last_name;
        $order->billing_email = $request->billing_email;
        $order->billing_mobile = $request->billing_mobile;
        $order->billing_alternate_mobile = $request->billing_alternate_mobile;
        $order->billing_address = $request->billing_address;
        $order->pickup_location = $request->pickup_location;
        $order->save();
        
        return response()->json([
            'message' => "Order created successfully",
            'booking_id' => $order->booking_id,
            'grand_total_usd' => getCurrencyFromUser($userCurrency, 'USD', $userCurrency, $request->grand_total, $jsoncurrency),
        ], 200);
    }

    /**
     * For order success
     *
     * @param Request $request request body from client
     * @return Json responce data with http responce code
     */
    public function successOrder(Request $request)
    {
        $email_regex = '/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix';
        $validator = Validator::make($request->all(), [
            'booking_id' => 'required|exists:orders,booking_id,deleted_at,NULL,status,1,order_status,1,user_id,'.\Auth::id(),
            'payment_method' => 'required|'.Rule::in(['Paypal', 'Stripe']),
            'bank_reference_id' => 'nullable|string|max:255',
            'transaction_id' => 'nullable|string|max:255',
            'time_zone' => 'required|string|max:255',
        ], [
            'booking_id.required' => 'Boking id is required',
            'payment_method.required' => 'Payment method is required',
            'bank_reference_id.required' => 'Bank reference id is required',
            'bank_reference_id.string' => 'Bank reference id is invalid',
            'bank_reference_id.max' => 'Bank reference id should be max 255 characters longs',
            'transaction_id.required' => 'Transaction id is required',
            'transaction_id.string' => 'Transaction id is invalid',
            'transaction_id.max' => 'Transaction id should be max 255 characters longs',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }
        //Get converted currency
        $jsoncurrency = currencyConverter();
        $userCurrency = $request->header('USER-LOCATION-CURRENCY');

        $order = Orders::where('booking_id', $request->booking_id)->first();
        $order->payment_method    = $request->payment_method;
        $order->bank_reference_id = $request->bank_reference_id;
        $order->transaction_id    = $request->transaction_id;
        $order->order_status      = 2;
        $order->save();
       
        $setting = Settings::first();

        $time_zone = $request->time_zone;
        
        if($time_zone != ''){
            $serverBokingFromDate = new DateTime($order->booking_from_date);
            $serverBokingFromDate->setTimezone(new DateTimeZone($time_zone));
            $bookingFrom = $serverBokingFromDate->format('Y-m-d H:i:s');
            
        }else{
            $bookingFrom = $order->booking_from_date ;
        }

        if($time_zone != ''){
            $serverBokingToDate = new DateTime($order->booking_to_date); 
            $serverBokingToDate->setTimezone(new DateTimeZone($time_zone));            
            $bookingTo = $serverBokingToDate->format('Y-m-d H:i:s');

        }else{
            $bookingTo = $order->booking_to_date ;
        }

        $pdf = PDF::loadView('order_invoice', compact('order', 'setting', 'bookingFrom','bookingTo'));
        $pdf->setPaper('a4', 'landscape');
       
        //$storage_path =  \Url('storage').'/invoice';
        
        $storage_path =  storage_path().'/invoice';

        if(!is_dir($storage_path)){            
            mkdir($storage_path,'0777',true);
        }
       
        $filename = date('Ymd').time().'_myorder.pdf';

        $pdf->save($storage_path.'/'.$filename);
        //$display_path = $storage_path.'/'.$filename;
        $updated_order = Orders::find($order->id);
        $updated_order->order_invoice = $filename;
        $updated_order->save();

        \Mail::to($updated_order->user->email)->send(new OrderSuccessBuyerMail($updated_order));
        \Mail::to($updated_order->car->owner->email)->send(new OrderSuccessSellerMail($updated_order));
        
        $keys = $updated_order->car->owner->devices->pluck('device_token')->toArray();
       
        //send push notification to Owner
        $msg =  'Dear '.ucfirst($updated_order->car->owner->first_name) .' '.ucfirst($updated_order->car->owner->last_name).' you have a new booking.';
        $action = "Owner";

        foreach($keys as $key){
            fcm_notification($msg, 'Reekar New Booking', $key, $action); 
        }
        


        return response()->json([
            'message' => "Order success",
            'car_name' => $order->car->brand->name.' '.$order->car->model->name,
            'car_image' => $order->car->image ? (\Url('storage/app/images/cars/').'/'.$order->car->image->image) :null,
            'order_id' => $order->id,
            'booking_id' => $order->booking_id,
            'pickup_location' => $order->pickup_location,
            'total_amount' => getCurrencyFromUser('USD', $userCurrency, $order->currency, $order->total_amount, $jsoncurrency),
            'tax_amount' => getCurrencyFromUser('USD', $userCurrency, $order->currency, $order->tax_amount, $jsoncurrency),
            'grand_total' => getCurrencyFromUser('USD', $userCurrency, $order->currency, $order->grand_total, $jsoncurrency),
            'invoice' => $updated_order->order_invoice ? \Url('storage').'/invoice'.'/'.$updated_order->order_invoice :null,
        ], 200);
    }

    /**
     * For upload bulk docs for car
     *
     * @param Request $request request body from client
     * @return Json responce data with http responce code
     */
    public function uploadIdentityDoc(Request $request)
    {  
         //$files = $request->file('file');
        //return $files->getClientOriginalExtension();
            $validator = Validator::make($request->all(), [
                'booking_id' => 'required|exists:orders,booking_id,deleted_at,NULL,status,1,order_status,1,user_id,'.\Auth::id(),
                "file" => ["required","array","min:2","max:6"],
                'file.*' => 'required|file|mimes:jpeg,png,jpg,pdf,doc,docx|max:20480',
               
            ], [
                'booking_id.required' => 'Boking id is required',
                'file.required' => 'File is required',
            ]);
            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 422);
            }

            if ($request->hasFile('file')) {
                
                $files = $request->file('file');
                $file_array = [];
                foreach($files as $file){

                    if ($file->getClientOriginalExtension() == 'jpeg' ||
                    $file->getClientOriginalExtension() == 'png' ||
                    $file->getClientOriginalExtension() == 'jpg') {
                        $file_new = $this->uploadImage($file, $this->imagePathDoc, $this->imageSizes);
                    } else {
                        $file_new = $this->uploadDocs($file, $this->imagePathDoc);
                    }
    
                $img = Orders::where('booking_id', $request->booking_id)->first();
                $file_array[] = $file_new;
                }
            $img->doc = json_encode($file_array);
            $img->save();
        }
   
        return response()->json([
            'message' => "Uploaded successfully",
        ], 200);
    }

    
}