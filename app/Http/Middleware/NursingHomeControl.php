<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class NursingHomeControl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (\Auth::check()) {
            
            if(Auth::user()->role_id != 4){
                Auth::logout(); 
                return redirect()->intended(route('login'))->with('message', 'Sorry, you are not authorized to access this page.');
            }
            else{
                if(Auth::user()->status != 1){
                    Auth::logout(); 
                    return redirect()->intended(route('login'))->with('message', 'Sorry, Your account has been deactivated. Please contact your site administrator.');
                }
                return $next($request); 
            } 
        }else {
            return redirect()->to('login');
        } 
         
        
    }
}
