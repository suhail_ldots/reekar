<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Session;

class FrontControl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (!(\Auth::check())) {
            //dd("dsfsf");
            if(!Session::has('riders_admin_timezone'))
            \Session::put('riders_admin_timezone', $request->tz);

            if(!Session::has('current_user_currency'))
            \Session::put('current_user_currency', $request->user_currency);

            if(!Session::has('currency_symbol'))
            \Session::put('currency_symbol', $request->user_currency_symbol);                       
        }
     
        return $next($request); 
        
    }
}
