<?php

namespace App\Http\Middleware;

use Closure;
//blog
use Illuminate\Contracts\Auth\Guard; 
use Response; 
use Session;

class HeaderKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if($request->header('USER-LOCATION-CURRENCY') == ''){ 
            
            return Response::json(['error' => 'Please set your currency header.'], 201);  
        }        
        
        $api_key = $request->header('USER-LOCATION-CURRENCY');
            //return response()->json($api_key);
        //$api_key = base64_decode($api_key);  
        
        $all_currency = ['USD','AED','ARS','AUD','BGN','BRL','BSD','CAD','CHF','CLP','CNY','COP','CZK','DKK','DOP','EGP','EUR','FJD','GBP',
        'GTQ','HKD','HRK','HUF','IDR','ILS','INR','ISK','JPY','KRW','KZT','MXN','MYR','NOK',
        'NZD','PAB','PEN','PHP','PKR','PLN','PYG','RON','RUB','SAR','SEK','SGD','THB','TRY','TWD','UAH','UYU','ZAR'];
       
        //return Response::json(['count'=>$api_key]);
        if( !in_array(ucwords($api_key), $all_currency) ){  

            return Response::json(['error' =>'wrong currency in header.'], 201);             
        }
        
       /*  if($request->session()->has('user_currency') && $request->session()->get('user_currency') == ucwords($api_key)){
            //return $next($request);
            return Response::json(['count'=> $request->session()->get('user_currency')]);

        }else{
            $request->session()->put('user_currency', ucwords($api_key));
            return $next($request);
        } */
        return $next($request);
       
    }
}
