<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminControl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (\Auth::check()) {
            
            if(Auth::user()->role_id != 1){
                Auth::logout(); 
                return redirect()->intended(route('login'))->with('message', 'Sorry, you are not authorized to access this page.');
            }
            else{
                return $next($request); 
            } 
        }else {
            return redirect()->to('login');
        } 
         
        
    }
}
