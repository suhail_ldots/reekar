<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'email', 'password','last_name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function user_country(){
        return $this->belongsTo('App\Models\Masters\Country', 'country');
    }
    public function role(){
        return $this->belongsTo('App\Models\Role', 'role_id');
    }
    public function doctor(){
        return $this->hasOne('App\Models\Doctor', 'user_id');
    }    
    public function nursinghome(){
        return $this->hasOne('App\Models\NursingHome', 'user_id');
    }    
    public function roles(){
        return $this->belongsToMany('App\Models\Role', 'role_id');
    }

    public function getFullNameAttribute(){
        return ucwords("{$this->first_name} {$this->last_name}");
    } 

    public function devices()
    {
        return $this->hasMany('App\Models\LoginTrack', 'user_id');
    }
    //Get Nearest User
    public function scopeIsWithinMaxDistance($query, $coordinates, $radius = 3) {

        // $lat = YOUR_CURRENT_LATTITUDE;

        // $lon = YOUR_CURRENT_LONGITUDE;
        
           
        
        // DB::table("posts")
        
        //     ->select("posts.id"
        
        //         ,DB::raw("6371 * acos(cos(radians(" . $lat . ")) 
        
        //         * cos(radians(posts.lat)) 
        
        //         * cos(radians(posts.lon) - radians(" . $lon . ")) 
        
        //         + sin(radians(" .$lat. ")) 
        
        //         * sin(radians(posts.lat))) AS distance"))
        
        //         ->groupBy("posts.id")
        
        //         ->get();



        $haversine = "(6371 * acos(cos(radians(" . $coordinates['latitude'] . ")) 
                        * cos(radians(`lat`)) 
                        * cos(radians(`long`) 
                        - radians(" . $coordinates['longitude'] . ")) 
                        + sin(radians(" . $coordinates['latitude'] . ")) 
                        * sin(radians(`lat`))))";
    
       // return $query->select('id', 'role_id', 'nursing', 'color_id', 'engine_type_id','transmission_id', 'vehicle_type_id', 'seat_id', 'year', 'car_location', 'available_from_date', 'available_from_time', 'available_to_date', 'available_to_time', 'currency', 'price_per_day', 'total_price', 'ac', 'gps', 'ipod_interface', 'sunroof', 'child_seat', 'electric_windows', 'heated_seat', 'panorma_roof', 'prm_gauge', 'abs', 'traction_control', 'audio_system', 'car_location_lat', 'car_location_long', 'listing_status', 'reject_reason')
       return $query->select('id', 'role_id', 'nursing_home_id', 'first_name', 'last_name','status','country','city','address','zipcode','mobile','profile_pic', 'lat','long',
      DB::raw($haversine.' '.'AS distance'))->whereRaw('distance', '<', 20);
        // ->selectRaw("{$haversine} AS distance");
        // ->selectRaw("distance", '<', $radius);
        
        //  ->whereRaw("{$haversine} < ?", [$radius]);
    }
}
