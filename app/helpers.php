<?php
use Illuminate\Support\Facades\Request;
use App\User;
use App\Models\Masters\Feature;
use App\Models\Masters\Country;
use App\Models\Masters\Department;
use App\Models\Specialities;

use App\Models\Brands;
use App\Models\VehicleTypes;
use App\Models\EngineTypes;
use App\Models\Seats;
use App\Models\Settings;
use App\Models\Transmissions;
use App\Models\Colors;
use App\Models\CurrencyConversion;

/* use DateTime;
use DateTimeZone; */


function isActiveRoute($route, $output = "active")
{
    if (Route::currentRouteName() == $route) {
        return $output;
    }
}

function CountryCodes(){
    $url = URL::to("/").'/storage/app/data/country_code_json.json';
    $data =  file_get_contents($url);
    $data = json_decode($data);
   if(count($data) >= 1){
       return $data;
   }else{
       return false;
   }  
}
//Plan Feature 
function getPlanFeature(){
    $items =  Feature::all()->where('status', 1);
    // dd($items);
    if($items){
        return $items;
    }else{
        return false;
    }   
}
//Plan Feature By Id
function getFeature_byid($id){
    $items =  Feature::where('status',1)->where('id', $id)->first();
    // dd($items);
    if($items){
        return $items;
    }else{
        return false;
    }   
}


function country(){
    $items =  Country::all()->where('status',1);
    if($items){
        return $items;
    }else{
        return false;
    }   
}
function speciality(){
    $items =  Specialities::all()->where('status',1);
    if($items){
        return $items;
    }else{
        return false;
    }   
}
function Departments(){
    $items =  Department::all()->where('status',1);
    if($items){
        return $items;
    }else{
        return false;
    }   
}

/**
* get working days ID
*
* @param $none
*/


//used
function getMyWorkingDay($data)
{
    if($data == '' || $data == 0){
        return $days_name = [];

    }
    $days = array(
        '1' => 'Monday',
        '2' => 'Tuesday',
        '3' => 'Wednesday',
        '4' => 'Thursday',
        '5' => 'Friday',
        '6' => 'Saturday',
        '7' => 'Sunday'
    );

    $rad = explode(',',$data);
    
    $days_name = [];

    foreach ($rad as $key => $value) {
        $days_name[$value]= $days[$value];
    }
    //dd($data."sdfsd");
    // dd($days_name);
    return $days_name;
}

//workin daya from start to end day name
function getWorkingDay($data)
{
    if($data == '' || $data == 0){
        return '';
    }

    $days = array(
        '1' => 'Mon',
        '2' => 'Tue',
        '3' => 'Wed',
        '4' => 'Thu',
        '5' => 'Fri',
        '6' => 'Sat',
        '7' => 'Sun'
    );

    $rad = explode(',',$data);
    $days_name = [];
    // dd($rad);
    /* $days_name = '';
     foreach ($rad as $key => $value) {
        $days_name.= $days[$value].', ';
    }
    return $days_name; */

    foreach ($rad as $key => $value) {
        $days_name[$key]= $days[$value];
    }
    if( count($days_name) > 1 ){
        return reset($days_name).' - '.end($days_name) ;
    }
    return reset($days_name);
}
//Time of dr availability
function getDoctorTiming($data)
{
   // dd($data) ;
    if($data == '' || $data == 0){
        return '';

    }
    if(!empty($data)){
        foreach($data as $key => $value){
            if($value['availability_from'] != '' && $value['availability_from'] != null){
                return date('g:i a', strtotime($value['availability_from'])).' - '.date('g:i a', strtotime($value['availability_to']));
            }
        }
    //
    }
   // $avail[1]['availability_from'] $avail[1]['availability_to'];
    $rad = explode(',',$data);
    $days_name = [];
        
    if( count($days_name) > 1 ){
        return reset($days_name).' - '.end($days_name) ;
    }
    return reset($days_name);
}

function WorkingDaywithDate($data)
{
    if($data == '' || $data == 0){
        return $days_name = '';
    }
    //dd(date('d-m-y','+1 day'));
    //date("D M j G:i:s T Y"); 
    //dd(date('j-M-Y', strtotime(' +1 day')));
    //dd(date("M-j-Y"));  //Note- for full name of day use 'l' instead 'D';
    $days = array(
        '1' => 'Mon',
        '2' => 'Tue',
        '3' => 'Wed',
        '4' => 'Thu',
        '5' => 'Fri',
        '6' => 'Sat',
        '7' => 'Sun'
    );
    $mydate = array(
            date('D j-M-Y'),
            date('D j-M-Y', strtotime(' +1 day')),
            date('D j-M-Y', strtotime(' +2 day')),
            date('D j-M-Y', strtotime(' +3 day')),
            date('D j-M-Y', strtotime(' +4 day')),
            date('D j-M-Y', strtotime(' +5 day')),
            date('D j-M-Y', strtotime(' +6 day')),
            
    );

    $rad = explode(',',$data);
    $days_name = '';
    $days_name_withdate ='';
    foreach ($rad as $key => $value) {
        //$days_name.= $days[$value].', ';
        $days_name = $days[$value];				
        foreach($mydate as $maindate){
            if (strpos($maindate, $days_name) !== false) {
                //dd($maindate);
                $days_name_withdate.= $maindate.', ';
            }
        }
    }
    return $days_name_withdate;
}
function getWorkingDay_withDate($data)
{
    if($data == '' || $data == 0){
        return $days_name_withdate = [];
    }
    //dd(date('d-m-y','+1 day'));
    //date("D M j G:i:s T Y"); 
    //dd(date('j-M-Y', strtotime(' +1 day')));
    //dd(date("M-j-Y"));  //Note- for full name of day use 'l' instead 'D';
    $days = array(
        '1' => 'Mon',
        '2' => 'Tue',
        '3' => 'Wed',
        '4' => 'Thu',
        '5' => 'Fri',
        '6' => 'Sat',
        '7' => 'Sun'
    );
    $mydate = array(
            date('D,  j-M-Y'),
            date('D,  j-M-Y', strtotime(' +1 day')),
            date('D,  j-M-Y', strtotime(' +2 day')),
            date('D,  j-M-Y', strtotime(' +3 day')),
            date('D,  j-M-Y', strtotime(' +4 day')),
            date('D,  j-M-Y', strtotime(' +5 day')),
            date('D,  j-M-Y', strtotime(' +6 day')),
            
    );

    $rad = explode(',',$data);
    $days_name = '';
    $days_name_withdate = [];
    foreach ($rad as $key => $value) {
        //$days_name.= $days[$value].', ';
        $days_name = $days[$value];				
        foreach($mydate as $maindate){
            if (strpos($maindate, $days_name) !== false) {
                // dd($maindate);
               $days_name_withdate[] = $maindate;
            }
        }
    }
    // return $newdata;
    return $days_name_withdate;
}
//Get Read more Or Break string
function shortString($string){
    // strip tags to avoid breaking any html
    if(empty($string)){
        return "Details not available for this nursing home";
    }    
    $string = strip_tags($string);

    if (strlen($string) > 300) {
        // truncate string
        $stringCut = substr($string, 0, 300);
        $endPoint = strrpos($stringCut, ' ');

        //if the string doesn't contain any space then it will cut without word basis.
        $string = ($endPoint ? substr($stringCut, 0, $endPoint) : substr($stringCut, 0));
        $string .= '...';
    }
    return $string;
}


function bloodGroup(){
    $items = ['A+', 'A-', 'B+', 'B-', 'AB+', 'AB-','O+', 'O-'];
    if($items){
        return $items;
    }else{
        return false;
    }   
}

function getBrands(){
    $items =  Brands::all()->where('status',1);
    if($items){
        return $items;
    }else{
        return false;
    }
   
}
function vehicleTypes(){
    $items =  vehicleTypes::all()->where('status',1);
    if($items){
        return $items;
    }else{
        return false;
    }   
}
function engineTypes(){
    $items =  EngineTypes::all()->where('status',1);
    if($items){
        return $items;
    }else{
        return false;
    }   
}

function getSeats(){
    $items =  Seats::all()->where('status',1);
    if($items){
        return $items;
    }else{
        return false;
    }
   
}
function getColor(){
    $items =  Colors::all()->where('status',1);
    if($items){
        return $items;
    }else{
        return false;
    }
   
}


function getTransmission(){
    $items =  Transmissions::all()->where('status',1);
    if($items){
        return $items;
    }else{
        return false;
    }
   
}
function mypagination() {
    $pagination = Settings::first();
    //return 2;
    if(isset($pagination)){
        return $pagination->data_per_page ? $pagination->data_per_page : 20;
    } else {
        return 0;
    } 
}

function date_in_mytimezone($date, $time_zone) {
    $serverBokingToDate = new DateTime($date); 
    $serverBokingToDate->setTimezone(new DateTimeZone($time_zone));            
    $newdate = $serverBokingToDate->format('d-M-Y');
    if($newdate){
        return $newdate;
    }else{
        return $newdate = false;
    }
}
function time_in_mytimezone($date, $time_zone) {
    $serverBokingToDate = new DateTime($date); 
    $serverBokingToDate->setTimezone(new DateTimeZone($time_zone));            
    $newdate = $serverBokingToDate->format('h:i A');
    if($newdate){
        return $newdate;
    }else{
        return $newdate = false;
    }
}

function getCurrentTimezoneTime($date)
{
    if(!\Session::has('reekar_admin_timezone')){
        \Session::put('reekar_admin_timezone', 'Asia/Kolkata');
    }
    $timezone = \Session::get('reekar_admin_timezone');

    return time_in_mytimezone($date, $timezone);
    /* $date = Carbon::createFromFormat('Y-m-d H:i:s', $date, 'UTC')
    ->setTimezone($timezone);
    return date("jS M Y g:i A", strtotime($date)); */
}
function getCurrentTimezoneDate($date)
{
    if(!\Session::has('reekar_admin_timezone')){
        \Session::put('reekar_admin_timezone', 'Asia/Kolkata');
    }
    $timezone = \Session::get('reekar_admin_timezone');

    return date_in_mytimezone($date, $timezone);
    /* $date = Carbon::createFromFormat('Y-m-d H:i:s', $date, 'UTC')
    ->setTimezone($timezone);
    return date("jS M Y g:i A", strtotime($date)); */
}

function getCurrentTimezoneDateTime($date)
{
    if(!\Session::has('reekar_admin_timezone')){
        \Session::put('reekar_admin_timezone', 'Asia/Kolkata');
    }
    $timezone = \Session::get('reekar_admin_timezone');
    
    $serverBokingToDate = new DateTime($date); 
    $serverBokingToDate->setTimezone(new DateTimeZone($timezone));            
    $newdate = $serverBokingToDate->format('d-M-Y h:i A');
    if($newdate){
        return $newdate;
    }else{
        return $newdate = false;
    }
    
}

function getUTCTimezoneTime($date)
{  
    if(!\Session::has('reekar_admin_timezone')){
        \Session::put('reekar_admin_timezone', 'Asia/Kolkata');
    }
    $timezone = \Session::get('reekar_admin_timezone');
    date_default_timezone_set($timezone);
     
    $time_zone = 'UTC';
    $serverBokingToDate = new DateTime($date); 
    $serverBokingToDate->setTimezone(new DateTimeZone($time_zone));            
    $newdate = $serverBokingToDate->format('H:i:s');
    if($newdate){
        return $newdate;
    }else{
        return $newdate = false;
    }
  
}
function getUTCTimezoneDate($date)
{  
    $time_zone = 'UTC';
    $serverBokingToDate = new DateTime($date); 
    $serverBokingToDate->setTimezone(new DateTimeZone($time_zone));            
    $newdate = $serverBokingToDate->format('Y-m-d');
    if($newdate){
        return $newdate;
    }else{
        return $newdate = false;
    }
  
}
function getUTCTimezoneDateTime($date)
{

    if(!\Session::has('reekar_admin_timezone')){
        \Session::put('reekar_admin_timezone', 'Asia/Kolkata');
    }
    $timezone = \Session::get('reekar_admin_timezone');
    date_default_timezone_set($timezone);
    $time_zone = 'UTC';
    $serverBokingToDate = new DateTime($date); 
    $serverBokingToDate->setTimezone(new DateTimeZone($time_zone));            
    $newdate = $serverBokingToDate->format('Y-m-d H:i:s');
    if($newdate){
        return $newdate;
    }else{
        return $newdate = false;
    }
    
}

/*Currency Convirsion start */
function currencyConverter(){
    //if(time() == '23:30:')
     
    try {
        $req_url = 'https://api.exchangerate-api.com/v4/latest/USD';
        $response_json = file_get_contents($req_url);
      }      
      //catch exception
      catch(Exception $e) {
        //echo 'Message: ' .$e->getMessage();
        $response_json = '';
      }
    
    
    if($response_json != ''){
        $item = CurrencyConversion::find(1);
        if($item){
            $item->currency_rate = $response_json;
            $item->save();            
        }
        return $item->currency_rate ;

    }else{

        $item = CurrencyConversion::first();

        return $item->currency_rate ;
    }
    

    //return $response_json;
}

function getCurrencyFromUser($from_currency, $to_currency, $base_currency, $num_currency = '',  $response_json){

    // $req_url = 'https://api.exchangerate-api.com/v4/latest/'.$currency;
    

    // Continuing if we got a result
    if($response_json !== false) { 
        try { 
        // Decoding
        $response_object = json_decode($response_json); 
        $base_price = ($num_currency != '') ? $num_currency : ''; // Your price in USD
        // echo $EUR_price = round(($base_price * $response_object->rates->INR), 2);
        
        if($from_currency == "USD"){
            $base_value = $base_price * (1/$response_object->rates->$base_currency);
            $EUR_price = round(($base_value * $response_object->rates->$to_currency), 2);
        
        }else{
            // $EUR_price = round(($base_price * (1/$response_object->rates->$from_currency)), 2);
            $EUR_price = $base_price * (1/$response_object->rates->$from_currency);
             
        }        
        return $EUR_price;

        }
        catch(Exception $e) {
            // Handle JSON parse error...
        }

    }
}
/*Currency Convirsion End */
//Available currency
function availCurrency(){
    $items = CurrencyConversion::first();
    if($items){
        $items = json_decode($items->currency_rate);
        $data = [];
        foreach ($items->rates as $key => $val){
            $data[] = $key;
        }
        return $data;       
    }else{
       return $data = [];
    } 
}
//Is rented
/* public function getIsRentedAttribute() {
    if(Orders::where('car_id', $this->id)->where('order_status', 2)->where('booking_from_date', '<=', date('Y-d-m H:i:s'))->where('booking_to_date', '>=', date('Y-m-d H:i:s'))->first()){
        return true;
    } else{
        return false;
    }
} */

//FCM 
function fcm_notification($message, $title, $key, $action){
			
    //$title = "Reekar NewBooking";
    //$action= "driver";
    //$message = "you have a booking request from  ".$userNam."  please wait for response to your manager";
        
    $server_key = "AAAADQ2yz9s:APA91bHQnpV4rPhtnJ6W1sNICnEmdUPy0Mbh5Exd_gJoojEcxzHcgM15858sonbL67LYNQzkxf1KEclrA-6h-3L66m5TdpDtNuGABjEAl7WuV-pV5GMz_nhAYxJMvxWxNlLnK3TtV2ZS";
    $url = 'https://fcm.googleapis.com/fcm/send';

    $headers = array('Authorization:key ='.$server_key,
                      'Content-Type: application/json');
    $id = rand(1, 100);
    $fileds = array('to'=>$key,
    'data'=>array('id' => $id,'click_action'=> $action),
    'notification'=>array('title' => $title, 'body'=>$message,'click_action'=> $action)
    );

    $payload = json_encode($fileds);
    $ch = curl_init();
    curl_setopt( $ch,CURLOPT_URL, $url);
    curl_setopt( $ch,CURLOPT_POST, true );
    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
    //curl_setopt( $ch,CURLOPT_IPRESOLVE, CURLOPT_IPRESOLVE_V4 );
    curl_setopt( $ch,CURLOPT_POSTFIELDS,$payload);
    $result = curl_exec($ch );
    $xy = json_decode($result);
    //print_r($result);
    
    //die(); 

    curl_close( $ch );
}

/* get current lcation and currency for all pages */
function getCurrentLocationData( $ip = ''){
    $curl = curl_init();
    //dd($url);
    curl_setopt_array($curl, array(
            CURLOPT_URL => "http://www.geoplugin.net/json.gp?ip=$ip",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                    'Accept:application/json'
            ),
    ));
    $response = curl_exec($curl);
    //dd($response);
    $err = curl_error($curl);
    curl_close($curl);

    if ($err) {
            echo "cURL Error #:" . $err;
    } else {
            return json_decode($response);
    }

     /* $totalPrice =  date_diff(date_create($request->available_from_date), date_create($request->available_to_date)); 
      
       return response()->json([
        'success' => false,
        'message' => $totalPrice->format("%a days"),
        'redirect' => 'javascript:void(0)',
        ]);  */
}