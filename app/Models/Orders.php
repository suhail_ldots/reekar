<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Orders extends Model
{
    use SoftDeletes;
    protected $casts = [
        'doc' => 'array',
    ];

    public function user(){
    	return $this->belongsTo('\App\User', 'user_id')->withTrashed();
    }
    public function car(){
    	return $this->belongsTo('\App\Models\Cars', 'car_id')->withTrashed();
    }
}
