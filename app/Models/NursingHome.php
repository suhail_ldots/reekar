<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NursingHome extends Model
{
    use SoftDeletes;

    public function package(){
        return $this->belongsTo('App\Models\Masters\SubscriptionPackages', 'subscription_package_id');
    }
   
    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }
    public function doctor_nursing_r(){
        return $this->hasMany('App\Models\DoctorNursingHomeRelation', 'nursing_home_id');
    }
    public function galleryImages(){
        return $this->hasMany('App\Models\GalleryImages', 'nursing_home_id');
    }

    public function subscription(){
        return $this->hasOne('App\Models\SubscriptionOrders', 'nursing_home_id');
    }

    public function subscription_order(){
        return $this->hasMany('App\Models\SubscriptionOrders', 'nursing_home_id')->orderBy('id', 'DESC');
    }
}
