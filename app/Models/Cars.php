<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cars extends Model
{
    use SoftDeletes;

    public function carImages(){
    	return $this->hasMany(CarImages::class, 'car_id');
    }
    public function carDocs(){
    	return $this->hasMany(CarDocs::class, 'car_id');
    }
    public function brand(){
    	return $this->belongsTo(Brands::class, 'brand_id')->withTrashed();
    }
    public function user(){
    	return $this->belongsTo(\App\User::class, 'user_id')->withTrashed();
    }

    public function image(){
    	return $this->hasOne(CarImages::class, 'car_id');
    }

    public function images(){
    	return $this->hasMany(CarImages::class, 'car_id');
    }

    public function docs(){
        return $this->hasMany(CarDocs::class, 'car_id');
    }

    public function model(){
    	return $this->belongsTo(Models::class, 'model_id')->withTrashed();
    }

    public function color(){
    	return $this->belongsTo(Colors::class, 'color_id')->withTrashed();
    }

    public function seat(){
    	return $this->belongsTo(Seats::class, 'seat_id');
    }

    public function enginetype(){
    	return $this->belongsTo(EngineTypes::class, 'engine_type_id')->withTrashed();
    }

    public function transmission(){
    	return $this->belongsTo(Transmissions::class, 'transmission_id')->withTrashed();
    }

    public function vehicletype(){
    	return $this->belongsTo(VehicleTypes::class, 'vehicle_type_id')->withTrashed();
    }

    public function owner(){
    	return $this->belongsTo('App\User', 'user_id')->withTrashed();
    }

    // public function getCurrentUsdValueAttribute(){
    //     $jsoncurrency = currencyConverter();
    //    return getCurrencyFromUser($this->currency, 'USD', $this->price_per_day, $jsoncurrency);
    // }
    // public function getCurrentSecurityUsdValueAttribute(){
    //     $jsoncurrency = currencyConverter();
    //    return getCurrencyFromUser($this->currency, 'USD', $this->security_amount, $jsoncurrency);
    // }

    public function scopeIsWithinMaxDistance($query, $coordinates, $radius = 300) {

        $haversine = "(6371 * acos(cos(radians(" . $coordinates['latitude'] . ")) 
                        * cos(radians(`car_location_lat`)) 
                        * cos(radians(`car_location_long`) 
                        - radians(" . $coordinates['longitude'] . ")) 
                        + sin(radians(" . $coordinates['latitude'] . ")) 
                        * sin(radians(`car_location_lat`))))";
    
        return $query->select('id', 'brand_id', 'model_id', 'color_id', 'engine_type_id', 'transmission_id', 'vehicle_type_id', 'seat_id', 'year', 'car_location', 'available_from_date', 'available_from_time', 'available_to_date', 'available_to_time', 'currency', 'price_per_day', 'total_price', 'ac', 'gps', 'ipod_interface', 'sunroof', 'child_seat', 'electric_windows', 'heated_seat', 'panorma_roof', 'prm_gauge', 'abs', 'traction_control', 'audio_system', 'car_location_lat', 'car_location_long', 'listing_status', 'reject_reason')
                    ->selectRaw("{$haversine} AS distance")
                     ->whereRaw("{$haversine} < ?", [$radius]);
    }

    public function getIsRentedAttribute() {
        if(Orders::where('car_id', $this->id)->where('order_status', 2)->where('booking_from_date', '<=', date('Y-d-m H:i:s'))->where('booking_to_date', '>=', date('Y-m-d H:i:s'))->first()){
            return true;
        } else{
            return false;
        }
    }

    public function getIsBeetweendAttribute() {
        if(Cars::where('id', $this->id)->where('status', 1)->whereBetween('price_per_day', [(10), (12)])->first()){
            return true;
        } else{
            return false;
        }
    }

    public function getIsRentedOnDateAttribute($from_date, $to_date) {
        $query = Orders::query();
        $query->where('car_id', $this->id)->where('order_status', 2);
        $query->where(function ($query1) use($from_date, $to_date){
            if($to_date){
                $query1
                ->where(function ($query2) use($from_date, $to_date){
                    $query2->where('booking_from_date', '>', date('Y-m-d H:i:s', strtotime($from_date)))
                ->orWhere('booking_from_date', '>', date('Y-m-d H:i:s', strtotime($to_date)));
                })
                ->orWhere(function ($query3) use($from_date, $to_date){
                    $query3->where('booking_to_date', '>', date('Y-m-d H:i:s', strtotime($from_date)))
                ->orWhere('booking_to_date', '>', date('Y-m-d H:i:s', strtotime($to_date)));
                });
            }else{
                $query1
                ->where('booking_from_date', '<', date('Y-d-m H:i:s', strtotime($from_date)))
                ->orWhere('booking_to_date', '>', date('Y-m-d H:i:s', strtotime($from_date)));
            }
            
        });
        $order = $query->first();
        if($order){
            return true;
        } else{
            return false;
        }
    }
}
