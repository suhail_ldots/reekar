<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Brands;

class Models extends Model
{
    use SoftDeletes;
    public function brand(){                
        return $this->belongsTo('App\Models\Brands', 'brand_id')->withTrashed();
    }
}
