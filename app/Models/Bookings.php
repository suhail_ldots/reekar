<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bookings extends Model
{
    public function doctor(){
        return $this->belongsTo('App\Models\Doctor', 'doctor_id');
    }    
    public function nursinghome(){
        return $this->belongsTo('App\Models\NursingHome', 'nursing_home_id');
    }

    //get patient 
    public function user(){
        return $this->belongsTo('App\User', 'patient_id');
    }
}
