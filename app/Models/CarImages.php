<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CarImages extends Model
{
    use SoftDeletes;
    public function car(){
        return $this->belongs_to(Cars::class, 'car_id')->withTrashed();
    }
}
