<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubscriptionPackages extends Model
{
   use SoftDeletes;

    public function features(){
    	return $this->hasMany(PlanFeature::class, 'subscription_package_id');
    }
}
