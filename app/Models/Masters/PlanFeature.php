<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;

class PlanFeature extends Model
{
    public function features(){
    	return $this->belongsTo(Feature::class, 'feature_id');
    }

    public function plan(){
    	return $this->belongsTo(SubscriptionPackages::class, 'subscription_package_id');
    }
}
