<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubscriptionOrders extends Model
{
    public function nursinghome(){
        return $this->belongsTo('App\Models\NursingHome', 'nursing_home_id');
    }
}
