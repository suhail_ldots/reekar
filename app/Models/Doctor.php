<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Doctor extends Model
{
    // use SoftDeletes;
    protected $casts = [
        'availability' => 'array',
    ];

    public function doctorSpeciality(){
        return $this->belongsTo('App\Models\Specialities', 'speciality');
    }
    public function department(){
        return $this->belongsTo('App\Models\masters\Department', 'department_id');
    }

    public function withTrashedUser(){
        return $this->belongsTo('App\User', 'user_id')->withTrashed();
    }
    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }
}
