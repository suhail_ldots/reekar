<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DoctorNursingHomeRelation extends Model
{
    public function doctor(){
        return $this->belongsTo('App\Models\Doctor', 'doctor_id');
    }
}
