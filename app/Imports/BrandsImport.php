<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Models\Brands;
use App\Models\Models;
use Auth;

class BrandsImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        foreach ($collection->slice(1) as $row) 
        {
            $brand = Brands::where('name',$row[0])->first();

            if(empty($brand)){

                $brand = new Brands();
                $brand->name = $row[0];
                $brand->save();

            }

            $model = Models::where('name',$row[1])->where('brand_id', $brand->id)->first();
            if(empty($state)){
                $model = new Models();
                $model->name = $row[1];
                $model->brand_id = $brand->id;
                $model->save();
            }

            /**
             * Important Note Here $row[3] has the city name in the excel sheet in 4th column column 3rd column has sub sub region 5th has zipcodes
              */
              
            //$city1 = MasterCity::where('name', $row[3])->first();
           // if($city1){ 
                
           // }
            
 
        }
    }
}
