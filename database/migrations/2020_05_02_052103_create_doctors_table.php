<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoctorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->string('reg_no')->nullable();
            $table->integer('department_id')->nullable();
            $table->string('speciality')->nullable();
            $table->string('degree')->nullable();
            $table->string('experience')->nullable();
            $table->string('fees')->nullable();
            $table->text('availability')->nullable();
            $table->string('working_days')->nullable();
            $table->string('past_experiences')->nullable();
            $table->string('stamp_image')->nullable();
            $table->string('bio')->nullable();
            $table->string('esign')->nullable();
            $table->string('profile_image')->nullable();
            $table->string('about_doc')->nullable();
            $table->string('facebook_url')->nullable();
            $table->string('twitter_url')->nullable();
            $table->string('linkedin_url')->nullable();
            $table->string('youtube_url')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctors');
    }
}
