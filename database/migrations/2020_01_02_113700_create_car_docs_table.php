<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarDocsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_docs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('car_id')->nullable();
            $table->string('doc')->nullable();
            $table->string('type')->default('pdf');
            $table->enum('status',['1','0'])->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_docs');
    }
}
