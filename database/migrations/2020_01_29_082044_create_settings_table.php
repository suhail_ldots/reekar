<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('data_per_page');
            $table->string('company_name');
            $table->string('logo')->nullable();
            $table->string('fav_icon')->nullable();            
            $table->string('contact_person')->nullable();
            $table->string('country_code','20')->nullable();            
            $table->string('contact_no');
            $table->string('alternate_contact_no')->nullable();
            $table->string('email');
            $table->string('company_full_address','400');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
