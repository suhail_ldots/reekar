<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->nullable();
            $table->bigInteger('car_id')->nullable();
            $table->string('booking_id')->nullable();
            $table->datetime('booking_from_date')->nullable();
            $table->datetime('booking_to_date')->nullable();
            $table->string('currency')->nullable();
            $table->double('paid_amount')->default(0);
            $table->double('total_amount')->default(0);
            $table->double('security_amount')->default(0);
            $table->double('tax_amount')->default(0);
            $table->double('grand_total')->default(0);
            $table->string('payment_method')->nullable();
            $table->string('bank_reference_id')->nullable();
            $table->string('transaction_id')->nullable();
            $table->string('billing_first_name')->nullable();
            $table->string('billing_last_name')->nullable();
            $table->string('billing_email')->nullable();
            $table->string('billing_mobile')->nullable();
            $table->string('billing_alternate_mobile')->nullable();
            $table->string('billing_address')->nullable();
            $table->string('pickup_location')->nullable();
            $table->string('doc')->nullable();
            $table->integer('order_status')->default(1)->comment('1 is using for pending, 2 for success, 3 for Failed and 4 is using for canceled');
            $table->enum('paid_to_owner_status',['1','0'])->default(0);
            $table->enum('status',['1','0'])->default(1);
            $table->string('order_invoice','400')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
