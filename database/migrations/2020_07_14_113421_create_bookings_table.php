<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->biginteger('patient_id')->comment('belongs to user table');;
            $table->biginteger('nursing_home_id');
            $table->biginteger('doctor_id')->nullable()->comment('belongs to doctor table');
            $table->string('desease')->nullable();
            $table->string('other_problems')->nullable();
            $table->string('appointment_day')->nullable();
            $table->string('appointment_time')->nullable();
            $table->tinyinteger('status')->default(0)->comment('0->pending, 1->success, 2->completed, 4->rejected');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
