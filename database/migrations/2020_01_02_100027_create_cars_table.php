<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->nullable();
            $table->string('brand_id')->nullable();
            $table->string('model_id')->nullable();
            $table->string('vehicle_type_id')->nullable();
            $table->string('engine_type_id')->nullable();
            $table->string('transmission_id')->nullable();
            $table->string('color_id')->nullable();
            $table->string('year')->nullable();
            $table->string('car_location')->nullable();
            $table->string('car_location_lat')->nullable();
            $table->string('car_location_long')->nullable();
            $table->date('available_from_date')->nullable();
            $table->time('available_from_time')->nullable();
            $table->date('available_to_date')->nullable();
            $table->time('available_to_time')->nullable();
            $table->string('currency')->default('USD');
            $table->double('price_per_day')->default(0);
            $table->double('total_price')->default(0);
            //$table->double('security_amount')->default(0);
            $table->integer('ac')->default(0);
            $table->integer('gps')->default(0);
            $table->integer('ipod_interface')->default(0);
            $table->integer('sunroof')->default(0);
            $table->integer('child_seat')->default(0);
            $table->integer('electric_windows')->default(0);
            $table->integer('heated_seat')->default(0);
            $table->integer('panorma_roof')->default(0);
            $table->integer('prm_gauge')->default(0);
            $table->integer('abs')->default(0);
            $table->integer('traction_control')->default(0);
            $table->integer('audio_system')->default(0);
            $table->enum('status',['1','0'])->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
