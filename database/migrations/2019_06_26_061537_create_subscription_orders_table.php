<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('nursing_home_id')->nullable();
            $table->string('email')->nullable();
            $table->string('booking_id')->nullable();
            $table->double('paid_amount')->nullable();
            $table->double('grand_total')->default(0);
            $table->string('payment_gateway')->nullable();
            $table->string('bank_ref_id')->nullable();
            $table->string('transaction_id')->nullable();
            $table->string('pg_auth_id')->nullable();
            $table->string('card_no')->nullable();
            $table->string('stripe_invoice')->nullable();
            $table->string('exp_date')->nullable();
            $table->string('card_type')->nullable();
            $table->integer('order_status')->default(0);
            $table->date('plan_exp')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_orders');
    }
}
