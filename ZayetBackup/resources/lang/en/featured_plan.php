<?php

return array (
  'plan_tag' => 'Please fill in account information and payment details to avail plan benefits. You can upgrade your plans later.',
  'form_fill_info' => 'Account Information',
  'account_type' => 'Choose your account type ?',
  'drop_down_select' => 'select',
  'single_doctor_clinic' => 'Single Doctor Clinic',
  'multispeciality_hospital' => 'Multispeciality Hospital',
  'last_name' => 'Last Name',
  'email' => 'Email',
  'phone' => 'Phone',
  'address' => 'Address',
  'first_name' => 'First Name',
  'country' => 'Country',
  'zipcode' => 'Zipcode',
  'city' => 'City',
);
