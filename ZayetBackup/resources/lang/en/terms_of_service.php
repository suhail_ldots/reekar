<?php

return array (
  'terms_of_service' => 'Terms of Service',
  'read_terms_of_service' => 'Please read these terms of service carefully as they contain important information regarding your legal rights, remedies and obligations. These include various limitations and exclusions, a clause that governs the jurisdiction and venue of disputes, an agreement to arbitrate on an individual basis (unless you opt out), and obligations to comply with applicable laws and regulations.',
  'definition' => 'Definitions:',
);
