@component('mail::message')

#Hi, {{$user->first_name}}<br/>


Thank you for choosing Reekar as your Car rental partner.<br/>


Your account have been successfully created on Reekar platform. Now you can enjoy renting cars in your area and listing your car for rent.<br/>

<br/>

Best Regards, <br/>
-{{ config('app.name') }}
@endcomponent