@component('mail::message')
# Dear {{ ucfirst($user->first_name).' '.ucfirst($user->last_name) }}

Your Request for registering Car on www.reekar.com has been rejected <br> Due to {{ $item->reject_reason }}


Thanks & Regards,<br>
{{ config('app.name') }}
@endcomponent
