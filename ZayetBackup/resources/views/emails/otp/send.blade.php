@component('mail::message')

#Hi, <br/>
Greetings!<br/>


You are just a step away from access your <b>Reekar</b> account.<br/>


We are sharing a verification code to access your account. The code is valid for only once.<br/>


<br/>

Your OTP- <b>{{$otp}}</b><br/>



Best Regards, <br/>
-{{ config('app.name') }}
@endcomponent