<!-- preloader Start -->
<div id="preloader">
        <div id="status">
            <img src="{{ asset('core/images/preloader.gif') }}" id="preloader_image" alt="loader">
        </div>
    </div>
    <!--top header start-->
    <div class="md_header_wrapper">
        <div class="top_header_section hidden-xs">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                        <div class="top_header_add">
                            <ul>
                                <li><i class="fa fa-map-marker" aria-hidden="true"></i><span>@lang('header_footer.address') :</span> -12/california,us</li>
                                <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="#"><span>@lang('header_footer.email') :</span> medical@example.com</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                        <div class="right_side_main_warpper">
                            <div class="md_right_side_warpper">
                                <ul>
                                    <li><a href="{{ route('login') }}">@lang('header_footer.login')</a></li>
                                    <li><a href="{{ route('register') }}">@lang('header_footer.sign_up')</a></li>
                                    <!-- <li><a href="#"><span class="Multi-language"><img src="{{ asset('core/images/English-multi.png') }}"></span>English</a></li> -->
                                    <li><div class="dropdown">
    <button class="dropdown-toggle" type="button" data-toggle="dropdown" style="background:none; border:0" aria-expanded="false"><a href="#">
                                    
                                    <span class="Multi-language">@lang('header_footer.lang')</a>
    <span class="caret"></span></button>
    <ul class="dropdown-menu">
      <ul>
                <li><a href="{{ url('locale/en') }}" ><span class="Multi-language"><img src="{{ asset('core/images/lang_icons/English-multi.png') }}"></span> EN</a></li>
                <li><a href="{{ url('locale/arabic') }}" ><span class="Multi-language"><img src="{{ asset('core/images/lang_icons/arabic_lang.png') }}"></span> ARABIC</a></li>
                </ul>
                                   
   
  </div></li><style>
                                       .dropdown-menu li { margin-left: 0px !important; width: 100% !important; padding-left:10px;}
                                       .dropdown-menu li a{ color: #000 !important; margin-left: 0px !important
                                       }
                                       .md_right_side_warpper .dropdown-menu{    border-radius: 0;  border: 0; min-width: 115px;}
                                    </style>
                                    <!-- <li style="display:none">
                                    <a href="javascrip:void(0)">Lang</a>
                                        <ul>
                                        <li><a href="{{ url('locale/en') }}" ><span class="Multi-language"><img src="{{ asset('core/images/English-multi.png') }}"></span> EN</a></li>
                                        <li><a href="{{ url('locale/arabic') }}" ><span class="Multi-language"><img src="{{ asset('core/images/arabic_lang.png') }}"></span> ARABIC</a></li>
                                        </ul>
                                    <span class="Multi-language"><img src="{{ asset('core/images/English-multi.png') }}"></span>English
                                    </a>
                                    </li> -->
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--top header end-->
    <!--header menu wrapper-->
    <div class="menu_wrapper header-area hidden-menu-bar stick">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <nav class="navbar hidden-xs">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse nav_response" id="bs-example-navbar-collapse-1">
                            <ul class="logo-header">
                                <li>
                                    <a href="{{ route('main') }}"><img src="{{ asset('core/images/index_4_logo.png') }}"></a>
                                </li>
                            </ul>
                            <ul class="nav navbar-nav" id="nav_filter">
                                
                                <li class="dropdown"><a href="#" class="first_menu">@lang('header_footer.about_us')</a>
                                </li>
                                <li class="dropdown"> <a href="#" class="dropdown-toggle first_menu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">@lang('header_footer.services')</a>
                                    <!-- <ul class="dropdown-menu hovr_nav_tab">
                                        <li><a href="#">services</a>
                                        </li>
                                        <li><a href="#">events</a>
                                        </li>
                                        <li><a href="#">pricing</a>
                                        </li>
                                    </ul> -->
                                </li>
                                <li class="dropdown"> <a href="#" class="dropdown-toggle first_menu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">@lang('header_footer.become_partner')</a></li>
                                <li class="dropdown"> <a href="{{ route('search.nursing') }}" class="first_menu" role="button" aria-haspopup="true" >@lang('header_footer.doctors')</a>
                                </li>
                                <li class="dropdown"> <a href="#" class="dropdown-toggle first_menu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">@lang('header_footer.contact')</a>
                                </li>
                                <li class="dropdown"> <a  href="{{ Route::currentRouteName() != 'main' ? route('main') : '#bookNow' }}" class="first_menu header-buttonn">@lang('header_footer.reg_as_member')</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /.navbar-collapse -->
                    </nav>
                    <div class="rp_mobail_menu_main_wrapper visible-xs">
                        <div class="row">
                            <div class="col-xs-8">
                                <div class="gc_logo logo_hidn">
                                    <img src="{{ asset('core/images/index_4_logo.png') }}" class="img-responsive" alt="logo">
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div id="toggle">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 31.177 31.177" style="enable-background:new 0 0 31.177 31.177;" xml:space="preserve" width="25px" height="25px">
                                        <g>
                                            <g>
                                                <path class="menubar" d="M30.23,1.775H0.946c-0.489,0-0.887-0.398-0.887-0.888S0.457,0,0.946,0H30.23    c0.49,0,0.888,0.398,0.888,0.888S30.72,1.775,30.23,1.775z" fill="white" />
                                            </g>
                                            <g>
                                                <path class="menubar" d="M30.23,9.126H12.069c-0.49,0-0.888-0.398-0.888-0.888c0-0.49,0.398-0.888,0.888-0.888H30.23    c0.49,0,0.888,0.397,0.888,0.888C31.118,8.729,30.72,9.126,30.23,9.126z" fill="white" />
                                            </g>
                                            <g>
                                                <path class="menubar" d="M30.23,16.477H0.946c-0.489,0-0.887-0.398-0.887-0.888c0-0.49,0.398-0.888,0.887-0.888H30.23    c0.49,0,0.888,0.397,0.888,0.888C31.118,16.079,30.72,16.477,30.23,16.477z" fill="white" />
                                            </g>
                                            <g>
                                                <path class="menubar" d="M30.23,23.826H12.069c-0.49,0-0.888-0.396-0.888-0.887c0-0.49,0.398-0.888,0.888-0.888H30.23    c0.49,0,0.888,0.397,0.888,0.888C31.118,23.43,30.72,23.826,30.23,23.826z" fill="white" />
                                            </g>
                                            <g>
                                                <path class="menubar" d="M30.23,31.177H0.946c-0.489,0-0.887-0.396-0.887-0.887c0-0.49,0.398-0.888,0.887-0.888H30.23    c0.49,0,0.888,0.398,0.888,0.888C31.118,30.78,30.72,31.177,30.23,31.177z" fill="white" />
                                            </g>
                                        </g>
                                    </svg>
                                </div>
                            </div>
                        </div>
                        <div id="sidebar">
                            <h1><a href="#">LOGO<span>Company</span></a></h1>
                            <div id="toggle_close">&times;</div>
                            <div id='cssmenu' class="wd_single_index_menu">
                                <ul>
                                    <li class='has-sub'><a href='#'>Home</a>
                                    </li>
                                    <li><a href="#">about us</a>
                                    </li>
                                    <li class='has-sub'><a href='#'>services</a>
                                        <ul>
                                            <li><a href="#">Services1</a>
                                            </li>
                                            <li><a href="#">Services2</a>
                                            </li>
                                            <li><a href="#">Services3</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class='has-sub'><a href='#'>Become a Doctor</a>
                                    </li>
                                    <li class='has-sub'><a href='#'>Doctors</a>
                                        <ul>
                                            <li><a href="gallery_2.html">Doctor 1</a>
                                            </li>
                                            <li><a href="gallery_3.html">Doctor 2</a>
                                            </li>
                                            <li><a href="gallery_4.html">Doctor 3</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class='has-sub'><a href='#'>contact</a> </li>
                                     <li class="dropdown"> <a href="#bookNow" class="first_menu header-buttonn">Register as Member</a>
                                    </li>
                                    
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--header wrapper end-->
    