    <!-- footer wrapper start-->
    <div class="footer_wrapper">
        <div class="container">
            <div class="box_1_wrapper">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="address_main">
                            <div class="footer_widget_add">
                                <a href="{{ route('main') }}"><img src="{{ asset('core/images/logo-white.png') }}" class="img-responsive" alt="footer_logo" /></a>
                                <p>@lang('header_footer.about_us_desc')</p>
                                <a href="#">@lang('home.read_more') <i class="fa fa-long-arrow-right"></i></a>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!--footer_1-->
            <div class="booking_box_div">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="footer_main_wrapper">

                            <!--footer_2-->
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 respons_footer_nav hidden-xs">
                                <div class="footer_heading footer_menu">
                                    <h1 class="med_bottompadder10">@lang('header_footer.contact_us')</h1>
                                   
                                </div>
                              <div class="footer_box_add">
                                <ul>
                                    <li><i class="fa fa-map-marker" aria-hidden="true"></i><span>Address : </span>-512/fonia,canada</li>
                                    <li><i class="fa fa-phone" aria-hidden="true"></i><span>Call us : </span>+61 5001444-122</li>
                                    <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="#"><span>Email :</span> dummy@example.com</a></li>
                                </ul>
                            </div>
                                    
                            </div>
                            
                            <!--footer_3-->
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 contact_last_div">
                                <div class="footer_heading">
                                    <h1 class="med_bottompadder10">@lang('header_footer.connect_with_us')</h1>
                                </div>
                                <div class="footer_btm_icon">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i>Facebook</a></li>
                                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i>Twitter</a></li>
                                        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i>Linkedin</a></li>                                                                                                                           
                                    </ul>
                                </div>
                                <!--footer_4-->
                            </div>
                        </div>
                        
                        <!--footer_5-->
                    </div>
                </div>
            </div>
            
            <div class="container-fluid">
                <div class="up_wrapper">
                    <a href="javascript:" id="return-to-top"><i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="footer_botm_wrapper">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="bottom_footer_copy_wrapper grey-">
						<span>Copyright © 2020- Doctors Loop</a></span>
					</div>
					
				</div>
			</div>
        </div>
    <!--footer wrapper end-->
        
@include('site_view.layouts.footer_script')
