@extends('site_view.layouts.main')
@section('content')
    <!--bio section start-->
    <div class="med_doctor_info">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                </div>
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 no-padding">
                        <div class="doctor_info">
                            <h1>{{ $item->first_name.' '.$item->last_name }} {{ $item->doctor->degree ? '('.$item->doctor->degree.')' : '' }} </h1>
                            
                        </div>
                        <div class="doctor_info">
                            <p class="lic-no">Licence no : {{ $item->doctor->reg_no ? $item->doctor->reg_no : '' }} </p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 no-padding">
                        <div class="doctor_info2">
                            <!-- <ul class="inner-for-ratings">
                                <li class="ratingg-text">Rating</li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                            </ul> -->
                        </div>
                        <div class="doctor_info3">
                            <ul class="inner-for-ratings22">
                                <li class="ratingg-text"><a href="{{ route('book.appointment', $item->id) }}">Book Now</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!--bio section end-->
    <!--single doc info start-->
    <div class="bio_section_doc">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="team_about doc_img_top fix-my-height">
                        <div class="team_img bio_team_img">
                            <img src="{{ @fopen( \Url('storage/app/images/doctor/profile/').'/'.$item->profile_pic, r) ? \Url('storage/app/images/doctor/profile/').'/'.$item->profile_pic : asset('core/images/single_doc.jpg') }}" alt="img" class="img-responsive">
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <div class="choose_heading_wrapper about_heading_wraper fonnt-color">
                        <h1 class="med_bottompadder20 med_toppadder50">@lang('clinic_details.biography')</h1>
                        <img src="{{ asset('core/images/dental_line_blue-single.png') }}" alt="line" class="med_bottompadder20">
                        <p>{{ $item->doctor->about_doc ? $item->doctor->about_doc : '' }}</p>
                        <!-- <p class="hidden-sm">Aenean sollicitudin, lorem quis bibendum auc tor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit.</p> -->
                    </div>
                </div>
                <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ant_txt">
                    <p>lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet yunibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed nonmauris vitae erat consequaiut auctor eu in elit. Class aptent taciti so- ciosqu ad litora torquent per conubia nostra, per inceptos himenaeos. </p>
                </div> -->
            </div>
        </div>
    </div>
    <!--single doc info end-->
    <!--our team wrapper start-->
    <div class="team_wrapper our-doctor-pagee">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2">
                    <div class="about_heading_wraper  text-center med_bottompadder50">
                        <h1 class="med_bottompadder20">@lang('clinic_details.available_timings')</h1>
                        <img src="{{ asset('core/images/dental_line_center.png') }}" alt="line" class="med_bottompadder20">
                    </div>
                </div>
                @php
                    $working_days = explode(",", isset($item->doctor->working_days) ? $item->doctor->working_days :'' );
                    if($item->doctor->availability){
                     // dd($item->doctor->availability); 
                     $avail = optional($item->doctor)->availability;
                    }else{
                        $avail = false;
                    }
                     
                    @endphp
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="timiee">
                        <div class="ull-1">
                            <ul class="ull-1-inner">
                            @php $workingday = getMyWorkingDay($item->doctor->working_days != '' ? $item->doctor->working_days : '0' ); @endphp
                                @forelse($workingday as $day)
                                    <li>{{ $day }}</li>
                                    <!-- <li>Tuesday</li>
                                    <li>Wednesday</li>
                                    <li>Thursday</li>
                                    <li>Friday</li>-->
                                @empty
                                <li>Not available</li>
                                @endforelse                                
                            </ul>

                        </div>
                        <div class="ull-2">
                            <ul class="ull-1-inner">
                               @if($avail)
                               @foreach( $working_days as $working_day)
                                <li>{{ $avail[$working_day]['availability_from'] }} - {{ $avail[$working_day]['availability_to'] }}</li>
                               @endforeach
                               @endif
                                <!-- <li>10:00 am - 2:00 pm</li>
                                <li>10:00 am - 2:00 pm</li>
                                <li>10:00 am - 2:00 pm</li>
                                <li>10:00 am - 2:00 pm</li>
                                <li>10:00 am - 2:00 pm</li>
                                <li>10:00 am - 2:00 pm</li> -->
                            </ul>
                        </div>

                        <!-- <div class="ull-2">
                            <ul class="ull-1-inner">
                                <li>10:00 am - 2:00 pm</li>
                                <li>10:00 am - 2:00 pm</li>
                                <li>10:00 am - 2:00 pm</li>
                                <li>10:00 am - 2:00 pm</li>
                                <li>10:00 am - 2:00 pm</li>
                                <li>10:00 am - 2:00 pm</li>
                                <li>10:00 am - 2:00 pm</li>
                            </ul>

                        </div> -->
                    </div>

                </div>

            </div>
        </div>
    </div>

    <!--team wrapper end-->
        <!--our team wrapper start-->
    <div class="team_wrapper appoint-book">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2">
                    <div class="about_heading_wraper  text-center med_bottompadder50">
                        <h1 class="med_bottompadder20">@lang('home.book_appointment')</h1>
                        <img src="{{ asset('core/images/dental_line_center.png') }}" alt="line" class="med_bottompadder20">
                    </div>
                </div>
                    <!-- appoint_section start -->
    <div class="booking_wrapper book_section med_toppadder100">
        <div class="container clearfix">
            <div class="booking_box">
                <div class="row">
                    <form>

                    <div class="box_side_icon clearfix">
                      <!--   <div class="left--img">
                            <img src="{{ asset('core/images/Icon_bk.png') }}" alt="img">
                        </div> -->
                        
                        <div class="about_heading_wraper  text-center med_bottompadder50">
                            <h1 class="med_bottompadder20">@lang('clinic_details.fill_info')</h1>
                            <p>@lang('clinic_details.fill_info_desc')</p>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="contect_form1">
                                <label for="cars">Treatment</label>

                                <!-- <select id="cars" class="classic">
                                  <option>Choose your treatment</option>
                                  <option>Treatment category 1</option>
                                  <option>Treatment category 2</option>
                                  <option>Treatment category 3</option>
                                </select> -->
                                <select id="cars" name="search_speciality" class="classic">
                                    <option value="">Choose doctor speciality </option>
                                    <?php $specialities = speciality(); if($specialities){ foreach($specialities as $speciality){?> 
                                    
                                    <option value="{{ $speciality->id }}" {{ isset($search_speciality) && $search_speciality == $speciality->id ? 'selected' :'' }}>{{ $speciality->name }}</option>

                                    <?php } } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="contect_form1">
                                <label for="cars">Problem (Disease)</label>
                                <select id="cars" name="patient_disease" class="classic">
                                    <option value="">Choose your problem </option>
                                    <option value="1">Cough </option>
                                    <option value="2">Fever</option>
                                    <option value="3">Vomitting</option>
                                    <option value="4">Others</option>
                                    @if(speciality())
                                    @foreach(speciality() as $speciality)                                    
                                    <option value="{{ $speciality->id }}" {{ isset($search_speciality) && $search_speciality == $speciality->id ? 'selected' :'' }}>{{ $speciality->name }}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-ke-beech">
                            <h2 class="seleectt-my-time">Select Your Time Slot</h2>
                            <div class="ull-1">
                                <ul class="ull-1-inner">
                                    @php $workingday = getMyWorkingDay($item->doctor->working_days != '' ? $item->doctor->working_days : '0' ); @endphp
                                    @forelse($workingday as $day)
                                        <input type="radio" name="day" id="" value="{{ $day }}" ><li>{{ $day }}</li>
                                        <!-- <li>Tuesday</li>
                                        <li>Wednesday</li>
                                        <li>Thursday</li>
                                        <li>Friday</li>-->
                                    @empty
                                    <li>Not available</li>
                                    @endforelse                                
                                </ul>
                            </div>
                            <div class="ull-2">
                                <ul class="ull-1-inner">
                                    @if($avail)
                                    @foreach( $working_days as $working_day)
                                        <li>{{ $avail[$working_day]['availability_from'] }} - {{ $avail[$working_day]['availability_to'] }}</li>
                                    @endforeach
                                    @endif
                                    <!-- <li>10:00 am - 2:00 pm</li>
                                    <li>10:00 am - 2:00 pm</li>
                                    <li>10:00 am - 2:00 pm</li>
                                    <li>10:00 am - 2:00 pm</li>
                                    <li class="no-spac">10:00 am - 2:00 pm</li> -->
                                </ul>
                            </div>
                            <!-- <div class="ull-2">
                                <ul class="ull-1-inner less-space">
                                    <li>10:00 am - 2:00 pm</li>
                                    <li>10:00 am - 2:00 pm</li>
                                    <li>10:00 am - 2:00 pm</li>
                                    <li>10:00 am - 2:00 pm</li>
                                    <li class="no-spac">10:00 am - 2:00 pm</li>
                                </ul>
                            </div> -->
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                            <div class="submiiit">
                                <a href="{{ route('book.appointment', $item->id) }}">Book Now</a>                                
                            </div>                           
                        </div>
                    </div>
                </form>
                </div>
            </div>
            <div class="chat_box chat_box_clr">
                <div class="row">
                    <div class="booking_box_2">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="booking_box_img">
                                <img src="{{ asset('core/images/booking_call.png') }}" alt="img" class="img-circle">
                            </div>
                            <div class="booking_chat bottom-gap">
                                <h1>Call Us at</h1>
                                <p>{{ $item->mobile }}</p>
                            </div>
                            <div class="booking_chat second--text bottom-gap">
                                <h1>Address</h1>
                                <p>{{ $item->address }}</p>
                                <p>{{ $item->city }} ({{ $item->user_country->name }})</p>
                                <!-- <p></p> -->
                            </div>
                            <div class="booking_chat second--text bottom-gap">
                                <h1>Email</h1>
                                <p>{{ $item->email }}</p>
                                
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--appoint_section end-->
                
            </div>
        </div>
    </div>

    <!--team wrapper end-->
        <!-- portfolio-section start -->
    <section class="portfolio-area med_toppadder100">
        <div class="container">
                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2">
                    <div class="about_heading_wraper  text-center med_bottompadder50">
                        <h1 class="med_bottompadder20">Gallery</h1>
                        <img src="{{ asset('core/images/dental_line_center.png') }}" alt="line" class="med_bottompadder20">
                    </div>
                </div>


            <div class="row three-column">
                <div id="gridWrapper" class="clearfix">
                    <div class="col-xs-12 col-sm-6 col-md-6 portfolio-wrapper III_column" data-groups='["all", "website", "photoshop"]'>
                        <div class="portfolio-thumb">
                            <div class="port_img_wrapper">
                                <img src="{{ asset('core/images/dc_galry_img_1.jpg') }}" alt="filter_img">
                            </div>
                            <div class="portfolio_icon_wrapper zoom_popup">
                                <a class="img-link" href="{{ asset('core/images/dc_galry_img_1.jpg') }}"> <i class="fa fa-search-plus"></i>
                                </a>
                            </div>

                            <div class="portfolio-content">
                                <div class="portfolio-info">
                                    <h3>Satisfied Patients</h3>
                                    <p class="hidden-xs">Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh elit. Duis sed amet nibh vulputate cursus a sit amet mauris erat.</p>
                                </div>
                                <!-- portfolio-info -->


                            </div>
                            <!-- portfolio-content -->

                        </div>
                        <!-- /.portfolio-thumb -->
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 portfolio-wrapper III_column" data-groups='["all", "business", "website", "photoshop"]'>
                        <div class="portfolio-thumb">
                            <div class="port_img_wrapper">
                                <img src="{{ asset('core/images/dc_galry_img_2.jpg') }}" alt="filter_img">
                            </div>
                            <div class="portfolio_icon_wrapper zoom_popup">
                                <a class="img-link" href="{{ asset('core/images/dc_galry_img_2.jpg') }}"> <i class="fa fa-search-plus"></i>
                                </a>
                            </div>

                            <div class="portfolio-content">
                                <div class="portfolio-info">
                                    <h3>Satisfied Patients</h3>
                                    <p class="hidden-xs">Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh elit. Duis sed amet nibh vulputate cursus a sit amet mauris erat.</p>
                                </div>
                                <!-- portfolio-info -->


                            </div>
                            <!-- portfolio-content -->

                        </div>
                        <!-- /.portfolio-thumb -->
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 portfolio-wrapper III_column" data-groups='["all", "website", "business"]'>
                        <div class="portfolio-thumb">
                            <div class="port_img_wrapper">
                                <img src="{{ asset('core/images/dc_galry_img_3.jpg') }}" alt="filter_img">
                            </div>
                            <div class="portfolio_icon_wrapper zoom_popup">
                                <a class="img-link" href="{{ asset('core/images/dc_galry_img_3.jpg') }}"> <i class="fa fa-search-plus"></i>
                                </a>
                            </div>

                            <div class="portfolio-content">
                                <div class="portfolio-info">
                                    <h3>Satisfied Patients</h3>
                                    <p class="hidden-xs">Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh elit. Duis sed amet nibh vulputate cursus a sit amet mauris erat.</p>
                                </div>
                                <!-- portfolio-info -->


                            </div>
                            <!-- portfolio-content -->

                        </div>
                        <!-- /.portfolio-thumb -->
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 portfolio-wrapper III_column" data-groups='["all", "business", "website", "photoshop"]'>
                        <div class="portfolio-thumb">
                            <div class="port_img_wrapper">
                                <img src="{{ asset('core/images/dc_galry_img_4.jpg') }}" alt="filter_img">
                            </div>
                            <div class="portfolio_icon_wrapper zoom_popup">
                                <a class="img-link" href="{{ asset('core/images/dc_galry_img_4.jpg') }}"> <i class="fa fa-search-plus"></i>
                                </a>
                            </div>

                            <div class="portfolio-content">
                                <div class="portfolio-info">
                                    <h3>Satisfied Patients</h3>
                                    <p class="hidden-xs">Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh elit. Duis sed amet nibh vulputate cursus a sit amet mauris erat.</p>
                                </div>
                                <!-- portfolio-info -->


                            </div>
                            <!-- portfolio-content -->

                        </div>
                        <!-- /.portfolio-thumb -->
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 portfolio-wrapper III_column" data-groups='["all", "photoshop", "website", "business"]'>
                        <div class="portfolio-thumb">
                            <div class="port_img_wrapper">
                                <img src="{{ asset('core/images/dc_galry_img_3.jpg') }}" alt="filter_img">
                            </div>
                            <div class="portfolio_icon_wrapper zoom_popup">
                                <a class="img-link" href="{{ asset('core/images/dc_galry_img_3.jpg') }}"> <i class="fa fa-search-plus"></i>
                                </a>
                            </div>

                            <div class="portfolio-content">
                                <div class="portfolio-info">
                                    <h3>Satisfied Patients</h3>
                                    <p class="hidden-xs">Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh elit. Duis sed amet nibh vulputate cursus a sit amet mauris erat.</p>
                                </div>
                                <!-- portfolio-info -->


                            </div>
                            <!-- portfolio-content -->

                        </div>
                        <!-- /.portfolio-thumb -->
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 portfolio-wrapper III_column" data-groups='["all", "business", "website", "all"]'>
                        <div class="portfolio-thumb">
                            <div class="port_img_wrapper">
                                <img src="{{ asset('core/images/dc_galry_img_6.jpg') }}" alt="filter_img">
                            </div>
                            <div class="portfolio_icon_wrapper zoom_popup">
                                <a class="img-link" href="{{ asset('core/images/dc_galry_img_6.jpg') }}"> <i class="fa fa-search-plus"></i>
                                </a>
                            </div>

                            <div class="portfolio-content">
                                <div class="portfolio-info">
                                    <h3>Satisfied Patients</h3>
                                    <p class="hidden-xs">Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh elit. Duis sed amet nibh vulputate cursus a sit amet mauris erat.</p>
                                </div>
                                <!-- portfolio-info -->


                            </div>
                            <!-- portfolio-content -->

                        </div>
                        <!-- /.portfolio-thumb -->
                    </div>
                </div>
                <!--/#gridWrapper-->
            </div>
            <!-- /.row -->

        </div>
        <!-- /.container -->
    </section>
    <!--/.portfolio-area-->
@endsection