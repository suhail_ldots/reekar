@extends('site_view.layouts.main')
@section('head')
<link rel="stylesheet" type="text/css" href="{{ asset('core/css/listing-page.css') }}" />
@endsection
@section('content')
    <!--TOp search start-->
    <div class="search-for-listing-page">
        <div class="container new-full-cont">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding padding-for-mobile">
                <!-- <ul class="search-for-listing--">
                    <li class="sellecctt">
                        <select id="category">
                            <option value="volvo">Choose your search category </option>
                            <option value="saab">1</option>
                            <option value="opel">2</option>
                            <option value="audi">3</option>
                        </select>
                    </li>
                    <li class="location2">
                        <input type="search" id="mySearch" placeholder="location">
                    </li>
                    <li class="namme-search2">
                        <input type="search2" id="mySearch" placeholder="search by name">
                    </li>
                    <li class="search----">
                        <a href="#"> Search</a>
                    </li>

                </ul> -->
                <form action="{{ route('search.nursing') }}" method="get">
                <ul class="search-for-listing--">
                    <li class="location form-group">
                        <input type="text" name="search_location" value="{{ isset($location) ? $location : old('search_location') }}" class="selelctt mySearch" id="addr_input_location" placeholder="Your location">	
                        <!-- <div class="help-block">sdfsfsa</div> -->                    
                    </li>
                    <li class="selelctt form-group">
                        <select id="cars" name="search_speciality" class="classic">
                            <option value="">Choose doctor speciality </option>
                            <?php $specialities = speciality(); if($specialities){ foreach($specialities as $speciality){?> 
                            
                            <option value="{{ $speciality->id }}" {{ isset($search_speciality) && $search_speciality == $speciality->id ? 'selected' :'' }}>{{ $speciality->name }}</option>

                            <?php } } ?>
                        </select>
                    </li>
                    
                    <li class="namme-search form-group">
                        <input type="text" name="search_text" value="{{ isset($search_text) ? $search_text : old('search_text') }}" id="mySearch" placeholder="Search Doctor, Clinic, Hospital etc.">
                    </li>
                     <!-- <a href="#" class="for-thhhe-search"> Search</a> -->
                    <input type="hidden" name="user_lat" value="{{ isset($user_lat) ? $user_lat : old('user_lat')}}" id="user_lat" >
                    <input type="hidden" name="user_long" value="{{ isset($user_long) ? $user_long : old('user_long')}}" id="user_long" >
                   <li class="for-thhhe-search" ><input type="submit" class="saveBtn btn btn-primary" value="Search"></li> 
                    

                </ul>
               </form>
            </div>
        </div>

    </div>
    <!--Search filter start-->
    <div class="filter-for-listing">
        <div class="container new-full-cont">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding padding-for-mobile">
                <div class="row clearfix">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 push-towards-left ipad-width1">
                        <div class="sort--"> 
                            <h4>Sort by</h4>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 push-towards-left ipad-width">
                        <div class="optionn-for-sort">
                            <ul>
                                <li>Availability</li>
                                <li>Sort by Distance </li>
                                <li>Visitor Experience</li>
                                <li>suggestion</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mobillee-displayy">
                        <div class="just-for-mob">
                            <ul>
                                <li class="filter----for-mob">
                                    <select id="category">
                                        <option value="volvo">Choose your filter</option>
                                        <option value="saab">Availability</option>
                                        <option value="opel">Sort by Distance</option>
                                        <option value="audi">Visitor Experience</option>
                                        <option value="audi">suggestion</option>
                                    </select>
                                </li>
                            </ul>
                            
                        </div>

                    </div>

                </div>

            </div>
        </div>

    </div>

    <!--map and listing start-->
    <div class="map-and">
        <div class="container new-full-cont">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding padding-for-mobile">
                <div class="row clearfix" id="tbody">
                    @include('site_view.modules.searched_doctor_listing_body')

                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="mapp-ssection">
                            <div class="mapouter">
                                <div class="gmap_canvas">
                                    <!-- <iframe width="100%" height="100%" id="gmap_canvas" src="https://maps.google.com/maps?q=university%20of%20san%20francisco&t=&z=9&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.embedgooglemap.org">embedgooglemap.org</a></div> -->
                                    <div id="map" style="position: absolute; overflow: hidden;  height:100%;"></div>
                                <style>
                                    .mapouter {
                                        position: relative;
                                        text-align: right;
                                        height: 850px;
                                        width: 100%;
                                    }
                                    
                                    .gmap_canvas {
                                        overflow: hidden;
                                        background: none!important;
                                        height: 850px;
                                        width: 100%;
                                    }
                                </style>
                            </div>

                        </div>
                        <div class="add-number-1">
                            <img src="{{ asset('core/images/addss-number-1.jpg') }}" alt="">

                        </div>
                        <div class="add-number-1">
                            <img src="{{ asset('core/images/addss-number-2.jpg') }}" alt="">

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- footer wrapper start-->
@endsection
@section('js')

        <!--js code-->
        <!-- <script>
            mapboxgl.accessToken = 'pk.eyJ1IjoiYWtzaGF5aGFuZGdlIiwiYSI6InI2bzhEcUUifQ.8r-lNnjAuIZUk8pjhtxlFw';
            var map = new mapboxgl.Map({
                container: 'map', // container id
                style: 'mapbox://styles/mapbox/streets-v9', // stylesheet location
                center: [-74.50, 40], // starting position [lng, lat]
                zoom: 10 // starting zoom
            });
            map.scrollZoom.disable();
            map.dragPan.disable();
        </script> -->
        <script>
            //auto complete car location
            var searchInput = 'addr_input_location';  
                $(document).ready(function () {  
                    var autocomplete;
                    autocomplete = new google.maps.places.Autocomplete((document.getElementById(searchInput)), {
                        types: ["geocode"],
                    });
                    google.maps.event.addListener(autocomplete, 'place_changed', function () {
                        var near_place = autocomplete.getPlace();
                        //assign lat long only for user location value to 
                        document.getElementById('user_lat').value = near_place.geometry.location.lat();
                        document.getElementById('user_long').value = near_place.geometry.location.lng();
                                
                    });   
                });
        </script>
        <script>
            //dRAW Map 
                @if(!empty($nursing_home))
                @foreach($nursing_home as $mapnursing_home)
                var lat = {{ $mapnursing_home->lat }};
                var long = {{ $mapnursing_home->long }};
                @break
                @endforeach
                @endif

                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 7,
                    center: new google.maps.LatLng(lat, long),
                    // url: "{{ URL::to('/car-details')}}"+'?car_id='+locations[i]['id']+window.mycurrency_code,
                    // icon: "{{ URL::asset('public/admin/images/reekar-flag-50x50map.png') }}"
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                    });

                    //Apppend marker and events
                    var marker, i;
                    var locations = [];
                    var i = 0;
                    @foreach($nursing_home as $nursing)
                    
                    
                    // for (i = 0; i < locations.length; i++) {  
                    // console.log(locations[i]['lat']);
                    // alert(locations[i]['lat']);
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng({{ $nursing->lat }}, {{ $nursing->long }}),
                        map: map,
                        url: "{{ route('nursing.list.details', $nursing->id) }}",
                        icon: "{{ URL::asset('public/favicon.png') }}"
                    });

                    google.maps.event.addListener(marker, 'click', (function(marker, i) {
                        return function() {
                        //infowindow.setContent(locations[i][0]);
                        //infowindow.open(map, marker);
                        window.location.href = this.url;

                        }
                    })(marker, i));
                    // }
                        i++;
                    @endforeach
        
        </script>
@if(count($nursing_home) > 0) 
<?php $items = $nursing_home; ?>  
    @include('lazyloading.loading')    
@endif
@endsection