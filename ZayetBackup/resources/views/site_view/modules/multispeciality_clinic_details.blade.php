@extends('site_view.layouts.main')
@section('content')
<body>
    
    <!--bio section start-->
    <div class="med_doctor_info">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                </div>
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 no-padding">
                        <div class="doctor_info">
                            <h1>{{ $item->first_name.' '.$item->last_name }}</h1>
                            <p>Licence no : </p>
                            <!-- <p>( Cancer Specialist )</p> -->
                        </div>
                        <div class="doctor_info">
                            <p class="lic-no">Phone No. : {{ $item->mobile }} </p>
                        </div>
                        <div class="doctor_info">
                            <p class="lic-no">Email : {{ $item->email }} </p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 no-padding">
                        <!-- <div class="doctor_info2">
                            <ul class="inner-for-ratings">
                                <li class="ratingg-text">Rating</li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                            </ul>
                        </div> -->
                        <div class="doctor_info3">
                            <ul class="inner-for-ratings22">
                                <li class="ratingg-text"><a href="#">Book Now</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!--bio section end-->
    <!--single doc info start-->
    <div class="bio_section_doc">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="team_about doc_img_top fix-my-height">
                        <div class="team_img bio_team_img">
                            <img src="{{ @fopen( \Url('storage/app/images/doctor/profile/').'/'.$item->profile_pic, r) ? \Url('storage/app/images/doctor/profile/').'/'.$item->profile_pic : asset('core/images/single_doc.jpg') }}" alt="img" class="img-responsive">
                        </div>

                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <div class="choose_heading_wrapper about_heading_wraper fonnt-color">
                        <h1 class="med_bottompadder20 med_toppadder50">About Hospital</h1>
                        <img src="{{ asset('core/images/dental_line_blue-single.png') }}" alt="line" class="med_bottompadder20">
                        <p>Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibht henhn id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat thr auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis.</p>
                        <p class="hidden-sm">Aenean sollicitudin, lorem quis bibendum auc tor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit.</p>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ant_txt">
                    <p>lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet yunibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed nonmauris vitae erat consequaiut auctor eu in elit. Class aptent taciti so- ciosqu ad litora torquent per conubia nostra, per inceptos himenaeos. </p>
                </div>
            </div>
        </div>
    </div>
    <!--single doc info end-->
    <!--our team wrapper start-->
    <div class="team_wrapper our-doctor-pagee">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2">
                    <div class="about_heading_wraper  text-center med_bottompadder50">
                        <h1 class="med_bottompadder20">Lorem ipsum dolor sit amet</h1>
                        <img src="{{ asset('core/images/dental_line_center.png') }}" alt="line" class="med_bottompadder20">
                        <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate.</p>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 team_about tabbingg">
                    <p class="for-thefull--">
                        <a class="btn btn-primary doocctoor" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="true" aria-controls="collapseExample">
    Our Doctors
  </a>
                        <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample2" role="button" aria-expanded="false" aria-controls="collapseExample">
    Our Specialties
  </a>
                    </p>
                    <div class="collapse" id="collapseExample">
                        <div class="card card-body">
                            <div class="col-lg-12 no-padding">
<div class="select_div">
<div class="col-lg-7 col-sm-6 col-md-7">  <h2>Search Doctor</h2></div>
<div class="col-lg-5 col-sm-6 col-md-5">
                        <div class="selectbox">
                            <h3 style="color: #000;  padding: 7px 20px 0 0px;">Filter</h3>
                        <select>
                        <option>Deparments</option>
                        </select>
                        <input type="button" value="Apply" class="btn btn-primary">
                        </div>
                        </div>     </div>
                        </div>   
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 portfolio-wrapper III_column med_bottompadder30 resp_div_img">
                            <div class="team_about our_doc_main dc_porftfolio_img">
                                <div class="team_icon_wrapper">
                                  <?xml version="1.0" encoding="iso-8859-1"?>
                                    <!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 300.346 300.346" style="enable-background:new 0 0 300.346 300.346;" xml:space="preserve" width="40px" height="40px">
                                    <g>
                                        <g>
                                            <g>
                                                <path d="M296.724,157.793c-3.611-5.821-9.552-9.841-16.299-11.03c-6.746-1.188-13.703,0.559-19.139,4.836l-21.379,17.126     c-3.533-3.749-8.209-6.31-13.359-7.218c-6.749-1.189-13.704,0.559-19.1,4.805l-12.552,9.921H162.66     c-5.152,0-10.301-1.238-14.891-3.579l-11.486-5.861c-9.678-4.938-20.532-7.328-31.385-6.908     c-15.046,0.579-29.448,6.497-40.554,16.666l-61.89,56.667c-2.901,2.656-3.28,7.093-0.873,10.203l32.406,41.867     c1.481,1.913,3.714,2.933,5.983,2.933c1.374,0,2.762-0.374,4.003-1.151L82.944,262.7c2.777-1.736,5.975-2.654,9.25-2.654h90.428     c12.842,0,25.446-4.407,35.489-12.409l73.145-58.281C300.815,181.745,303.166,168.176,296.724,157.793z M216.81,178.183     c2.037-1.601,4.564-2.236,7.114-1.787c1.536,0.271,2.924,0.913,4.087,1.856l-12.645,10.129c-1.126-2.111-2.581-4.019-4.283-5.672     L216.81,178.183z M281.837,177.528L208.69,235.81c-7.377,5.878-16.635,9.116-26.068,9.116H92.194     c-6.113,0-12.083,1.714-17.267,4.954l-33.169,20.743l-23.959-30.954L74.554,187.7c8.469-7.753,19.451-12.267,30.924-12.708     c8.279-0.323,16.554,1.504,23.933,5.268l11.486,5.861c6.707,3.422,14.233,5.231,21.763,5.231h32.504     c4.278,0,7.758,3.48,7.758,7.758c0,4.105-3.211,7.507-7.309,7.745l-90.45,5.252c-4.168,0.242-7.351,3.817-7.109,7.985     s3.822,7.346,7.985,7.109l90.45-5.252c9.461-0.549,17.317-6.817,20.282-15.32l53.916-43.189c2.036-1.602,4.561-2.237,7.114-1.787     c2.552,0.449,4.709,1.909,6.075,4.111C286.277,169.634,285.401,174.691,281.837,177.528z" fill="#FFFFFF"/>
                                                <path d="M161.7,132.383c13.183,0,25.302-6.625,32.418-17.722c7.117-11.097,8.082-24.875,2.581-36.855L168.57,16.531     c-1.232-2.685-3.916-4.406-6.87-4.406s-5.638,1.721-6.87,4.406l-28.129,61.274c-5.5,11.981-4.535,25.759,2.581,36.855     C136.398,125.757,148.517,132.383,161.7,132.383z M140.441,84.114L161.7,37.807l21.258,46.307     c3.341,7.277,2.754,15.645-1.568,22.385c-4.323,6.74-11.683,10.764-19.69,10.764c-8.007,0-15.368-4.024-19.69-10.765     C137.687,99.759,137.101,91.391,140.441,84.114z" fill="#FFFFFF"/>
                                            </g>
                                        </g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    </svg>

                                </div>
                                <div class="team_img special_team_img_mn">
                               <img src="{{ asset('core/images/team_1.jpg') }}" alt="img" class="img-responsive">
                                </div>
                                <div class="team_txt">
                                    <h1><a href="#">Dr. Johan Doe</a></h1>
                                    <p>(Hepatologist)</p>
                                </div>

                            </div>

                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3  portfolio-wrapper III_column med_bottompadder30 resp_div_img">
                            <div class="team_about our_doc_main dc_porftfolio_img">
                                <div class="team_icon_wrapper">
                                    <?xml version="1.0" encoding="iso-8859-1"?>
                                <!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 300.346 300.346" style="enable-background:new 0 0 300.346 300.346;" xml:space="preserve" width="40px" height="40px">
                                <g>
                                    <g>
                                        <g>
                                            <path d="M296.724,157.793c-3.611-5.821-9.552-9.841-16.299-11.03c-6.746-1.188-13.703,0.559-19.139,4.836l-21.379,17.126     c-3.533-3.749-8.209-6.31-13.359-7.218c-6.749-1.189-13.704,0.559-19.1,4.805l-12.552,9.921H162.66     c-5.152,0-10.301-1.238-14.891-3.579l-11.486-5.861c-9.678-4.938-20.532-7.328-31.385-6.908     c-15.046,0.579-29.448,6.497-40.554,16.666l-61.89,56.667c-2.901,2.656-3.28,7.093-0.873,10.203l32.406,41.867     c1.481,1.913,3.714,2.933,5.983,2.933c1.374,0,2.762-0.374,4.003-1.151L82.944,262.7c2.777-1.736,5.975-2.654,9.25-2.654h90.428     c12.842,0,25.446-4.407,35.489-12.409l73.145-58.281C300.815,181.745,303.166,168.176,296.724,157.793z M216.81,178.183     c2.037-1.601,4.564-2.236,7.114-1.787c1.536,0.271,2.924,0.913,4.087,1.856l-12.645,10.129c-1.126-2.111-2.581-4.019-4.283-5.672     L216.81,178.183z M281.837,177.528L208.69,235.81c-7.377,5.878-16.635,9.116-26.068,9.116H92.194     c-6.113,0-12.083,1.714-17.267,4.954l-33.169,20.743l-23.959-30.954L74.554,187.7c8.469-7.753,19.451-12.267,30.924-12.708     c8.279-0.323,16.554,1.504,23.933,5.268l11.486,5.861c6.707,3.422,14.233,5.231,21.763,5.231h32.504     c4.278,0,7.758,3.48,7.758,7.758c0,4.105-3.211,7.507-7.309,7.745l-90.45,5.252c-4.168,0.242-7.351,3.817-7.109,7.985     s3.822,7.346,7.985,7.109l90.45-5.252c9.461-0.549,17.317-6.817,20.282-15.32l53.916-43.189c2.036-1.602,4.561-2.237,7.114-1.787     c2.552,0.449,4.709,1.909,6.075,4.111C286.277,169.634,285.401,174.691,281.837,177.528z" fill="#FFFFFF"/>
                                            <path d="M161.7,132.383c13.183,0,25.302-6.625,32.418-17.722c7.117-11.097,8.082-24.875,2.581-36.855L168.57,16.531     c-1.232-2.685-3.916-4.406-6.87-4.406s-5.638,1.721-6.87,4.406l-28.129,61.274c-5.5,11.981-4.535,25.759,2.581,36.855     C136.398,125.757,148.517,132.383,161.7,132.383z M140.441,84.114L161.7,37.807l21.258,46.307     c3.341,7.277,2.754,15.645-1.568,22.385c-4.323,6.74-11.683,10.764-19.69,10.764c-8.007,0-15.368-4.024-19.69-10.765     C137.687,99.759,137.101,91.391,140.441,84.114z" fill="#FFFFFF"/>
                                        </g>
                                    </g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                </svg>
                                </div>
                                <div class="team_img special_team_img_mn">
                                    <img src="{{ asset('core/images/team_2.jpg') }}" alt="img" class="img-responsive">
                                </div>
                                <div class="team_txt">
                                    <h1><a href="#">Dr. Johan Doe</a></h1>
                                    <p>(Hepatologist)</p>
                                </div>
                            </div>
                        </div>
                         <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 portfolio-wrapper III_column med_bottompadder30 resp_div_img" data-groups='["all", "business", "logo", "surgery"]'>
                            <div class="team_about our_doc_main dc_porftfolio_img">
                                <div class="team_icon_wrapper">
                                     <?xml version="1.0" encoding="iso-8859-1"?>
                                <!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 300.346 300.346" style="enable-background:new 0 0 300.346 300.346;" xml:space="preserve" width="40px" height="40px">
                                <g>
                                    <g>
                                        <g>
                                            <path d="M296.724,157.793c-3.611-5.821-9.552-9.841-16.299-11.03c-6.746-1.188-13.703,0.559-19.139,4.836l-21.379,17.126     c-3.533-3.749-8.209-6.31-13.359-7.218c-6.749-1.189-13.704,0.559-19.1,4.805l-12.552,9.921H162.66     c-5.152,0-10.301-1.238-14.891-3.579l-11.486-5.861c-9.678-4.938-20.532-7.328-31.385-6.908     c-15.046,0.579-29.448,6.497-40.554,16.666l-61.89,56.667c-2.901,2.656-3.28,7.093-0.873,10.203l32.406,41.867     c1.481,1.913,3.714,2.933,5.983,2.933c1.374,0,2.762-0.374,4.003-1.151L82.944,262.7c2.777-1.736,5.975-2.654,9.25-2.654h90.428     c12.842,0,25.446-4.407,35.489-12.409l73.145-58.281C300.815,181.745,303.166,168.176,296.724,157.793z M216.81,178.183     c2.037-1.601,4.564-2.236,7.114-1.787c1.536,0.271,2.924,0.913,4.087,1.856l-12.645,10.129c-1.126-2.111-2.581-4.019-4.283-5.672     L216.81,178.183z M281.837,177.528L208.69,235.81c-7.377,5.878-16.635,9.116-26.068,9.116H92.194     c-6.113,0-12.083,1.714-17.267,4.954l-33.169,20.743l-23.959-30.954L74.554,187.7c8.469-7.753,19.451-12.267,30.924-12.708     c8.279-0.323,16.554,1.504,23.933,5.268l11.486,5.861c6.707,3.422,14.233,5.231,21.763,5.231h32.504     c4.278,0,7.758,3.48,7.758,7.758c0,4.105-3.211,7.507-7.309,7.745l-90.45,5.252c-4.168,0.242-7.351,3.817-7.109,7.985     s3.822,7.346,7.985,7.109l90.45-5.252c9.461-0.549,17.317-6.817,20.282-15.32l53.916-43.189c2.036-1.602,4.561-2.237,7.114-1.787     c2.552,0.449,4.709,1.909,6.075,4.111C286.277,169.634,285.401,174.691,281.837,177.528z" fill="#FFFFFF"/>
                                            <path d="M161.7,132.383c13.183,0,25.302-6.625,32.418-17.722c7.117-11.097,8.082-24.875,2.581-36.855L168.57,16.531     c-1.232-2.685-3.916-4.406-6.87-4.406s-5.638,1.721-6.87,4.406l-28.129,61.274c-5.5,11.981-4.535,25.759,2.581,36.855     C136.398,125.757,148.517,132.383,161.7,132.383z M140.441,84.114L161.7,37.807l21.258,46.307     c3.341,7.277,2.754,15.645-1.568,22.385c-4.323,6.74-11.683,10.764-19.69,10.764c-8.007,0-15.368-4.024-19.69-10.765     C137.687,99.759,137.101,91.391,140.441,84.114z" fill="#FFFFFF"/>
                                        </g>
                                    </g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                </svg>
                                </div>
                                <div class="team_img special_team_img_mn">
                                    <img src="{{ asset('core/images/team_3.jpg') }}" alt="img" class="img-responsive">
                                </div>
                                <div class="team_txt">
                                    <h1><a href="#">Dr. Johan Doe</a></h1>
                                    <p>(Hepatologist)</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 portfolio-wrapper III_column med_bottompadder30 resp_div_img" data-groups='["all", "business", "surgery", "photoshop"]'>
                            <div class="team_about our_doc_main dc_porftfolio_img">
                                <div class="team_icon_wrapper">
                                     <?xml version="1.0" encoding="iso-8859-1"?>
                                <!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 300.346 300.346" style="enable-background:new 0 0 300.346 300.346;" xml:space="preserve" width="40px" height="40px">
                                <g>
                                    <g>
                                        <g>
                                            <path d="M296.724,157.793c-3.611-5.821-9.552-9.841-16.299-11.03c-6.746-1.188-13.703,0.559-19.139,4.836l-21.379,17.126     c-3.533-3.749-8.209-6.31-13.359-7.218c-6.749-1.189-13.704,0.559-19.1,4.805l-12.552,9.921H162.66     c-5.152,0-10.301-1.238-14.891-3.579l-11.486-5.861c-9.678-4.938-20.532-7.328-31.385-6.908     c-15.046,0.579-29.448,6.497-40.554,16.666l-61.89,56.667c-2.901,2.656-3.28,7.093-0.873,10.203l32.406,41.867     c1.481,1.913,3.714,2.933,5.983,2.933c1.374,0,2.762-0.374,4.003-1.151L82.944,262.7c2.777-1.736,5.975-2.654,9.25-2.654h90.428     c12.842,0,25.446-4.407,35.489-12.409l73.145-58.281C300.815,181.745,303.166,168.176,296.724,157.793z M216.81,178.183     c2.037-1.601,4.564-2.236,7.114-1.787c1.536,0.271,2.924,0.913,4.087,1.856l-12.645,10.129c-1.126-2.111-2.581-4.019-4.283-5.672     L216.81,178.183z M281.837,177.528L208.69,235.81c-7.377,5.878-16.635,9.116-26.068,9.116H92.194     c-6.113,0-12.083,1.714-17.267,4.954l-33.169,20.743l-23.959-30.954L74.554,187.7c8.469-7.753,19.451-12.267,30.924-12.708     c8.279-0.323,16.554,1.504,23.933,5.268l11.486,5.861c6.707,3.422,14.233,5.231,21.763,5.231h32.504     c4.278,0,7.758,3.48,7.758,7.758c0,4.105-3.211,7.507-7.309,7.745l-90.45,5.252c-4.168,0.242-7.351,3.817-7.109,7.985     s3.822,7.346,7.985,7.109l90.45-5.252c9.461-0.549,17.317-6.817,20.282-15.32l53.916-43.189c2.036-1.602,4.561-2.237,7.114-1.787     c2.552,0.449,4.709,1.909,6.075,4.111C286.277,169.634,285.401,174.691,281.837,177.528z" fill="#FFFFFF"/>
                                            <path d="M161.7,132.383c13.183,0,25.302-6.625,32.418-17.722c7.117-11.097,8.082-24.875,2.581-36.855L168.57,16.531     c-1.232-2.685-3.916-4.406-6.87-4.406s-5.638,1.721-6.87,4.406l-28.129,61.274c-5.5,11.981-4.535,25.759,2.581,36.855     C136.398,125.757,148.517,132.383,161.7,132.383z M140.441,84.114L161.7,37.807l21.258,46.307     c3.341,7.277,2.754,15.645-1.568,22.385c-4.323,6.74-11.683,10.764-19.69,10.764c-8.007,0-15.368-4.024-19.69-10.765     C137.687,99.759,137.101,91.391,140.441,84.114z" fill="#FFFFFF"/>
                                        </g>
                                    </g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                </svg>
                                </div>
                                <div class="team_img special_team_img_mn">
                                    <img src="{{ asset('core/images/team_4.jpg') }}" alt="img" class="img-responsive">
                                </div>
                                <div class="team_txt">
                                    <h1><a href="#">Dr. Johan Doe</a></h1>
                                    <p>(Hepatologist)</p>
                                </div>  
                            </div>
                        </div>
                                
                            </div>

                        </div>
                    </div>
                    <div class="collapse" id="collapseExample2">
                        <div class="card card-body">
<div class="col-lg-12 no-padding">
<div class="select_div">
<div class="col-lg-7 col-sm-6 col-md-7">  <h2>Search Specialties</h2></div>
<div class="col-lg-5 col-sm-6 col-md-5">
                        <div class="selectbox">
                        <select>
                        <option>Interventional Cardiology</option>
                        </select>
                        <input type="button" value="Apply" class="btn btn-primary">
                        </div>
                        </div>     </div>
                        </div>                  
                        
                        <div class="col-lg-12">
                        <div class="informationdiv">
                        <h2>Interventional Cardiology</h2>
                        <ul>
                        <li><b>Contact Information</b></li>
                        <li><i class="fa fa-phone"></i>+91 8842365890</li>
                        <li><i class="fa fa-envelope"></i>Examphospital@gmail.com</li>
                        </ul>
                        </div>
                        <div class="informationdiv">
                        <h2>Interventional Cardiology</h2>
                        <ul>
                        <li><b>Contact Information</b></li>
                        <li><i class="fa fa-phone"></i>+91 8842365890</li>
                        <li><i class="fa fa-envelope"></i>Examphospital@gmail.com</li>
                        </ul>
                        </div>
                        <div class="informationdiv">
                        <h2>Interventional Cardiology</h2>
                        <ul>
                        <li><b>Contact Information</b></li>
                        <li><i class="fa fa-phone"></i>+91 8842365890</li>
                        <li><i class="fa fa-envelope"></i>Examphospital@gmail.com</li>
                        </ul>
                        </div>
                        <div class="informationdiv">
                        <h2>Interventional Cardiology</h2>
                        <ul>
                        <li><b>Contact Information</b></li>
                        <li><i class="fa fa-phone"></i>+91 8842365890</li>
                        <li><i class="fa fa-envelope"></i>Examphospital@gmail.com</li>
                        </ul>
                        </div>
                        <div class="informationdiv">
                        <h2>Interventional Cardiology</h2>
                        <ul>
                        <li><b>Contact Information</b></li>
                        <li><i class="fa fa-phone"></i>+91 8842365890</li>
                        <li><i class="fa fa-envelope"></i>Examphospital@gmail.com</li>
                        </ul>
                        </div>
                        </div>
                        
                        
                         </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

    <!--team wrapper end-->
    
    
    
    <div class="Equipments-specialisation med_toppadder100">
    <div class="container">
    <div class="row">
    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2">
                    <div class="about_heading_wraper  text-center med_bottompadder50">
                        <h1 class="med_bottompadder20">Book Appointment</h1>
                        <img src="{{ asset('core/images/dental_line_center.png') }}" alt="line" class="med_bottompadder20">
                        <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate.</p>
                    </div>
                </div>
    
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="col-lg-6 col-sm-6">
    <div class="magneticdiv">
    <img src="{{ asset('core/images/equpment.jpg') }}">
    <h3><a href="#">Magnetic Resonance Imaging </a></h3>
    <h4>(MRI MACHINE)</h4>
    </div>
    </div>
    <div class="col-lg-6 col-sm-6">
    <div class="magneticdiv">
    <img src="{{ asset('core/images/equpment.jpg') }}">
    <h3><a href="#">ventilator Support Machine</a></h3>
    <h4>(VSR MACHINE)</h4>
    </div>
    </div>
    </div>   
             
    </div>
    
    </div>
    </div>
    
    <div class="team_wrapper bg-white">
    <div class="container">
    <div class="row">
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2">
                    <div class="about_heading_wraper  text-center med_bottompadder50">
                        <h1 class="med_bottompadder20">Available Timings</h1>
                        <img src="{{ asset('core/images/dental_line_center.png') }}" alt="line" class="med_bottompadder20">
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="timiee">
                        <div class="ull-1">
                            <ul class="ull-1-inner">
                                <li>Monday</li>
                                <li>Tuesday</li>
                                <li>Wednesday</li>
                                <li>Thursday</li>
                                <li>Friday</li>
                            </ul>

                        </div>
                        <div class="ull-2">
                            <ul class="ull-1-inner">
                                <li>10:00 am - 2:00 pm</li>
                                <li>10:00 am - 2:00 pm</li>
                                <li>10:00 am - 2:00 pm</li>
                                <li>10:00 am - 2:00 pm</li>
                                <li>10:00 am - 2:00 pm</li>
                            </ul>
                        </div>

                        <div class="ull-2">
                            <ul class="ull-1-inner">
                                <li>10:00 am - 2:00 pm</li>
                                <li>10:00 am - 2:00 pm</li>
                                <li>10:00 am - 2:00 pm</li>
                                <li>10:00 am - 2:00 pm</li>
                                <li>10:00 am - 2:00 pm</li>
                            </ul>

                        </div>
                    </div>

                </div>

            </div>
    </div>
    </div>
    
    
    
    
    <!--our team wrapper start-->
    <div class="team_wrapper  particular_time"> 
        <div class="container">
        
            <div class="row">
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2">
                    <div class="about_heading_wraper  text-center med_bottompadder50">
                        <h1 class="med_bottompadder20">Book Appointment</h1>
                        <img src="{{ asset('core/images/dental_line_center.png') }}" alt="line" class="med_bottompadder20">
                    </div>
                </div>
                <!-- appoint_section start -->
                <div class="booking_wrapper book_section med_toppadder100" style="background:none">
                    <div class="container clearfix">
                        <div class="booking_box bg-white">
                            <div class="row">
                                <form>

                                    <div class="box_side_icon clearfix">
                                        <!--   <div class="left--img">
                            <img src="{{ asset('core/images/Icon_bk.png') }}" alt="img">
                        </div> -->

                                        <div class="about_heading_wraper  text-center med_bottompadder50">
                                            <h1 class="med_bottompadder20">Fill Information</h1>
                                            <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum.</p>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <div class="contect_form1">
                                                <label for="cars">Treatment</label>

                                                <select id="cars" class="classic">
                                                    <option>Choose your treatment</option>
                                                    <option>Treatment category 1</option>
                                                    <option>Treatment category 2</option>
                                                    <option>Treatment category 3</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <div class="contect_form1">
                                                <label for="cars">Appointment Day</label>
                                                <select id="cars" class="classic">
                                                    <option>Choose your appointment day</option>
                                                    <option>Saab (Swedish Aeroplane AB)</option>
                                                    <option>Mercedes (Mercedes-Benz)</option>
                                                    <option>Audi (Auto Union Deutschland Ingolstadt)</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <div class="contect_form1">
                                                <label for="cars">Department</label>

                                                <select id="cars" class="classic">
                                                    <option>Choose your Department</option>
                                                    <option>Treatment category 1</option>
                                                    <option>Treatment category 2</option>
                                                    <option>Treatment category 3</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <div class="contect_form1">
                                                <label for="cars">Doctor</label>
                                                <select id="cars" class="classic">
                                                    <option>Choose your Doctor</option>
                                                    <option>Saab (Swedish Aeroplane AB)</option>
                                                    <option>Mercedes (Mercedes-Benz)</option>
                                                    <option>Audi (Auto Union Deutschland Ingolstadt)</option>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-ke-beech">
                                            <h2 class="seleectt-my-time">Select Your Time Slot</h2>
                                            <div class="ull-2">
                                                <ul class="ull-1-inner">
                                                    <li>10:00 am - 2:00 pm</li>
                                                    <li>10:00 am - 2:00 pm</li>
                                                    <li>10:00 am - 2:00 pm</li>
                                                    <li>10:00 am - 2:00 pm</li>
                                                    <li class="no-spac">10:00 am - 2:00 pm</li>
                                                </ul>
                                            </div>
                                            <div class="ull-2">
                                                <ul class="ull-1-inner less-space">
                                                    <li>10:00 am - 2:00 pm</li>
                                                    <li>10:00 am - 2:00 pm</li>
                                                    <li>10:00 am - 2:00 pm</li>
                                                    <li>10:00 am - 2:00 pm</li>
                                                    <li class="no-spac">10:00 am - 2:00 pm</li>
                                                </ul>
                                            </div>

                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                            <div class="submiiit">
                                                <a href="#">Book Now</a>

                                            </div>

                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="chat_box chat_box_clr">
                            <div class="row">
                                <div class="booking_box_2">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="booking_box_img">
                                            <img src="{{ asset('core/images/booking_call.png') }}" alt="img" class="img-circle">
                                        </div>
                                        <div class="booking_chat bottom-gap">
                                            <h1>Call Us at</h1>
                                            <p>1800-149-123</p>
                                        </div>
                                        <div class="booking_chat second--text bottom-gap">
                                            <h1>Address</h1>
                                            <p>Cecilia Chapman</p>
                                            <p>711-2880 Nulla St.</p>
                                            <p>Mankato Mississippi 96522</p>
                                        </div>
                                        <div class="booking_chat second--text bottom-gap">
                                            <h1>Email</h1>
                                            <p>Examplemail@gamil.com</p>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--appoint_section end-->

            </div>
        </div>
    </div>
    

    <!--team wrapper end-->
    <!-- portfolio-section start -->
    <section class="portfolio-area med_toppadder100 bg-white">
        <div class="container">
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2">
                <div class="about_heading_wraper  text-center med_bottompadder50">
                    <h1 class="med_bottompadder20">Gallery</h1>
                    <img src="{{ asset('core/images/dental_line_center.png') }}" alt="line" class="med_bottompadder20">
                </div>
            </div>

           <div class="row three-column">
                <div id="gridWrapper" class="clearfix">
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 portfolio-wrapper III_column">
                        <div class="portfolio-thumb">
                            <div class="port_img_wrapper">
                                <img src="{{ asset('core/images/dc_galry_img_1.jpg') }}" alt="filter_img">
                            </div>
                            <div class="portfolio_icon_wrapper zoom_popup">
                                <a class="img-link" href="{{ asset('core/images/dc_galry_img_1.jpg') }}"> <i class="fa fa-search-plus"></i>
                                </a>
                            </div>

                            <!-- portfolio-content -->

                        </div>
                        <!-- /.portfolio-thumb -->
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 portfolio-wrapper III_column">
                        <div class="portfolio-thumb">
                            <div class="port_img_wrapper">
                                <img src="{{ asset('core/images/dc_galry_img_2.jpg') }}" alt="filter_img">
                            </div>
                            <div class="portfolio_icon_wrapper zoom_popup">
                                <a class="img-link" href="{{ asset('core/images/dc_galry_img_2.jpg') }}"> <i class="fa fa-search-plus"></i>
                                </a>
                            </div>
                            <!-- portfolio-content -->

                        </div>
                        <!-- /.portfolio-thumb -->
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 portfolio-wrapper III_column">
                        <div class="portfolio-thumb">
                            <div class="port_img_wrapper">
                                <img src="{{ asset('core/images/dc_galry_img_3.jpg') }}" alt="filter_img">
                            </div>
                            <div class="portfolio_icon_wrapper zoom_popup">
                                <a class="img-link" href="{{ asset('core/images/dc_galry_img_3.jpg') }}"> <i class="fa fa-search-plus"></i>
                                </a>
                            </div>

                        </div>
                        <!-- /.portfolio-thumb -->
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 portfolio-wrapper III_column">
                        <div class="portfolio-thumb">
                            <div class="port_img_wrapper">
                                <img src="{{ asset('core/images/dc_galry_img_4.jpg') }}" alt="filter_img">
                            </div>
                            <div class="portfolio_icon_wrapper zoom_popup">
                                <a class="img-link" href="{{ asset('core/images/dc_galry_img_4.jpg') }}"> <i class="fa fa-search-plus"></i>
                                </a>
                            </div>

                        </div>
                        <!-- /.portfolio-thumb -->
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 portfolio-wrapper III_column">
                        <div class="portfolio-thumb">
                            <div class="port_img_wrapper">
                                <img src="{{ asset('core/images/dc_galry_img_3.jpg') }}" alt="filter_img">
                            </div>
                            <div class="portfolio_icon_wrapper zoom_popup">
                                <a class="img-link" href="{{ asset('core/images/dc_galry_img_3.jpg') }}"> <i class="fa fa-search-plus"></i>
                                </a>
                            </div>

                        </div>
                        <!-- /.portfolio-thumb -->
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 portfolio-wrapper III_column">
                        <div class="portfolio-thumb">
                            <div class="port_img_wrapper">
                                <img src="{{ asset('core/images/dc_galry_img_6.jpg') }}" alt="filter_img">
                            </div>
                            <div class="portfolio_icon_wrapper zoom_popup">
                                <a class="img-link" href="{{ asset('core/images/dc_galry_img_6.jpg') }}"> <i class="fa fa-search-plus"></i>
                                </a>
                            </div>

                        </div>
                        <!-- /.portfolio-thumb -->
                    </div>
                    
                    
                </div>
                
                <!--/#gridWrapper-->
            </div>
            <!-- /.row -->

        </div>
        <!-- /.container -->
    </section>
    <!--/.portfolio-area-->
@endsection
