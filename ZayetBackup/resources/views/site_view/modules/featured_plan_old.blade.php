@extends('site_view.layouts.main')
@section('head')
<style>
.required{
    color:"red";
}

input {
    position: relative;
    width: 150px; height: 20px;
    color: white;
}

input:before {
    position: absolute;
    top: 3px; left: 3px;
    content: attr(data-date);
    display: inline-block;
    color: black;
}

input::-webkit-datetime-edit, input::-webkit-inner-spin-button, input::-webkit-clear-button {
    display: none;
}

input::-webkit-calendar-picker-indicator {
    position: absolute;
    top: 3px;
    right: 0;
    color: black;
    opacity: 1;
}
</style>
@endsection
@section('content')
<div class="subscriptionplan med_toppadder100">
  <div class="container">
    <div class="row">
        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2">
            <div class="about_heading_wraper  text-center med_bottompadder50">
                <h1 class="med_bottompadder20">{{ $plans->name }}</h1>
                <img src="{{ asset('core/images/dental_line_center.png') }}" alt="line" class="med_bottompadder20">
                <p>{{ __('featured_plan.plan_tag') }}</p>
            </div>
        </div>
                            
            <!-- <div class="listli med_toppadder0 med_bottompadder100">
                <ul style="clear:both">
                    @foreach($plans->features as $feature)
                    @php $item = getFeature_byid($feature->feature_id); @endphp
                    <li><i class="fa fa-arrow-circle-right"></i>{{ $item ? $item->count.' '. $item->name : ''}} </li>
                    @endforeach
                </ul>
          
            </div>                 -->
                            
            <div class="booking_wrapper book_section med_toppadder100 featuredplan med_bottompadder100">
                <div class="container clearfix">
                    <div class="booking_box">
                        <div class="row">
                            <form action="{{ route('payment.store') }}" method="POST" autocomplete="off">
                            <!-- <form action="{{ route('payment.store') }}" method="POST" id="upload_form" autocomplete="off"> -->
                            @csrf
                            <div class="box_side_icon clearfix">
                            <!--   <div class="left--img">
                                    <img src="images/Icon_bk.png" alt="img">
                                </div> -->
                                
                                <div class="about_heading_wraper  text-center med_bottompadder10">
                                    <h1 class="med_bottompadder20">{{ __('featured_plan.form_fill_info') }}</h1>
                                
                                </div>
                                <input type="hidden" name="package_id" value="{{ $plans->id }}">
                                <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                                    <div class="contect_form1 form-group">
                                        <label>{{ __('featured_plan.account_type') }}</label>

                                        <select name="user_type" class="classic" value="{{ old('user_type') }}">
                                        <option>{{ __('featured_plan.drop_down_select') }}</option>
                                        <option value="3">{{ __('featured_plan.single_doctor_clinic') }}</option>
                                        <option value="4">{{ __('featured_plan.multispeciality_hospital') }}</option>
                                        </select>
                                        <div class="help-block"></div>
                                         @error('user_type')
                                            <span class="help-block required" role="alert">
                                                <li>{{ $message }}</li>
                                            </span>
                                            @enderror	
                                    </div>
                                </div>
                                <br>
                                <h6>Account Owner Information</h6>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="contect_form1 form-group">
                                        <label for="fname">{{ __('featured_plan.first_name') }}</label>
                                        <input type="text"  id="fname" name="fname" class="classic" value="{{ old('fname') }}">
                                        <div class="help-block"></div>
                                        @error('fname')
                                        <span class="help-block required" role="alert">
                                            <li>{{ $message }}</li>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="contect_form1 form-group">
                                        <label for="lname">{{ __('featured_plan.last_name') }}</label>
                                        <input type="text"  id="lname" name="lname" class="classic" value="{{ old('lname') }}">
                                        <div class="help-block"></div>
                                        @error('lname')
                                        <span class="help-block required" role="alert">
                                            <li>{{ $message }}</li>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="contect_form1 form-group">
                                        <label for="email" >{{ __('featured_plan.email') }}</label>
                                        <input type="text" id="email" name="email" class="classic" value="{{ old('email') }}">
                                        <div class="help-block"></div>
                                        @error('email')
                                            <span class="help-block required" role="alert">
                                                <li>{{ $message }}</li>
                                            </span>
                                            @enderror
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="contect_form1 form-group">
                                        <label>{{ __('featured_plan.phone') }}</label>
                                        <input type="text" name="mobile" class="classic" value="{{ old('mobile') }}">
                                        <div class="help-block"></div>
                                        @error('mobile')
                                            <span class="help-block required" role="alert">
                                                <li>{{ $message }}</li>
                                            </span>
                                            @enderror
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="contect_form1 form-group">
                                        <label>{{ __('featured_plan.address') }}</label>
                                        <input type="text" name="address" id="addr_input_location" class="classic" autocomplete="off">
                                        <div class="help-block"></div>
                                        @error('address')
                                            <span class="help-block required" role="alert">
                                                <li>{{ $message }}</li>
                                            </span>
                                            @enderror
                                        <input type="hidden" name="user_lat" value="{{old('user_lat')}}" id="user_lat" >
                                        <input type="hidden" name="user_long" value="{{old('user_long')}}" id="user_long" >
                                       
                                    </div>
                                </div>
                                <!-- <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="contect_form1 divhide">
                                        <label for="cars" style="visibility:hidden">Full Name</label>
                                        <input type="buton" value="Save Information" id="cars" class="btn btn-primary">
                                        <div class="help-block"></div>                                       
                                    </div>
                                </div> -->
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="contect_form1 form-group">
                                        <label>{{ __('featured_plan.country') }}</label>
                                        <select name="country" class="classic" value="{{ old('country') }}">

                                        @foreach(country() as $country)
                                         <option value="{{ $country->id }}">{{ $country->name }}</option>
                                        @endforeach 
                                        </select>
                                        @error('country')
                                        <span class="help-block required" role="alert">
                                            <li>{{ $message }}</li>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="clear:both">
                                    <div class="contect_form1 form-group">
                                        <label>{{ __('featured_plan.city') }}</label>
                                        <input type="text"  name="city" class="classic" value="{{ old('city') }}">
                                        <div class="help-block"></div>
                                        @error('city')
                                            <span class="help-block required" role="alert">
                                                <li>{{ $message }}</li>
                                            </span>
                                            @enderror
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="contect_form1 form-group">
                                        <label>{{ __('featured_plan.zipcode') }}</label>
                                        <input type="text"  name="zipcode" class="classic" value="{{ old('zipcode') }}">
                                        <div class="help-block"></div>
                                        @error('zipcode')
                                            <span class="help-block required" role="alert">
                                                <li>{{ $message }}</li>
                                            </span>
                                            @enderror
                                    </div>
                                </div>
                            </div>                       
                        </div>
                    </div>
                    <div class="chat_box chat_box_clr">
                        <div class="row">
                            <div class="booking_box_2 payment_div">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="about_heading_wraper  text-center med_bottompadder10">
                                        <h1 class="med_bottompadd0">Payment</h1>                                
                                    </div>    

                                    <h3>Card Type</h3>                                    
                                    <label><input type="radio" name="card_type" value="cc">Credit Card</label>&nbsp      
                                    <label><input type="radio" name="card_type" value="dc">Debit Card </label>
                                    <div class="help-block"></div>
                                    @error('card_type')
                                    <br>
                                    <label style="color:#ffc903 ;">
                                        <p>{{ $message }}</p>
                                    </label>
                                    @enderror 
                                            

                                    <div class="col-lg-12 col-xs-12 med_toppadder10" >
                                    <div class="contect_form1 form-group">
                                        <label >Name On card</label>
                                        <input type="text" name="name_on_card" class="classic" value="{{ old('name_on_card') }}">
                                        <div class="help-block"></div>
                                        @error('name_on_card')
                                            <span class="help-block required" role="alert">
                                                <li>{{ $message }}</li>
                                            </span>
                                        @enderror

                                    </div>
                                </div>
                                
                                <div class="col-lg-12 col-xs-12 med_toppadder10">
                                    <div class="contect_form1 form-group">
                                        <label>Card Number</label>
                                        <input type="text"  name="card_no" class="classic" value="{{ old('card_no') }}">
                                        <div class="help-block"></div>
                                        @error('card_no')
                                            <span class="help-block required" role="alert">
                                                <li>{{ $message }}</li>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-lg-12 col-xs-12 med_toppadder10">
                                    <div class="col-lg-6 col-sm-6 p-5">
                                    <div class="contect_form1 form-group">
                                        <label>CVV</label>
                                        <input type="text" name="cvv_no" class="classic" value="{{ old('cvv_no') }}">
                                        <div class="help-block"></div>
                                        @error('cvv')
                                            <span class="help-block required" role="alert">
                                                <li>{{ $message }}</li>
                                            </span>
                                        @enderror
                                    </div>
                                    </div><div class="col-lg-6 col-sm-6 p-5">
                                    <div class="contect_form1 form-group">
                                        <label>Expiry</label>
                                        <!-- <input type="date" name="expiry_date" data-date="" data-date-format="MMMM YYYY" class="classic" value="{{ old('expiry_date') }}" id="exp_date_input"> -->
                                        <input type="date" data-date="" data-date-format="MMMM YYYY" name="expiry_date" value="{{ date('Y-m-d') }}">
                                        <div class="help-block"></div>
                                         @error('expiry_date')
                                        <span class="help-block required" role="alert">
                                            <li>{{ $message }}</li>
                                        </span>
                                        @enderror
                                    </div>
                                    </div>
                                </div>
                                <!-- <div class="col-lg-12 col-xs-12 med_toppadder10">
                                    <input type="submit" name="" value="save" id="">
                                </div> -->
                                <input type="hidden" name="amount" value ="{{ $plans->price }}">
                                <div class="col-lg-10 col-lg-offset-1 med_toppadder20 ">
                                    <div class="contect_form1 form-group">
                                        <input type="submit" value="PAY ( {{ '$'.$plans->price }} )" id="cars" class="btn btn-primary saveBtn">
                                    </div>
                                    <div id="ajaxloader" style="display: none;"><img
                                        src="{{ asset('public/admin/images/ajax-loader.gif')}}" />{{ __('l.processing') }}
                                    </div>
                                </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
            </form>
        
        
        </div>     

        <div class="listli med_toppadder0 med_bottompadder100">
            <div class="about_heading_wraper  text-center med_bottompadder50">
                <h6  class="med_bottompadder20">plan features</h6>
                <img src="http://103.25.128.66/~doctorzayet/core/images/dental_line_center.png" alt="line" class="med_bottompadder20">
                </div>
                <ul style="clear:both">                
                    @foreach($plans->features as $feature)
                    @php $item = getFeature_byid($feature->feature_id); @endphp
                    <li><i class="fa fa-arrow-circle-right"></i>{{ $item ? $item->count.' '. $item->name : ''}} </li>
                    @endforeach
                </ul>
          
            </div>                             
    </div>
    </div>
  
</div>

@endsection
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script>
     //auto complete car location 

     var searchInput = 'addr_input_location';  
        $(document).ready(function () {  
            var autocomplete;
            autocomplete = new google.maps.places.Autocomplete((document.getElementById(searchInput)), {
                types: ["geocode"],
            });
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var near_place = autocomplete.getPlace();
                //assign lat long only for user location value to 
                document.getElementById('user_lat').value = near_place.geometry.location.lat();
                document.getElementById('user_long').value = near_place.geometry.location.lng();
                           
            });   
        }); 
        $(document).ready(function () { 
         $("input").on("change", function() {
           
            this.setAttribute(
                "data-date",
                moment(this.value, "YYYY-MM")
                .format( this.getAttribute("data-date-format") )
            )
            }).trigger("change") 
        });   
</script>
@endsection