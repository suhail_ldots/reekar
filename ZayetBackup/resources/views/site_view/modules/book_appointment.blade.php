@extends('site_view.layouts.main')
@section('content')
<div class="subscriptionplan med_toppadder100">
  <div class="container">
  <div class="row">
  <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2">
    <div class="about_heading_wraper  text-center med_bottompadder50">
        <h1 class="med_bottompadder20">Book Appointment</h1>
        <img src="{{ asset('core/images/dental_line_center.png') }}" alt="line" class="med_bottompadder20">
        <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate.</p>
    </div>
    </div>
                    
<div class="booking_wrapper book_section med_toppadder100 featuredplan med_bottompadder100">
        <div class="container clearfix">
            <div class="book_appointment">
            <div class="about_heading_wraper  text-center med_bottompadder10">
                            <h1 class="med_bottompadder20">Book Appointment</h1>
                          
                        </div>
                <div class="row">
                    <form>

                    <div class="box_side_icon clearfix">
                      <!--   <div class="left--img">
                            <img src="{{ asset('core/images/Icon_bk.png') }}" alt="img">
                        </div> -->
                        
                        
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="contect_form1">
                                <label for="cars">Choose Doctor</label>

                                <select id="cars" class="classic">
                                  <option>Choose your treatment</option>
                                  <option>Treatment category 1</option>
                                  <option>Treatment category 2</option>
                                  <option>Treatment category 3</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="contect_form1">
                                <label for="cars">Full Name</label>
                                <input type="text" placeholder="Enter Your Name "  id="cars" class="classic">
                                  
                               
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="contect_form1">
                                <label for="cars">Email</label>
                                <input type="text" placeholder="Enter Your Email here "  id="cars" class="classic">
                                  
                                
                            </div>
                        </div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="contect_form1">
                                <label for="cars">Phone Number</label>
                                <input type="text" placeholder="Enter Your Phone Number here "  id="cars" class="classic">
                                  
                                
                            </div>
                        </div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="contect_form1">
                                <label for="cars">Address</label>
                                <input type="text" placeholder="Enter Your Address here "  id="cars" class="classic">
                                  
                                
                            </div>
                        </div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="contect_form1">
                                <label for="cars">Zip Code</label>
                                <input type="text" placeholder="Enter Your Zip Code here "  id="cars" class="classic">
                               
                            </div>
                        </div><div class="col-lg-12  col-xs-12">
                            <div class="contect_form1 ">
                                
                                <input type="buton" value="Book Now" class="btn btn-primary">
                                  
                                
                            </div>
                        </div>


                        
                    </div>
                </form>
                </div>
            </div>
            
        </div>
    </div>
              
   <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2">
  <div class="about_heading_wraper  text-center med_bottompadder50">
                        <h1 class="med_bottompadder20">Appointment Calander</h1>
                        <img src="{{ asset('core/images/dental_line_center.png') }}" alt="line" class="med_bottompadder20">
                        <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate.</p>
                    </div>
                    </div>      
       <div class="col-lg-12">             
   <div class="calanderdiv med_bottompadder100" >
   <div class="cal1"></div>
   </div>                
   </div>
                                   
                    </div>
  </div>
  
  </div>                  
  </div>
  </div>
  
  </div>
@endsection
    