@extends('clinics.layouts.doctor_main')
@section('content')
<div class="container fixed-mar-top-inner"> 
      <div class="row">
        <div class="col-12 col-lg-12"> 
          <form class="formbox" action="{{ route('doctor.profile.update') }}" method="post" enctype="multipart/form-data">
          @csrf
          <input type="hidden" name="doctor_id" value="{{$doctor->id}}">
          
          <div class="col-12 col-lg-12 text-center">
    
           <div class="user-more">
           <!-- <img alt="User Pic" class="rounded-circle profile_users_img" @if($doctor->profile_image) src="{{ asset($doctor->profile_image) }}" @else src="https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg" @endif id="profile-image1"  width="150" height="150" > -->
           <img alt="User Pic" class="rounded-circle profile_users_img" width="150" height="150" src="{{ @fopen(\Url('storage/app/images/doctor/profile/').'/'.Auth::user()->profile_pic, 'r') ? \Url('storage/app/images/doctor/profile/').'/'.Auth::user()->profile_pic : asset('public/nobody_user.jpg') }}" id="profile-image1"  width="150" height="150" >
           </div>
          </div>
          

          <div class="col-12 col-md-8 mx-auto white mt-2">
          @if (count($errors) > 0)
          <div class="row">
            <div class="error" style="color:red;">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          </div>
        @endif
           <div class="row">
              <div class="col-md-12">
                  <div class="md-form mb-0">
                      <label for="name" class="">Your Medical Reg. No.</label>
                      <input type="text" name="reg_no" value="{!! $doctor->reg_no !!}" class="form-control" readonly="readonly" />
                  </div>
              </div>
            </div>
           <div class="row">
              <div class="col-md-12">
                  @if($doctor)
                  <input type="hidden" name="doc_id" value="{{ $doctor->user_id }}">
                  @endif
                  <div class="md-form mb-0">
                      <label for="name" class="">Your First Name</label>
                      <input type="text" name="first_name" value="{!! Auth::user()->first_name !!}" class="form-control">
                  </div>
              </div>
            </div>
           <div class="row">
              <div class="col-md-12">
                  <div class="md-form mb-0">
                  <label for="name" class="">Your Last Name</label>
                      <input type="text" name="last_name" value="{!! Auth::user()->last_name !!}" class="form-control">
                  </div>
              </div>
            </div>


           <div class="row">
              <div class="col-md-12">
                  <div class="md-form mb-0">
                    <label for="email" class="">Email</label>
                    <input type="text" name="email" value="{{Auth::user()->email}}" class="form-control">
                  </div>
              </div>
            </div>


           <div class="row">
              <div class="col-md-12">
                  <div class="md-form mb-0">
                  <label for="email" class="">Phone</label>
                      <input type="text" name="mobile" value="{{Auth::user()->mobile}}" class="form-control">
                  </div>
              </div>
            </div>

           <div class="row">
              <div class="col-md-12 mb-3">
                  <div class="md-form mb-0">
                      <label for="form1" >Enter Your Qualification</label>
                      <input type="text" id="form1" name="degree" class="form-control" value="{{old('degree', $doctor->degree)}}">
                  </div>
              </div>
            </div>
           <div class="row">
              <div class="col-md-12 mb-3">
                  <div class="md-form mb-0">
                      <label for="form1" >Enter Your Experience <small>(in years)</small></label>
                      <input type="text" id="form1" name="experience" class="form-control" value="{{old('experience', $doctor->experience)}}">
                  </div>
              </div>
            </div>
           <div class="row">
              <div class="col-md-12 mb-3">
                  <div class="md-form mb-0">
                      <label for="form1" >Enter Your Past-Experience</label>
                      <input type="text" id="form1" name="past_experience" class="form-control" value="{{old('past_experience', $doctor->past_experience)}}">
                  </div>
              </div>
            </div>
           <div class="row">
              <div class="col-md-12 mb-3">
                  <div class="md-form mb-0">
                    <label for="form2" >Enter Your Department</label>
                    <select name="department" class="form-control" id="">
                    @foreach(departments() as $department)
                      <option value="{{ $department->id }}" {{ isset($doctor) && $doctor->department_id == $department->id ? 'selected' : '' }} >{{ $department->name }}</option>
                    @endforeach
                    </select>
                      <!-- <input type="text" id="form2" name="speciality" class="form-control" value="{{old('speciality', $doctor->speciality)}}"> -->
                  </div>
              </div>
            </div>
           <div class="row">
              <div class="col-md-12 mb-3">
                  <div class="md-form mb-0">
                      <label for="form2" >Enter Your Speciality</label>
                     <select name="speciality" class="form-control" id="">
                      @foreach(speciality() as $speciality)
                        <option value="{{ $speciality->id }}" {{ isset($doctor) && $doctor->speciality == $speciality->id ? 'selected' : '' }} >{{ $speciality->name }}</option>
                      @endforeach
                      </select>
                      <!-- <input type="text" id="form2" name="speciality" class="form-control" value="{{old('speciality', $doctor->speciality)}}"> -->
                  </div>
              </div>
            </div>


              <div class="enter-section">
                 <ul class="list-group list-group-flush">
                    @php
                    $working_days = explode(",",$doctor->working_days);
                    if($doctor->availability){
                     // dd($doctor->availability); 
                     $avail = optional($doctor)->availability;                    
                     // $avail = get_object_vars(optional($doctor)->availability);
                    }
                     
                    @endphp

                    <li class="list-group-item">
                       <h2 class="pb-3"> Availability</h2>
                       <div class=" input-container {{ $errors->has('availability') ? ' has-error' : '' }}">
                           @if ($errors->has('availability'))
                               <span class="help-block">
                                   <strong>{{ $errors->first('availability') }}</strong>
                               </span>
                           @endif
                      </div>

                      <div class="availclass">
                        <div class="row justify-content-between">
                          <div class="col-4">
                          <div class="form-check mb-3">
                           <input type="checkbox" class="form-check-input daycheck" id="materialChecked2" name="availability[1]" value="1" @if(old('availability.1') == 1 || in_array(1,$working_days)) checked @endif>
                          <label class="form-check-label" for="materialChecked2">Monday</label>
                          </div>
                          </div>
                          
                          <div class="col-4">
                            <div class="form-group">
                              <input type="number" min="0" step="1" class="form-control fees" name="fees[1]" placeholder="Fees" @if(in_array(1,$working_days)) value="{{old('fees.1',$avail[1]['fees'])}}" @else value="{{old('fees.1')}}" @endif disabled>
                            </div>
                          </div>

                      </div>

                    <div class="row justify-content-between">
                        <div class="col-6">
                          <div class="form-group label-floating">
                              <h3 class="pb-0 h3-responsive">
                              <strong class="h6"> Available From</strong></h3>
                              <div class="{{ $errors->has('availability_from.1') ? ' has-error' : '' }}">
                              <input type="text" id="time" name="availability_from[1]" class="form-control time_from" data-dtp="" placeholder="Select Time" value="{{ in_array(1,$working_days) ? $avail[1]['availability_from'] : old('availability_from.1') }}" disabled>
                              <!-- <label for="time" class="control-label">Available From</label> -->
                                   @if ($errors->has('availability_from.1'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('availability_from.1') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                          <div class="form-group label-floating">
                            <h3 class="pb-0 h3-responsive">
                            <strong class="h6"> Available To</strong></h3>
                            <div class="{{ $errors->has('availability_to.1') ? ' has-error' : '' }}">
                            <input type="text" id="time2" name="availability_to[1]" class="form-control time_to" data-dtp="" placeholder="Select Time" value="{{ in_array(1,$working_days) ? $avail[1]['availability_to'] : old('availability_to.1')}}" disabled>
                            <!-- <label for="time" class="control-label">Available To</label> -->
                                 @if ($errors->has('availability_to.1'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('availability_to.1') }}</strong>
                                      </span>
                                  @endif
                              </div>
                              </div>
                        </div>
                    </div>
                  </div>
                  <hr>
                  <div class="availclass">
                      <div class="row justify-content-between">
                      <div class="col-4">
                      <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input daycheck" id="materialChecked3" name="availability[2]" value="2" @if(old('availability.2') == 2 || in_array(2,$working_days)) checked @endif>
                        <label class="form-check-label" for="materialChecked3">Tuesday</label>
                      </div>
                  </div>

                    <div class="col-4">
                      <div class="form-group">
                          <input type="number" min="0" step="1" class="form-control fees" name="fees[2]"  placeholder="Fees" @if(in_array(2,$working_days)) value="{{old('fees.2',$avail[2]['fees'])}}" @else value="{{old('fees.2')}}" @endif disabled>
                        </div>
                    </div>
                    </div>
                    <div class="row justify-content-between">
                        <div class="col-6">
                          <div class="form-group label-floating">
                              <h3 class="pb-0 h3-responsive">
                              <strong class="h6"> Available From</strong></h3>
                              <div class="{{ $errors->has('availability_from.2') ? ' has-error' : '' }}">
                              <input type="text" id="time3" name="availability_from[2]" class="form-control time_from" data-dtp="" placeholder="Select Time" value="{{ in_array(2,$working_days) ? $avail[2]['availability_from'] : old('availability_from.2') }}" disabled>
                              <!-- <label for="time" class="control-label">Available From</label> -->
                                   @if ($errors->has('availability_from.2'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('availability_from.2') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                        </div>
                        <div class="col-6">
                          <div class="form-group label-floating">
                            <h3 class="pb-0 h3-responsive">
                            <strong class="h6"> Available To</strong></h3>
                            <div class="{{ $errors->has('availability_to.2') ? ' has-error' : '' }}">
                            <input type="text" id="time4" name="availability_to[2]" class="form-control time_to" data-dtp="" placeholder="Select Time" value="{{ in_array(2,$working_days) ? $avail[2]['availability_to'] : old('availability_to.2') }}" disabled>
                            <!-- <label for="time" class="control-label">Available To</label> -->
                                 @if ($errors->has('availability_to.2'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('availability_to.2') }}</strong>
                                      </span>
                                  @endif
                              </div>
                              </div>
                        </div>
                    </div>
                  </div>
                      <hr>
                      <div class="availclass">
                      <div class="row justify-content-between">
                      <div class="col-4">
                      <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input daycheck" id="materialChecked4" name="availability[3]" value="3" @if(old('availability.3') == 3 || in_array(3,$working_days)) checked @endif>
                        <label class="form-check-label" for="materialChecked4">Wednesday</label>
                      </div>
                    </div>
                    <div class="col-4">
                      <div class="form-group">
                        <input type="number" min="0" step="1" class="form-control fees" name="fees[3]"  placeholder="Fees" @if(in_array(3,$working_days)) value="{{old('fees.3',$avail[3]['fees'])}}" @else value="{{old('fees.3')}}" @endif disabled>
                      </div>
                    </div>
                  </div>
                  <div class="row justify-content-between">
                      <div class="col-6">
                        <div class="form-group label-floating">
                            <h3 class="pb-0 h3-responsive">
                            <strong class="h6"> Available From</strong></h3>
                            <div class="{{ $errors->has('availability_from.3') ? ' has-error' : '' }}">
                            <input type="text" id="time15" name="availability_from[3]" class="form-control time_from" data-dtp="" placeholder="Select Time" value="{{ !in_array(3,$working_days) ? old('availability_from.3') : $avail[3]['availability_from'] }}" disabled>
                            <!-- <label for="time" class="control-label">Available From</label> -->
                                 @if ($errors->has('availability_from.3'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('availability_from.3') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                      </div>
                      <div class="col-6">
                        <div class="form-group label-floating">
                          <h3 class="pb-0 h3-responsive">
                          <strong class="h6"> Available To</strong></h3>
                          <div class="{{ $errors->has('availability_to.3') ? ' has-error' : '' }}">
                          <input type="text" id="time7" name="availability_to[3]" class="form-control time_to" data-dtp="" placeholder="Select Time" value="{{ !in_array(3,$working_days) ? old('availability_to.3') : $avail[3]['availability_to'] }}" disabled>
                          <!-- <label for="time" class="control-label">Available To</label> -->
                               @if ($errors->has('availability_to.3'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('availability_to.3') }}</strong>
                                    </span>
                                @endif
                            </div>
                            </div>
                      </div>
                  </div>
                </div>
                    <hr>
                    <div class="availclass">
                    <div class="row justify-content-between">
                    <div class="col-4">
                      <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input daycheck" id="materialChecked5" name="availability[4]" value="4" @if(old('availability.4') == 4 || in_array(4,$working_days)) checked @endif>
                        <label class="form-check-label" for="materialChecked5">Thursday</label>
                      </div>
                    </div>
                    <div class="col-4">
                      <div class="form-group">
                        <input type="number" min="0" step="1" class="form-control fees" name="fees[4]"  placeholder="Fees" @if(in_array(4,$working_days)) value="{{old('fees.4',$avail[4]['fees'])}}" @else value="{{old('fees.4')}}" @endif disabled>
                      </div>
                    </div>
                  </div>
                  <div class="row justify-content-between">
                      <div class="col-6">
                        <div class="form-group label-floating">
                            <h3 class="pb-0 h3-responsive">
                            <strong class="h6"> Available From</strong></h3>
                            <div class="{{ $errors->has('availability_from.4') ? ' has-error' : '' }}">
                            <input type="text" id="time8" name="availability_from[4]" class="form-control time_from" data-dtp="" placeholder="Select Time" value="{{ !in_array(4,$working_days) ? old('availability_from.4') : $avail[4]['availability_from'] }}" disabled>
                            <!-- <label for="time" class="control-label">Available From</label> -->
                                 @if ($errors->has('availability_from.4'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('availability_from.4') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                      </div>
                      <div class="col-6">
                        <div class="form-group label-floating">
                          <h3 class="pb-0 h3-responsive">
                          <strong class="h6"> Available To</strong></h3>
                          <div class="{{ $errors->has('availability_to.4') ? ' has-error' : '' }}">
                          <input type="text" id="time9" name="availability_to[4]" class="form-control time_to" data-dtp="" placeholder="Select Time" value="{{ !in_array(4,$working_days) ? old('availability_to.4') : $avail[4]['availability_to'] }}" disabled>
                          <!-- <label for="time" class="control-label">Available To</label> -->
                               @if ($errors->has('availability_to.4'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('availability_to.4') }}</strong>
                                    </span>
                                @endif
                            </div>
                            </div>
                      </div>
                  </div>
                </div>
                    <hr>
                    <div class="availclass">
                    <div class="row justify-content-between">
                    <div class="col-4">
                      <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input daycheck" id="materialChecked6" name="availability[5]" value="5" @if(old('availability.5') == 5 || in_array(5,$working_days)) checked @endif>
                        <label class="form-check-label" for="materialChecked6">Friday</label>
                      </div>
                    </div>
                    <div class="col-4">
                      <div class="form-group">
                        <input type="number" min="0" step="1" class="form-control fees" name="fees[5]"  placeholder="Fees" @if(in_array(5,$working_days)) value="{{old('fees.5',$avail[5]['fees'])}}" @else value="{{old('fees.5')}}" @endif disabled>
                      </div>
                    </div>
                  </div>
                  <div class="row justify-content-between">
                      <div class="col-6">
                        <div class="form-group label-floating">
                            <h3 class="pb-0 h3-responsive">
                            <strong class="h6"> Available From</strong></h3>
                            <div class="{{ $errors->has('availability_from.5') ? ' has-error' : '' }}">
                            <input type="text" id="time10" name="availability_from[5]" class="form-control time_from" data-dtp="" placeholder="Select Time" value="{{ !in_array(5,$working_days) ? old('availability_from.5') : $avail[5]['availability_from'] }}" disabled>
                            <!-- <label for="time" class="control-label">Available From</label> -->
                                 @if ($errors->has('availability_from.5'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('availability_from.5') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                      </div>
                      <div class="col-6">
                        <div class="form-group label-floating">
                          <h3 class="pb-0 h3-responsive">
                          <strong class="h6"> Available To</strong></h3>
                          <div class="{{ $errors->has('availability_to.5') ? ' has-error' : '' }}">
                          <input type="text" id="time11" name="availability_to[5]" class="form-control time_to" data-dtp="" placeholder="Select Time" value="{{ !in_array(4,$working_days) ? old('availability_to.5') : $avail[5]['availability_to'] }}" disabled>
                          <!-- <label for="time" class="control-label">Available To</label> -->
                               @if ($errors->has('availability_to.5'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('availability_to.5') }}</strong>
                                    </span>
                                @endif
                            </div>
                            </div>
                      </div>
                  </div>
                </div>
                    <hr>
                    <div class="availclass">
                    <div class="row justify-content-between">
                    <div class="col-4">
                      <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input daycheck" id="materialChecked7" name="availability[6]" value="6" @if(old('availability.6') == 6 || in_array(6,$working_days)) checked @endif>
                        <label class="form-check-label" for="materialChecked7">Saturday</label>
                      </div>
                    </div>
                    <div class="col-4">
                      <div class="form-group">
                        <input type="number" min="0" step="1" class="form-control fees" name="fees[6]"  placeholder="Fees" @if(in_array(6,$working_days)) value="{{old('fees.6',$avail[6]['fees'])}}" @else value="{{old('fees.6')}}" @endif disabled>
                      </div>
                    </div>
                  </div>
                  <div class="row justify-content-between">
                      <div class="col-6">
                        <div class="form-group label-floating">
                            <h3 class="pb-0 h3-responsive">
                            <strong class="h6"> Available From</strong></h3>
                            <div class="{{ $errors->has('availability_from.6') ? ' has-error' : '' }}">
                            <input type="text" id="time12" name="availability_from[6]" class="form-control time_from" data-dtp="" placeholder="Select Time" value="{{ !in_array(4,$working_days) ? old('availability_from.6') : $avail[6]['availability_from'] }}" disabled>
                            <!-- <label for="time" class="control-label">Available From</label> -->
                                 @if ($errors->has('availability_from.6'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('availability_from.6') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                      </div>
                      <div class="col-6">
                        <div class="form-group label-floating">
                          <h3 class="pb-0 h3-responsive">
                          <strong class="h6"> Available To</strong></h3>
                          <div class="{{ $errors->has('availability_to.6') ? ' has-error' : '' }}">
                          <input type="text" id="time13" name="availability_to[6]" class="form-control time_to" data-dtp="" placeholder="Select Time" value="{{ !in_array(6,$working_days) ? old('availability_to.6') : $avail[6]['availability_to'] }}" disabled>
                          <!-- <label for="time" class="control-label">Available To</label> -->
                               @if ($errors->has('availability_to.6'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('availability_to.6') }}</strong>
                                    </span>
                                @endif
                            </div>
                            </div>
                      </div>
                  </div>
                </div>
                    <hr>
                    <div class="availclass">
                    <div class="row justify-content-between">
                    <div class="col-4">
                      <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input daycheck" id="materialChecked8" name="availability[7]" value="7" @if(old('availability.7') == 7 || in_array(7,$working_days)) checked @endif>
                        <label class="form-check-label" for="materialChecked8">Sunday</label>
                      </div>
                    </div>
                    <div class="col-4">
                      <div class="form-group">
                        <input type="number" min="0" step="1" class="form-control fees" name="fees[7]"  placeholder="Fees" @if(in_array(7,$working_days)) value="{{old('fees.7',$avail[7]['fees'])}}" @else value="{{old('fees.7')}}" @endif disabled>
                      </div>
                    </div>
                  </div>
                  <div class="row justify-content-between">
                      <div class="col-6">
                        <div class="form-group label-floating">
                            <h3 class="pb-0 h3-responsive">
                            <strong class="h6"> Available From</strong></h3>
                            <div class="{{ $errors->has('availability_from.7') ? ' has-error' : '' }}">
                            <input type="text" id="time14" name="availability_from[7]" class="form-control time_from" data-dtp="" placeholder="Select Time" value="{{ !in_array(7,$working_days) ? old('availability_from.7') : $avail[7]['availability_from'] }}" disabled>
                            <!-- <label for="time" class="control-label">Available From</label> -->
                                 @if ($errors->has('availability_from.7'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('availability_from.7') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                      </div>
                      <div class="col-6">
                        <div class="form-group label-floating">
                          <h3 class="pb-0 h3-responsive">
                          <strong class="h6"> Available To</strong></h3>
                          <div class="{{ $errors->has('availability_to.7') ? ' has-error' : '' }}">
                          <input type="text" id="time16" name="availability_to[7]" class="form-control time_to" data-dtp="" placeholder="Select Time" value="{{ !in_array(7,$working_days) ? old('availability_to.7') : $avail[7]['availability_to'] }}" disabled>
                          <!-- <label for="time" class="control-label">Available To</label> -->
                               @if ($errors->has('availability_to.7'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('availability_to.7') }}</strong>
                                    </span>
                                @endif
                            </div>
                            </div>
                      </div>
                  </div>
                </div>
                    </li>
                   {{-- <!-- <li class="list-group-item">
                      <h3 class="pb-0">Your Signature<span class="required">*</span></h3>
                            <div class="file-field mt-2">
                               <a class="btn-floating blue-gradient mt-0 float-left upload-btn">
                               <i class="fa fa-paperclip" aria-hidden="true"></i>
                               <input type="file" id="select_sign" name="esign">
                               </a>
                               <div class="file-path-wrapper">
                                  <input class="file-path validate" type="text" placeholder="Upload Your Signature Picture" disabled>
                               </div>
                               <img id="signpreview" height="150" style="display:none"/>
                            </div>
                            <div class="{{ $errors->has('esign') ? ' has-error' : '' }}">
                              @if ($errors->has('esign'))
                                   <span class="help-block">
                                       <strong>{{ $errors->first('esign') }}</strong>
                                   </span>
                               @endif
                            </div>
                    </li> -->--}}

                    <li class="list-group-item">
                       <h2 class="pb-0 mt-2">About Me</h2>
                       <div class="md-form">
                         <label for="textareaBasic">Enter about yourself in brief.</label>
                         <textarea type="text" id="textareaBasic" class="form-control md-textarea" rows="3" name="about_doc">{{old('about_doc',$doctor->about_doc)}}</textarea>
                     </div>                    
                     <div class="col-12 col-lg-12 text-center my-4">
                      <button type="submit" class="btn btn-primary">Save</button>
                     </div>
                    </li>

                 </ul>
              </div>
          </div>
        </form>
      </div>
  </div>
</div>
<!--container end here-->

<div class="modal fade" id="img-modal" tabindex="-1" role="dialog" aria-labelledby="edit-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header p-2 text-center primary-color-dark">
                <h2 class="modal-title white-text w-100" id="edit-modal-label">Change profile picture</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class='white-text' aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="alert" id="message" style="display: none"></div>
            <form method="post" id="upload_form" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <div class="row">
                        <div class="col-12 col-md-8 mx-auto">
                           
                            <div class="w-100 text-center">
                                <!-- <input type="file" name="select_file" id="select_file"/> -->
                                <input class="file-path validate" type="file" name="profile_image" id="select_file" placeholder="Choose File">
                            </div>
                             <div class="row">
                                  <div class="col-12 col-md-12 text-center py-3">
                                    <img id="preview" height="100" class="img-responsive"/>
                                  </div>
                             </div>                     
                            <div class="text-center py-2 white">
                                <input type="submit" name="upload" id="upload" class="btn btn-primary text-white waves-effect waves-light" value="Upload">
                            </div>
                        </div>
                    </div>
                </div>
                <!--form group end-->
            </form>
            <span id="uploaded_image"></span>
        </div>
    </div>
</div>
</div>

@endsection

@section('script')
<script type="text/javascript">
		$(document).ready(function()
		{
			// $('#date').timepicki()
			// ({
			// 	//time: false,
			// 	clearButton: true
			// });

			$('#time').timepicki()
		
      $('#time3').timepicki()
      

      $('#time2').timepicki()
		
      $('#time4').timepicki()
			
      $('#time5').timepicki()
      
      $('#time6').timepicki()
      
      $('#time7').timepicki()
      

      $('#time8').timepicki()
      
      $('#time9').timepicki()
      
      $('#time10').timepicki()
      
      $('#time11').timepicki()
      
      $('#time12').timepicki()
      
      $('#time13').timepicki()
      

      $('#time14').timepicki()
      
      $('#time15').timepicki()
      
      $('#time16').timepicki()
      // ({
      //   date: false,
      //   //shortTime: false,
      //   format: 'HH:mm'
      // });
      
			// $('#date-format').timepicki()
			// ({
			// 	//format: 'dddd DD MMMM YYYY - HH:mm'
      //   format: 'YYYY-MM-DD HH:mm'
      //
			// });
			// $('#date-fr').timepicki()
			// ({
			// 	format: 'DD/MM/YYYY HH:mm',
			// 	lang: 'fr',
			// 	weekStart: 1,
			// 	cancelText : 'ANNULER',
			// 	nowButton : true,
			// 	switchOnClick : true
			// });

			// $('#date-end').timepicki()
			// ({
			// 	weekStart: 0, format: 'DD/MM/YYYY HH:mm'
			// });
			// $('#date-start').timepicki()
			// ({
			// 	weekStart: 0, format: 'DD/MM/YYYY HH:mm', shortTime : true
			// }).on('change', function(e, date)
			// {
			// 	$('#date-end').timepicki()('setMinDate', date);
			// });

		//	$.material.init()
		});
		</script>

    <script type="text/javascript">
    $( document ).ready(function() {
      $('.daycheck').change(function() {
        if(this.checked) {
            $(this).closest('.availclass').find('.time_to').prop('disabled', false);
            $(this).closest('.availclass').find('.time_from').prop('disabled', false);
            $(this).closest('.availclass').find('.fees').prop('disabled', false);
        }else{
            $(this).closest('.availclass').find('.time_to').prop('disabled', true);
            $(this).closest('.availclass').find('.time_from').prop('disabled', true);
            $(this).closest('.availclass').find('.fees').prop('disabled', true);
        }
    });

    $('input[type=checkbox]').each(function () {
        if(this.checked){
          $(this).closest('.availclass').find('.time_to').prop('disabled', false);
          $(this).closest('.availclass').find('.time_from').prop('disabled', false);
          $(this).closest('.availclass').find('.fees').prop('disabled', false);
        }
    });
    // $("#home").hover(function(){
    // $('#img-modal').modal('show');
          
        // });
    $(".profile_users_img").hover(function(){
      //alert("Df")
        $(this).disable = true;
    })   
    $(".profile_users_img").click(function(){
            $('#img-modal').modal('show');
        })
    $('#upload_form').on('submit', function(event){
      event.preventDefault();
      $.ajax({
        url:"{{ route('change_profile_pic') }}",

        method:"POST",
        data:new FormData(this),
        dataType:'JSON',
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function() {
          $('#message').css('display', 'block');
            $(".ajaxmsg").html("<div class='alert alert-info'><p>Please wait...</p></div>");
            $('#upload').prop('disabled', true);
        },

        success:function(data)
        {
          $(".ajaxmsg").empty();
          $('#message').css('display', 'block');
          $('#message').html(data.message);
          $('#message').addClass(data.class_name);
        // $('#uploaded_image').html(data.uploaded_image);
          if(data.status ==200){
            location.reload();
          }
        },
        error:function(data)
        {
            $('#upload').prop('disabled', false);
            var toAppend = '';
             $(".ajaxmsg").empty();
            console.log(data.responseJSON.errors);
           //alert(data.errors);
            $.each(data.responseJSON.errors, function(key, error) {
                 toAppend +='<div class="alert alert-success bg-danger">'+ error +'</div>';
            });
            $(".ajaxmsg").append(toAppend).fadeOut(5000);

        }
      })
      });
 
    });
    $(function() {
    $("#select_file").on('change', function() {
      // Display image on the page for viewing
      readURL(this, "preview");

    });
});  

function readURL(input, tar) {
  if (input.files && input.files[0]) { // got sth

    // Clear image container
    $("#" + tar).removeAttr('src');

    $.each(input.files, function(index, ff) // loop each image 
      {

        var reader = new FileReader();

        // Put image in created image tags
        reader.onload = function(e) {
          $('#' + tar).attr('src', e.target.result);
        }

        reader.readAsDataURL(ff);

      });
  }
}  
</script>

@endsection
