   
        @extends('clinics.layouts.nursing_home_main')
        @section('content')
       
        <div class="content-wrap">
            <div class="main">
                <div class="container-fluid">
                    
                    <!-- /# row -->
                    <section id="main-content">
                        
                     
                        <div class="row mt-20">
                            <div class="col-lg-7 col-sm-6 col-md-5 p-r-0 ">
                            <div class="page-header">
                                <div class="page-title">
                                    <h1>Hospital Dashboard</h1>
                                </div>
                            </div>
                        </div>
								  
						<div class="col-lg-5 col-sm-6 col-md-7 labeldiv">
                             <label>Filter <select class="selectbtn"><option>Today's Appointment</option></select>
							 <span class="fa fa-bars adddiv">
							 </span></label>
                                  </div>
                                    
                                </div>
                        <div class="row mt-20">
                        <div class="col-lg-12">
                        <div class="doctor_apointment card today_appointment">
                        <h3 class="pull-left">Mr. Abhiraj sirohi</h3>
                        <div>
                        <label><i class="fa fa-male"></i>Mr. Amit Kumar</label>
                        <label><i class="fa fa-map-marker"></i>California, Uk</label>
                        <label><i class="fa fa-calendar-minus-o"></i>31 March, 2020</label>
                        </div>
                        
                        <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."Lorem ipsum dolor sit amet,</p><div>
                        <label>Scheduled on   </label>
                        <label><i class="fa fa-calendar"></i>Monday</label>
                        <label><i class="fa fa-calendar-minus-o"></i>10:00am - 11:00am </label>
                        </div>
                      
                      <button class="pull-right btn btn-primary">Contact Now</button>  
                        </div>
						<div class="doctor_apointment card today_appointment">
                        <h3 class="pull-left">Mr. Abhiraj sirohi</h3>
                        <div>
                        <label><i class="fa fa-male"></i>Mr. Amit Kumar</label>
                        <label><i class="fa fa-map-marker"></i>California, Uk</label>
                        <label><i class="fa fa-calendar-minus-o"></i>31 March, 2020</label>
                        </div>
                        
                        <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."Lorem ipsum dolor sit amet,</p><div>
                        <label>Scheduled on   </label>
                        <label><i class="fa fa-calendar"></i>Monday</label>
                        <label><i class="fa fa-calendar-minus-o"></i>10:00am - 11:00am </label>
                        </div>
                      
                      <button class="pull-right btn btn-primary">Contact Now</button>  
                        </div>
						<div class="doctor_apointment card today_appointment">
                        <h3 class="pull-left">Mr. Abhiraj sirohi</h3>
                        <div>
                        <label><i class="fa fa-male"></i>Mr. Amit Kumar</label>
                        <label><i class="fa fa-map-marker"></i>California, Uk</label>
                        <label><i class="fa fa-calendar-minus-o"></i>31 March, 2020</label>
                        </div>
                        
                        <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."Lorem ipsum dolor sit amet,</p><div>
                        <label>Scheduled on   </label>
                        <label><i class="fa fa-calendar"></i>Monday</label>
                        <label><i class="fa fa-calendar-minus-o"></i>10:00am - 11:00am </label>
                        </div>
                      
                      <button class="pull-right btn btn-primary">Contact Now</button>  
                        </div>
                        
                        </div>
                        </div>     
                            </div>
                        </div>
                    </div>                    
                    </section>
                </div>
            </div>
        </div>
@endsection
@section('script')
<script>
    $(document).ready(function(){
        $(".adddiv").click(function(){
        $(".today_appointment").toggleClass("divwidth");
        
        });
    });
    
</script>
@endsection
