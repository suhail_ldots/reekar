@extends('clinics.layouts.patient_main')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-8 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>Patient Dashboard</h1>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
                
                <!-- /# column -->
            </div>
            <!-- /# row -->
            <section id="main-content">
                
                <!-- /# row -->


                <div class="row">
                    <!-- column -->
                    <div class="col-lg-6">
                        <div class="card sales">
                            <div class="card-body">
                                <h2>Overall Appointments <span style="float: right;  color: #9999; font-size:26px;">1800</span></h2>
                                
                            </div>
                            <div class="graph_div">
                            <div class="graph">
                            <span>2000</span>
                            <div class="graph_height">
                            </div>
                            <span>Mar</span>
                            </div><div class="graph">
                            <span>4000</span>
                            <div class="graph_height heightdiv">
                            </div>
                            <span>A</span>
                            </div><div class="graph">
                            <span>2000</span>
                            <div class="graph_height">
                            </div>
                            <span>Mar</span>
                            </div><div class="graph">
                            <span>4000</span>
                            <div class="graph_height heightdiv">
                            </div>
                            <span>A</span>
                            </div><div class="graph">
                            <span>2000</span>
                            <div class="graph_height">
                            </div>
                            <span>Mar</span>
                            </div><div class="graph">
                            <span>4000</span>
                            <div class="graph_height heightdiv">
                            </div>
                            <span>A</span>
                            </div>
                            </div>
                        </div>
                    </div>
                    <!-- column -->
                    <div class="col-lg-6">
                    
                        <div class="card sales ">
                            <div class="card-body">
                                <h2>Department Wise Data <span style="float: right;  color: #9999; font-size:26px;">280</span></h2>
                                
                            </div>
                            <div class="graph_div department doctdashboard">
                            <div class="graph">
                            <span>2000</span>
                            <div class="graph_height">
                            </div>
                            <span>Dep1</span>
                            </div><div class="graph">
                            <span>4000</span>
                            <div class="graph_height heightdiv">
                            </div>
                            <span>Dep2</span>
                            </div><div class="graph">
                            <span>2000</span>
                            <div class="graph_height">
                            </div>
                            <span>Dep3</span>
                            </div><div class="graph">
                            <span>4000</span>
                            <div class="graph_height heightdiv">
                            </div>
                            <span>Dep4</span>
                            </div><div class="graph">
                            <span>2000</span>
                            <div class="graph_height">
                            </div>
                            <span>Dep5</span>
                            </div>
                            </div>
                        </div>
                    </div>
                    <!-- column -->
                    <div class="col-lg-6">
                        <div class="card">
                        <div class="card-body">
                                <h2>New Messages </h2>
                                
                            </div>
                        <div class="newmessage">
                    <img src="assets/images/booking_call.png">
                            <div class="message_content">
                            <h4>148 total New Messages</h4>
                            <a href="#">click to check Message</a>
                            </div></div>	
                        <label class="timm"><i class="fa fa-clock-o"></i>10:00 am (Last Updated)<span style="float:right">24/03/2020</span></label  
                            
                        ></div>
                        
                    </div>
                    
                        <div class="col-lg-6">
                        <div class="card">
                        <div class="card-body">
                                <h2>New Messages </h2>
                                
                            </div>
                        <div class="newmessage">
                    <img src="assets/images/booking_call.png">
                            <div class="message_content">
                            <h4>148 total New Messages</h4>
                            <a href="#">click to check Message</a>
                            <span class="fa fa-phone" ></span>
                            </div></div>	
                        <label class="timm"><i class="fa fa-clock-o"></i>10:00 am (Last Updated)<span style="float:right">View</span></label> </div>
                        
                    </div>
                    <!-- column -->
                </div>

<div class="card cardapp">
                <div class="row mt-20">
                    <div class="col-lg-8">
                        <h2>Today’s Upcoming Appointment</h2>
                            </div>
                            
                            <div class="col-lg-4">
                        <label>Filter <select class="selectbtn"><option>Today's Appointment</option></select></label>
                            </div>
                            
                        </div>
                        
                <div class="row mt-20">
                <div class="col-lg-12">
                <div class="doctor_apointment">
                <h3 class="pull-left">Mr. Abhiraj sirohi</h3>
                <div class="pull-right">
                <label><i class="fa fa-male"></i>Mr. Amit Kumar</label>
                <label><i class="fa fa-map-marker"></i>California, Uk</label>
                <label><i class="fa fa-clock-o"></i>11.00am</label>
                </div>
                
                <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."Lorem ipsum dolor sit amet,</p>
                
                <h6 class="pull-right">Contact Now</h6>  
                </div>
                <div class="doctor_apointment">
                <h3 class="pull-left">Mr. Abhiraj sirohi</h3>
                <div class="pull-right">
                <label><i class="fa fa-male"></i>Mr. Amit Kumar</label>
                <label><i class="fa fa-map-marker"></i>California, Uk</label>
                <label><i class="fa fa-clock-o"></i>11.00am</label>
                </div>
                
                <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."Lorem ipsum dolor sit amet,</p>
                
                <h6 class="pull-right">Contact Now</h6>  
                </div>
                <div class="doctor_apointment">
                <h3 class="pull-left">Mr. Abhiraj sirohi</h3>
                <div class="pull-right">
                <label><i class="fa fa-male"></i>Mr. Amit Kumar</label>
                <label><i class="fa fa-map-marker"></i>California, Uk</label>
                <label><i class="fa fa-clock-o"></i>11.00am</label>
                </div>
                
                <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."Lorem ipsum dolor sit amet,</p>
                
                <h6 class="pull-right">Contact Now</h6>  
                </div>
                <div class="doctor_apointment">
                <h3 class="pull-left">Mr. Abhiraj sirohi</h3>
                <div class="pull-right">
                <label><i class="fa fa-male"></i>Mr. Amit Kumar</label>
                <label><i class="fa fa-map-marker"></i>California, Uk</label>
                <label><i class="fa fa-clock-o"></i>11.00am</label>
                </div>
                
                <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."Lorem ipsum dolor sit amet,</p>
                
                <h6 class="pull-right">Contact Now</h6>  
                </div>
                </div>
                </div>   
                        
                    </div>
                    
                        
                    </div>
                </div>
</div>
@endsection
                




                
                
            </section>
        </div>
    </div>
</div>
        