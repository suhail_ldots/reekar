<!-- Header wrapper Start -->
<div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
    <div class="nano">
        <div class="nano-content">
            <div class="logo"><a href="#"><img src="{{ asset('core/assets/images/FORTIS.png') }}"><span>Fortis Hospital</span></a></div>
            <ul>                        
                <li class="{{ isActiveRoute('clinic.dashboard','active') }}"><a href="{{ route('clinic.dashboard') }}" ><i><img src="{{ asset('core/assets/images/icon.png') }}"></i> Dashboard  </a>                    
                </li>                        
                <li class="{{ isActiveRoute('nursing-doctor.index','active') }}"><a href="{{ route('nursing-doctor.index') }}"><i><img src="{{ asset('core/assets/images/icon2.png') }}"></i> Doctors </a></li>
                <li class="{{ isActiveRoute('nursing_all.appointments','active') }}" ><a href="{{ route('nursing_all.appointments') }}"><i><img src="{{ asset('core/assets/images/icon3.png') }}"></i>Appointment</a></li>
                <li><a href="#"><i><img src="{{ asset('core/assets/images/icon4.png') }}"></i> Patients</a></li>
                <li><a href="#"><i><img src="{{ asset('core/assets/images/icon5.png') }}"></i> Messages</a></li>
                <li><a href="#" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"><i><img src="{{ asset('core/assets/images/icon6.png') }}"></i> Logout</a></li>
               <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
                </form>

                <!-- <li><a href="app-widget-card.html"><i><img src="{{ asset('core/assets/images/icon6.png') }}" class="sidebar-sub-toggle"></i> Multi Menu</a></li> -->
            </ul>
        </div>
    </div>
</div>
<!-- /# sidebar -->
<div class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="float-left">
                <img src="{{ asset('core/images/index_4_logo.png') }}" style="width:120px">
                    <div class="hamburger sidebar-toggle">
                        <span class="line"></span>
                        <span class="line"></span>
                        <span class="line"></span>
                    </div>
                </div>
                <div class="float-right">
                    <ul>
                        <li class="header-icon dib"><span><img src="{{ asset('core/assets/images/english.png') }}">English </span>
                            
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--header wrapper end-->
    