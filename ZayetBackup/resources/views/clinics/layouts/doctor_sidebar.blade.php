<!-- /# sidebar -->
<div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
    <div class="nano">
        <div class="nano-content">
            <div class="logo"><a href="{{ route('doctor.profile.view') }}"><img class="rounded-circle profile_users_img" src="{{ @fopen(\Url('storage/app/images/doctor/profile/').'/'.Auth::user()->profile_pic, 'r') ? \Url('storage/app/images/doctor/profile/').'/'.Auth::user()->profile_pic : asset('public/nobody_user.jpg') }}" ><span>{{ Auth::user()->first_name.' '.Auth::user()->last_name }}</span></a></div>
            <ul>                      
                <li class="{{ isActiveRoute('doctor.dashboard','active') }}"><a href="{{ route('doctor.dashboard') }}" ><i><img src="{{ asset('core/assets/images/icon.png') }}"></i> Dashboard  </a>        
                </li>                       
                <li class="{{ isActiveRoute('doctors_all.appointments','active') }}"><a href="{{ route('doctors_all.appointments') }}"><i><img src="{{ asset('core/assets/images/icon3.png') }}"></i>Appointment</a></li>
                <li><a href="#"><i><img src="{{ asset('core/assets/images/icon4.png') }}"></i> Patients</a></li>
                <li><a href="#"><i><img src="{{ asset('core/assets/images/icon5.png') }}"></i> Messages</a></li>
                <li><a href="#" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"><i><img src="{{ asset('core/assets/images/icon6.png') }}"></i> Logout</a></li>                        
                

               <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
                </form>
            </ul>
        </div>
    </div>
</div>
<!-- /# sidebar -->
<div class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="float-left">
                <img src="{{ asset('core/images/index_4_logo.png') }}" style="width:120px">
                    <div class="hamburger sidebar-toggle">
                        <span class="line"></span>
                        <span class="line"></span>
                        <span class="line"></span>
                    </div>
                </div>
                <div class="float-right">
                    <ul>                                
                        <li class="header-icon dib"><span><img src="{{ asset('assets/images/english.png') }}">English </span>                                 
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>