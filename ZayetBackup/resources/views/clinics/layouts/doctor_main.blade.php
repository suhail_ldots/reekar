<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" >
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @yield('title')
        <title>Doctors Loop - Patient access to hope</title>

<!-- ================= Favicon ================== -->
         <!-- Standard -->
         <link rel="shortcut icon" href="{{ asset('core/images/favicon.png') }}">
        <!-- Retina iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('core/images/favicon.png') }}">
        <!-- Retina iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('core/images/favicon.png') }}">
        <!-- Standard iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('core/images/favicon.png') }}">
        <!-- Standard iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('core/images/favicon.png') }}">

        <!-- Styles -->     
        <link href="{{ asset('core/assets/css/lib/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('core/assets/css/lib/themify-icons.css') }}" rel="stylesheet">
        <link href="{{ asset('core/assets/css/lib/menubar/sidebar.css') }}" rel="stylesheet">
        <link href="{{ asset('core/assets/css/lib/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('core/assets/css/lib/helper.css') }}" rel="stylesheet">
        <link href="{{ asset('core/assets/css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('core/css/timepicki.css') }}" rel="stylesheet">

    @yield('head')
</head>
    @yield('div')
    <body>       
        @include('clinics.layouts.doctor_sidebar')
        @include('clinics.partials.messages')
        @yield('content')
        @include('clinics.layouts.footer') 
        
    </body>
</html>