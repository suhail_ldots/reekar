@extends('admin.layouts.master_blank')
@section('css')
<style>
    .required {
        color: red;
    }
</style>
<link href="{{ URL::asset('public/admin/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<!--  <div class="home-btn d-none d-sm-block">
            <a href="index" class="text-dark"><i class="fas fa-home h2"></i></a>
        </div> -->

<div class="wrapper-page">
    <div class="card overflow-hidden account-card mx-3">
        <div class="bg-primary p-4 text-white text-center position-relative">
            <h4 class="font-20 m-b-5">Get your Reekar account now</h4>
            <p class="text-white-50 mb-4" style="color:rgb(255, 255, 255) !important;">&nbsp;</p>
            <a href="{{ URL::to('/') }}" class="logo logo-admin"><img
                    src="{{ URL::asset('public/admin/images/zayet_logo.png') }}" height="32" alt="logo"></a>
        </div>
        @if(Session::has('success'))
        <div class="alert alert-dismissible alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <a href="#" class="alert-link">{{Session::get('success')}}</a>
        </div>
        @endif

        @if(Session::has('error'))
        <div class="alert alert-dismissible alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <a href="#" class="alert-link">{{Session::get('error')}}</a>
        </div>
        @endif

        @if (count($errors) > 0)
        <br>
        <div class="alert alert-danger" style="margin-top: 30px;margin-bottom: -5px;">
            <ul>
                <li>Please fill valid data</li>
            </ul>
        </div>
        @endif
        <div class="account-card-content" style="padding-top: 20px;">
            <form class="form-horizontal m-t-30" action="{{route('register')}}" method="post">
                {{ csrf_field() }}

                <div class="form-group">
                    <label>Organization Name</label><span class="required">*</span>
                    <input type="text" name="organization_name" maxlength="100" id="orgname"
                        class="form-control @error('organization_name') is-invalid @enderror"
                        value="{{old('organization_name')}}" placeholder="Organization Name">

                    @if($errors->has('organization_name'))
                    <span class="help-block">
                        {{ $errors->first('organization_name') }}
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label>Registration No.</label>
                    <input type="text" name="registration_no" maxlength="50" id="regno"
                        class="form-control @error('registration_no') is-invalid @enderror"
                        value="{{old('registration_no')}}" placeholder="Registration No.">

                    @if($errors->has('registration_no'))
                    <span class="help-block">
                        {{ $errors->first('registration_no') }}
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="userfname">First Name</label><span class="required">*</span>
                    <input type="text" name="first_name" maxlength="50" maxlength="50" id="userfname"
                        class="form-control @error('first_name') is-invalid @enderror" value="{{old('first_name')}}"
                        placeholder="Firtst Name">

                    @if($errors->has('first_name'))
                    <span class="help-block">
                        {{ $errors->first('first_name') }}
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="userlname">Last Name</label><span class="required">*</span>
                    <input type="text" name="last_name" maxlength="50" id="userlname"
                        class="form-control @error('last_name') is-invalid @enderror" value="{{old('last_name')}}"
                        placeholder="Last Name">

                    @if($errors->has('last_name'))
                    <span class="help-block">
                        {{ $errors->first('last_name') }}
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="useremail">Email</label><span class="required">*</span>
                    <input type="email" name="email" maxlength="70" id="useremail"
                        class="form-control @error('email') is-invalid @enderror" value="{{old('email')}}"
                        placeholder="Email">

                    @if($errors->has('email'))
                    <span class="help-block">
                        {{ $errors->first('email') }}
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label>Select Package</label><span class="required">*</span>
                    <select name="subscription_packege_id"
                        class="form-control @error('subscription_packege_id') is-invalid @enderror select2"
                        value="{{old('subscription_packege_id')}}" id="packege">
                        @foreach($subscriptions as $subscription)
                        <option value="{{$subscription->id}}"  @if(old("subscription_packege_id") == $subscription->id) selected @endif>{{$subscription->name}}</option>
                        @endforeach
                    </select>
                    @if($errors->has('subscription_packege_id'))
                    <span class="help-block">
                        {{ $errors->first('subscription_packege_id') }}
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label>Role</label><span class="required">*</span>
                    <select name="role_id" class="select2 form-control @error('role_id') is-invalid @enderror"
                        value="{{old('role_id')}}" id="roleid">
                        <option value="2"  @if(old("role_id") == 2) selected @endif >Supplier</option>
                        <option value="3"  @if(old("role_id") == 3) selected @endif>Distributor</option>
                    </select>
                    @if($errors->has('role_id'))
                    <span class="help-block">
                        {{ $errors->first('role_id') }}
                    </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="inputMobile">@lang('l.mobile') No.</label><span class="required">*</span>
                    <input type="text" name="mobile" maxlength="20" id="inputMobile"
                        class="form-control @error('mobile') is-invalid @enderror" value="{{old('mobile')}}"
                        placeholder="@lang('l.mobile') No.">

                    @if($errors->has('mobile'))
                    <span class="help-block">
                        {{ $errors->first('mobile') }}
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="inputMobile2">Alternate Mobile.</label>
                    <input type="text" name="alternate_mobile" maxlength="20" id="inputMobile2"
                        class="form-control @error('alternate_mobile') is-invalid @enderror"
                        value="{{old('alternate_mobile')}}" placeholder="Alternate @lang('l.mobile') No.">

                    @if($errors->has('alternate_mobile'))
                    <span class="help-block">
                        {{ $errors->first('alternate_mobile') }}
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="inputaddress">Address</label>
                    <input type="text" name="address" maxlength="250" id="inputaddress"
                        class="form-control @error('address') is-invalid @enderror" value="{{old('address')}}"
                        placeholder="Address">

                    @if($errors->has('address'))
                    <span class="help-block">
                        {{ $errors->first('address') }}
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label>Country</label><span class="required">*</span>
                    <select name="country" id="country" class="form-control select2" onchange="getStateByCountry(this.value);">
                        <option value="">Select Country*</option>
                        @foreach(signup_country() as $country)
                        <option value="{{$country->id}}"   @if(old("country") == $country->id) selected @endif >{{$country->name}}</option>
                        @endforeach
                    </select>

                    @if($errors->has('country'))
                    <span class="help-block">
                        {{ $errors->first('country') }}
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label>State</label><span class="required"></span>
                    <select name="state" id="state" class="form-control select2" onchange="getCityByState(this.value);">
                       @php $currentState = (old("country") !== NULL) ? state(old("country")):array(); @endphp
                        <option value="">Select State</option>
                        @if(count($currentState) > 0)
                        @foreach($currentState as $state)
                        <option value="{{$state->id}}" @if($state->id == old("state")) selected
                            @endif>{{$state->name}}</option>
                        @endforeach
                        @endif
                    </select>

                    @if($errors->has('state'))
                    <span class="help-block">
                        {{ $errors->first('state') }}
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label>City</label><span class="required"></span>
                    <select name="city" id="city" class="form-control select2" onchange="getZipByCity(this.value);">                   
                        @php $currentCity = (old("state") !== NULL) ? city(old("state")) : array(); @endphp
                        <option value="">Select City</option>
                        @foreach($currentCity as $city)
                        <option value="{{$city->id}}" @if($city->id == old("city")) selected
                            @endif >{{$city->name}}</option>
                        @endforeach
                    </select>

                    @if($errors->has('city'))
                    <span class="help-block">
                        {{ $errors->first('city') }}
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label>@lang('l.postcode')</label>
                    <select name="postcode" id="postcode" class="form-control select2">
                       @php $currentpostcode = (old("city") !== NULL) ? us_zipByCityId(old("city")):array(); @endphp
                        <option value="">Select postcode</option>
                        @if(count($currentpostcode) > 0)
                        @foreach($currentpostcode as $postcode)
                        <option value="{{$postcode->id}}" @if($postcode->id == old("postcode")) selected
                            @endif>{{$postcode->zipcode}}</option>
                        @endforeach
                        @endif
                    </select>
                   <!--  <input type="text" name="postcode" maxlength="6" id="postcode"
                        class="form-control @error('postcode') is-invalid @enderror" value="{{old('postcode')}}"
                        placeholder="@lang('l.postcode')"> -->

                    @if($errors->has('postcode'))
                    <span class="help-block">
                        {{ $errors->first('postcode') }}
                    </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="userpassword">Password</label><span class="required">*</span>
                    <small class="form-text text-muted">(<b>Hint: </b> Your password must be equal or more than 8 characters.)</small>
                    <input type="password" name="password" maxlength="250" id="userpassword"
                        class="form-control showpass @error('password') is-invalid @enderror"
                        value="{{old('password')}}" placeholder="Password">

                    @if($errors->has('password'))
                    <span class="help-block">
                        {{ $errors->first('password') }}
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="password_confirmation">Confirm Password</label><span class="required">*</span>
                    <input type="password" name="password_confirmation" maxlength="250" id="password_confirmation"
                        class="form-control showpass @error('password_confirmation') is-invalid @enderror"
                        value="{{old('password_confirmation')}}" placeholder="Confirm Password">
                    <span toggle=".showpass" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                    @if($errors->has('password_confirmation'))
                    <span class="help-block">
                        {{ $errors->first('password_confirmation') }}
                    </span>
                    @endif
                </div>

                <div class="form-group row m-t-20">
                    <div class="col-12 text-right">
                        <button class="btn btn-primary w-md waves-effect waves-light" type="submit" id="registerBtn">Register</button>
                    </div>
                </div>

                <div class="form-group m-t-10 mb-0 row">
                    <div class="col-12 m-t-20">
                        <p class="mb-0" id="termAndcondition" style="text-align:center;">By registering you agree to the Reekar <a href="#" class="text-primary">Privacy Policy</a></p>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="m-t-40 text-center">
        <p>Already have an account ? <a href="{{route('login')}}" class="font-500 text-primary"> Login </a> </p>
        <p>Facing any problem? Please<a href="{{route('contact')}}" class="font-500 text-primary"> Contact Us</a> </p>
        
        <p>© {{date('Y')}} Reekar</p>
    </div>

</div>
<!-- end wrapper-pags -->

<!-- include term and condition -->
@include('admin._term_and_condition_modal')
@endsection

@section('script')
<script>
    $( document ).ready(function() {
        
        $('input').attr('autocomplete', 'off');

        var email = $("#useremail").val().length;
        var pass  = $("#userpassword").val().length;
        var pass_conf  = $("#password_confirmation").val().length;              
        if(email == 0 || pass == 0 || pass_conf == 0){
            $('#registerBtn').prop( "disabled", true );
        }
        $("#userpassword").keyup(function(){           
            var email1 = $("#useremail").val().length;
            var pass1  = $("#userpassword").val().length; 
            var pass_conf1 = $("#password_confirmation").val().length; 
            
            if(email1 != 0 && pass1 != 0 && pass_conf1 != 0){
                $('#registerBtn').prop( "disabled", false);
            }else{
                $('#registerBtn').prop( "disabled", true );
            }
        });
        $("#useremail").keyup(function(){            
            var email2 = $("#useremail").val().length;
            var pass2  = $("#userpassword").val().length;  
            var pass_conf2  = $("#password_confirmation").val().length;  
           
            if(email2 != 0 && pass2 != 0 && pass_conf2 != 0){
                $('#registerBtn').prop( "disabled", false);
            }else{
                $('#registerBtn').prop( "disabled", true );
            }
        });

        $("#password_confirmation").keyup(function(){            
            var email3 = $("#useremail").val().length;
            var pass3  = $("#userpassword").val().length;  
            var pass_conf3  = $("#password_confirmation").val().length;  
           
            if(email3 != 0 && pass3 != 0 && pass_conf3 != 0){
                $('#registerBtn').prop( "disabled", false);
            }else{
                $('#registerBtn').prop( "disabled", true );
            }
        });

        //term and ccondition
        $("#termAndcondition").click(function(){
            $('#condition-model').modal('show');
        });

    });
</script>
<script>   
    $(".toggle-password").click(function () {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "password");
        } else {
            input.attr("type", "text");
        }
    });
</script>
<script>
    function getStateByCountry(country) {
        var toAppend = '';      
        $('#city').html('<option value="" selected>Select City</option>');
        $('#state').html('<option value="" selected >Select State</option>');
        $('#postcode').html('<option value="" selected >Select Zip cCde</option>');
        if (country !== '') {
            $.ajax({
                type: 'GET',
                url: "{{route('get-state-by-country-ajax')}}?id=" + country,
                dataType: 'json',
                success: function (data) {
                    for (var i = 0; i < data.length; i++) {
                        toAppend += '<option value=' + data[i]['id'] + '>' + data[i]['name'] + '</option>';
                    }                    
                    $('#state').append(toAppend);
                    toAppend = '';
                }
            });
        } else {
           
            alert("please choose country");
        }

    }

    function getCityByState(city) {
        var toAppend = '';
        $('#city').html('<option value="" selected >Select City</option>');
        $('#postcode').html('<option value="" selected >Select Zip cCde</option>');
        if (city !== '') {
            $.ajax({
                type: 'GET',
                url: "{{route('get-city-by-state-ajax')}}?id=" + city,
                dataType: 'json',
                success: function (data) {
                    for (var i = 0; i < data.length; i++) {
                        toAppend += '<option value=' + data[i]['id'] + '>' + data[i]['name'] + '</option>';
                    }
                    //$('#city').html('');
                    $('#city').append(toAppend);
                    toAppend = '';
                }
            });
        } else {
            alert("please choose state");
        }

    }
    function getZipByCity(cid) {         
           // return false;
            var toAppend = '';
            $('#postcode').html('<option value="" selected >Select Zip Code</option>');
            if (cid !== '') {
                $.ajax({
                    type: 'GET',
                    url: "{{route('getZipByCity')}}?city_id=" + cid,
                    dataType: 'json',
                    success: function (data) {
                        for (var i = 0; i < data.length; i++) {
                            toAppend += '<option value=' + data[i]['id'] + '>' + data[i]['zipcode'] +
                                '</option>';
                        }
                        $('#postcode').append(toAppend);
                        toAppend = '';
                    }
                });
            } else {
                alert("please choose City");
            }
    }
</script>
 <script src="{{ URL::asset('public/admin/plugins/select2/js/select2.min.js') }}"></script>
    <script>
        $(document).ready(function () {            
            $('#state').select2();
            $('#city').select2();
            $('#country').select2();
            $('#packege').select2();
            $('#postcode').select2();
            $('#roleid').select2();
        });
    </script>
@stop