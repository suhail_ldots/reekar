<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">

        <title>Doctors Loop- Patient access to hope</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="Doctors Loop" name="author" />
        @include('admin.layouts.head')
        <style type="text/css">
           .help-block {color: red;}
        
        </style>
    </head>
<body>
    <div id="wrapper">
        @include('admin.layouts.header')
        @include('admin.layouts.sidebar')
        <div class="content-page">  
            <div class="content">
                <div class="container-fluid">
                  {{-- @include('admin.layouts.settings')--}}
                   @yield('content')
                </div> 
            </div> 
        </div> 
        {{--@include('admin.layouts.footer')--}}  
        
    </div>
        @include('admin.layouts.footer-script')   
        @stack('appendJs')  
        @stack('javascript')  
       
    </body>

</html>
