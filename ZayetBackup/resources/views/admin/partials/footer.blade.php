<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; {{ date('Y') }} Positiive Demo.</strong> All rights
    reserved.
</footer>