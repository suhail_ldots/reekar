@extends('admin.layouts.master')
<?php
    $jsoncurrency = currencyConverter();
?>
@section('content')
<div class="page-title-box">
    {{-- <div class="row align-items-center">
        <div class="col-sm-6">
            <h4 class="page-title"> &nbsp</h4>
            <!-- <h4 class="page-title"> {{ __('l.members') }}</h4> -->
        </div>
        <div class="col-sm-6">
            <div class="float-right d-md-block">
                 
                <a href="{{ route('account.create')}}">
                    <button class="btn btn-primary arrow-none waves-effect waves-light" type="button">
                        <i class="fas fa-plus"></i> @lang('l.add') @lang('l.order')
                    </button>
                </a>
                 
            </div>
        </div>
    </div> --}}
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mt-0 header-title m-b-30">{{ __('l.account') }} {{ __('l.list') }}</h4>
                @include('admin.partials.messages')
                <div class="table-responsive table-full-width">
                    <div style="overflow: auto;"> 
                    @php
                        $ownerShare = 0;
                        $reekar_cleared_share = 0;
                        $reekar_not_cleared_share = 0;
                        $ReekarTotalShare = 0;
                        
                        //Note-> order_status 1 pending 2 succ. 3 failed 4 canceled
                        foreach($items as $item){

                            if($item->order_status == 2){  

                               // $ownerShare = $ownerShare + $item->paid_amount * 80/100;
                                $ownerShare = $ownerShare +getCurrencyFromUser('USD', 'USD', $item->currency, $item->paid_amount * (80/100), $jsoncurrency);

                                //$ReekarTotalShare = $ReekarTotalShare + $item->paid_amount * 20/100;
                                $ReekarTotalShare = $ReekarTotalShare + getCurrencyFromUser('USD', 'USD', $item->currency, $item->paid_amount * (20/100), $jsoncurrency);

                                if($item->paid_to_owner_status == 1){
                                    //$reekar_cleared_share = $reekar_cleared_share + $item->paid_amount * 20/100;
                                    $reekar_cleared_share = $reekar_cleared_share + getCurrencyFromUser('USD', 'USD', $item->currency, $item->paid_amount * (20/100), $jsoncurrency);
                                }
                                elseif($item->paid_to_owner_status == 0){
                                    //$reekar_not_cleared_share = $reekar_not_cleared_share + $item->paid_amount * 20/100;
                                    $reekar_not_cleared_share = $reekar_not_cleared_share + getCurrencyFromUser('USD', 'USD', $item->currency, $item->paid_amount * (20/100), $jsoncurrency);
                                }

                            }
                            
                        }
                    
                    @endphp                                            
                        <div class="row"> 
                            <div class="col-md-12">
                                <div class="col-md-6" style="float:left;">
                                    <div class="form-group">
                                        <label for="">Reekar Share</label><br>
                                        <span>Cleared Share :<b> {{ '$'. $reekar_cleared_share }} </b></span>&nbsp&nbsp
                                        <span>None-Cleared Share :<b> {{ '$'. $reekar_not_cleared_share }} </b></span><br>
                                        <span>Total Amount :<b> {{ '$'. $ReekarTotalShare }} </b></span>                                        
                                    </div>
                                </div>

                                <div class="col-md-4" style="float:left;">
                                    <div class="form-group">
                                        <label for="">Owner Share</label><br>
                                        <span>Total Amount :<b> {{'$'. $ownerShare }}</b></span>
                                        
                                    </div>
                                </div>
                                
                            </div> 
                        </div>
                    
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>@lang('l.#')</th>
                                    <th>Booking Id</th>
                                    <th>Paid By</th>
                                    <th>Total Amount</th>
                                    <th>Owner Share</th>
                                    <th>Reekar Share</th>                                    
                                    <th>Action</th>
                                    
                                </tr>
                            </thead>
                            <tbody id="tbody">
                                @include('admin.modules.account.tbody')
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
@if(count($items) > 0)
     
    @include('lazyloading.loading')
    
@endif
<script>
    function pay_confirm(event, msg){
        var ok = confirm(msg);
        if(!ok){
            event.preventDefault();
            //return false;
        }
    }
</script>
@endsection

