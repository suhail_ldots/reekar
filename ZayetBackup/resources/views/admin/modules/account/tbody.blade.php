@if(!empty($items))
<?php
    $jsoncurrency = currencyConverter();
?>
@forelse($items as $item)
    @php 
        if($item->currency == 'USD'){
            $icon =  '$ ' ;
        }elseif($item->currency == 'EURO'){
            $icon = '&euro; '; 
        }else{
            $icon = $item->currency.' '; 
        } 
    @endphp   
<tr>
    <td>{{(($items->currentPage() * $items->perPage()) + $loop->iteration) - $items->perPage()}}</td>
    {{--<td ><a href="{{ route('account.show',  $item->id)}}" class="link" title="See Details">{{$item->full_name}}</a></td>--}}
    <td>{{ isset($item->booking_id) ? $item->booking_id : ''}}</td>
    <td>{{ isset($item->user->first_name) ? $item->user->first_name : ''}} {{isset($item->user->last_name) ? $item->user->last_name :''}}</td>
    <!-- <td>{!! isset($item->paid_amount) ? $icon.$item->paid_amount :'' !!}</td> -->
    <td>{!! isset($item->paid_amount) ? '$'.getCurrencyFromUser('USD', 'USD', $item->currency, $item->paid_amount, $jsoncurrency) :"" !!}</td>
    <!-- <td>{!! isset($item->paid_amount) ? $icon.($item->paid_amount *( 80/100)) :'' !!}</td> -->
    <td>{!! isset($item->paid_amount) ? '$'.getCurrencyFromUser('USD', 'USD', $item->currency, $item->paid_amount * ( 80/100), $jsoncurrency) :"" !!}</td>
    <!-- <td>{!! isset($item->paid_amount) ? $icon.($item->paid_amount *( 20/100)) :'' !!}</td> -->
    <td>{!! isset($item->paid_amount) ? '$'.getCurrencyFromUser('USD', 'USD', $item->currency, $item->paid_amount *( 20/100), $jsoncurrency) :"" !!}</td>
 
    <td> 
        @if($item->paid_to_owner_status == 1)
        <a href="{{ route('account.owner_payment_status',[ 'id' => $item->id,'owner_payment' => $item->paid_to_owner_status])}}"
            class="btn btn-default badge-success badge mr-2" title="Cleared" onclick="pay_confirm(event, 'Are you sure to reject payment?')">Cleared</a>
        @elseif($item->paid_to_owner_status == 0)
        <a href="{{ route('account.owner_payment_status',[ 'id' => $item->id,'owner_payment' => $item->paid_to_owner_status])}}"
            class="btn btn-default badge-danger badge mr-2" title="Not Cleared" onclick="pay_confirm(event,'Are you sure to pay?')">Not Cleared</a>
        @endif
        <div class="btn-group" role="group" aria-label="...">            
            <a href="{{ route('order.show',$item->id)}}" class="btn btn-default link" title="See Details">
            <i class="fas fa-eye"></i></a>
        </div> 
    </td>
    <!-- <td><?php /* if($item->paid_to_owner_status == 1) { echo '<span class="badge-warning badge mr-2">Pending</span>'; }
      elseif($item->order_status == 2)
     { echo '<span class="badge-success badge mr-2">Success</span>'; }
     else{echo '<span class="badge-danger badge mr-2">Failed</span>';} */ ?>
    </td> -->  
    <!-- <td>{{ isset($item->updated_at) ? date('d/M/Y h:i A', strtotime($item->updated_at)) : '' }}</td> -->
    <!-- <td>
        <div class="btn-group" role="group" aria-label="...">            
            <a href="{{ route('order.show',$item->id)}}" class="btn btn-default link" title="See Details">
            <i class="fas fa-eye"></i></a>
        </div>
    </td> -->
</tr>
@empty
<tr>
    @if($items->currentPage() == $items->lastPage())
    <td colspan="14" align="center"> {{ __('l.transact_not_found') }}</td>
    @else
        <td colspan="14" align="center"> {{ __('l.no_more_record') }}</td>
    @endif
</tr>
@endforelse
@else
<tr>
    <td colspan="14" align="center"> {{ __('l.transact_not_found') }}</td>
</tr>
@endif