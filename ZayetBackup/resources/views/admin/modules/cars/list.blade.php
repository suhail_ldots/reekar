@extends('admin.layouts.master')
@section('content')
<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <h4 class="page-title"> &nbsp</h4>
            <!-- <h4 class="page-title"> {{ __('l.members') }}</h4> -->
        </div>
        <div class="col-sm-6">
            <div class="float-right d-md-block">
                 
                <a href="{{ route('car.create')}}">
                    <button class="btn btn-primary arrow-none waves-effect waves-light" type="button">
                        <i class="fas fa-plus"></i> @lang('l.add') @lang('l.car')
                    </button>
                </a>
                 
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mt-0 header-title m-b-30">{{ __('l.car') }} {{ __('l.list') }}</h4>
                @include('admin.partials.messages')
                <div class="table-responsive table-full-width">
                    <div style="overflow: auto;">
                        <form action="{{ route('car.search')}}" method="GET">
                            <div class="row"> 
                                <div class="col-md-12">
                                    <div class="col-md-3" style="float:left;">
                                        <div class="form-group">
                                            <input type="text" name="owner_name" maxlength="250" class="form-control" placeholder="Owner First Name"
                                                @if(isset($name)) value="{{ $name }}" @endif autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="float:left;">
                                        <div class="form-group">
                                            <input type="text" name="brand" maxlength="250" class="form-control" placeholder="Brand"
                                                @if(isset($brand)) value="{{ $brand }}" @endif autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="float:left;">
                                        <div class="form-group">
                                            <input type="text" name="model" maxlength="250" class="form-control" placeholder="Model"
                                                @if(isset($model)) value="{{ $model }}" @endif autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="float:left;">
                                        <div class="form-group">
                                            <input type="text" name="year" maxlength="250" class="form-control" placeholder="Model Year"
                                                @if(isset($year)) value="{{ $year }}" @endif autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="float:left;">
                                        <div class="form-group"><br>
                                            <input type="text" name="color" maxlength="250" class="form-control" placeholder="Color"
                                                @if(isset($color)) value="{{ $color }}" @endif autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="float:left;">
                                        <div class="form-group">
                                        <small class="form-text text-muted mt-0" style="color: #9ca8b3 !important;  font-size: 13px;">Available from :</small>
                                            <input type="date" name="available_from_date"  class="form-control" placeholder="Available From Date"
                                                @if(isset($available_from_date)) value="{{ $available_from_date }}" @endif autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="float:left;">
                                        <div class="form-group">
                                        <small class="form-text text-muted mt-0" style="color: #9ca8b3 !important;  font-size: 13px;">Available to :</small>
                                            <input type="date" name="available_to_date" class="form-control" placeholder="Available To Date"
                                                @if(isset($available_to_date)) value="{{ $available_to_date }}" @endif autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="float:left;">
                                        <div class="form-group"><br>
                                            <button class="btn btn-primary dropdown-toggle arrow-none waves-effect waves-light"
                                                type="submit"><i class="fa fa-search"></i> Search</button>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </form>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>@lang('l.#')</th>
                                    <th>Owner @lang('l.name')</th>
                                    <th>@lang('l.brand') @lang('l.name')</th>
                                    <th>@lang('l.model') @lang('l.name')</th>
                                    <th>@lang('l.color')</th>
                                    <th>@lang('l.model') Year</th>
                                    <th>Available From Date </th>
                                    <th>Available From Time </th>
                                    <th>Available To Date </th>
                                    <th>Available To Time </th>
                                    <th>Price/day</th>
                                   <!--  <th>Total Price</th> -->
                                    <th>Status</th>
                                    <th>Action</th>                                    
                                </tr>
                            </thead>
                            <tbody id="tbody">
                                @include('admin.modules.cars.tbody')
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="img-modal" tabindex="-1" role="dialog" aria-labelledby="edit-modal-label"
    aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0">Change Car Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body" style="height: 100%;">
                <div class="alert" id="message" style="display: none"></div>
                <div class="alert ajaxmsg" style="display: none"></div>
                <form method="post" action="{{route('car.listingStatus')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <div class="row">
                            <div class="col-12 col-md-8 ">
                           
                                <input type="radio" name="listStatus" class="approve" value="approve"> Approve                             
                                <br><input type="radio" name="listStatus" class="reject" value="reject"> Reject                              
                                <input type="hidden" name="carid" id="car_id">  
                            </div>
                        </div><br>
                        <div class="row reason" id="" style="display:none;">
                            <div class="col-12 col-md-8 ">
                            <label for="reason">Please Enter Reason for Reject</label>
                            <textarea name="reason" cols="60" rows="7"></textarea>
                            </div>
                        </div>
                        <!-- <div class="text-center py-2 white">
                            <input  class="btn btn-primary waves-eff" value="Upload">
                        </div> -->

                    </div>
                </div>        
            <br/> 
            <div class="modal-footer"><button type="submit" name="upload" id="upload" class="btn btn-primary">Save</button> <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button></div>
        </form>               
        </div>
        
    </div>
    
</div>
@endsection

@section('script')
<script>
function change_status(id){       
    var inputF = document.getElementById("car_id");   
    inputF.setAttribute('value', id); 
    $('#img-modal').modal('show');
}
</script>
<script>
    $(function () {
        $(".reject").change(function () {             
            if ($(this.checked)) {
                $(".reason").show();
            } else {
                $(".reason").hide();
            }
        });

        $(".approve").change(function () {
           if ($(this.checked)) {
                $(".reason").hide();
            }  
        });
    });
    
</script>
@if(count($items) > 0)
    
    @include('lazyloading.loading')
    
@endif
@endsection


