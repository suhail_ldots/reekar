@extends('admin.layouts.master')
@section('css')
<style>
    iframe{ width:100%; height:90vh}
</style>
@endsection
@section('content')
<div class="row">
    <div class="col col-md-12">
        <iframe src="{{ URL::to('/translations') }}" frameborder="0"></iframe>

    </div>
</div>
@endsection