@php use App\User; @endphp
@extends('admin.layouts.master')
@section('css')
<style>
    .required {
        color: red;
    }
</style>
<link href="{{ URL::asset('assets/admin/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{ URL::asset('assets/admin/plugins/summernote/summernote-bs4.css') }}">
@endsection
@section('content')
<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <h4 class="page-title"> Import</h4>
        </div>
        <div class="col-sm-6">
            <div class="float-right d-none d-md-block">
                <a href="{{ url()->previous() }}">
                    <button class="btn btn-primary arrow-none waves-effect waves-light" type="button">
                        <i class="fas fa-arrow-left"></i> {{ __('l.back') }}
                    </button>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mt-0 header-title m-b-30">@lang('l.upload') @lang('l.brands') with @lang('l.models') </h4>
                @include('admin.partials.messages')
                <div class="account-card-content">
                    <form class="form-horizontal m-t-30" enctype="multipart/form-data" action="{{ url()->current() }}" method="post" autocomplete="off">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>File</label>
                                    <small id="imageHelp" class="form-text text-muted">(Max Upload 25MB, Only xls, xlsx is allowed.)</small>
                                    
                                    <input type="file" id="inputGroupFile01" name="file"
                                        accept="application/vnd.ms-excel" class="imgInp filestyle custom-file-input"
                                        aria-describedby="inputGroupFileAddon01" data-buttonname="btn-secondary">
                                    @isset($errors)
                                    <div class="help-block">{{ $errors->first('file') }}</div>
                                    @endisset
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-12">
                                <div class="p-20">
                                    <button type="submit"
                                        class="btn btn-primary waves-effect waves-light saveBtn">@lang('l.save')</button>
                                   
                                    <div id="ajaxloader" style="display: none;"><img
                                            src="{{ asset('assets/admin/images/ajax-loader.gif')}}" /> Processing...
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    @endsection
    @section('script')

    <script src="{{ URL::asset('public/admin/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/pages/form-editors.int.js') }}"></script>

    <script src="{{ URL::asset('public/admin/plugins/select2/js/select2.min.js') }}"></script>

    @stop