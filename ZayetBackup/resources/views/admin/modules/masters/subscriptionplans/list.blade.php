@extends('admin.layouts.master')
@section('content')
{{--@if(canWrite(2))--}}
<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <h4 class="page-title">&nbsp</h4>
            <!-- <h4 class="page-title"> {{ __('l.plans') }}</h4> -->
        </div>
        <div class="col-sm-6">
            <div class="float-right d-md-block">
                <a href="{{ route('plans.create')}}">
                    <button class="btn btn-primary arrow-none waves-effect waves-light" type="button">
                        <i class="fas fa-plus"></i> {{ __('l.add') }} Plan
                    </button>
                </a>
            </div>
        </div>
    </div>
</div>
{{--@endif--}}
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mt-0 header-title">Plan List</h4>
                <!-- <small class="form-text text-muted  m-b-30" style="color: #9ca8b3 !important;  font-size: 15px;">(Add a plan for an Organization.)</small> -->
                @include('admin.partials.messages')
                <div class="table-responsive table-full-width">
                    <div style="overflow: auto;">
                        <form action="{{route('admin.plan_search')}}" method="GET">                        
                        <!-- <div class="row"> -->
                            <div class="col-md-12">
                                <div class="col-md-3" style="float:left;">
                                    <div class="form-group">
                                        <input type="text" name="name" maxlength="250" class="form-control" placeholder="Name"
                                            @if(isset($name)) value="{{ $name }}" @endif autocomplete="off">
                                    </div>
                                </div>                                
                            </div>
                            
                            <div class="col-md-2" style="float:left;">
                                <div class="form-group">
                                    <button class="btn btn-primary dropdown-toggle arrow-none waves-effect waves-light"
                                        type="submit"><i class="fa fa-search"></i> Search</button>
                                </div>
                            </div>
                        </form>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>{{ __('l.#') }}</th>
                                    <th>{{ __('l.name') }}</th>
                                    <th>{{ __('l.time_duration') }}</th>
                                    <th>{{ __('l.price') }}</th>
                                    <th>{{ __('l.status') }}</th>
                                    
                                    <th>{{ __('l.actions') }}</th>
                                    
                                </tr>
                            </thead>
                            <tbody id="tbody">
                                @include('admin.modules.masters.subscriptionplans.tbody')
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
@include('lazyloading.loading')
@endsection