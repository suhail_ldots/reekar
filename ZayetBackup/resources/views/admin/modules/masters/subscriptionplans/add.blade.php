@extends('admin.layouts.master')
@section('css')
<style>
    .myrequired {
        color: red;
    }
</style>
@endsection

@section('content')
<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <h4 class="page-title"> &nbsp</h4>
            <!-- <h4 class="page-title"> {{ __('l.plans') }}</h4> -->
        </div>
        <div class="col-sm-6">
            <div class="float-right d-md-block">
                <a href="{{ route('plans.index')}}">
                    <button class="btn btn-primary arrow-none waves-effect waves-light" type="button">
                        <i class="fas fa-arrow-left"></i> @lang('l.back')
                    </button>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">

                @if($item)
                <h4 class="mt-0 header-title m-b-30">Edit Plan</h4>
                @else
                <h4 class="mt-0 header-title m-b-30">Add Plan</h4>
                @endif
                @include('admin.partials.messages')
                <form action="{{route('plans.store')}}" method="POST" onsubmit="return saveData(this)">
                @csrf
                    @if($item)
                    <input type="hidden" name="id" id="id" value="{{ $item->id }}">
                    @endif
                    <div class="row">
                        <div class="col-md-6">
                            <div class="p-20">
                                <div class="form-group">
                                    <label>Plan Name</label><span class="myrequired"> *</span>
                                    <input value="{{$item ? $item->name : ''}}" type="text" name="name" maxlength="250" id="name"
                                        class="form-control" placeholder="plan name">
                                    <div class="help-block"></div>
                                </div>
                            </div>

                            <div class="p-20">
                                <div class="form-group">
                                    <label>{{ __('l.time_duration') }}</label><span class="myrequired"> *</span>
                                    <input value="{{$item ? $item->time_duration : ''}}" type="text"
                                        name="time_duration" id="time_duration" class="form-control"
                                        placeholder="In days">
                                    <div class="help-block"></div>
                                </div>
                            </div>

                            <div class="p-20">
                                <div class="form-group">
                                    <label>{{ __('l.price') }}</label><span class="myrequired"> *</span>
                                    <input value="{{$item ? $item->price : ''}}" type="text" name="price" id="price"
                                        class="form-control" placeholder="{{ __('l.price') }}">
                                    <div class="help-block"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>{{ __('l.status') }}</label><span class="myrequired"> *</span>
                                <select name="status" id="status" class="form-control dynamic"
                                    data-dependent="state_id">
                                    <?php $statuses = ['Inactive','Active']; ?>
                                    @foreach($statuses as $key => $value)
                                    <option value="{{$key}}" {{$item && $item->status == $key ? 'selected' : ''}}>
                                        {{$value}}</option>
                                    @endforeach
                                </select>
                                <div class="help-block"></div>
                            </div>
                        </div>
                       </div> 
                       <div class="row">
                        <div class="col-md-6">
                            <h5>Assign feature to this Plan</h5>
                            @foreach($features as $feature)
                            <input type="checkbox" name="feature_id[]" value="{{ $feature->id }}" id="" multiple>&nbsp {{ $feature->count }} {{ $feature->name }} <br><br>
                            @endforeach
                            <br>

                        <!-- </div>
                        <div class="col-md-6"> -->
                            <div class="p-20">
                                <button type="submit"
                                    class="btn btn-primary waves-effect waves-light saveBtn">{{ __('l.save') }}</button>
                                <a href="{{ route('plans.index') }}">
                                    <button type="button"
                                        class="btn btn-secondary waves-effect m-l-5">{{ __('l.cancel') }}</button>
                                </a>
                                <div id="ajaxloader" style="display: none;"><img
                                        src="{{ asset('public/admin/images/ajax-loader.gif')}}" />{{ __('l.processing') }}
                                </div>

                            </div>
                        </div>
                    </div>
                        
                    <!-- </div> -->
                    
                    <div class="clearfix"></div>
            </div>


        </div>
        </form>
    </div>
</div>
@endsection
@push('appendJs')
<script type="text/javascript" src="{{ asset('public/admin/js/post-jobs.js')}}"></script>
@endpush