@extends('admin.layouts.master')
@push('styles')
@endpush

@section('content')
<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <h4 class="page-title">&nbsp</h4>
        </div>
        <div class="col-sm-6">
            <div class="float-right d-md-block">
                <a href="{{ route('car.index')}}">
                    <button class="btn btn-primary arrow-none waves-effect waves-light" type="button">
                        <i class="fas fa-arrow-left"></i> @lang('l.back')
                    </button>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                @if($item)
                <h4 class="mt-0 header-title m-b-30">Edit Car</h4>
                @else
                <h4 class="mt-0 header-title m-b-30">Add Car</h4>
                @endif
                @include('admin.partials.messages')
                <form action="{{route('car.store')}}" method="POST" onsubmit="return saveData(this)">

                    @if($item)
                    <input type="hidden" name="id" id="id" value="{{ $item->id }}">
                    @endif
                    <div class="row">
                        <div class="col-md-6">
                            <div class="p-20">
                                <div class="form-group">
                                    <label>{{__('l.car')}} {{ __('l.name') }}</label>
                                    <input value="{{$item ? $item->name : ''}}" type="text" name="name" maxlength="250" id="inputEmail"
                                        class="form-control" placeholder="{{ __('l.car') }} {{ __('l.name') }}">
                                    <div class="help-block"></div>
                                </div>
                                <div class="form-group">
                                    <label>{{ __('l.brand') }}</label>
                                    <select name="brand_id" class="form-control">
                                    <option value="">Select Brand</option>
                                        @foreach(getBrands() as $brand)
                                        <option value="{{$brand->id}}"
                                            {{$item && $item->brand_id == $brand->id ? 'selected' : ''}}>
                                            {{$brand->name}}</option>
                                        @endforeach
                                    </select>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="p-20">
                                <button type="submit"
                                    class="btn btn-primary waves-effect waves-light saveBtn">{{ __('l.save') }}</button>
                                <a href="{{ route('car.index') }}">
                                    <button type="button"
                                        class="btn btn-secondary waves-effect m-l-5">{{ __('l.cancel') }}</button>
                                </a>
                                <div id="ajaxloader" style="display: none;"><img
                                        src="{{ asset('public/admin/images/ajax-loader.gif')}}" />{{ __('l.processing') }}
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
            </div>


        </div>
        </form>
    </div>
</div>

@endsection
@push('appendJs')
<script type="text/javascript" src="{{ asset('public/admin/js/post-jobs.js')}}"></script>
@endpush