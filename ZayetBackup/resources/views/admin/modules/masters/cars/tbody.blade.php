@forelse($items as $item)
<tr>
<td>{{(($items->currentPage() * $items->perPage()) + $loop->iteration) - $items->perPage()}}</td>
    <td>{{$item->name}}</td>   
    <td>{{$item->brand->name}}</td>   
    <td>
        <div class="btn-group" role="group" aria-label="...">            
            <a href="{{ route('car.edit',$item->id)}}" class="btn btn-default"><i
                    class="far fa-edit"></i></a> 
            <a href="{{ route('car.delete',$item->id)}}" class="btn btn-default"><i class="far fa-trash-alt"></i></a> 
            @if($item->status == 1)
            <a href="{{ route('car.status',[ 'id' => $item->id,'status' => $item->status])}}"
                class="btn btn-default" title="Active"><i class="fas fa-check text-success"></i></a>
            @else
            <a href="{{ route('car.status',[ 'id' => $item->id,'status' => $item->status])}}"
                class="btn btn-default" title="Inactive"><i class="fas fa-times text-danger"></i></a>
            @endif
        
        </div>
    </td>
</tr>
@empty
<tr>
    @if($items->currentPage() == $items->lastPage())
        <td colspan="11" align="center">{{ __('l.car_not_found') }}</td>
    @else
        <td colspan="10" align="center"> {{ __('l.no_more_record') }}</td>
    @endif
</tr>
@endforelse