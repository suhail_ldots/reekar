@extends('admin.layouts.master')
@push('styles')

@endpush
@section('content')
<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <h4 class="page-title"> &nbsp</h4>
            <!--  <h4 class="page-title"> {{ __('l.models') }}</h4> -->
        </div>
        <div class="col-sm-6">
            <div class="float-right d-none d-md-block">
                <a href="{{ route('countries.index')}}">
                    <button class="btn btn-primary arrow-none waves-effect waves-light" type="button">
                     <i class="fas fa-arrow-left"></i> @lang('Back')
                    </button>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mt-0 header-title m-b-30">@if($item) @lang('Edit') @else @lang('Add') @endif
                    @lang('Country')</h4>
                @include('admin.partials.messages')
                <form action="{{route('countries.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                    @if($item)
                    <input type="hidden" name="id" id="id" value="{{ $item->id }}">
                    @endif
                    <div class="row">
                        <div class="col-md-6">
                            <div class="p-20">                            
                            <div class="form-group">
                                    <label>Enter Country Name:</label>
                                    <input value="{{$item ? $item->name : old('name')}}" type="text" name="name" maxlength="250" id="inputname"
                                        class="form-control" placeholder="Enter Country Name">
                                        @if($errors->has('name'))
                                            <div class="help-block required">{{ $errors->first('name') }}</div>
                                        @endif
                            </div>
                            </div>
                        </div>
                    </div>                        
                    <div class="row">
                        <div class="col-md-12">
                            <div class="p-20">
                                <button type="submit"
                                    class="btn btn-primary waves-effect waves-light saveBtn">{{ __('save') }}</button>
                                <a href="{{ route('countries.index') }}">
                                    <button type="button"
                                        class="btn btn-secondary waves-effect m-l-5">{{ __('cancel') }}</button>
                                </a>
                                <div id="ajaxloader" style="display: none;"><img
                                        src="{{ asset('public/admin/images/ajax-loader.gif')}}" />{{ __('processing') }}
                                </div>
                            </div>
                        </div>
                    </div>
                        
                   
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
@push('appendJs')
<script src="{{ URL::asset('public/admin/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/admin/js/post-jobs.js')}}"></script>
@endpush