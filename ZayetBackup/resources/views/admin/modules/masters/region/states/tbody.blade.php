@forelse($items as $item)
<tr>
<td>{{(($items->currentPage() * $items->perPage()) + $loop->iteration) - $items->perPage()}}</td>
    <td>{{ $item->name}}</td>
    <td>@if(isset($item->country->name)){{ $item->country->name}} @endif</td>
    @if(canUpdate(2) || canDelete(2))
    <td>
        <div class="btn-group" role="group" aria-label="...">
            @if(canUpdate(2))
            <a href="{{ route('states.edit',$item->id)}}" class="btn btn-default"><i
                    class="far fa-edit"></i></a>
            @endif
            <!-- @if(canDelete(2))
            <a href="{{ route('states.delete',$item->id)}}" class="btn btn-default"><i class="far fa-trash-alt"></i></a>
            @endif -->
        </div>
    </td>
    @endif
</tr>

@empty
<tr>
    @if($items->currentPage() == $items->lastPage())
        <td colspan="11" align="center">{{ __('l.state_not_found') }}</td>
    @else
        <td colspan="10" align="center"> {{ __('l.no_more_record') }}</td>
    @endif
</tr>
@endforelse