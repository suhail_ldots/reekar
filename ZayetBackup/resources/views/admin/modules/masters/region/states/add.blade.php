@extends('admin.layouts.master')
@push('styles')
@endpush

@section('content')
<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <h4 class="page-title"> &nbsp</h4>
            <!-- <h4 class="page-title"> {{ __('l.states') }}</h4> -->
        </div>
        <div class="col-sm-6">
            <div class="float-right d-md-block">
                <a href="{{ route('states.index')}}">
                    <button class="btn btn-primary arrow-none waves-effect waves-light" type="button">
                        <i class="fas fa-arrow-left"></i> @lang('l.back')
                    </button>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                @if($item)
                <h4 class="mt-0 header-title m-b-30">{{ __('l.edit_state') }}</h4>
                @else
                <h4 class="mt-0 header-title m-b-30">{{ __('l.add_state') }}</h4>
                @endif
                @include('admin.partials.messages')
                <form action="{{route('states.store')}}" method="POST" onsubmit="return saveData(this)">

                    @if($item)
                    <input type="hidden" name="id" id="id" value="{{ $item->id }}">
                    @endif
                    <div class="row">
                        <div class="col-md-6">
                            <div class="p-20">
                                <div class="form-group">
                                    <label>{{ __('l.country') }}</label>
                                    <select name="country_id" class="form-control">

                                        @foreach($countries as $country)
                                        <option value="{{$country->id}}"
                                            {{$item && $item->country_id == $country->id ? 'selected' : ''}}>
                                            {{$country->name}}</option>
                                        @endforeach
                                    </select>
                                    <div class="help-block"></div>
                                </div>
                                <div class="form-group">
                                    <label>{{ __('l.state_name') }}</label>
                                    <input value="{{$item ? $item->name : ''}}" type="text" name="name" maxlength="250" id="inputEmail"
                                        class="form-control" placeholder="{{ __('l.state_name') }}">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="p-20">
                                <button type="submit"
                                    class="btn btn-primary waves-effect waves-light saveBtn">{{ __('l.save') }}</button>
                                <a href="{{ route('states.index') }}">
                                    <button type="button"
                                        class="btn btn-secondary waves-effect m-l-5">{{ __('l.cancel') }}</button>
                                </a>
                                <div id="ajaxloader" style="display: none;"><img
                                        src="{{ asset('assets/images/ajax-loader.gif')}}" />{{ __('l.processing') }}
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
            </div>


        </div>
        </form>
    </div>
</div>

@endsection
@push('appendJs')
<script type="text/javascript" src="{{ asset('public/admin/js/post-jobs.js')}}"></script>
@endpush