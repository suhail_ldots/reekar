@if(!empty($items))
@forelse($items as $item) 
<tr>
    <td>{{(($items->currentPage() * $items->perPage()) + $loop->iteration) - $items->perPage()}}</td>
    <td ><a href="{{ route('patient.show',  $item->id)}}" class="link" title="See Details">{{$item->full_name}}</a></td>
    <td>{{$item->email}}</td>
    <td>{{$item->country_code}} {{$item->mobile}}</td> 
    <td><?php if($item->status == 1) { echo '<span class="badge-success badge mr-2">Active</span>'; } else{ echo '<span class="badge-danger badge mr-2">Inactive</span>'; } ?>
    </td> 
     
    <td>
        <div class="btn-group" role="group" aria-label="...">
            @if($item->id != Auth::user()->id)
             
            <a href="{{ route('patient.edit',$item->id)}}" class="btn btn-default" title="Edit"><i
                    class="far fa-edit"></i></a>
             
            @if($item->status == 1)
            <a href="{{ route('patient.status',[ 'id' => $item->id,'status' => $item->status])}}"
                class="btn btn-default" title="Active"><i class="fas fa-check text-success"></i></a>
            @else
            <a href="{{ route('patient.status',[ 'id' => $item->id,'status' => $item->status])}}"
                class="btn btn-default" title="Inactive"><i class="fas fa-times text-danger"></i></a>
            @endif 
            <a href="{{ route('patient.delete',$item->id)}}" class="btn btn-default" title="Delete"><i
                    class="far fa-trash-alt"></i></a>
           
            @endif
        </div>
    </td>
    
</tr>
@empty
<tr>
    @if($items->currentPage() == $items->lastPage())
    <td colspan="11" align="center">{{ __('l.patient') }} {{ __('l.not_found') }}</td>
    @else
        <td colspan="10" align="center"> {{ __('l.no_more_record') }}</td>
    @endif
</tr>
@endforelse
@else
<tr>
    <td colspan="11" align="center"> {{ __('l.patient') }} {{ __('l.not_found') }}</td>
</tr>
@endif