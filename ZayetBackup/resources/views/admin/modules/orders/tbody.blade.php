@if(!empty($items))
@php $jsoncurrency = currencyConverter(); @endphp
@forelse($items as $item)
    @php 
    if($item->currency == 'USD'){
            $icon =  '$ ' ;
        }elseif($item->currency == 'EURO'){
            $icon = '&euro; '; 
        }else{
            $icon = $item->currency.' '; 
        } 
    @endphp  
<tr>
    <td>{{(($items->currentPage() * $items->perPage()) + $loop->iteration) - $items->perPage()}}</td>
    {{--<td ><a href="{{ route('order.show',  $item->id)}}" class="link" title="See Details">{{$item->full_name}}</a></td>--}}
    <td>{{isset($item->user->first_name) ? $item->user->first_name : ''}} {{isset($item->user->last_name) ? $item->user->last_name :''}}</td>
    <td>{{ isset($item->car->brand->name ) ? $item->car->brand->name :''}}
      {{isset($item->car->model->name) ? $item->car->model->name : ''}}</td>
     <td>{{$item->booking_id}}</td>
     <td>{!! isset($item->paid_amount) ? '$'.getCurrencyFromUser('USD', 'USD', $item->currency, $item->paid_amount, $jsoncurrency) :"" !!}</td>
     <!-- <td>{!! isset($item->paid_amount) ? $icon.$item->paid_amount :'' !!}</td> -->
     <td><?php if($item->order_status == 1) { echo '<span class="badge-warning badge mr-2">Pending</span>'; }
      elseif($item->order_status == 2)
     { echo '<span class="badge-success badge mr-2">Success</span>'; }
     elseif($item->order_status == 3){echo '<span class="badge-danger badge mr-2">Failed</span>';} 
     elseif($item->order_status == 4){echo '<span class="badge-info badge mr-2">Canceled</span>';}
     ?>
    </td> 
    <td>{{ isset($item->updated_at) ? getCurrentTimezoneDateTime($item->updated_at) : '' }}</td>
    <td>
        @if($item->order_status != 4)
        <div class="btn-group" role="group" aria-label="...">            
            <a href="{{ route('order.status',[$item->id, $item->order_status])}}" class="btn btn-default" title="Cancel Order">
            <i class="fa fa-times text-danger"></i></a>
        </div>
        @endif
        <div class="btn-group" role="group" aria-label="...">            
            <a href="{{ route('order.show',$item->id)}}" class="btn btn-default link" title="See Details">
            <i class="fas fa-eye"></i></a>
        </div>
    </td>
</tr>
@empty
<tr>
    @if($items->currentPage() == $items->lastPage())
    <td colspan="14" align="center"> {{ __('l.order_not_found') }}</td>
    @else
        <td colspan="14" align="center"> {{ __('l.no_more_record') }}</td>
    @endif
</tr>
@endforelse
@else
<tr>
    <td colspan="14" align="center"> {{ __('l.order_not_found') }}</td>
</tr>
@endif