@extends('admin.layouts.master')
@section('css')
<style>
    .required {
        color: red;
    }

    .pad_right {
        padding-right: 0px !important;
    }

    .padd_rl {
        padding-left: 0px !important;
        padding-right: 0px !important;
    }

    @media screen and (max-width: 992px) and (min-width: 768px) {
        .padd_rl {
            font-size: 11px !important;
        }
    }

    @media(max-width:767px) {
        .pad_right {
            padding-right: 15px !important;
        }
    }
</style>

<link href="{{ URL::asset('public/admin/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('content')

<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <h4 class="page-title"> &nbsp</h4>
            <!-- <h4 class="page-title">@lang('l.users')</h4> -->
        </div>         
        <div class="col-sm-6">
            <div class="float-right d-md-block">
                @if($item)
                <a href="{{ route('car.show', $item->id)}}">
                    <button class="btn btn-primary arrow-none waves-effect waves-light" type="button">
                        <i class="fas fa-eye"></i> @lang("l.car") Details
                    </button>
                </a>
                @endif
                <a href="{{ route('car.index')}}">
                    <button class="btn btn-primary arrow-none waves-effect waves-light" type="button">
                        <i class="fas fa-arrow-left"></i> @lang('l.back')
                    </button>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">            
                <h4 class="mb-0 mt-0 header-title"> @if($item){{ __('l.edit') }} @else @lang('l.add') @endif @lang('l.car') </h4>
                <small class="form-text text-muted mt-0" style="color: #9ca8b3 !important;  font-size: 15px;">@if(!$item) (New Car)@endif</small>
                @include('admin.partials.messages')
                <form action="{{route('contents.store')}}" method="POST" id="upload_form5675" autocomplete="off" enctype="multipart/form-data">
                <!-- <form action="{{route('car.store')}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                    @csrf -->
                    @csrf
                     
                    @if($item)
                    <input type="hidden" name="id" id="id" value="{{ $item->id }}">
                    @endif
                    <div class="p-20">
                        <h6>{{  __('l.booking_a_car') }}</h6>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Select Contents Language</label><span class="required">*</span>
                                    <select name="content_type" id="content_type" class="form-control">
                                        <option value="">Select</option>
                                        <option value="en" >English (US)</option>
                                        <option value="arabic">Arabic</option>
                                    </select>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>
                            

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{  __('l.booking_a_car') }}</label>
                                    <input value="{{  __('l.booking_a_car') }}" type="text" name="booking_a_car"
                                        id="inputSecurityAmount" class="form-control" placeholder="{{  __('l.booking_a_car') }}">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{  __('l.verify') }}</label>
                                    <input value="{{  __('l.verify') }}" type="text" name="verify" maxlength="200"
                                        id="inputSecurityAmount" class="form-control" placeholder="{{  __('l.verify') }}">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>    

                        {{-- @if(!$item)
                        <div class="row">                        
                            <div class="col-md-6">
                                <div class="form-group">
                                <!-- <div class="form-group" style="margin-top:17px;"> -->
                                    <!-- <label>Car Pic</label><span class="required">*</span> --> 
                                    <small id="imageHelp" class="form-text text-muted" >(<b>Car Image:</b> Only jpeg, jpg, png are allowed.)</small>
                                    <input type="file" id="inputGroupFile01" name="car_img[]"
                                        class="imgInp filestyle custom-file-input car_img"
                                        aria-describedby="inputGroupFileAddon01"
                                        data-buttonname="btn-secondary" multiple>
                                        <div class="help-block"></div>
                                </div>                               
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group"> 
                                    <small id="imageHelp" class="form-text text-muted" >(<b>Car Docs:</b> Only jpeg, jpg, png, pdf, doc, docx are allowed.)</small>
                                    <input type="file" id="inputGroupFile02" name="car_docs[]"
                                        class="imgInp filestyle custom-file-input car_docs"
                                        aria-describedby="inputGroupFileAddon02"
                                        data-buttonname="btn-secondary" multiple>
                                        <div class="help-block"></div>
                                </div>                               
                            </div>
                        </div>
                        @endif --}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                <label>{{ __('l.upload_needed_docs') }}</label>
                                    <input value="{{ __('l.upload_needed_docs') }}" type="text" name="upload_needed_docs" 
                                         class="form-control">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                <label>{{ __('l.choose_and_request') }}</label>
                                    <input value="{{ __('l.choose_and_request') }}" type="text" name="choose_and_request" 
                                         class="form-control" >
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div> 
                        <div class="row"> 
                            <div class="col-md-6">
                                <div class="form-group">
                                <label>{{ __('l.enter_exact_date') }}</label>
                                    <input value="{{ __('l.enter_exact_date') }}" type="text" name="enter_exact_date"  
                                         class="form-control" placeholder="{{ __('l.enter_exact_date') }}">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                <label>{{ __('l.meet_owner') }} </label>
                                    <input value="{{ __('l.meet_owner') }}" type="text" name="meet_owner"   
                                         class="form-control" placeholder="{{ __('l.meet_owner') }}">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                <label>{{ __('l.meet_to_reekar_owner') }}</label><!-- <span class="required">*</span> -->
                                    <input  value="{{ __('l.meet_to_reekar_owner') }}" type="text" name="meet_to_reekar_owner" 
                                         class="form-control" placeholder="{{ __('l.meet_to_reekar_owner') }}">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row"> 
                            <div class="col-md-6">
                                <div class="form-group">
                                <label>{{ __('l.enjoy_experience') }}</label><!-- <span class="required">*</span> -->
                                    <input  value="{{ __('l.enjoy_experience') }}" type="text" name="enjoy_experience" 
                                         class="form-control" placeholder="{{ __('l.enjoy_experience') }}">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                <label>{{ __('l.need_to_say_more') }}</label><!-- <span class="required">*</span> -->
                                    <input  value="{{ __('l.need_to_say_more') }}" type="text" name="need_to_say_more" 
                                         class="form-control" placeholder="{{ __('l.need_to_say_more') }}">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            {{-- <!-- <div class="col-md-6">
                                <div class="form-group">
                                    <label>Total Amount</label><span class="required">*</span>
                                    <input value="{{$item ? $item->total_price : old('total_amount')}}" type="int" name="total_amount" maxlength="6"
                                        id="inputSecurityAmount" class="form-control" placeholder="Total Price">
                                    <div class="help-block"></div>
                                </div>
                            </div> --> --}}
                            <!-- <div class="col-md-4">
                                <div class="form-group">
                                    <label>Security Amount</label>
                                    <input value="{{$item ? $item->security_amount : old('security_amount')}}" type="int" name="security_amount" maxlength="6"
                                        id="inputSecurityAmount" class="form-control" placeholder="Security Amount">
                                    <div class="help-block"></div>
                                </div>
                            </div> -->
                        </div>
                      
                        <h6>{{ __('l.listing_your_car') }}</h6>
                        
                        <div class="row"> 
                            <div class="col-md-6">
                                <div class="form-group">
                                <label>{{ __('l.listing_your_car') }}</label><!-- <span class="required">*</span> -->
                                    <input  value="{{ __('l.listing_your_car') }}" type="text" name="listing_your_car" 
                                         class="form-control" placeholder="{{ __('l.listing_your_car') }}">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                <label>{{ __('l.list_your_car') }}</label><!-- <span class="required">*</span> -->
                                    <input  value="{{ __('l.list_your_car') }}" type="text" name="list_your_car" 
                                         class="form-control" placeholder="{{ __('l.list_your_car') }}">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="clearfix"></div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="p-20">
                                <button type="submit"
                                    class="btn btn-primary waves-effect waves-light saveBtn">@lang('l.save')</button>
                                <a href="{{ route('car.index') }}">
                                    <button type="button"
                                        class="btn btn-secondary waves-effect m-l-5">@lang('l.cancel')</button>
                                </a>
                                <div id="ajaxloader" style="display: none;"><img
                                        src="{{ asset('public/admin/images/ajax-loader.gif')}}" /> Processing...</div>

                            </div>
                        </div>
                    </div>
            </div>
        </div>
        </form>
    </div>

    @endsection
    @push('appendJs')
    <!-- <script type="text/javascript" src="{{ asset('public/admin/js/post-jobs.js')}}"></script> -->
    <script src="{{ URL::asset('public/admin/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}"></script>    
    <script src="{{ URL::asset('public/admin/plugins/select2/js/select2.min.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyA2X23zOQ4hIRy3ZoHPWxSqnPRzjAdOoCc"></script>

    @endpush