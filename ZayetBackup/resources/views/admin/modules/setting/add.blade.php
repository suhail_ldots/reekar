@extends('admin.layouts.master')
@section('css')
<style>
    .required {
        color: red;
    }

    .pad_right {
        padding-right: 0px !important;
    }

    .padd_rl {
        padding-left: 0px !important;
        padding-right: 0px !important;
    }

    @media screen and (max-width: 992px) and (min-width: 768px) {
        .padd_rl {
            font-size: 11px !important;
        }
    }

    @media(max-width:767px) {
        .pad_right {
            padding-right: 15px !important;
        }
    }
</style>

<link href="{{ URL::asset('public/admin/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('content')

<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <h4 class="page-title"> &nbsp</h4>
            <!-- <h4 class="page-title">@lang('l.users')</h4> -->
        </div>         
        <div class="col-sm-6">
            <div class="float-right d-md-block">
                @if($item)
               <!--  <a href="{{ route('settings.show', $item->id)}}">
                    <button class="btn btn-primary arrow-none waves-effect waves-light" type="button">
                        <i class="fas fa-eye"></i> @lang("l.site") Details
                    </button>
                </a> -->
                @endif
                <a href="{{ url()->previous() }}">
                    <button class="btn btn-primary arrow-none waves-effect waves-light" type="button">
                        <i class="fas fa-arrow-left"></i> @lang('l.back')
                    </button>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">            
                <h4 class="mb-0 mt-0 header-title"> @if($item){{ __('l.edit') }} @else @lang('l.add') @endif @lang('l.site') </h4>
                <small class="form-text text-muted mt-0" style="color: #9ca8b3 !important;  font-size: 15px;">@if(!$item) (New Car)@endif</small><br>
                @include('admin.partials.messages')
                <form action="{{route('settings.store')}}" method="POST" id="upload_form" autocomplete="off" enctype="multipart/form-data">
                <!-- <form action="{{route('settings.store')}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                    @csrf -->
                    @if($item)
                    <input type="hidden" name="id" id="id" value="{{ $item->id }}">
                    @endif
                    <div class="p-20">  
                    @if(!$item)
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group"> 
                                    <label for="">Upload Site Logo</label>
                                    <small id="imageHelp" class="form-text text-muted" >(Only jpeg, jpg, png, pdf, doc, docx are allowed.)</small>
                                    <input type="file" id="inputGroupFile02" name="logo"
                                        class="imgInp filestyle custom-file-input logo"
                                        aria-describedby="inputGroupFileAddon02"
                                        data-buttonname="btn-secondary" multiple>
                                        <div class="help-block"></div>
                                </div>                               
                            </div>                             
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Site Fav Icon</label> 
                                    <small id="imageHelp" class="form-text text-muted" >(Only jpeg, jpg, png are allowed.)</small>
                                    <input type="file" id="inputGroupFile01" name="fav_icon"
                                        class="imgInp filestyle custom-file-input fav_icon"
                                        aria-describedby="inputGroupFileAddon01"
                                        data-buttonname="btn-secondary" multiple>
                                        <div class="help-block"></div>
                                </div>                               
                            </div>
                            
                        </div>
                        @endif
                       <!--  <h6>Car Availability</h6> -->
                        <div class="row"> 
                            <div class="col-md-6">
                                <div class="form-group">
                                <label>Company Name</label><span class="required">*</span> 
                                    <input value="{{$item ? $item->company_name : old('company_name')}}" type="text" name="company_name" 
                                        id="" class="form-control" placeholder="Company Name*">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                <label>Contact Person</label><span class="required">*</span> 
                                    <input value="{{$item ? $item->contact_person : old('contact_person')}}" type="text" name="contact_person" 
                                        id="" class="form-control" placeholder="Contact Person*">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                             
                        </div> 
                        <div class="row"> 
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>@lang('l.email')</label>
                                    <input value="{{$item ? $item->email : old('email')}}" type="text" name="email" maxlength="25"
                                        id="inputMob" class="form-control" placeholder="@lang('l.email')">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Country Code</label>
                                    <select name="country_code" id="country_code" class="form-control">
                                        <option value="">Select Country Code</option>
                                        @if(CountryCodes())
                                            @foreach(CountryCodes() as $code)
                                            <option value="{{$code->dial_code}}" {{isset($item->country_code) && ($item->country_code == $code->dial_code) ? "selected" :''}}>{{$code->dial_code}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    <div class="help-block"></div>
                                </div>
                            </div> 
                        </div> 
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="">How many Records you want to display on each page?</label>
                                    <input value="{{$item && $item->data_per_page ? $item->data_per_page : old('data_per_page')}}"
                                        type="text" name="data_per_page" maxlength="2" id="inputEmail" class="form-control"
                                        placeholder="Enter the no. of records">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="">Contact No. </label>
                                    <input value="{{$item ? $item->contact_no : old('contact_no')}}" type="text" name="contact_no" maxlength="25"
                                        id="inputMob" class="form-control" placeholder="@lang('l.mobile') No.*">
                                    <div class="help-block"></div>
                                </div>
                            </div>                            
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="">Alternate Contact No. </label>
                                    <input value="{{$item && $item->alternate_contact_no ? $item->alternate_contact_no : old('alternate_contact_no')}}"
                                        type="text" name="alternate_contact_no" maxlength="25" id="inputEmail" class="form-control"
                                        placeholder="Alternate Contact No.">
                                    <div class="help-block"></div>
                                </div>
                            </div>                            
                            <div class="col-md-12">
                                <div class="form-group">
                                <label for="">Company Full Address </label>
                                    <input value="{{$item && $item->company_full_address ? $item->company_full_address : old('company_full_address')}}"
                                        type="text" name="company_full_address" maxlength="250" id="inputEmail" class="form-control"
                                        placeholder="Company full address">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div> 
                    <div class="row">
                        <div class="col-12">
                            <div class="p-20">
                                <button type="submit"
                                    class="btn btn-primary waves-effect waves-light saveBtn">@lang('l.save')</button>
                                <a href="{{ URL::to('/home') }}">
                                    <button type="button"
                                        class="btn btn-secondary waves-effect m-l-5">@lang('l.cancel')</button>
                                </a>
                                <div id="ajaxloader" style="display: none;"><img src="{{ asset('public/admin/images/ajax-loader.gif')}}" /> Processing...</div>

                            </div>
                        </div>
                    </div>
            </div>
        </div>
        </form>
    </div>

    @endsection
    @push('appendJs')
    <script>
        $(document).ready(function () {

            $("#country_code").select2();            
        });
    </script>
    <script src="{{ URL::asset('public/admin/plugins/select2/js/select2.min.js') }}"></script>
    <!-- <script type="text/javascript" src="{{ asset('public/admin/js/post-jobs.js')}}"></script> -->
    <script src="{{ URL::asset('public/admin/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}"></script>    
    
     
    @endpush