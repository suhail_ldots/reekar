<?php use App\User; ?>
@extends('admin.layouts.master')
@section('content')
<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <h4 class="page-title">&nbsp</h4>
        </div>
        <div class="col-sm-6">
            <div class="float-right d-md-block">
               
                <a href="{{ route('settings.edit', $item->id)}}">
                    <button class="btn btn-primary arrow-none waves-effect waves-light" type="button">
                        <i class="far fa-edit"></i> {{ __('l.edit') }}
                    </button>
                </a>
                
                <a href="{{ route('settings.index')}}">
                    <button class="btn btn-primary arrow-none waves-effect waves-light" type="button">
                        <i class="fa fa-arrow-left"></i> @lang("l.back")
                    </button>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mt-0 header-title">{{ __('l.car') }} Details</h4>
                <small class="form-text text-muted m-b-30" style="color: #9ca8b3 !important;  font-size: 15px;">(Your user's car details)</small>
                @include('admin.partials.messages')
                <div class="row">
                    <div class="col-md-4">
                    <p><b>Car Image</b></p>
                        @if(count($item->carImages) < 1)
                        <img src="{{asset('public/car_image_placeholder.png')}}" 
                                id="car_pic"  width="150" height="150">
                        @else        
                        @foreach($item->carImages as $carimage)                                                
                            <img alt="Car Pic"  src={{ $carimage->image ?? asset('public/car_image_placeholder.jpeg')}} id="car_pic" class="" width="150" height="150">
                        @endforeach
                        @endif
                        @if(count($item->carDocs) >= 1)                        
                            <br><br><p><b>Car Doc</b></p>
                            @foreach($item->carDocs as $cardocs)
                            @php $exist =  @fopen($cardocs->doc, 'r'); @endphp   
                            @if($exist)
                                               
                            <iframe src="{{$cardocs->doc ?? ''}}" frameborder="0" width="150" height="120" scrolling="no">                        
                            </iframe>
                            <a href="{{$cardocs->doc ?? ''}}" target="blank">Download</a>
                            @endif
                            @endforeach
                        @endif
                    </div>
                    <div class="col-md-8">
                        <div class="table-responsive">
                            <table class="table mb-0">
                                <thead class="thead-light">
                                    <tr>
                                        <th colspan="4">Car Details</th>
                                    </tr>
                                </thead>
                                @php if($item->currency == 'USD')
                                    $icon =  '$' ;
                                    
                                    if($item->currency == 'EURO')
                                    $icon = '&euro;'; 

                                    if($item->currency == 'AED')
                                    $icon = 'AED'; 
                                    
                                @endphp  
                                <tbody>
                                    <tr>
                                        <td><b>Owner Name :</b></td>
                                        <td> {{ isset($item->user->full_name) ? $item->user->full_name : '' }}</td>
                                        <td><b>Contact No. :</b></td>
                                        <td>{{ isset($item->user->mobile) ? $item->user->mobile : '' }}</td>
                                    </tr> 
                                    
                                    <tr>
                                        <td><b>Model Year :</b></td>
                                        <td>{{ isset($item->year) ? $item->year : '' }}</td>
                                        <td><b>Model:</b></td>
                                        <td>{{ isset($item->model->name) ? $item->model->name : '' }}</td>
                                    </tr>
                                     
                                    <tr>
                                        <td><b>Brand :</b></td>
                                        <td>{{ isset($item->brand->name) ? $item->brand->name : '' }}</td>
                                        <td><b>Vehicle Type :</b></td>
                                        <td>{{ isset($item->vehicletype->name) ? $item->vehicletype->name : '' }}</td>
                                    </tr>
                                     
                                    <tr>
                                        <td><b>Engine Type :</b></td>
                                        <td>{{ isset($item->enginetype->name) ? $item->enginetype->name : '' }}</td>
                                        <td><b>Transmission :</b></td>
                                        <td>{{ isset($item->transmission->name) ? $item->transmission->name : '' }}</td>
                                    </tr>
                                     
                                    <tr>
                                        <td><b>Color :</b></td>
                                        <td>{{ isset($item->color->name) ? $item->color->name : '' }}</td>
                                        <td><b>Car Location :</b></td>
                                        <td>{{ isset($item->car_location) ? $item->car_location : '' }}</td>
                                    </tr>
                                    
                                    <tr>
                                        <td colspan="2"><b>Availability</b><br>
                                        <b>From : </b>{{ isset($item->available_from_date) ? date('d/M/Y', strtotime($item->available_from_date)) : '' }}&nbsp&nbsp{{ isset($item->available_from_time) ? date('h:i A', strtotime($item->available_from_time)) : '' }}
                                        </td>
                                        <td colspan="2"><br><b> To : </b>{{ isset($item->available_to_date) ? date('d/M/Y', strtotime($item->available_to_date)) : '' }}&nbsp&nbsp{{ isset($item->available_to_time) ? date('h:i A', strtotime($item->available_to_time)) : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Price/Day  :</b></td>
                                        <td>{!! isset($item->price_per_day) ? $icon.$item->price_per_day : '' !!}</td>
                                        <td><b>Total Cost  :</b></td>
                                        <td>{!! isset($item->total_price) ? $icon.$item->total_price : '' !!}</td>
                                    </tr>                                   
                                                                      
                                    <tr>
                                        <td><b>Security Amount :</b></td>
                                        <td>{!! isset($item->security_amount) ? $icon.$item->security_amount : '' !!}</td>
                                        <td></td>
                                        <td></td>
                                    </tr> 
                                    <tr><td colspan="4"><b>Extra Feature</b></td></tr>                                  
                                   <tr>
                                        <td><input type="checkbox"  name="ac" value="yes" {!! isset($item->ac) && ($item->ac == '1') ? 'checked' :'' !!} {{'disabled'}}>&nbsp AC</td>
                                        <td><input type="checkbox"  name="gps" value="yes" {{isset($item->gps) && ($item->gps == '1') ? 'checked' :''}} {{'disabled'}}>&nbsp GPS</td>
                                        <td><input type="checkbox"  name="ipod_interface" value="yes" {{isset($item->ipod_interface) && ($item->ipod_interface == '1') ? 'checked' :''}} {{'disabled'}}>&nbsp Ipod Interface</td>
                                        <td><input type="checkbox"  name="sunroof" value="yes" {{isset($item->sunroof) && ($item->sunroof == '1') ? 'checked' :''}} {{'disabled'}}>&nbsp Sunroof</td>
                                   </tr>
                                   <tr>
                                        <td><input type="checkbox"  name="electric_windows" value="yes" {{isset($item->electric_windows) && ($item->electric_windows == '1') ? 'checked' :''}} {{'disabled'}}>&nbsp Electric Windows</td>
                                        <td><input type="checkbox"  name="heated_seat" value="yes" {{isset($item->heated_seat) && ($item->heated_seat == '1') ? 'checked' :''}} {{'disabled'}}>&nbsp Heated Seat</td>
                                        <td><input type="checkbox"  name="panorma_roof" value="yes" {{isset($item->panorma_roof) && ($item->panorma_roof == '1') ? 'checked' :''}} {{'disabled'}}>&nbsp Panorma Roof</td>
                                        <td><input type="checkbox"  name="prm_gauge" value="yes" {{isset($item->prm_gauge) && ($item->prm_gauge == '1') ? 'checked' :''}} {{'disabled'}}>&nbsp Prm Gauge</td>
                                   </tr>
                                   
                                   <tr>
                                        <td><input type="checkbox"  name="child_seat" value="yes" {{isset($item->child_seat) && ($item->child_seat == '1') ? 'checked' :''}} {{'disabled'}}>&nbsp Child Seat</td>
                                        <td><input type="checkbox"  name="abs" value="yes" {{isset($item->abs) && ($item->abs == '1') ? 'checked' :''}} {{'disabled'}}>&nbsp ABS</td>
                                        <td><input type="checkbox"  name="traction_control" value="yes" {{isset($item->traction_control) && ($item->traction_control == '1') ? 'checked' :''}} {{'disabled'}}>&nbsp Traction Control</td>
                                        <td><input type="checkbox"  name="audio_system" value="yes" {{isset($item->audio_system) && ($item->audio_system == '1') ? 'checked' :''}} {{'disabled'}}>&nbsp Audio System</td>
                                   </tr>
                                </tbody>
                            </table>
                        </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection