@forelse($items as $item)
    <tr>
        <!-- <td>{{ ($items->currentpage()-1) * $items->perpage() + $loop->index + 1 }}</td> -->
        <td>{{(($items->currentPage() * $items->perPage()) + $loop->iteration) - $items->perPage()}}</td>
        <td>@if(isset($item->users->first_name)){{ $item->users->first_name.' '.$item->users->last_name }}@endif</td>
        <td>@if(isset($item->company->organization_name)){{ $item->company->organization_name }} @endif</td>
        <td>{{ $item->ip}}</td>
        <td>{{ $item->event}}</td>
        <td>{{ $item->action}}</td>
        <td>{{ date('M d, Y', strtotime($item->created_at))}}</td>
    </tr>
    @empty
    <tr>
    @if($items->currentPage() == $items->lastPage())
        <td colspan="11" align="center">{{ __('l.log_not_found') }}</td>
    @endif
    </tr>
@endforelse  
    