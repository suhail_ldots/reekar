@extends('site_view.layouts.main')
@section('head')
<style>
    #upload_form{     padding: 50px 30px;  background: #f6f6f6;  width: 100%; display: table;margin: 50px auto;  box-shadow: 1px 2px 5px #0000004d;}
     #upload_form .form-control{ border: 1px solid #e0e0e0;   border-radius: 30px; height: 49px;}
     #upload_form .has-error{}
     #upload_form .form-group{ margin-bottom:15px !important; position: relative; min-height: 90px; }
     #upload_form .help-block {  margin-top: 0;  margin-bottom: 0;    position: absolute;   bottom: 0;}
     .fixed-mar-top-inner{ clear:both;}
     
     @media only screen and (min-width:320px) and (max-width:991px) {
         #upload_form{ margin:110px auto 100px;}
        }
</style>
@endsection
@section('content')
@if (\Session::has('message'))
  <div class="alert alert-danger alert-dismissible customalertradhe" id="success-alert">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong></strong>{{ \Session::get('message') }}
  </div>
 @endif
<div class="container fixed-mar-top-inner"> 
      <div class="row">
        <div class="col-12 col-lg-12"> 
          <form class="formbox" action="{{ route('register') }}" method="post" id="upload_form" enctype="multipart/form-data">
          @csrf

          <div class="col-12 col-md-12 mx-auto white mt-2">
          @if (count($errors) > 0)
          <div class="row">
            <div class="error" style="color:red;">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          </div>
          @endif
           <div class="row">
              <div class="col-md-6">
                  <div class="md-form mb-0 form-group">
                      <label for="name" class="">Your First Name</label>
                      <input type="text" name="first_name" value="" class="form-control">
                      <div class="help-block"></div>
                  </div>
              </div>
               <div class="col-md-6">
                  <div class="md-form mb-0 form-group">
                  <label for="name" class="">Your Last Name</label>
                      <input type="text" name="last_name" value="" class="form-control">
                      <div class="help-block"></div>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="md-form mb-0 form-group">
                  <label for="name" class="">Your Gender</label>
                      <input type="text" name="gender" value="" class="form-control">
                      <div class="help-block"></div>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="md-form mb-0 form-group">
                  <label for="name" class="">Your Age <small>(in years)</small></label>
                      <input type="number" name="age" value="" class="form-control">
                      <div class="help-block"></div>
                  </div>
              </div>
              <div class="col-md-12">
                  <div class="md-form mb-0 form-group">
                  <label for="name" class="">Your Blood Group</label><br>
                      <input type="radio" name="blood_group" value="A"> A &nbsp&nbsp
                      <input type="radio" name="blood_group" value="B"> B &nbsp&nbsp
                      <input type="radio" name="blood_group" value="AB"> AB &nbsp&nbsp
                      <input type="radio" name="blood_group" value="O"> O
                  </div>
                  <div class="help-block"></div>

              </div>
              
           
         
          
          

         
              <div class="col-md-6">
                  <div class="md-form mb-0 form-group">
                    <label for="email" class="">Email</label>
                    <input type="text" name="email" value="" class="form-control">
                    <div class="help-block"></div>
                  </div>
              </div>
           
          
              <div class="col-md-6">
                  <div class="md-form mb-0 form-group">
                  <label class="">Phone</label>
                      <input type="text" name="mobile" value="" class="form-control">
                      <div class="help-block"></div>
                  </div>
              </div>
           
          
              <div class="col-md-6">
                  <div class="md-form mb-0 form-group">
                  <label for="email" class="">{{ __('featured_plan.address') }}</label>
                  
                        <div class="input-container {{ $errors->has('user_address') ? ' has-error' : '' }}"> 
                          <input type="text" name="user_address" id="addr_input_location" class="classic form-control" autocomplete="off">
                          <div class="help-block"></div>
                         
                          <input type="hidden" name="user_lat" value="{{old('user_lat')}}" id="user_lat" >
                          <input type="hidden" name="user_long" value="{{old('user_long')}}" id="user_long" >
                  </div>
                  </div>
              </div>
           
          
              <div class="col-md-6">
                  <div class="md-form mb-0 form-group">
                  <label for="email" class="">{{ __('featured_plan.country') }}</label>
                        <div class="input-container {{ $errors->has('country') ? ' has-error' : '' }}">                        
                            <select name="country" class="classic form-control" value="{{ old('country') }}">
                            @foreach(country() as $country)
                              <option value="{{ $country->id }}">{{ $country->name }}</option>
                            @endforeach 
                            </select>
                            <div class="help-block"></div>

                            @error('country')
                            <span class="help-block required" role="alert">
                                <li>{{ $message }}</li>
                            </span>
                            @enderror
                       </div>
               
             </div>
            </div>
         
              <div class="col-md-6">
                  <div class="md-form mb-0 form-group">
                  <label for="email" class="">{{ __('featured_plan.city') }}</label>
                        <div class="input-container {{ $errors->has('city') ? ' has-error' : '' }}"> 
                            <input type="text"  name="city" class="classic form-control" value="{{ old('city') }}">
                            <div class="help-block"></div>
                            @error('city')
                            <span class="help-block required" role="alert">
                                <li>{{ $message }}</li>
                            </span>
                            @enderror
                        </div>
                 </div>
                </div>
            
          
              <div class="col-md-6">
                  <div class="md-form mb-0 form-group">
                  <label for="email" class="">{{ __('featured_plan.zipcode') }}</label>
                        <div class="input-container {{ $errors->has('zipcode') ? ' has-error' : '' }}">
                            <input type="text"  name="zipcode" class="classic form-control" value="{{ old('zipcode') }}">
                            <div class="help-block"></div>
                            @error('zipcode')
                            <span class="help-block required" role="alert">
                                <li>{{ $message }}</li>
                            </span>
                            @enderror
                        </div>
                 </div>
                </div>
         
           
              <div class="col-md-6">
                  <div class="md-form mb-0 form-group">
                  <label for="email" class="">Password</label>
                       <div class="{{ $errors->has('password') ? ' has-error' : '' }}">
                          <input type="password" name="password" class="form-control" value="{{old('password')}}" placeholder="Enter Doctor Password...">
                          <div class="help-block"></div>
                            @error('password')
                                <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                 </div>
                </div>
            
          
              <div class="col-md-6">
                  <div class="md-form mb-0 form-group">
                       <label for="email" class="">Re-Type Password</label>
                       <div class="{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                          <input type="password" name="password_confirmation" class="classic form-control" value="{{old('password_confirmation')}}" placeholder="Enter a confirm password...">
                          <div class="help-block"></div>
                           @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                 </div>
                </div>
           
            <!-- <div class="row">
              <div class="col-md-12">
                  <div class="md-form mb-0">
                      <label for="name" class="">Choose Profile Picture</label>
                      <input type="file" name="profile_image" value="" class="form-control">
                  </div>
              </div>
            </div> -->
              <div class="col-12 col-lg-12 text-center my-4" style="margin:20px auto">
                    <button class="btn btn-primary w-100 p-0 m-0 py-3 saveBtn" type="submit" style="padding:10px 20px; font-size:15px">Create my Account</button>
                   
                    <div id="ajaxloader" style="display: none;"><img
                            src="{{ asset('public/admin/images/ajax-loader.gif')}}" /> Processing...</div>

                </div><hr>
                <div class="text-center py-1 my-3">
                    
                    Already a Member?<a href="{{route('login')}}"> {{ __('Login') }}</a>
                    
                </div>
              </div>
             </div>
          </div>
        </form>
      </div>
  </div>
</div>
<!--container end here-->
</div>
@endsection
@section('js')
<script type="text/javascript">
      //Auto Complete Address
      var searchInput = 'addr_input_location';  
        $(document).ready(function () {  
            var autocomplete;
            autocomplete = new google.maps.places.Autocomplete((document.getElementById(searchInput)), {
                types: ["geocode"],
            });
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var near_place = autocomplete.getPlace();
                //assign lat long only for user location value to 
                document.getElementById('user_lat').value = near_place.geometry.location.lat();
                document.getElementById('user_long').value = near_place.geometry.location.lng();
                           
            });   
        });
</script>
<script type="text/javascript">
    $(".profile_users_img").hover(function(){
      //alert("Df")
        $(this).disable = true;
    })   
    $(".profile_users_img").click(function(){
            $('#img-modal').modal('show');
        })
    
    $(function() {
    $("#select_file").on('change', function() {
      // Display image on the page for viewing
      readURL(this, "preview");

    });
});  

function readURL(input, tar) {
  if (input.files && input.files[0]) { // got sth

    // Clear image container
    $("#" + tar).removeAttr('src');

    $.each(input.files, function(index, ff) // loop each image 
      {

        var reader = new FileReader();

        // Put image in created image tags
        reader.onload = function(e) {
          $('#' + tar).attr('src', e.target.result);
        }

        reader.readAsDataURL(ff);

      });
  }
}  
</script>
@endsection