<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\SuccessfulPasswordChange;

use App\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dd("dfwf");
        return view('admin.modules.profile.view');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function store(Request $request)
    {
        $email_error = '';       
        $item = Auth::user();
            $this->validate($request, [
                'email' => 'required|string|email|max:255|unique:users,email,' . $request->id . ',id',
                'first_name' => 'required|regex:/^[a-zA-Z ]+$/u|max:255',
                'last_name' => 'required|regex:/^[a-zA-Z ]+$/u|max:255',
                'mobile' => 'required|string|max:25',
                'country_code' => 'required',              
                'address' => 'nullable|max:255',  
            ]); 

            $item = User::find($request->id);

            if ($request->password != '') {
                $this->validate($request, [
                    'password' => 'required|string|min:8|max:20|confirmed',
                    'password_confirmation' => 'required|string|min:8|max:20',
                ]);
                $item->password = Hash::make($request->password); 
            }
            $item->first_name = $request->first_name;
            $item->last_name = $request->last_name;           
            $item->email = $request->email;
            $item->mobile = $request->mobile; 
            $item->country_code = $request->country_code; 
            if($item->email_verified_at == '') { 
                $item->email_verified_at =  date('Y-m-d H:i:s');
            }           
            $item->address = $request->address; 
            $item->save();    
         
        \Session::flash('message', 'Profile Updated.');        
        return response()->json([
            'success' => true,
            'message' => 'Saved',
            'redirect' => route('adminprofile.index'),
        ]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.modules.profile.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function action(Request $request)
    {
        //return  $usrid = Auth::user()->id;
        $validation = Validator::make($request->all(), [
            'select_file' => 'required|image|mimes:jpeg,png,jpg|max:8210',
        ], [], ['select_file' => 'Image file']);
        if ($validation->passes()) {
            $image = $request->file('select_file');
            $imageName = time() . rand() . '.' . $image->getClientOriginalExtension();
            
            $folderPath = 'storage/app/images/users/';
            // $folderPath = 'public/profileImages';
            if(!file_exists($folderPath)) {
                mkdir($folderPath, 0777, true);
            }
             
            //$filePath = 'profile_images/' . $imageName;
            //$folderPath = public_path('assets/main/profileImages');
            
            $image->move($folderPath, $imageName);
            //file_put_contents($path, $image);
            $path = $imageName;
            $returnpath = url('/') . '/' . $folderPath . '/' . $imageName;
            //return $path;

        } else {
            return response()->json([
                'message' => $validation->errors()->all(),
                'uploaded_image' => '',
                'class_name' => 'alert-danger',
            ]);
        }

        if (isset($request->uid)) {
            $user = User::findOrFail($request->uid);
            $user->profile_pic = $path;
            $user->save(); 
            return response()->json([
                'status' => 200,
                'message' => 'Image Uploaded Successfully',
                'uploaded_image' => '<img src="$returnpath" class="img-thumbnail" width="300" />',
                'class_name' => 'alert-success',
            ]);
        } else { 
            return response()->json([
                'message' => $validation->errors()->all(),
                'uploaded_image' => '',
                'class_name' => 'alert-danger',
            ]);
        }
    }
}
