/* save into database */
var saveData = function(_form){
    var frm = jQuery(_form);       
    var btn = frm.find(".saveBtn");       
    var loader = frm.find("#ajaxloader"); 

    axios({
       method: 'post',
       url: frm.attr('action'),
       data:frm.serialize(),
       onUploadProgress: function (progressEvent) {
        $('.loading').show();
         btn.hide();
         loader.show();
       }
     })
     .then(function (response){            
         if(response.data){
           if(response.data.success){
            frm[0].reset(); 
            frm.find('.help-block').html('');
            toastr.success(response.data.message);
           setTimeout(function(){
             if(response.data.redirect)
            window.location.href=response.data.redirect;
             loader.hide();
             btn.show();
             $('.loading').hide();
           },3000);
          }else{
            swal ( "There was a problem with your submission." ,  response.data.message ,  "error" ); 
            toastr.error(response.data.message);
            loader.hide();
             btn.show();
             $('.loading').hide();
          }
         }
       })
       .catch(function (error) { 
        swal ( "There was a problem with your submission." ,  "Errors have been highlighted in the fields. If the problem persists, contact us at 08 9200 1850 to complete your request." ,  "error" );       
         loader.hide();
         btn.show();
         $('.loading').hide();
         if (error.response) {
            var errors =  error.response.data.errors;
             frm.find('.help-block').html('');
             jQuery.each(errors, function(i, _msg) {
                 var el = frm.find("[name="+i+"]");
               el.parents('.fieldParent').find('.help-block').html(_msg[0]);
             });
         }  
       }); 
    return false;    
};

//proceed checkout
var proceedCheckout = function(_form){
  var frm = jQuery(_form);       
  var btn = frm.find(".saveBtn");       
  var loader = frm.find("#ajaxloader"); 

  axios({
     method: 'post',
     url: frm.attr('action'),
     data:frm.serialize(),
     onUploadProgress: function (progressEvent) {
      $('.loading').show();
       btn.hide();
       loader.show();
     }
   })
   .then(function (response){            
       if(response.data){
         if(response.data.success){
          frm[0].reset(); 
          frm.find('.help-block').html('');
          swal ( response.data.heading ,  response.data.message ,  "success" );
          toastr.success(response.data.message);
         setTimeout(function(){
           if(response.data.redirect)
          window.location.href=response.data.redirect;
           loader.hide();
           btn.show();
           $('.loading').hide();
         },3000);
        }else{
          swal ( response.data.heading ,  response.data.message ,  "error" );
          loader.hide();
           btn.show();
           $('.loading').hide();
        }
       }
     })
     .catch(function (error) { 
      swal ( error.response.data.heading != null ? error.response.data.heading : "There was a problem with your submission." ,  error.response.data.message ,  "error" ); 
       loader.hide();
       btn.show();
       $('.loading').hide();
       if (error.response) {
          var errors =  error.response.data.errors;
           frm.find('.help-block').html('');
           jQuery.each(errors, function(i, _msg) {
               var el = frm.find("[name="+i+"]");
             el.parents('.fieldParent').find('.help-block').html(_msg[0]);
           });
       }  
     }); 
  return false;    
};

//Save Contact us and hire enquiry
var saveContactdata = function(_form){
  var frm = jQuery(_form);       
  var btn = frm.find(".saveBtn");       
  var loader = frm.find("#ajaxloader");
  
  axios({
     method: 'post',
     url: frm.attr('action'),
     data:frm.serialize(),
     onUploadProgress: function (progressEvent) {
      $('.loading').show();
       btn.hide();
       loader.show();
     }
   })
   .then(function (response){            
       if(response.data){
         if(response.data.success){
          frm[0].reset(); 
          frm.find('.help-block').html('');
          swal ( "Submitted Successfully" ,  "Thanks for getting in touch. A Liberty Liquors representative will get back to you as soon as possible." ,  "success" );
          captchaRefreash(); 
         // toastr.success(response.data.message);
         setTimeout(function(){
           if(response.data.redirect)
          window.location.href=response.data.redirect;
           loader.hide();
           btn.show();
           $('.loading').hide();
         },3000);
        }else{
          swal ( "There was a problem with your submission." ,  response.data.message ,  "error" ); 
          toastr.error(response.data.message);
          captchaRefreash();
          loader.hide();
           btn.show();
          $('.loading').hide();
        }
       }
     })
     .catch(function (error) { 
      swal ( "There was a problem with your submission." ,  "Errors have been highlighted in the fields. If the problem persists, contact us at 08 9200 1850 to complete your request." ,  "error" ); 
      captchaRefreash();           
       loader.hide();
       btn.show();
       $('.loading').hide();
       if (error.response) {
          var errors =  error.response.data.errors;
           frm.find('.help-block').html('');
           jQuery.each(errors, function(i, _msg) {
              var el = frm.find("[name="+i+"]");
             el.parents('.fieldParent').find('.help-block').html(_msg[0]);
           });
       } 
        
     }); 
  return false;    
};

//Save hire enquiry
var saveHiredata = function(_form){
  var frm = jQuery(_form);       
  var btn = frm.find(".saveBtn");       
  var loader = frm.find("#ajaxloader");
  
  axios({
     method: 'post',
     url: frm.attr('action'),
     data:frm.serialize(),
     onUploadProgress: function (progressEvent) {
      $('.loading').show();
       btn.hide();
       loader.show();
     }
   })
   .then(function (response){            
       if(response.data){
         if(response.data.success){
          frm[0].reset(); 
          frm.find('.help-block').html('');
          swal ( "Submitted Successfully" ,  "Thanks for getting in touch. A Liberty Liquors representative will get back to you as soon as possible." ,  "success" );
          captchaRefreash(); 
          setTimeout(function(){
           if(response.data.redirect)
          window.location.href=response.data.redirect;
           loader.hide();
           btn.show();
           $('.loading').hide();
         },3000);
        }else{
          swal ( "There was a problem with your submission." ,  response.data.message ,  "error" ); 
          toastr.error(response.data.message);
          captchaRefreash();
          loader.hide();
       btn.show();
          $('.loading').hide();
        }
       }
     })
     .catch(function (error) { 
      swal ( "There was a problem with your submission." ,  "Errors have been highlighted in the fields. If the problem persists, contact us at 08 9200 1850 to complete your request." ,  "error" ); 
      captchaRefreash();           
       loader.hide();
       btn.show();
       $('.loading').hide();
       if (error.response) {
          var errors =  error.response.data.errors;
           frm.find('.help-block').html('');
           jQuery.each(errors, function(i, _msg) {
               var el = frm.find("[name='"+i+"']").length > 0 ? frm.find("[name="+i+"]") : frm.find("[id='"+i+"']");
             el.parents('.fieldParent').find('.help-block').html(_msg[0]);
           });
       } 
        
     }); 
  return false;    
};

/* register user */
var registerUser = function(_form){
  var frm = jQuery(_form);       
  var btn = frm.find(".saveBtn");       
  var loader = frm.find("#ajaxloader"); 

  axios({
     method: 'post',
     url: frm.attr('action'),
     data:frm.serialize(),
     onUploadProgress: function (progressEvent) {
      $('.loading').show();
       btn.hide();
       loader.show();
     }
   })
   .then(function (response){            
       if(response.data){
         if(response.data.success){
          frm[0].reset();  
          toastr.success(response.data.message);
         setTimeout(function(){
           if(response.data.redirect)
           window.location.href=response.data.redirect;
           else
           swal ( "Congratulations!!" ,  "We are excited to have you as a member, as part of this premium group, you will be eligible for exciting offers. " ,  "success" );
           loader.hide();
           btn.show();
           captchaRefreash(); 
           $('.loading').hide();
         },3000);
        }else{
          swal ( "There was a problem with your submission." , response.data.message ,  "error" ); 
          loader.hide();
           btn.show();
           captchaRefreash(); 
          $('.loading').hide();
        }
       }
     })
     .catch(function (error) { 
      swal ( "There was a problem with your submission." , "Errors have been highlighted in the fields. If the problem persists, contact us at 08 9200 1850 to complete your request." ,  "error" );            
       loader.hide();
       btn.show();
       try{
	captchaRefreash(); 
	}catch (e) {}
       $('.loading').hide();
       if (error.response) {
          var errors =  error.response.data.errors;
           frm.find('.help-block').html('');
           jQuery.each(errors, function(i, _msg) {
               var el = frm.find("[name="+i+"]");
             el.parents('.fieldParent').find('.help-block').html(_msg[0]);
           });
       }  
     }); 
  return false;    
};

/* add items to cart */
var addCart = function(_form){
  var frm = jQuery(_form);       
  axios({
     method: 'post',
     url: frm.attr('action'),
     data:frm.serialize()
   })
   .then(function (response){
    $('.loading').hide();            
       if(response.data){
        if(response.data.success){
          frm.parents('.cart-btn').find('.help-block').html('');
          frm.parents('.add-btn-parent').find('.button-add').hide();
          frm.parents('.add-btn-parent').find('.button-added').show();
          toastr.success(response.data.message);
          cartListHeader();
          //$("HTML, BODY").animate({ scrollTop: 0 }, 2000);
          $('#addCartMsg').html('Added to Cart');
        }else {
          toastr.error(response.data.message);
          frm.parents('.cart-btn').find('.help-block').html(response.data.message);
         }
       }
     })
     .catch(function (error) {
      $('.loading').hide();            
      if (error.response) {
        var errors =  error.response.data.errors;
        jQuery.each(errors, function(i, _msg) {
          frm.parents('.cart-btn').find('.help-block').html(_msg[0]);
        });
    }
     }); 
  return false;    
}; 


/* update items to cart */
var updateCart = function(ele,product_id, qty){     
  axios({
     method: 'post',
     url: updateCartUrl,
     data:{product_id:product_id, qty:qty}
   })
   .then(function (response){ 
    $('.loading').hide();           
       if(response.data){
        if(response.data.success){
          ele.parents('.cart-btn').find('.help-block').html('');
          toastr.success(response.data.message);
          cartListHeader();
          cartList();
          checkoutList();
        }else {
          toastr.error(response.data.message);
          ele.parents('.cart-btn').find('.help-block').html(response.data.message);
         }
       }
     })
     .catch(function (error) {
      $('.loading').hide();            
      if (error.response) {
        var errors =  error.response.data.errors;
        jQuery.each(errors, function(i, _msg) {
          //toastr.error(_msg[0]);
          ele.parents('.cart-btn').find('.help-block').html(_msg[0]);
        });
    }
     }); 
  return false;    
}; 

/* remove items from cart */
var removeCart = function(product_id){  
  $('.loading').show();   
  axios({
     method: 'post',
     url: removeCartUrl,
     data:{product_id:product_id}
   })
   .then(function (response){  
    $('.loading').hide();          
       if(response.data){
        if(response.data.success){
          location.reload();
          /*toastr.success(response.data.message);
          cartListHeader();
          cartList();
          checkoutList();*/
        }else {
          toastr.error(response.data.message);
         }
       }
     })
     .catch(function (error) { 
      $('.loading').hide();           
      if (error.response) {
        var errors =  error.response.data.errors;
        jQuery.each(errors, function(i, _msg) {
          toastr.error(_msg[0]);
        });
    }
     }); 
  return false;    
}; 


/* update items to cart */
var updateIceCart = function(qty){     
  axios({
     method: 'post',
     url: updateIceCartUrl,
     data:{qty:qty}
   })
   .then(function (response){            
       if(response.data){
        if(response.data.success){
          toastr.success(response.data.message);
          checkoutList();
        }else {
          toastr.error(response.data.message);
         }
       }
     })
     .catch(function (error) {            
     }); 
  return false;    
}; 

var cartListHeader = function(){
  var divObj = $("#cart-list");
    $.ajax({
        url: divObj.data('url'),
        data: {
            sync: 1
        },
        dataType: 'json',
        cache: false,
        success: function(response){
            divObj.html(response.rows);
            $('.cart_qty').html(response.qty);
            $('.loading').hide();
        }
      });
}

cartListHeader();

var cartList = function(){
  var divObj = $("#cart-list1");
    $.ajax({
        url: divObj.data('url'),
        data: {
            sync: 1
        },
        dataType: 'json',
        cache: false,
        success: function(response){
            divObj.html(response.rows);
            $("#cartCheckoutContent").html(response.rows1);
            $('.loading').hide();
            cartListHeader();
        }
      });
}



var checkoutList = function(){
  var divObj = $("#checkout-detail-list");

  axios({
    method: 'post',
    url: divObj.data('url'),
    data:{
      sync: 1,
      pincode: $('#delivery_postcode').val(),
      collection: $('#collectionValue').val()
  }
  })
  .then(function (response){ 
    if(response.data.redirect){
      window.location.href=response.data.redirect;
      $('#checkoutSubmitButton').addClass('d-none');
    }else{
      divObj.html(response.data.rows);
      if($('#collectionValue').val() == 'NO'){
        $('#checkoutTotalAmount').html("$"+response.data.grand_total);
      if(response.data.shipping_amount && response.data.shipping_amount != "NONE"){
        $('#checkoutSubmitButton').removeClass('d-none');
        $('#checkoutShippingAmount').html("$"+response.data.shipping_amount);
        swal ("A delivery charge of $"+response.data.shipping_amount+" will be added to your order.")
      }else if(response.data.shipping_amount && response.data.shipping_amount == "NONE"){
        $('#checkoutSubmitButton').removeClass('d-none');
        $('#checkoutShippingAmount').html("$"+'0.00');
        swal ("Congrats, You are eligible for free delivery on this order")
      }else{
        $('#checkoutSubmitButton').addClass('d-none');
        $('#checkoutShippingAmount').html("$"+'0.00');
        if($('#delivery_postcode').val())
        swal ( "Delivery Not Available" ,  "For further delivery options or to request a delivery time for your order please call us on 9200 1850 or email admin@libertyliquors.com.au" ,  "error" );
      }
    }
      
    }
      $('.loading').hide();
    })
    .catch(function (error) {            
      $('.loading').hide();
    }); 
}

checkoutList();

var minusQty = function(_ele, product_id, qty){
  var ele = jQuery(_ele);
  var input = ele.parent('.cart-btn-box').find('#quantity');
  if(parseInt(input.val()) > qty){
  input.val(parseInt(input.val()) - 1);
  $('.loading').show();
  updateCart(ele, product_id, parseInt(input.val()));
  }
}

var addQty = function(_ele, product_id, qty){
  var ele = jQuery(_ele);
  var input = ele.parent('.cart-btn-box').find('#quantity');
  input.val(parseInt(input.val()) + 1); 
  $('.loading').show();
  updateCart(ele, product_id, parseInt(input.val()));
  if(parseInt(input.val()) > qty){
  input.val(parseInt(input.val()) - 1); 
  }
  
}

var removeValue = function(list, value, separator) {
  separator = separator || ",";
  var values = list.split(separator);
  for(var i = 0 ; i < values.length ; i++) {
    if(values[i] == value) {
      values.splice(i, 1);
      return values.join(separator);
    }
  }
  return list;
}


var autoFillFloat = function() {
$( ".fieldParent" ).each(function() {
  if($(this).find('input').length){
    //$(this).find('input').focus();
  if($(this).find('input').val() != null && $(this).find('input').val() != '' &&$(this).find('input').val() != 'undefined' ){
    if($(this).find('.floating-label').length){
    $(this).find('.floating-label').addClass('freeze');
    }
  }
}
    });
  }
  autoFillFloat();


  var setCollectionTimeOptions = function(date){
  $('#timeMsg').show();
    axios({
      method: 'post',
      url: fillCollectionTimeOptionsUrl,
      data:{
        date: date
    }
    })
    .then(function (response){ 
      if(response.data.success){
        $('#collectionTimeField').html(response.data.options);
      }
      $('#timeMsg').hide();
      })
      .catch(function (error) { 
        $('#timeMsg').hide();           
      }); 
  }

  /* apply promocode */
var applyPromocode = function(_form){
  var frm = jQuery(_form);       
  var btn = frm.find("#applyBtn");  
  frm.find('#applyerror').html('');      

  axios({
     method: 'post',
     url: frm.attr('action'),
     data:frm.serialize(),
     onUploadProgress: function (progressEvent) {
       //btn.hide();
       btn.html('Applying...');
     }
   })
   .then(function (response){            
       if(response.data){
         if(response.data.success){
          /*$('#promocode_field').attr('readonly', 'true');
           btn.html('Remove');
           btn.removeClass('btn-success');
           btn.addClass('btn-danger');
           frm.removeAttr('action');
           frm.attr('action', removePromocodeURL)*/
           checkoutList();
           cartList();
        }else{
          btn.html('Apply');
          $('#applyerror').html(response.data.message);
        }
       }
     })
     .catch(function (error) { 
      btn.html('Apply');
       if (error.response) {
          var errors =  error.response.data.errors;
           jQuery.each(errors, function(i, _msg) {
               //var el = frm.find("[name="+i+"]");
             $('#applyerror').html(_msg[0]);
           });
       }  
     }); 
  return false;    
};

/* remove promocode */
var removePromocode = function(_form){
  var frm = jQuery(_form);       
  var btn = frm.find("#applyBtn");  
  frm.find('#applyerror').html('');      

  axios({
     method: 'post',
     url: frm.attr('action'),
     data:frm.serialize(),
     onUploadProgress: function (progressEvent) {
       //btn.hide();
       btn.html('Removing...');
     }
   })
   .then(function (response){            
       if(response.data){
         if(response.data.success){
          /*$('#promocode_field').val('');
          $('#promocode_field').removeAttr('readonly');
           btn.html('Apply');
           btn.removeClass('btn-danger');
           btn.addClass('btn-success');
           frm.removeAttr('action');
           frm.attr('action', applyPromocodeURL)*/
           checkoutList();
           cartList();
       
        }else{
          btn.html('Remove');
          $('#applyerror').html(response.data.message);
        }
       }
     })
     .catch(function (error) { 
      btn.html('Apply');
       if (error.response) {
          var errors =  error.response.data.errors;
           jQuery.each(errors, function(i, _msg) {
               //var el = frm.find("[name="+i+"]");
             $('#applyerror').html(_msg[0]);
           });
       }  
     }); 
  return false;    
};