/* save into database */
var saveData = function(_form){
  
    var frm = jQuery(_form);       
    var btn = frm.find(".saveBtn");       
    var loader = frm.find("#ajaxloader"); 
    var msg = frm.find("#msg"); 
    btn.attr("disabled", true);
    //alert(btn.text()); exit();
    btn.hide();
    loader.show();
    axios({
       method: 'post',
       url: frm.attr('action'),
       data:frm.serialize(),
       onUploadProgress: function (progressEvent) {
         btn.hide();
         btn.attr("disabled", true);
         loader.show();
       }
     })
     .then(function (response){
        btn.attr("disabled", false);            
         if(response.data){
           frm.find('.form-group').removeClass('has-error');
           frm.find('.help-block').html('');
           if(frm.find("#id") ){
              //frm[0].reset();  
           } 
           setTimeout(function(){
            msg.html(response.data.message);
            window.location.href=response.data.redirect;             
             loader.hide();
             btn.show();
           },2000);
           setTimeout(function(){
            msg.html('');
           },8000);
         }
       })
       .catch(function (error) {            
         loader.hide();
         btn.attr("disabled", false);
         btn.show();
         if (error.response) {
            var errors =  error.response.data.errors;
             frm.find('.form-group').removeClass('has-error');
             frm.find('.help-block').html('');
             jQuery.each(errors, function(i, _msg) {
              if(i == 'privillage'){
                $(".privillage").show();
              } 
              console.log(i);
               /*if(i == 'image'){
                  var el = frm.find(".upload_image");
               }
               else if(i == 'file'){
                var el = frm.find(".upload_file");
              }
               else{*/
                 var el = frm.find("[name="+i+"]");
               //}
	
               el.parents('.form-group').addClass('has-error');
               el.parents('.form-group').find('.help-block').html(_msg[0]);
		window.scrollTo({ top: 0, behavior: 'smooth' });
             });
         }  
       }); 
    return false;    
};  


/* save into database */
var saveDataWithMultiSelect = function(_form){
  var selectionMulti = $.map($("#multi-selection option:selected"), function (el, i) {
    if($(el).val() != "")
    return $(el).val();
  });
  $("#multi-selection-value").val(selectionMulti.join(","));
  var frm = jQuery(_form);       
  var btn = frm.find(".saveBtn");       
  var loader = frm.find("#ajaxloader"); 
  var msg = frm.find("#msg"); 
  
  axios({
     method: 'post',
     url: frm.attr('action'),
     data:frm.serialize(),
     onUploadProgress: function (progressEvent) {
       btn.hide();
       loader.show();
     }
   })
   .then(function (response){            
       if(response.data){
         frm.find('.form-group').removeClass('has-error');
         frm.find('.help-block').html('');
         if(frm.find("#id") ){
            //frm[0].reset();  
         } 
         setTimeout(function(){
          msg.html(response.data.message);
          window.location.href=response.data.redirect;             
           loader.hide();
           btn.show();
         },2000);
         setTimeout(function(){
          msg.html('');
         },8000);
       }
     })
     .catch(function (error) {            
       loader.hide();
       btn.show();
       if (error.response) {
          var errors =  error.response.data.errors;
           frm.find('.form-group').removeClass('has-error');
           frm.find('.help-block').html('');
           jQuery.each(errors, function(i, _msg) {
             //console.log(i);
             /*if(i == 'image'){
                var el = frm.find(".upload_image");
             }
             else if(i == 'file'){
              var el = frm.find(".upload_file");
            }
             else{*/
               var el = frm.find("[name="+i+"]");
             //}
             el.parents('.form-group').addClass('has-error');
             el.parents('.form-group').find('.help-block').html(_msg[0]);
             
           });
       }  
     }); 
  return false;    
};

/// Password Show Hide

$(".toggle-password").click(function() {

    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
        input.attr("type", "text");
    } else {
        input.attr("type", "password");
    }
});

//Sk Submit Form with file 

$('#upload_form').on('submit', function (event) {
        event.preventDefault();
       
  var frm = $('#upload_form');
  var btn =  $('#upload_form').find(".saveBtn"); 
  var loader = frm.find("#ajaxloader"); 
  var msg = frm.find("#msg"); 
  btn.attr("disabled", true);
  //alert(btn.text()); exit();
  btn.hide();
  loader.show();
  axios({
     method: 'post',
     url: frm.attr('action'),
    // data:frm.serialize(),
      data:new FormData(this),
      dataType: 'JSON',
      contentType: false,
      cache: false,
      processData: true,
     onUploadProgress: function (progressEvent) {
       btn.hide();
       btn.attr("disabled", true);
       loader.show();
     }
   })
   .then(function (response){
      btn.attr("disabled", false);            
       if(response.data){
         frm.find('.form-group').removeClass('has-error');
         frm.find('.help-block').html('');
         if(frm.find("#id") ){
            //frm[0].reset();  
         } 
         setTimeout(function(){
          msg.html(response.data.message);
          window.location.href=response.data.redirect;             
           loader.hide();
           btn.show();
         },2000);
         setTimeout(function(){
          msg.html('');
         },8000);
       }
     })
     .catch(function (error) {            
       loader.hide();
       btn.attr("disabled", false);
       btn.show();
       if (error.response) {
          var errors =  error.response.data.errors;
           frm.find('.form-group').removeClass('has-error');
           frm.find('.help-block').html('');
           jQuery.each(errors, function(i, _msg) {
            if(i == 'privillage'){
              $(".privillage").show();
            } 
            console.log(i);
            
            if(i == "car_img" || i.includes("car_img")){
              var el = frm.find(".car_img");
            }else if(i.includes('car_docs')){ 
              var el = frm.find(".car_docs"); 
            }else{ 
              var el = frm.find("[name="+i+"]");
            }
             /*if(i == 'image'){
                var el = frm.find(".upload_image");
             }
             else if(i == 'file'){
              var el = frm.find(".upload_file");
            }
             else{*/
              //var el = frm.find("[name="+i+"]");
             //}
             el.parents('.form-group').addClass('has-error');
             el.parents('.form-group').find('.help-block').html(_msg[0]);
             
           });
       }  
     }); 
  return false;    
});  

/* Save Quiz answer */

var saveQuiz = function(_form){
  
  var frm = jQuery(_form);       
  var btn = frm.find(".saveBtn");       
  var loader = frm.find("#ajaxloader"); 
  var msg = frm.find("#msg"); 
  btn.attr("disabled", true);
  //alert(btn.text()); exit();
  btn.hide();
  loader.show();
  axios({
     method: 'post',
     url: frm.attr('action'),
     data:frm.serialize(),
     onUploadProgress: function (progressEvent) {
       btn.hide();
       btn.attr("disabled", true);
       loader.show();
     }
   })
   .then(function (response){
      btn.attr("disabled", false);            
       if(response.data){
         frm.find('.form-group').removeClass('has-error');
         frm.find('.help-block').html('');
         if(frm.find("#id") ){
            //frm[0].reset();  
         } 
         setTimeout(function(){
          msg.html(response.data.message);
          window.location.href=response.data.redirect;             
           loader.hide();
           btn.show();
         },2000);
         setTimeout(function(){
          msg.html('');
         },8000);
       }
     })
     .catch(function (error) {            
       loader.hide();
       btn.attr("disabled", false);
       btn.show();
       if (error.response) {
          var errors =  error.response.data.errors;
           frm.find('.form-group').removeClass('has-error');
           frm.find('.help-block').html('');
           
           jQuery.each(errors, function(i, _msg) {
           
           // console.log(i);
             /*if(i == 'image'){
                var el = frm.find(".upload_image");
             }
             else if(i == 'file'){
              var el = frm.find(".upload_file");
            }
             else{*/
              
             //}

             //var myarray = ["answer.1","answer.2","answer.3","answer.4"] ;
             var myarray = [];
		$('.options').each(function(){
			var newval = this.name;
			var vallnn = newval.replace("answer[", "answer.");
			var vallmm = vallnn.replace("]", "");
		    myarray.push(vallmm); 
		});
             if(jQuery.inArray(i, myarray) !== -1){
              var str = i;
              var mystr = str.replace(".","_");
              frm.find("."+mystr).html(_msg[0]);
             }else{
                var el = frm.find("[name="+i+"]");
                el.parents('.form-group').addClass('has-error');
                el.parents('.form-group').find('.help-block').html(_msg[0]);
             }
             if(i == 'answer'){  
                frm.find('#answer_hide').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button><div class="answer_hide help-block">'+_msg[0]+'</div></div>');   
                $("#answer_hide").show();
                $("#answer_hide").fadeOut(7000); 
             } 
            if(i == 'right'){               
              $("#answer_right").show()    ;             
              frm.find('#answer_right').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button><div class="answer_right help-block">'+_msg[0]+'</div></div>');   
              $("#answer_right").show();
              $("#answer_right").fadeOut(8000);            
            } 
            
  window.scrollTo({ top: 0, behavior: 'smooth' });
           });
       }  
     }); 
  return false;    
};
