<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
Route::group(['middleware' => 'HeaderKey'], function(){
    // Route::post('login', 'Auth\LoginController@login');
    Route::post('check-email', 'Auth\LoginController@checkEmail');
    // Route::post('register', 'Auth\LoginController@register');
    // Route::post('social-register', 'Auth\LoginController@socialRegister');
    Route::post('/resend-otp', 'Auth\LoginController@resend');  
    Route::post('/verify-otp', 'Auth\LoginController@otpVerify');
    Route::post('/get-master-data', 'ProfileController@getMasterData');
});
Route::post('login', 'Auth\LoginController@login');
Route::post('register', 'Auth\LoginController@register');
Route::post('social-register', 'Auth\LoginController@socialRegister');
Route::post('contact-us', 'ProfileController@contactUs');


Route::group(['middleware' => ['auth:api', 'HeaderKey']], function() {
	Route::post('/change-password', 'Auth\LoginController@changePassword');
    Route::post('/profile', 'ProfileController@profile');
    Route::post('/update-profile', 'ProfileController@updateProfile');
    Route::post('/update-profile-image', 'ProfileController@updateProfileImage');
    Route::post('/logout', 'Auth\LoginController@logout');
    Route::post('/save-payment-method', 'ProfileController@savePaymentMethod');  
    Route::post('/save-car-info', 'ProfileController@saveCarInfo');  
    Route::post('/save-car-location', 'ProfileController@saveCarLocation');  
    Route::post('/save-car-availability', 'ProfileController@saveCarAvailability');  
    Route::post('/upload-car-images', 'ProfileController@uploadBulkPhoto');  
    Route::post('/upload-car-docs', 'ProfileController@uploadBulkDocs');  
    Route::post('/list-cars', 'ProfileController@listCars');  
    Route::post('/car-details', 'ProfileController@carDetails');  
    Route::post('/delete-car', 'ProfileController@deleteCar');  
    Route::post('/delete-car-file', 'ProfileController@deleteCarFile');  
    Route::post('/Payment-Intent', 'ProfileController@getClientSc');  
    Route::post('/Braintree-Payment', 'ProfileController@getBrainTree');  
    Route::post('/Braintree-Payment-Save', 'ProfileController@saveBrainSale'); 
      
});