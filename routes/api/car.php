<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'HeaderKey'], function(){
    Route::post('test', 'CarController@test');
    Route::post('search-cars', 'CarController@searchCars');
    Route::post('get-current-currency', 'CarController@currentCurrency');
    Route::post('car-details', 'CarController@carDetails');
    Route::post('term-and-service-and-policy', 'CarController@termAndServiceAndPolicy');
});


