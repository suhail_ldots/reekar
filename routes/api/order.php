<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['auth:api', 'HeaderKey']], function() {
    Route::post('/list', 'OrderController@list');  
    Route::post('/list-my-car-booking', 'OrderController@listMyCarBooking');  
    Route::post('/details', 'OrderController@orderDetails');  
    Route::post('/save-order', 'OrderController@saveOrder');  
    Route::post('/upload-identity-doc', 'OrderController@uploadIdentityDoc');  
    Route::post('/success-order', 'OrderController@successOrder');  
    
});