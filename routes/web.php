<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('locale/{locale}', function ($locale){
    Session::put('locale', $locale);
    return redirect()->back();
});

include('front.php');

Route::get('/admin', function () {
    return view('auth.login');
});
Auth::routes();
Auth::routes(['verify' => true]);

Route::get('/home', 'Controller@index')->name('home');
Route::get('admin/masters/model-by-brand', 'Admin\Masters\ModelController@modelByBrand')->name('get-model-by-brand-ajax');
//Route::get('dashboard', 'Admin\DashboardController@index')->name('dashboard');

//drozone auto upload
Route::post('nursinghome/gallery-image', 'Admin\NursingHomeController@createGalleryImage')->name('projects.storeMedia');
Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'AdminControl']], function () {
    Route::get('dashboard', 'Admin\DashboardController@index')->name('admin.panel');

    Route::resource('profile', 'Admin\ProfileController', ['names' => [
        'create' => 'adminprofile.create',
        'index' => 'adminprofile.index',
        'update' => 'adminprofile.update',
        'edit' => 'adminprofile.edit',
        'store' => 'adminprofile.store',
        'show' => 'adminprofile.show',
        'destroy' => 'adminprofile.destroy',
    ]]);
    // Route::post('profilepic', 'Admin\ProfileController@action')->name('change_profile_pic');
    
    //Speciality
    Route::resource('masters/speciality', 'Admin\Masters\SpecialityController');
    Route::get('masters/speciality/search/data', 'Admin\Masters\SpecialityController@search')->name('admin.speciality_search');
    Route::get('masters/delete-speciality/{id}', 'Admin\Masters\SpecialityController@delete')->name('speciality.delete');
    Route::get('masters/speciality/{id}/{status}', 'Admin\Masters\SpecialityController@status')->name('speciality.status');
   
    //Departments
    Route::resource('masters/departments', 'Admin\Masters\DepartmentController');
    Route::get('masters/departments/search/data', 'Admin\Masters\DepartmentController@search')->name('admin.departments_search');
    Route::get('masters/delete-departments/{id}', 'Admin\Masters\DepartmentController@delete')->name('departments.delete');
    Route::get('masters/departments/{id}/{status}', 'Admin\Masters\DepartmentController@status')->name('departments.status');
    
    //Equipments
    Route::resource('masters/equipments', 'Admin\Masters\EquipmentController');
    Route::get('masters/equipments/search/data', 'Admin\Masters\EquipmentController@search')->name('admin.equipments_search');
    Route::get('masters/delete-equipments/{id}', 'Admin\Masters\EquipmentController@delete')->name('equipments.delete');
    Route::get('masters/equipments/{id}/{status}', 'Admin\Masters\EquipmentController@status')->name('equipments.status');
    
    //subscription plans
    Route::resource('masters/plans', 'Admin\Masters\SubscriptionPlansController');
    Route::get('masters/plans/search/data', 'Admin\Masters\SubscriptionPlansController@search')->name('admin.plan_search');
    Route::get('masters/delete-plans/{id}', 'Admin\Masters\SubscriptionPlansController@delete')->name('plans.delete');
    //settings
    Route::resource('settings', 'Admin\SettingController');
    //Colors
    Route::resource('masters/colors', 'Admin\Masters\ColorsController');
    Route::get('masters/colors/search/data', 'Admin\Masters\ColorsController@search')->name('admin.colors_search');
    Route::get('masters/delete-colors/{id}', 'Admin\Masters\ColorsController@delete')->name('colors.delete');
    Route::get('masters/colors/{id}/{status}', 'Admin\Masters\ColorsController@status')->name('colors.status');

    //Engine Types
    Route::resource('masters/engine-types', 'Admin\Masters\EngineTypesController');
    Route::get('masters/engine-types/search/data', 'Admin\Masters\EngineTypesController@search')->name('admin.type_search');
    Route::get('masters/delete-engine-types/{id}', 'Admin\Masters\EngineTypesController@delete')->name('engine-types.delete');
    Route::get('masters/engine-types/{id}/{status}', 'Admin\Masters\EngineTypesController@status')->name('engine-types.status');

    //Vehicle Types
    Route::resource('masters/vehicle-types', 'Admin\Masters\VehicleTypesController');
    Route::get('masters/vehicle-types/search/data', 'Admin\Masters\VehicleTypesController@search')->name('admin.vehicle-type_search');
    Route::get('masters/delete-vehicle-types/{id}', 'Admin\Masters\VehicleTypesController@delete')->name('vehicle-types.delete');
    Route::get('masters/vehicle-types/{id}/{status}', 'Admin\Masters\VehicleTypesController@status')->name('vehicle-types.status');

    //Transmission
    Route::resource('masters/transmission', 'Admin\Masters\TransmissionController');
    Route::get('masters/transmission/search/data', 'Admin\Masters\TransmissionController@search')->name('admin.transmission_search');
    Route::get('masters/delete-transmission/{id}', 'Admin\Masters\TransmissionController@delete')->name('transmission.delete');
    Route::get('masters/transmission/{id}/{status}', 'Admin\Masters\TransmissionController@status')->name('transmission.status');
  
    //Model
    
    Route::resource('masters/model', 'Admin\Masters\ModelController');
    Route::get('masters/model/search/data', 'Admin\Masters\ModelController@search')->name('admin.model_search');
    Route::get('masters/delete-model/{id}', 'Admin\Masters\ModelController@delete')->name('model.delete');
    Route::get('masters/model/{id}/{status}', 'Admin\Masters\ModelController@status')->name('model.status');
    
   
    /* //Cars
    Route::resource('masters/car', 'Admin\Masters\CarController');
    Route::get('masters/car/search/data', 'Admin\Masters\CarController@search')->name('admin.car_search');
    Route::get('masters/delete-car/{id}', 'Admin\Masters\CarController@delete')->name('car.delete');
    Route::get('masters/car/{id}/{status}', 'Admin\Masters\CarController@status')->name('car.status'); */
    
    //Nursing Home
    Route::resource('nursinghome', 'Admin\NursingHomeController');
    Route::get('nursinghome/search/data', 'Admin\NursingHomeController@search')->name('nursinghome.search');
    Route::get('delete-nursinghome/{id}', 'Admin\NursingHomeController@delete')->name('nursinghome.delete');
    Route::get('nursinghome/{id}/{status}', 'Admin\NursingHomeController@status')->name('nursinghome.status');
    // Route::post('nursinghome/gallery-image', 'Admin\NursingHomeController@createGalleryImage')->name('projects.storeMedia');
    Route::get('gallery-image/fetch/{nhid}', 'Admin\NursingHomeController@fetch')->name('dropzone.fetch');
    Route::get('gallery-image/delete', 'Admin\NursingHomeController@deleteImage')->name('dropzone.delete');

    //Doctors
    Route::resource('doctor', 'Admin\DoctorController');
    Route::get('doctor/search/data', 'Admin\DoctorController@search')->name('doctor.search');
    Route::get('delete-doctor/{id}', 'Admin\DoctorController@delete')->name('doctor.delete');
    Route::get('doctor/{id}/{status}', 'Admin\DoctorController@status')->name('doctor.status');
    //Patient
    Route::resource('patient', 'Admin\PatientController');
    Route::get('patient/search/data', 'Admin\PatientController@search')->name('patient.search');
    Route::get('delete-patient/{id}', 'Admin\PatientController@delete')->name('patient.delete');
    Route::get('patient/{id}/{status}', 'Admin\PatientController@status')->name('patient.status');
    
    //Daynamic Contents translation manager
    Route::get('site-contents', 'Admin\LangContentsController@index')->name('contents.index');
    //Cars
    Route::resource('car', 'Admin\CarController');
    Route::get('car/search/data', 'Admin\CarController@search')->name('car.search');
    Route::get('delete-car/{id}', 'Admin\CarController@delete')->name('car.delete');
    Route::get('car/{id}/{status}', 'Admin\CarController@status')->name('car.status');
    Route::post('car/list-status/', 'Admin\CarController@listingStatus')->name('car.listingStatus');
    
    //Orders
    Route::resource('order', 'Admin\OrderController');
    Route::get('order/search/data', 'Admin\OrderController@search')->name('order.search');
    Route::get('order/{id}/{status}', 'Admin\OrderController@orderStatus')->name('order.status');

    //Account
    Route::resource('account', 'Admin\AccountController');
    Route::get('account/search/data', 'Admin\AccountController@search')->name('account.search');
    Route::get('account/owner-payment/{id}/{owner_payment}', 'Admin\AccountController@ownerPayment')->name('account.owner_payment_status');
    //Account
    Route::resource('settings', 'Admin\SettingController');

    //Import region
    Route::get('masters/import-brands-models', 'Admin\Masters\ImportBrandsModelsController@index')->name('import-brands-models.index');
    Route::post('masters/import-brands-models', 'Admin\Masters\ImportBrandsModelsController@import')->name('import-brands-models.import');

    // Country
    Route::resource('/master/countries', 'Admin\Masters\CountryController');
    Route::get('master/countries', 'Admin\Masters\CountryController@index')->name('countries.index');
    Route::get('master/countries-search', 'Admin\Masters\CountryController@search')->name('admin.countries_search');
    Route::get('master/delete-countries/{id}', 'Admin\Masters\CountryController@delete')->name('countries.delete');
    Route::get('master/countries/create', 'Admin\Masters\CountryController@create')->name('countries.create');

});

Route::get('static/content/{id}', 'MobileTermAndSeriveceUrl@termAndService');
Route::get('content/{id}', 'MobileTermAndSeriveceUrl@webtermAndService');
require_once('web_customer.php');

Route::get('/sk',function(){
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    Artisan::call('config:clear');
    Artisan::call('view:clear');
    Artisan::call('route:clear');
});
