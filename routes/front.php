<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 
// Route::get('/', function () {
//     return view('welcome');
// });

//Nurshing
Route::get('/', "Front\HomeController@getNursing")->name('main');
Route::get('nursing/', "Front\HomeController@searchNursing")->name('search.nursing');
Route::get('doctor', "Front\HomeController@searchDoctor")->name('search.doctor');
Route::get('doctor-availability', "Front\HomeController@searchDoctorAvailability")->name('doctor.availability');
// Route::get('doctor/search/data', 'Admin\DoctorController@search')->name('doctor.search');

Route::get('nursing-details/{id}', "Front\HomeController@nursingDetails")->name('nursing.list.details');
Route::get('create-appointment/', "Front\HomeController@createAppointment")->name('book.appointment');
//Book Appointment (Save)
Route::post('book-appointment/', "Clinics\PatientDashboardController@bookAppointment")->name('create.appointment');
Route::group(['middleware' =>'auth'], function(){
    Route::get('appointment-status/{id}/{status}', "Front\HomeController@bookingStatus")->name('booking.status');
});

Route::get('/plan-details/{planid}', "Front\HomeController@featuredPlan")->name('featured.plan');
Route::post('/hospital-registration', "Front\PaymentController@store")->name('payment.store');
Route::get('/payment-success/{nhid}', "Front\PaymentController@paymentSuccess")->name('payment.thankyou');
Route::post('/newsletter-save', "Front\NewsLetterController@store")->name('newsletter.store');

/*Doctor Dashboard*/
Route::group(['prefix' => 'clinic','middleware' =>'auth' ], function(){
    //DropZone
    Route::get('galleryimage/fetch/{nhid}', 'Admin\NursingHomeController@fetch')->name('gallery_dropzone.fetch');
    Route::get('galleryimage/delete', 'Admin\NursingHomeController@deleteImage')->name('gallery_dropzone.delete');

    //Change Profile For All Types user
    Route::post('/user-profile', "Clinics\DoctorDashboardController@action")->name('change_profile_pic');

    //use Middleware AdminControl
    Route::group(['prefix' => 'admin', 'middleware' =>'NursingHomeControl'], function(){
        Route::get('/dashboard', "Clinics\ClinicDashboardController@index")->name('nursing.dashboard');
        Route::get('/appointments', "Clinics\ClinicDashboardController@nursingAppointments")->name('nursing_all.appointments');
        Route::get('/patients', "Clinics\ClinicDashboardController@myPatients")->name('nursing.patients');
        Route::get('/patient-search', "Clinics\ClinicDashboardController@search")->name('nursing_patient.search');
        Route::get('/profile', "Clinics\ClinicDashboardController@profileView")->name('nursing.profile.view');
        Route::match(['GET', 'POST'],'/update-profile', "Clinics\ClinicDashboardController@profileUpdate")->name('nursing.profile.update');
        Route::resource('/nursing-doctor', "Clinics\NursingDoctorController");
        Route::get('nursing-doctor/{id}/{status}', 'Clinics\NursingDoctorController@status')->name('nursing-doctor.status');

    });
    //use Middleware Dr.Control
    Route::group(['prefix' => 'doctor', 'middleware' =>'DoctorControl'], function(){
        Route::get('/appointments', "Clinics\DoctorDashboardController@myAppointments")->name('doctors_all.appointments');
        Route::get('/appointment/details/{id}', "Clinics\DoctorDashboardController@appointmentDetails")->name('doctor.appointment.details');
        Route::get('/patients', "Clinics\DoctorDashboardController@myPatients")->name('doctor.patients');
        Route::get('/patient-search', "Clinics\DoctorDashboardController@search")->name('doctor_patient.search');
        Route::get('/dashboard', "Clinics\DoctorDashboardController@index")->name('doctor.dashboard');
        Route::get('/profile', "Clinics\DoctorDashboardController@profileView")->name('doctor.profile.view');
        Route::match(['GET', 'POST'],'/update-profile', "Clinics\DoctorDashboardController@profileUpdate")->name('doctor.profile.update');
    });    

});

Route::group(['prefix' => 'clinic','middleware' =>['auth', 'PatientControl']], function(){
    Route::get('/patient-appointments', "Clinics\PatientDashboardController@myAppointments")->name('patient.appointments');
    Route::get('/patient-dashboard', "Clinics\PatientDashboardController@index")->name('patient.dashboard');
    // Route::get('appointment-status/{id}/{status}', "Clinics\PatientDashboardController@bookingStatus")->name('booking.status');
    //Profile saection
    Route::get('/patient-profile', "Clinics\PatientDashboardController@profileView")->name('patient.profile.view');
    Route::match(['GET', 'POST'],'/update-patient-profile', "Clinics\PatientDashboardController@profileUpdate")->name('patient.profile.update');
});
// Route::get('/skpayment-success', "Front\PaymentController@paymentSuccess")->name('payment.thankyou1');

    // echo "sdfdsaf";

Route::get('/search', function () {
    return view('front.search');
})->name('search');


Route::get('/list-car',"Auth\LoginController@mysiteUser")->name('customer-login');
Route::get('/user-login',"Auth\LoginController@siteFrontUser")->name('front-customer-login');
Route::get('/user-signup',"Auth\LoginController@siteFrontUserSignup")->name('front-customer-signup');
Route::post('/email-otp',"Auth\RegisterController@userVerification")->name('user.verify');

Route::get('/myLocation',"Front\CarController@currentLocation")->name('myLocation');

Route::group(['middleware' => 'FrontControl'], function(){    
    Route::get('/available-car',"Front\CarController@searchCar")->name('find-car');
    Route::get('/available-car-map',"Front\CarController@carsOnMap")->name('map-car');
    Route::get('/car-details',"Front\CarController@carDetails")->name('car.details');
    Route::get('/add-tocart',"Front\OrderController@fillCart")->name('add.to.cart');
    Route::post('/order-now',"Front\OrderController@saveOrder")->name('order.now');

    //Route::get('stripe', 'Front\OrderController@PayNow')->name('pay.now');
    Route::get('stripe', 'Front\OrderController@stripePost')->name('stripe.post');
    
});
//paypal
Route::get('PayPal', 'Front\OrderController@paypalPost')->name('paypal-post');


Route::get('payment', 'Front\PayPalController@payment')->name('payment');
Route::get('cancel', 'Front\PayPalController@cancel')->name('payment.cancel');
Route::get('payment/success', 'Front\PayPalController@success')->name('payment.success');



Route::get('/pdfdownload/{url}',"Front\OrderController@pdfDownload");


Route::get('/how-it-works',function () {
    return view('front.sample-page');
})->name('how-it-works');


Route::get('/terms-of-service',function () {
    return view('front.term-of-service');
})->name('term.service');

Route::get('/privacy-policy',function () {
    return view('front.privacy-policy');
})->name('privacy.policy');

Route::get('/login/{social}','Auth\LoginController@socialLogin')->where('social','twitter|facebook|linkedin|google|github|bitbucket');
Route::get('/login/{social}/callback','Auth\LoginController@handleProviderCallback')->where('social','twitter|facebook|linkedin|google|github|bitbucket');