<?php

/* Route::get('/customer', function () {
    return view('auth.customer_login');
}); */
//Route::get('dashboard', 'customer\DashboardController@index')->name('dashboard');
Route::group(['prefix' => 'customer', 'middleware' => ['auth','UserControl']], function () {
    Route::resource('profile', 'Customer\ProfileController', ['names' => [
        'create' => 'customerprofile.create',
        'index' => 'customerprofile.index',
        'update' => 'customerprofile.update',
        'edit' => 'customerprofile.edit',
        'store' => 'customerprofile.store',
        'show' => 'customerprofile.show',
        'destroy' => 'customerprofile.destroy',
    ]]);
    /* Route::post('profilepic', 'customer\ProfileController@action')->name('change_profile_pic');
    //Users
    Route::resource('user', 'customer\UserController');
    Route::get('user/search/data', 'customer\UserController@search')->name('user.search');
    Route::get('delete-user/{id}', 'customer\UserController@delete')->name('user.delete');
    Route::get('user/{id}/{status}', 'customer\UserController@status')->name('user.status');
     
    //Account
    Route::resource('account', 'customer\AccountController');
    Route::get('account/search/data', 'customer\AccountController@search')->name('account.search');
    Route::get('account/owner-payment/{id}/{owner_payment}', 'customer\AccountController@ownerPayment')->name('account.owner_payment_status');
    //Account
    Route::resource('settings', 'customer\SettingController'); */
    //Cars
    Route::resource('car', 'Customer\CarController',['names' => [
        'create' => 'customercar.create',
        'index' => 'customercar.index',
        'update' => 'customercar.update',
        'edit' => 'customercar.edit',
        'store' => 'customercar.store',
        'show' => 'customercar.show',
        'destroy' => 'customercar.destroy',
    ]]); 
    Route::post('profile-image', 'Customer\ProfileController@action')->name('change_customer_profile_pic');   
    Route::get('payment-method-create', 'Customer\ProfileController@paymentMethodCreate')->name('pay-method.create');
    Route::post('payment-method', 'Customer\ProfileController@paymentMethod')->name('pay-method');
    
    Route::get('car/search/data', 'Customer\CarController@search')->name('customer_car.search');
    Route::get('delete-car/{id}', 'Customer\CarController@delete')->name('customer_car.delete');
    Route::get('car/{id}/{status}', 'Customer\CarController@status')->name('customer_car.status');
    Route::post('car/list-status/', 'Customer\CarController@listingStatus')->name('customer_car.listingStatus');

    //Orders
    Route::resource('myorder', 'Customer\OrderController');
    Route::get('myorder/search/data', 'Customer\OrderController@search')->name('myorder.search');
    Route::get('myorder/{id}/{status}', 'Customer\OrderController@orderStatus')->name('myorder.status');
    
    //My Bookings
    Route::resource('mybooking', 'Customer\BookingController');
    Route::get('mybooking/search/data', 'Customer\BookingController@search')->name('mybooking.search');
    Route::get('mybooking/{id}/{status}', 'Customer\BookingController@orderStatus')->name('mybooking.status');
});