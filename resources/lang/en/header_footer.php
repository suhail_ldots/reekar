<?php

return array (
  'home' => 'Home',
  'address' => 'Address',
  'email' => 'Email',
  'login' => 'Login',
  'sign_up' => 'Sign Up',
  'lang' => 'Lang',
  'about_us' => 'About Us',
  'services' => 'Services',
  'become_partner' => 'Become a Partner',
  'doctors' => 'Doctors',
  'contact' => 'Contact',
  'reg_as_member' => 'Reg As Member',
  'about_us_desc' => 'Reg As Member',
  'read_more' => 'Read More',
  'contact_us' => 'Contact Us',
  'connect_with_us' => 'Connect With Us',
);
