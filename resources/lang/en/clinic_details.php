<?php

return array (
  'biography' => 'My Biography',
  
  'read_more' => 'Read More',
  'consultation' => 'Consultation',
  
  'availability' => 'Availability',
  'available_timings' => 'Available Timings',
  'book_appointment' => 'Book Appointment',
  'fill_info' => 'Fill Information',
  'fill_info_desc' => 'Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum.',
);
