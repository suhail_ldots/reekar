<?php 
use App\Models\Bookings;
$items = Bookings::where('doctor_id', Auth::user()->doctor->id)->get();
?>
@extends('clinics.layouts.doctor_main')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid m-t-15">
        @include('clinics.partials.messages')
            <div class="row">
                <div class="col-lg-8 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>Basic Doctor Dashboard</h1>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
                
                <!-- /# column -->
            </div>
            <!-- /# row -->
            <section id="main-content">
                
                <!-- /# row -->


                <div class="row">
                    <!-- column -->
                    <div class="col-lg-6">
                        <div class="card sales">
                            <div class="card-body">
                                <h2>Overall Appointments <span style="float: right;  color: #9999; font-size:26px;">{{ Bookings::where('doctor_id', Auth::user()->doctor->id)->count() }}</span></h2>
                                
                            </div>
                            <div class="graph_div">
                            <div class="graph">
                            <span>2000</span>
                            <div class="graph_height">
                            </div>
                            <span>Mar</span>
                            </div><div class="graph">
                            <span>4000</span>
                            <div class="graph_height heightdiv">
                            </div>
                            <span>A</span>
                            </div><div class="graph">
                            <span>2000</span>
                            <div class="graph_height">
                            </div>
                            <span>Mar</span>
                            </div><div class="graph">
                            <span>4000</span>
                            <div class="graph_height heightdiv">
                            </div>
                            <span>A</span>
                            </div><div class="graph">
                            <span>2000</span>
                            <div class="graph_height">
                            </div>
                            <span>Mar</span>
                            </div><div class="graph">
                            <span>4000</span>
                            <div class="graph_height heightdiv">
                            </div>
                            <span>A</span>
                            </div>
                            </div>
                        </div>
                    </div>
                    <!-- column -->
                    <div class="col-lg-6">
                    
                        <div class="card sales ">
                            <div class="card-body">
                                <h2>Completed appointments <span style="float: right;  color: #9999; font-size:26px;">{{ Bookings::where('doctor_id', Auth::user()->doctor->id)->where('status', 1)->count() }}</span></h2>
                                
                            </div>
                            <div class="graph_div department doctdashboard">
                            <div class="graph">
                            <span>2000</span>
                            <div class="graph_height">
                            </div>
                            <span>Dep1</span>
                            </div><div class="graph">
                            <span>4000</span>
                            <div class="graph_height heightdiv">
                            </div>
                            <span>Dep2</span>
                            </div><div class="graph">
                            <span>2000</span>
                            <div class="graph_height">
                            </div>
                            <span>Dep3</span>
                            </div><div class="graph">
                            <span>4000</span>
                            <div class="graph_height heightdiv">
                            </div>
                            <span>Dep4</span>
                            </div><div class="graph">
                            <span>2000</span>
                            <div class="graph_height">
                            </div>
                            <span>Dep5</span>
                            </div>
                            </div>
                        </div>
                    </div>
                    <!-- column -->
                    <div class="col-lg-6">
                        <div class="card">
                                <div class="card-body">
                                        <h2>New Messages </h2>
                                        
                                </div>
                                <div class="newmessage">
                                    <img src="assets/images/booking_call.png">
                                    <div class="message_content">
                                        <h4>148 total New Messages</h4>
                                        <a href="#">click to check Message</a>
                                    </div>
                                </div>	
                                <label class="timm"><i class="fa fa-clock-o"></i>10:00 am (Last Updated)<span style="float:right">24/03/2020</span></label>
                        </div>
                        
                    </div>
                    <!-- column -->
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <h2>New Messages </h2>                                
                            </div>
                            <div class="newmessage">
                                <img src="assets/images/booking_call.png">
                                    <div class="message_content">
                                        <h4>148 total New Messages</h4>
                                        <a href="#">click to check Message</a>
                                        <span class="fa fa-phone" ></span>
                                    </div>
                            </div>	
                        <label class="timm"><i class="fa fa-clock-o"></i>10:00 am (Last Updated)<span style="float:right">View</span></label> </div>
                        
                    </div>
                   
                </div>
                <!-- column -->
                <div class="card cardapp">
                    <div class="row mt-20">
                        <div class="col-lg-8">
                            <h2>Today’s Upcoming Appointments</h2>
                        </div>        
                        <div class="col-lg-4">
                            <label>Filter <select class="selectbtn"><option>Today's Appointment</option></select>
                            <!--<span class="fa fa-bars adddiv">
                            </span>-->
                            </label>
                        </div>
                    </div>
                    <div class="row mt-20">  
                        <div class="col-lg-12">                      
                        @forelse($items as $item)
                            <div class="doctor_apointment card today_appointment">
                                <!-- <h3 class="pull-left">{{ isset($item->nursingHome->organization_name) ? ucfirst($item->nursingHome->organization_name) : ''}}</h3> -->
                                <div>
                                    <label><i class="fa fa-male"></i>{{ isset($item->user) ? $item->user->full_name : ''}}</label>
                                    <label><i class="fa fa-map-marker"></i>{{ isset($item->user->user_country) ? ucfirst($item->user->user_country->name).', ' : ''}} {{ isset($item->user->city) ? ucfirst($item->user->city) : ''}}</label>
                                    <label><i class="fa fa-calendar-minus-o"></i>{{ isset($item->created_at) ? date('d M y', strtotime($item->created_at)) : '' }}</label>
                                </div>
                                    
                                <p>{{ isset($item->desease) ? $item->desease : ''}}</p>
                                <div>
                                    <label>Scheduled on</label>
                                    <label><i class="fa fa-calendar"></i>{{ isset($item->appointment_day) ? getMyWorkingDay($item->appointment_day)[$item->appointment_day] : ''}}</label>
                                    <label><i class="fa fa-calendar-minus-o"></i>{{ isset($item->appointment_time) ? $item->appointment_time : ''}}</label>
                                </div>
                                <button class="pull-right btn btn-primary">Contact Now</button>  
                            </div>
                        
                            @empty
                            <div class="col-lg-12">
                            <div class="doctor_apointment card" style="text-align: center;">
                                <p>Appointment not found</p>
                            </div>
                            </div>
                            @endforelse
                        </div>
                    </div>  
                </div>
                </div>
            </div>       
            </section>
        </div>
    </div>
</div>
@endsection        
@section('script')
<script>
    $(document).ready(function(){
        // $(".adddiv").click(function(){
        
        $(".today_appointment").toggleClass("divwidth");
        
        // });
    });
    
</script>
@endsection