@extends('clinics.layouts.doctor_main')
@section('content')
<div class="content-wrap">
            <div class="main">
                <div class="container-fluid m-t-15">
                @include('clinics.partials.messages')
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <h4 class="page-title">&nbsp</h4>
                    </div>
                    <div class="col-sm-6">
                        <div class="float-right d-md-block">
                            <a href="{{ route('doctors_all.appointments')}}">
                                <button class="btn btn-primary arrow-none waves-effect waves-light" type="button">
                                    <i class="fa fa-arrow-left"></i> @lang("l.back")
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">Appointment Details</h4>
                            <!-- <small class="form-text text-muted m-b-30" style="color: #9ca8b3 !important;  font-size: 15px;">(Your's user details)</small> -->
                            @include('admin.partials.messages')
                            <div class="row">
                                <div class="col-md-4">                        
                                    <img alt="User Pic" @if($item->profile_pic) src="{{\Url('storage/app/images/doctor/profile').'/'.$item->profile_pic}}" @else
                                            src="{{asset('public/nobody_user.jpg')}}" @endif
                                            id="profile_pic" class="" width="150" height="150">                         
                                </div>
                                <div class="col-md-8">
                                    <div class="table-responsive">
                                        <table class="table mb-0">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th colspan=2>Patient Details</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if($appointment->desease != '')
                                                <tr>
                                                    <td><b>Problem :</b></td>
                                                    <td> {{ isset($appointment->desease) ? $appointment->desease : '' }}</td>
                                                </tr>
                                                @endif
                                                @if($appointment->appointment_day != '')
                                                <tr>
                                                    <td><b> Appointment Date :</b></td>
                                                    <td> {{ isset($appointment->appointment_day) ? $appointment->appointment_day : '' }}</td>
                                                </tr>
                                                @endif
                                                @if($appointment->appointment_time != '')
                                                <tr>
                                                    <td><b> Appointment Time :</b></td>
                                                    <td> {{ isset($appointment->appointment_time) ? $appointment->appointment_time : '' }}</td>
                                                </tr>
                                                @endif
                                                @if($item->first_name != '')
                                                <tr>
                                                    <td><b>Name :</b></td>
                                                    <td> {{ isset($item->first_name) ? $item->getFullNameAttribute() : '' }}</td>
                                                </tr>
                                                @endif
                                                <tr>
                                                    <td><b>Age (in years) :</b></td>
                                                    <td>{{ isset($item->age) ? $item->age : '' }}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Gender :</b></td>
                                                    <td>{{ isset($item->gender) ? $item->gender : '' }}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Bloog Group :</b></td>
                                                    <td>{{ isset($item->blood_group) ? $item->blood_group : '' }}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Contact No. :</b></td>
                                                    <td>{{ isset($item->country_code) ? $item->country_code : '' }} {{ isset($item->mobile) ? $item->mobile : '' }}</td>
                                                </tr>
                                                
                                                <tr>
                                                    <td><b>Email :</b></td>
                                                    <td>{{ isset($item->email) ? $item->email : '' }}</td>
                                                </tr>

                                                <tr>
                                                    <td><b>Designation :</b></td>
                                                    <td>{{ isset($item->role->name) ? $item->role->name : '' }}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Country :</b></td>
                                                    <td>{{ isset($item->user_country->name) ? $item->user_country->name : '' }}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>City :</b></td>
                                                    <td>{{ isset($item->city) ? $item->city : '' }}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Address :</b></td>
                                                    <td>{!! isset($item->address) ? $item->address : '' !!}<br>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection