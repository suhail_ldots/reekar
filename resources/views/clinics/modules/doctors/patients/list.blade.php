@extends('clinics.layouts.doctor_main')
@section('content')
<div class="content-wrap">
            <div class="main">
                <div class="container-fluid m-t-15">
                @include('clinics.partials.messages')
<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <h4 class="page-title"> &nbsp</h4>
            <!-- <h4 class="page-title"> {{ __('l.members') }}</h4> -->
        </div>
        <div class="col-sm-6">
            <div class="float-right d-md-block">
                 
                <a href="{{ url()->previous() }}">
                    <button class="btn btn-primary arrow-none waves-effect waves-light" type="button">
                        <i class="fa fa-arrow-left"></i> Back
                    </button>
                </a>
                 
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mt-0 header-title m-b-30">Patient List</h4>
                @include('site_view.partials.messages')
                <div class="table-responsive table-full-width">
                    <div style="overflow: auto;">
                        <form action="{{ route('doctor_patient.search')}}" method="GET">
                            <div class="row"> 
                                <div class="col-md-12">
                                    <div class="col-md-3" style="float:left;">
                                        <div class="form-group">
                                            <input type="text" name="name" maxlength="250" class="form-control" placeholder="First Name"
                                                @if(isset($name)) value="{{ $name }}" @endif autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="float:left;">
                                        <div class="form-group">
                                            <input type="text" name="email" maxlength="250" class="form-control" placeholder="Email"
                                                @if(isset($email)) value="{{ $email }}" @endif autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="float:left;">
                                        <div class="form-group">
                                            <input type="text" name="mobile" maxlength="250" class="form-control" placeholder="mobile"
                                                @if(isset($mobile)) value="{{ $mobile }}" @endif autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="float:left;">
                                        <div class="form-group">
                                            <button
                                                class="btn btn-primary arrow-none waves-effect waves-light"
                                                type="submit"><i class="fa fa-search"></i> Search</button>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </form>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>@lang('l.#')</th>
                                    <th> @lang('l.name')</th>
                                    <th> Gender</th>
                                    <th> Age (in years)</th>
                                    <th>@lang('l.email')</th>
                                    <th>@lang('l.mobile')</th>                                   
                                    <th>Country</th>  
                                    <th>City</th>                                  
                                    <th>@lang('l.address')</th>
                                </tr>
                            </thead>
                            <tbody id="tbody">
                                @include('clinics.modules.doctors.patients.tbody')
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<div class="modal fade" id="img-modal" tabindex="-1" role="dialog" aria-labelledby="edit-modal-label"
    aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0">Change Car Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body" style="height: 100%;">
                <div class="alert" id="message" style="display: none"></div>
                <div class="alert ajaxmsg" style="display: none"></div>
                <form method="post" action="{{route('car.listingStatus')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <div class="row">
                            <div class="col-12 col-md-8 ">
                           
                                <input type="radio" name="listStatus" class="approve" value="approve"> Approve                             
                                <br><input type="radio" name="listStatus" class="reject" value="reject"> Reject                              
                                <input type="hidden" name="carid" id="car_id">  
                            </div>
                        </div><br>
                        <div class="row reason" id="" style="display:none;">
                            <div class="col-12 col-md-8 ">
                            <label for="reason">Please Enter Reason for Reject</label>
                            <textarea name="reason" cols="60" rows="7"></textarea>
                            </div>
                        </div>
                        <!-- <div class="text-center py-2 white">
                            <input  class="btn btn-primary waves-eff" value="Upload">
                        </div> -->

                    </div>
                </div>        
            <br/> 
            <div class="modal-footer"><button type="submit" name="upload" id="upload" class="btn btn-primary">Save</button> <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button></div>
        </form>               
        </div>
        
    </div>
    
</div>
@endsection

@section('script')
<script>
function change_status(id){       
    var inputF = document.getElementById("car_id");   
    inputF.setAttribute('value', id); 
    $('#img-modal').modal('show');
}
</script>
<script>
    $(function () {
        $(".reject").change(function () {             
            if ($(this.checked)) {
                $(".reason").show();
            } else {
                $(".reason").hide();
            }
        });

        $(".approve").change(function () {
           if ($(this.checked)) {
                $(".reason").hide();
            }  
        });
    });
    
</script>
@if(count($items) > 0)
    
    @include('lazyloading.loading')
    
@endif
@endsection


