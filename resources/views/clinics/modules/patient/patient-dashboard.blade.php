<?php 
use App\Models\Bookings;
?>
@extends('clinics.layouts.patient_main')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid m-t-10">
        @include('clinics.partials.messages')
            <div class="row">
                <div class="col-lg-8 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>Patient Dashboard</h1>
                            <p><a href="{{ route('main') }}"><i class="fa fa-arrow-left"></i> Go to web</a></p>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
                
                <!-- /# column -->
            </div>
            <!-- /# row -->
            <section id="main-content">
                
                <!-- /# row -->


                <div class="row">
                    <!-- column -->
                    <div class="col-lg-6">
                        <div class="card sales">
                            <div class="card-body">
                                <h2>Overall Appointments <span style="float: right;  color: #9999; font-size:26px;">{{ Bookings::where('patient_id', Auth::user()->id)->count() }}</span></h2>
                                
                            </div>
                            <div class="graph_div">
                            <div class="graph">
                            <span>2000</span>
                            <div class="graph_height">
                            </div>
                            <span>Mar</span>
                            </div><div class="graph">
                            <span>4000</span>
                            <div class="graph_height heightdiv">
                            </div>
                            <span>A</span>
                            </div><div class="graph">
                            <span>2000</span>
                            <div class="graph_height">
                            </div>
                            <span>Mar</span>
                            </div><div class="graph">
                            <span>4000</span>
                            <div class="graph_height heightdiv">
                            </div>
                            <span>A</span>
                            </div><div class="graph">
                            <span>2000</span>
                            <div class="graph_height">
                            </div>
                            <span>Mar</span>
                            </div><div class="graph">
                            <span>4000</span>
                            <div class="graph_height heightdiv">
                            </div>
                            <span>A</span>
                            </div>
                            </div>
                        </div>
                    </div>
                    <!-- column -->
                    <div class="col-lg-6">
                    
                        <div class="card sales ">
                            <div class="card-body">
                                <h2>Department Wise Data <span style="float: right;  color: #9999; font-size:26px;">280</span></h2>
                                
                            </div>
                            <div class="graph_div department doctdashboard">
                            <div class="graph">
                            <span>2000</span>
                            <div class="graph_height">
                            </div>
                            <span>Dep1</span>
                            </div><div class="graph">
                            <span>4000</span>
                            <div class="graph_height heightdiv">
                            </div>
                            <span>Dep2</span>
                            </div><div class="graph">
                            <span>2000</span>
                            <div class="graph_height">
                            </div>
                            <span>Dep3</span>
                            </div><div class="graph">
                            <span>4000</span>
                            <div class="graph_height heightdiv">
                            </div>
                            <span>Dep4</span>
                            </div><div class="graph">
                            <span>2000</span>
                            <div class="graph_height">
                            </div>
                            <span>Dep5</span>
                            </div>
                            </div>
                        </div>
                    </div>
                    <!-- column -->
                    <!-- <div class="col-lg-6">
                        <div class="card">
                        <div class="card-body">
                                <h2>New Messages </h2>
                                
                            </div>
                        <div class="newmessage">
                    <img src="assets/images/booking_call.png">
                            <div class="message_content">
                            <h4>148 total New Messages</h4>
                            <a href="#">click to check Message</a>
                            </div></div>	
                        <label class="timm"><i class="fa fa-clock-o"></i>10:00 am (Last Updated)<span style="float:right">24/03/2020</span></label  
                            
                        ></div>
                        
                    </div>
                    
                        <div class="col-lg-6">
                        <div class="card">
                        <div class="card-body">
                                <h2>New Messages </h2>
                                
                            </div>
                        <div class="newmessage">
                    <img src="assets/images/booking_call.png">
                            <div class="message_content">
                            <h4>148 total New Messages</h4>
                            <a href="#">click to check Message</a>
                            <span class="fa fa-phone" ></span>
                            </div></div>	
                        <label class="timm"><i class="fa fa-clock-o"></i>10:00 am (Last Updated)<span style="float:right">View</span></label> </div>
                        
                    </div> -->
                </div>

            
                <!-- </div>
                </div>   
                        
                    </div>
                    
                        
                    </div>
                </div> -->
</div>
@endsection
                




                
                
            </section>
        </div>
    </div>
</div>
        