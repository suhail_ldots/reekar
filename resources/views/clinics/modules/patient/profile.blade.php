@extends('clinics.layouts.patient_main')
@section('content')
<div class="container fixed-mar-top-inner"> 
      <div class="row">
        <div class="col-12 col-lg-12"> 
          <form class="formbox" action="{{ route('patient.profile.update') }}" id="upload_form" method="post" enctype="multipart/form-data">
          @csrf
          <div class="col-12 col-lg-12 text-center">
    
           <div class="user-more">
           <!-- <img alt="User Pic" class="rounded-circle profile_users_img" @if($user->profile_image) src="{{ asset($user->profile_image) }}" @else src="https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg" @endif id="profile-image1"  width="150" height="150" > -->
           <img alt="User Pic" class="rounded-circle profile_users_img" width="150" height="150" src="{{ @fopen(\Url('storage/app/images/doctor/profile/').'/'.Auth::user()->profile_pic, 'r') ? \Url('storage/app/images/doctor/profile/').'/'.Auth::user()->profile_pic : asset('public/nobody_user.jpg') }}" id="profile-image1"  width="150" height="150" >
           </div>
          </div>
          

          <div class="col-12 col-md-8 mx-auto white mt-2">
          @if (count($errors) > 0)
          <div class="row">
            <div class="error" style="color:red;">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          </div>
        @endif
         
           <div class="row">
              <div class="col-md-6">
                  @if($user)
                  <input type="hidden" name="id" value="{{ $user->id }}">
                  @endif
                  <div class="md-form mb-0 form-group">
                      <label for="name" class="">Your First Name</label>
                      <input type="text" name="first_name" value="{!! Auth::user()->first_name !!}" class="form-control">
                      <div class="help-block"></div>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="md-form mb-0 form-group">
                      <label for="name" class="">Your Last Name</label>
                      <input type="text" name="last_name" value="{!! Auth::user()->last_name !!}" class="form-control">
                      <div class="help-block"></div>
                  </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                  <div class="md-form mb-0 form-group">
                    <label for="gndr" class="">Your Gender</label>
                    <!-- <input type="radio" name="" id="" class="form-control">Male
                    <input type="radio" name="" id="" class="form-control">Female -->
                    <input type="text" name="gender" value="{{Auth::user()->gender}}" class="form-control">
                    <div class="help-block"></div>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="md-form mb-0 form-group">
                    <label for="age" class="">Your Age<small>( in years)</small></label>
                    <!-- <input type="radio" name="" id="" class="form-control">Male
                    <input type="radio" name="" id="" class="form-control">Female -->
                    <input type="text" name="age" value="{{Auth::user()->age}}" class="form-control">
                    <div class="help-block"></div>
                  </div>
              </div>
            </div>
           <div class="row">
              <div class="col-md-12">
                  <div class="md-form mb-0 form-group">
                    <label for="email" class="">Email</label>
                    <input type="text" name="email" value="{!! Auth::user()->email !!}" class="form-control" readonly="readonly" />
                    <div class="help-block"></div>
                  </div>
              </div>
            </div>
           <div class="row">
              <div class="col-md-12">
                  <div class="md-form mb-0 form-group">
                      <label for="name" class="">Your Blood Group</label><br>
                      <input type="radio" name="blood_group" value="A+" {{ $user->blood_group == 'A+' ? 'checked' : ''}}> A+ &nbsp&nbsp
                      <input type="radio" name="blood_group" value="A-" {{ $user->blood_group == 'A-' ? 'checked' : ''}}> A- &nbsp&nbsp
                      <input type="radio" name="blood_group" value="B+" {{ $user->blood_group == 'B+' ? 'checked' : ''}}> B+ &nbsp&nbsp
                      <input type="radio" name="blood_group" value="B-" {{ $user->blood_group == 'B-' ? 'checked' : ''}}> B- &nbsp&nbsp
                      <input type="radio" name="blood_group" value="AB+" {{ $user->blood_group == 'AB+' ? 'checked' : ''}}> AB+ &nbsp&nbsp
                      <input type="radio" name="blood_group" value="AB-" {{ $user->blood_group == 'AB-' ? 'checked' : ''}}> AB- &nbsp&nbsp
                      <input type="radio" name="blood_group" value="O+" {{ $user->blood_group == 'O+' ? 'checked' : ''}}> O+ &nbsp&nbsp
                      <input type="radio" name="blood_group" value="O-" {{ $user->blood_group == 'O-' ? 'checked' : ''}}> O-
                  </div>
                  <div class="help-block"></div>
                </div>
            </div>
            
           <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                      <label>Country Code</label>
                      <select name="country_code" id="country_code" class="form-control">
                          <option value="">Select Country Code</option>
                          @if(CountryCodes())
                              @foreach(CountryCodes() as $code)
                              <option value="{{$code->dial_code}}" {{isset($user->country_code) && ($user->country_code == $code->dial_code) ? "selected" :''}}>{{$code->dial_code}}</option>
                              @endforeach
                          @endif
                      </select>
                      <div class="help-block"></div>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="md-form mb-0 form-group">
                  <label for="email" class="">Phone</label>
                      <input type="text" name="mobile" value="{{Auth::user()->mobile}}" class="form-control">
                      <div class="help-block"></div>
                  </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6 mb-3">
                  <div class="md-form mb-0 form-group">
                      <label for="form1" >Address</label>
                      <input type="text" id="addr_input_location" name="address" class="form-control" value="{{old('address', $user->address)}}">
                      <div class="help-block"></div>
                      <input type="hidden" name="user_lat" value="{{old('user_lat')}}" id="user_lat" >
                      <input type="hidden" name="user_long" value="{{old('user_long')}}" id="user_long" >
                  </div>
              </div>
              <div class="col-md-6 mb-3">
                  <div class="md-form mb-0 form-group">
                      <label for="form1" >Country</label>
                      <select name="country" id="country" class="form-control">
                          <option value="">Select Country*</option>
                          @if(country())
                              @foreach(country() as $code)
                              <option value="{{$code->id}}" {{isset($user->country) && ($user->country == $code->id) ? "selected" :''}}>{{$code->name }}</option>
                              @endforeach
                          @endif
                      </select>                                   
                      <div class="help-block"></div>
                  </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 mb-3">
                  <div class="md-form mb-0 form-group">
                      <label for="form12" >City</label>
                      <input type="text" id="form12" name="city" class="form-control" value="{{old('city', $user->city)}}">
                  </div>
              </div>
              <div class="col-md-6 mb-3">
                  <div class="md-form mb-0 form-group">
                      <label for="form1" >Zipcode</label>
                      <input type="text" id="form12" name="zipcode" class="form-control" value="{{old('zipcode', $user->zipcode)}}">                                  
                      <div class="help-block"></div>
                  </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                  <small class="form-text text-muted">(<b>Hint: </b> Your password must be equal or more than 8 characters.)</small>
                      <!--  <label for="userpassword">Password</label><span class="required">*</span> -->
                      <input type="password" name="password" maxlength="250" id="userpassword"
                          class="form-control @error('password') is-invalid @enderror"
                          value="{{old('password')}}" placeholder="Password">
                      <div class="help-block"></div>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                  <small class="form-text text-muted">&nbsp;</small>
                      <!--   <label for="password_confirmation" >Confirm Password</label><span class="required">*</span> -->
                      <input type="password" name="password_confirmation" maxlength="250" id="password_confirmation"
                          class="form-control @error('password_confirmation') is-invalid @enderror"
                          value="{{old('password_confirmation')}}" placeholder="Confirm Password">
                      <div class="help-block"></div>
                  </div>
              </div>
          </div>

              <!-- <div class="enter-section">
                 <ul>
                   <li class="list-group-user">                                           
                     <div class="col-12 col-lg-12 text-center my-4">
                      <button type="submit" class="btn btn-primary">Save</button>
                     </div>
                    </li>

                 </ul>
              </div> -->
          <!-- </div> -->
          <div class="row">
              <div class="col-12">
                  <div class="p-20">
                      <button type="submit"
                          class="btn btn-primary waves-effect waves-light saveBtn">@lang('l.save')</button>
                      <a href="{{URL::previous()}}">
                          <button type="button"
                              class="btn btn-secondary waves-effect m-l-5">@lang('l.cancel')</button>
                      </a>
                      <div id="ajaxloader" style="display: none;"><img
                              src="{{ asset('public/admin/images/ajax-loader.gif')}}" /> Processing...
                      </div>

                  </div>
              </div>
          </div>
          <div class="clearfix"></div>
        </form>
      </div>
  </div>
</div>
<!--container end here-->

<div class="modal fade" id="img-modal" tabindex="-1" role="dialog" aria-labelledby="edit-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header p-2 text-center primary-color-dark">
                <h2 class="modal-title white-text w-100" id="edit-modal-label">Change profile picture</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class='white-text' aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="alert" id="message" style="display: none"></div>
            <form method="post" id="update_form"  enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <div class="row">
                        <div class="col-12 col-md-8 mx-auto">
                           
                            <div class="w-100 text-center">
                                <!-- <input type="file" name="select_file" id="select_file"/> -->
                                <input class="file-path validate" type="file" name="profile_image" id="select_file" placeholder="Choose File">
                            </div>
                             <div class="row">
                                  <div class="col-12 col-md-12 text-center py-3">
                                    <img id="preview" height="100" class="img-responsive"/>
                                  </div>
                             </div>                     
                            <div class="text-center py-2 white">
                                <input type="submit" name="upload" id="upload" class="btn btn-primary text-white waves-effect waves-light" value="Upload">
                            </div>
                        </div>
                    </div>
                </div>
                <!--form group end-->
            </form>
            <span id="uploaded_image"></span>
        </div>
    </div>
</div>
</div>

@endsection

@section('js')
<script type="text/javascript">
      //Auto Complete Address
      var searchInput = 'addr_input_location';  
        $(document).ready(function () {  
            var autocomplete;
            autocomplete = new google.maps.places.Autocomplete((document.getElementById(searchInput)), {
                types: ["geocode"],
            });
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var near_place = autocomplete.getPlace();
                //assign lat long only for user location value to 
                document.getElementById('user_lat').value = near_place.geometry.location.lat();
                document.getElementById('user_long').value = near_place.geometry.location.lng();
                           
            });   
        });
</script>
<script type="text/javascript">

    // $("#home").hover(function(){
    // $('#img-modal').modal('show');
        // });
    $(".profile_users_img").hover(function(){
      //alert("Df")
        $(this).disable = true;
    })   
    $(".profile_users_img").click(function(){
            $('#img-modal').modal('show');
        })
    $('#update_form').on('submit', function(event){
      event.preventDefault();
      $.ajax({
        url:"{{ route('change_profile_pic') }}",

        method:"POST",
        data:new FormData(this),
        dataType:'JSON',
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function() {
          $('#message').css('display', 'block');
            $(".ajaxmsg").html("<div class='alert alert-info'><p>Please wait...</p></div>");
            $('#upload').prop('disabled', true);
        },

        success:function(data)
        {
          $(".ajaxmsg").empty();
          $('#message').css('display', 'block');
          $('#message').html(data.message);
          $('#message').addClass(data.class_name);
        // $('#uploaded_image').html(data.uploaded_image);
          if(data.status ==200){
            location.reload();
          }
        },
        error:function(data)
        {
            $('#upload').prop('disabled', false);
            var toAppend = '';
             $(".ajaxmsg").empty();
            console.log(data.responseJSON.errors);
           //alert(data.errors);
            $.each(data.responseJSON.errors, function(key, error) {
                 toAppend +='<div class="alert alert-success bg-danger">'+ error +'</div>';
            });
            $(".ajaxmsg").append(toAppend).fadeOut(5000);

        }
      })
    });
 
    // });
    $(function() {
    $("#select_file").on('change', function() {
      // Display image on the page for viewing
      readURL(this, "preview");

    });
});  

function readURL(input, tar) {
  if (input.files && input.files[0]) { // got sth

    // Clear image container
    $("#" + tar).removeAttr('src');

    $.each(input.files, function(index, ff) // loop each image 
      {

        var reader = new FileReader();

        // Put image in created image tags
        reader.onload = function(e) {
          $('#' + tar).attr('src', e.target.result);
        }

        reader.readAsDataURL(ff);

      });
  }
}  
</script>

@endsection
