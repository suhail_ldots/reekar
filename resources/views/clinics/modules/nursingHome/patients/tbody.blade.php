@if(!empty($items))
@forelse($items as $item)
<tr>
<td>{{(($items->currentPage() * $items->perPage()) + $loop->iteration) - $items->perPage()}}</td>
    <td>{{ $item->full_name }}</td>
    <td>{{ $item->gender }}</td>
    <td>{{ $item->age }}</td>
    <td>{{ $item->email }}</td>
    <td>{{ $item->mobile }}</td>
    <td>{{ $item->user_country->name }}</td>
    <td>{{ $item->city }}</td>
    <td>{{ $item->address }}</td>
        
</tr>
@empty
<tr>
    @if($items->currentPage() == $items->lastPage())
    <td colspan="9" style="text-align:center"> Patient {{ __('l.not_found') }}</td>
    @else
        <td colspan="9" style="text-align:center"> {{ __('l.no_more_record') }}</td>
    @endif
</tr>
@endforelse
@else
<tr>
    <td colspan="9" style="text-align:center">Patient {{ __('l.not_found') }}</td>
</tr>
@endif