   
        @extends('clinics.layouts.nursing_home_main')
        @section('content')
       
        <div class="content-wrap">
            <div class="main">
                <div class="container-fluid m-t-15">
                @include('clinics.partials.messages')
                    <!-- /# row -->
                    <section id="main-content">                      
                     
                        <div class="row mt-20">
                            <div class="col-lg-7 col-sm-6 col-md-5 p-r-0 ">
                            <div class="page-header">
                                <div class="page-title">
                                    <h1>All Doctors</h1>
                                    <a href="{{ route('nursing-doctor.create')}}">
                                <button class="btn btn-primary arrow-none waves-effect waves-light" type="button">
                                    <i class="fa fa-plus"></i> Add Doctor
                                </button>
                            </a>
                                </div>
                            </div>
                        </div>
								  
						<div class="col-lg-5 col-sm-6 col-md-7 labeldiv">
                             <!-- <label>Filter <select class="selectbtn"><option>Today's Appointment</option></select> -->
                             <label>&nbsp 
							 <span class="fa fa-bars adddiv">
							 </span>
                             
                             </label>
                        </div>
                                    
                                </div>
                        <div class="row mt-20">
                        <div class="col-lg-12">

                        @forelse($items as $item)
                        <div class="doctor_apointment card today_appointment">
                            <h3 class="pull-left">Dr. {{ $item->first_name.' '.$item->last_name }}</h3>
                            <div>
                            <!-- <label><i class="fa fa-male"></i>Mr. Amit Kumar</label> -->
                            <label><i class="fa fa-map-marker"></i>{{ $item->address }}</label>
                            <!-- <label><i class="fa fa-calendar-minus-o"></i>31 March, 2020</label>  -->
                            </div>
                            
                            <p>"{{ $item->doctor->about_doc }}"</p><div>
                            <!-- <label>Scheduled on   </label>
                            <label><i class="fa fa-calendar"></i>Monday</label>
                            <label><i class="fa fa-calendar-minus-o"></i>10:00am - 11:00am </label> -->
                            </div>
                        
                            <button class="pull-right btn btn-primary"><a href="{{ route('nursing-doctor.edit', $item->id) }}" style="color: #fff !important;">Edit</a></button>&nbsp  
                            <button class="pull-right btn btn-primary"><a href="{{ route('nursing-doctor.show', $item->id) }}" style="color: #fff !important;">View</a></button>  
                            @if($item->status == 1)
                            <a href="{{ route('nursing-doctor.status',[ 'id' => $item->id,'status' => $item->status])}}"
                                 class="pull-right btn btn-primary" title="Active"><i class="fa fa-check"></i></a>
                            @else
                            <a href="{{ route('nursing-doctor.status',[ 'id' => $item->id,'status' => $item->status])}}"
                            class="pull-right btn btn-primary" title="Inactive"><i class="fa fa-times"></i></a>
                            @endif 
                        </div>
                        @empty
                        <div class="doctor_apointment card today_appointment">
                            <p>Record not found</p>
                        </div>
                        @endforelse
                        
                        </div>
                        </div>     
                            </div>
                        </div>
                    </div>                    
                    </section>
                </div>
            </div>
        </div>
@endsection
@section('script')
<script>
    $(document).ready(function(){
        $(".adddiv").click(function(){
        $(".today_appointment").toggleClass("divwidth");
        
        });
    });
    
</script>
@endsection
