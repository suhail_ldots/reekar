@extends('clinics.layouts.nursing_home_main')
@section('head')

@endsection
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
        @include('clinics.partials.messages')
        <div class="mt-20">
            <div class="col-lg-7 col-sm-6 col-md-5 p-r-0 ">
            <div class="page-header">
                <div class="page-title">
                    <h1>{{ $item ? 'Edit' : 'Add'}} Doctor</h1>
                </div>
            </div>
        </div>
   <!-- Section: Blog v.3 -->
    @if (\Session::has('message'))
     <div class="alert alert-danger alert-dismissible customalertradhe" id="success-alert">
       <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
       <strong></strong>{{ \Session::get('message') }}
     </div>
    @endif
    <section  id="main-content">
      <div class="row">
        <div class="card" style="width:100%">
        <form class="formbox" action="{{route('nursing-doctor.store')}}" method="post" enctype="multipart/form-data">
          @csrf
        {{--  <!-- <div class="col-md-3 col-xs-12 col-sm-3 col-lg-3">
           <div class="user-more">
           <img alt="User Pic" src="https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg" id="profile-image1" class="rounded-circle" width="150" >
           <h6>{{Auth::user()->name}} </h6>
           </div>
          </div> --> --}}
          <div class="col-md-12" style="margin:auto">           
              <div class="enter-section">
              <div id="admsg" style="display:none"></div>
              <div class="row">
              <div class="col-lg-6">
                  <h6 class="pb-0 h5-responsive">
                  <strong>Medical Council Registration No</strong><span class="required">*</span></h6>
                  <div class=" input-container {{ $errors->has('reg_no') ? ' has-error' : '' }}">
                      <input type="text" id="reg_no" name="reg_no" class="form-control" value="{{ $item ?  $item->doctor->reg_no : old('reg_no')}}" placeholder="Enter Doctor Registration No">
                      @if ($errors->has('reg_no'))
                            <span class="help-block">
                                <strong>{{ $errors->first('reg_no') }}</strong>
                            </span>
                      @endif
                    </div></div>
                    
                      @if($item)
                      <input type="hidden" name="id" value="{{ $item->id }}">
                      @endif
					  <div class="col-lg-6">
                        <h6 class="pb-0 h5-responsive">
                        <strong>First Name</strong><span class="required">*</span></h6>
                        <div class=" input-container {{ $errors->has('first_name') ? ' has-error' : '' }}">
                           <input type="text" name="first_name" id="first name" class="form-control" value="{{ $item ?  $item->first_name : old('first_name')}}" placeholder="Enter Doctor First Name">
                            @if ($errors->has('first_name'))
                                 <span class="help-block">
                                     <strong>{{ $errors->first('first_name') }}</strong>
                                 </span>
                             @endif
                         </div></div>
						 <div class="col-lg-6">
                        <h6 class="pb-0 h5-responsive">
                        <strong>Last Name</strong></h6>
                        <div class=" input-container {{ $errors->has('last_name') ? ' has-error' : '' }}">
                           <input type="text" name="last_name" id="last name" class="form-control" value="{{ $item ?  $item->last_name : old('last_name')}}" placeholder="Enter Doctor Last Name">
                            @if ($errors->has('last_name'))
                                 <span class="help-block">
                                     <strong>{{ $errors->first('last_name') }}</strong>
                                 </span>
                             @endif
                         </div>
						 </div>
                   <div class="col-lg-6">
                          <h6 class="pb-0 h5-responsive">
                          <strong>Department</strong><span class="required">*</span></h6>
                          <div class=" input-container {{ $errors->has('department') ? ' has-error' : '' }}">
                             <select name="department" class="classic form-control" value="{{ old('department') }}">

                              @foreach(departments() as $department)
                                <option value="{{ $department->id }}" {{ isset($item->doctor) && $item->doctor->department_id == $department->id ? 'selected' : '' }} >{{ $department->name }}</option>
                              @endforeach 
                              </select>
                              @if ($errors->has('department'))
                                   <span class="help-block">
                                       <strong>{{ $errors->first('department') }}</strong>
                                   </span>
                               @endif
                           </div>
						   </div>
               <div class="col-lg-6">
                          <h6 class="pb-0 h5-responsive">
                          <strong>Email</strong><span class="required">*</span></h6>
                          <div class=" input-container {{ $errors->has('email') ? ' has-error' : '' }}">
                             <input type="text" name="email" id="email" class="form-control" value="{{$item ? $item->email :  old('email')}}" placeholder="Enter Doctor Email">
                              <!-- <label >Enter Doctor Email</label> -->
                              @if ($errors->has('email'))
                                   <span class="help-block">
                                       <strong>{{ $errors->first('email') }}</strong>
                                   </span>
                               @endif
                           </div>
                </div>
						   <div class="col-lg-6">
                          <h6 class="pb-0 h5-responsive">
                          <strong>Speciality</strong><span class="required">*</span></h6>
                          <div class=" input-container {{ $errors->has('speciality') ? ' has-error' : '' }}">
                             <select name="speciality" class="classic form-control">

                              @foreach(speciality() as $speciality)
                                <option value="{{ $speciality->id }}" {{ isset($item->doctor->speciality) && $item->doctor->speciality == $speciality->id ? 'selected' : '' }}>{{ $speciality->name }}</option>
                              @endforeach 
                              </select>
                              @if ($errors->has('speciality'))
                                   <span class="help-block">
                                       <strong>{{ $errors->first('speciality') }}</strong>
                                   </span>
                               @endif
                           </div></div>
                     
                       <div class="col-lg-6">
                            <h6 class="pb-0 h5-responsive">
                            <strong>Mobile*</strong></h6>
                            <div class=" input-container {{ $errors->has('mobile') ? ' has-error' : '' }}">
                               <input type="text" name="mobile" id="mobile" class="form-control"  value="{{$item ? $item->mobile : old('mobile')}}" maxlength="13" placeholder="Enter Doctor Mobile" onkeyup="textNumeric(this.value, 'The mobile must be a number.')" >
                               <!-- <p id="p6"></p> -->
                                <!-- <label >Enter Doctor Email</label> -->
                                 @if($errors->has('mobile'))
                                     <span class="help-block">
                                         <strong>{{$errors->first('mobile')}}</strong>
                                     </span>
                                 @endif
                            </div></div>
                       <div class="col-lg-6">
                       <h6 class="pb-0 h5-responsive">
                       <strong> Qualifications<span>*</span></strong></h6>
                       <div class=" input-container {{ $errors->has('degree') ? ' has-error' : '' }}">
                          <input type="text" name="degree" id="degree" class="form-control" value="{{isset($item->doctor) ? $item->doctor->degree : old('degree')}}" placeholder="Enter Doctor Qualifications">
                           <!-- <label >Enter Doctor Degrees</label> -->
                           @if ($errors->has('degree'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('degree') }}</strong>
                                </span>
                            @endif
                        </div></div>
                    <div class="col-lg-6">
                         <h6 class="pb-0 h5-responsive">
                         <strong> Experience</strong>(Years)</h6>
                         <div class=" input-container {{ $errors->has('experience') ? ' has-error' : '' }}">
                            <input type="text" name="experience" id="experience" class="form-control" value="{{ isset($item->doctor) ? $item->doctor->experience : old('experience')}}" placeholder="Enter Doctor Experience in Years">
                             <!-- <label >Enter Doctor Experience</label> -->
                             @if ($errors->has('experience'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('experience') }}</strong>
                                  </span>
                              @endif
                          </div></div>
                      <div class="col-lg-6">
                          <h6 class="pb-0 h5-responsive">
                          <strong> Past Experiences</strong></h6>
                          <div class="input-container {{ $errors->has('past_experiences') ? ' has-error' : '' }}">
                            <input type="text" name="past_experiences" id="past_experiences" class="form-control" value="{{isset($item->doctor) ? $item->doctor->past_experiences : old('past_experiences')}}" placeholder="Enter Doctor Past Experiences">
                              <!-- <label >Enter Doctor Experience</label> -->
                              @if ($errors->has('past_experiences'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('past_experiences') }}</strong>
                                  </span>
                              @endif
                          </div></div>

                        <div class="col-lg-6">
                        <h6 class="contect_form1 h5-responsive">
                        <strong>{{ __('featured_plan.country') }}</strong><span class="required">*</span></h6>
                        <div class="input-container {{ $errors->has('country') ? ' has-error' : '' }}">                        
                            <select name="country" class="classic form-control">

                            @foreach(country() as $country)
                              <option value="{{ $country->id }}" {{ isset($item->country) &&  $item->country == $country->id ? 'selected' : ''}}>{{ $country->name }}</option>
                            @endforeach 
                            </select>
                            @error('country')
                            <span class="help-block required" role="alert">
                                <li>{{ $message }}</li>
                            </span>
                            @enderror
                        </div></div>
						<div class="col-lg-6">
                        <h6 class="pb-0 h5-responsive">
                        <strong>{{ __('featured_plan.city') }}</strong></h6>
                        <div class="input-container {{ $errors->has('city') ? ' has-error' : '' }}"> 
                            <input type="text"  name="city" class="classic form-control" value="{{ isset($item->city) ? $item->city : old('city') }}">
                            <div class="help-block"></div>
                            @error('city')
                            <span class="help-block required" role="alert">
                                <li>{{ $message }}</li>
                            </span>
                            @enderror
                        </div></div>
						<div class="col-lg-6">
                        <h6 class="h5-responsive">
                        <strong>{{ __('featured_plan.address') }}</strong><span class="required">*</span></h6>
                        <div class="input-container {{ $errors->has('address') ? ' has-error' : '' }}"> 
                          <input type="text" name="address" id="addr_input_location" value="{{ isset($item->address) ? $item->address  : old('address') }}" class="classic form-control" autocomplete="off">
                          <div class="help-block"></div>
                          @error('address')
                          <span class="help-block">
                              <strong>{{ $message }}</strong>
                          </span>
                          @enderror
                          <input type="hidden" name="user_lat" value="{{ isset($item->user_lat) ? $item->user_lat : old('user_lat')}}" id="user_lat" >
                          <input type="hidden" name="user_long" value="{{ isset($item->user_long) ? $item->user_long  : old('user_long')}}" id="user_long" >
                          
                      </div></div>
					  <div class="col-lg-6">
                        <h6 class="pb-0 h5-responsive">                        
                        <strong>{{ __('featured_plan.zipcode') }}</strong></h6>
                        <div class="input-container {{ $errors->has('zipcode') ? ' has-error' : '' }}">
                            <input type="text"  name="zipcode" class="classic form-control" value="{{ isset($item->zipcode) ? $item->zipcode : old('zipcode') }}">
                            <div class="help-block"></div>
                            @error('zipcode')
                            <span class="help-block required" role="alert">
                                <li>{{ $message }}</li>
                            </span>
                            @enderror
                        </div>
                      </div>
                      <div class="col-lg-6">
                       <h6 class="pb-0 h5-responsive">
                       <strong> Password</strong><span class="required">*</span></h6>
                       <div class=" input-container {{ $errors->has('password') ? ' has-error' : '' }}">
                          <input type="password" name="password" class="form-control" value="{{old('password')}}" placeholder="Enter Doctor Password...">
                           <!-- <label >Enter Doctor password</label> -->
                            @error('password')
                                <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div></div>
                    <div class="col-lg-6">
                       <h6 class="pb-0 h5-responsive">
                       <strong> Re-Type Password</strong></h6>
                       <div class=" input-container {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                          <input type="password" name="password_confirmation" class="form-control" value="{{old('password_confirmation')}}" placeholder="Enter a confirm password...">
                           <!-- <label >Enter Doctor password</label> -->
                           @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div></div>
						</div>
           
                    @php
                    if(isset($item->doctor)){
                      $working_days = explode(",",$item->doctor->working_days);
                      if($item->doctor->availability){                      
                        $avail = optional($item->doctor)->availability;     
                      }else{
                        $avail =[];
                      }
                    }else{
                      $working_days = [];
                      $avail = [];
                    }
                     
                    @endphp
                       <h6 class="pb-3 h5-responsive" style="margin-top:20px">
                       <strong >Availability</strong><span class="required">*</span></h6>
                       <div class=" input-container {{ $errors->has('availability') ? ' has-error' : '' }}">
                          @if($errors->has('availability'))
                               <span class="help-block">
                                   <strong>{{ $errors->first('availability') }}</strong>
                               </span>
                           @endif
                      </div>
                      <div class="availclass">
                        <div class="row justify-content-between">
                          <div class="col-4">
                          <div class="form-check mb-3">
                           <input type="checkbox" class="form-check-input daycheck" id="materialChecked2" name="availability[1]" value="1" @if(old('availability.1') == 1 || in_array(1,$working_days)) checked @endif>
                          <label class="form-check-label" for="materialChecked2">Monday</label>
                          </div>
                          </div>
                          
                          <div class="col-4">
                            <div class="form-group">
                              <input type="number" min="0" step="1" class="form-control fees" name="fees[1]" placeholder="Fees" @if(in_array(1,$working_days)) value="{{old('fees.1',$avail[1]['fees'])}}" @else value="{{old('fees.1')}}" @endif disabled>
                            </div>
                          </div>

                      </div>

                    <div class="row justify-content-between">
                        <div class="col-6">
                          <div class="form-group label-floating">
                              <h3 class="pb-0 h3-responsive">
                              <strong class="h6"> Available From</strong></h3>
                              <div class="{{ $errors->has('availability_from.morning.1') ? ' has-error' : '' }}">
                              <input type="text" id="time" name="availability_from['morning'][1]" class="form-control time_from" data-dtp="" placeholder="Select Time" value="{{ in_array(1,$working_days) ? $avail[1]['availability_from'] : old('availability_from.1') }}" disabled>
                              <!-- <label for="time" class="control-label">Available From</label> -->
                                   @if ($errors->has('availability_from.morning.1'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('availability_from.morning.1') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                          <div class="form-group label-floating">
                            <h3 class="pb-0 h3-responsive">
                            <strong class="h6"> Available To</strong></h3>
                            <div class="{{ $errors->has('availability_to.morning.1') ? ' has-error' : '' }}">
                            <input type="text" id="time2" name="availability_to['morning'][1]" class="form-control time_to" data-dtp="" placeholder="Select Time" value="{{ in_array(1,$working_days) ? $avail[1]['morning']['availability_to'] : old('availability_to.1')}}" disabled>
                            <!-- <label for="time" class="control-label">Available To</label> -->
                                 @if ($errors->has('availability_to.morning.1'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('availability_to.morning.1') }}</strong>
                                      </span>
                                  @endif
                              </div>
                              </div>
                        </div>
                    </div>
                  </div>
                  <hr>
                  <div class="availclass">
                      <div class="row justify-content-between">
                      <div class="col-4">
                      <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input daycheck" id="materialChecked3" name="availability[2]" value="2" @if(old('availability.2') == 2 || in_array(2,$working_days)) checked @endif>
                        <label class="form-check-label" for="materialChecked3">Tuesday</label>
                      </div>
                  </div>

                    <div class="col-4">
                      <div class="form-group">
                          <input type="number" min="0" step="1" class="form-control fees" name="fees[2]"  placeholder="Fees" @if(in_array(2,$working_days)) value="{{old('fees.2',$avail[2]['fees'])}}" @else value="{{old('fees.2')}}" @endif disabled>
                        </div>
                    </div>
                    </div>
                    <div class="row justify-content-between">
                        <div class="col-6">
                          <div class="form-group label-floating">
                              <h3 class="pb-0 h3-responsive">
                              <strong class="h6"> Available From</strong></h3>
                              <div class="{{ $errors->has('availability_from.2') ? ' has-error' : '' }}">
                              <input type="text" id="time3" name="availability_from[2]" class="form-control time_from" data-dtp="" placeholder="Select Time" value="{{ in_array(2,$working_days) ? $avail[2]['availability_from'] : old('availability_from.2') }}" disabled>
                              <!-- <label for="time" class="control-label">Available From</label> -->
                                   @if ($errors->has('availability_from.2'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('availability_from.2') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                        </div>
                        <div class="col-6">
                          <div class="form-group label-floating">
                            <h3 class="pb-0 h3-responsive">
                            <strong class="h6"> Available To</strong></h3>
                            <div class="{{ $errors->has('availability_to.2') ? ' has-error' : '' }}">
                            <input type="text" id="time4" name="availability_to[2]" class="form-control time_to" data-dtp="" placeholder="Select Time" value="{{ in_array(2,$working_days) ? $avail[2]['availability_to'] : old('availability_to.2') }}" disabled>
                            <!-- <label for="time" class="control-label">Available To</label> -->
                                 @if ($errors->has('availability_to.2'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('availability_to.2') }}</strong>
                                      </span>
                                  @endif
                              </div>
                              </div>
                        </div>
                    </div>
                  </div>
                      <hr>
                      <div class="availclass">
                      <div class="row justify-content-between">
                      <div class="col-4">
                      <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input daycheck" id="materialChecked4" name="availability[3]" value="3" @if(old('availability.3') == 3 || in_array(3,$working_days)) checked @endif>
                        <label class="form-check-label" for="materialChecked4">Wednesday</label>
                      </div>
                    </div>
                    <div class="col-4">
                      <div class="form-group">
                        <input type="number" min="0" step="1" class="form-control fees" name="fees[3]"  placeholder="Fees" @if(in_array(3,$working_days)) value="{{old('fees.3',$avail[3]['fees'])}}" @else value="{{old('fees.3')}}" @endif disabled>
                      </div>
                    </div>
                  </div>
                  <div class="row justify-content-between">
                      <div class="col-6">
                        <div class="form-group label-floating">
                            <h3 class="pb-0 h3-responsive">
                            <strong class="h6"> Available From</strong></h3>
                            <div class="{{ $errors->has('availability_from.3') ? ' has-error' : '' }}">
                            <input type="text" id="time15" name="availability_from[3]" class="form-control time_from" data-dtp="" placeholder="Select Time" value="{{ !in_array(3,$working_days) ? old('availability_from.3') : $avail[3]['availability_from'] }}" disabled>
                            <!-- <label for="time" class="control-label">Available From</label> -->
                                 @if ($errors->has('availability_from.3'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('availability_from.3') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                      </div>
                      <div class="col-6">
                        <div class="form-group label-floating">
                          <h3 class="pb-0 h3-responsive">
                          <strong class="h6"> Available To</strong></h3>
                          <div class="{{ $errors->has('availability_to.3') ? ' has-error' : '' }}">
                          <input type="text" id="time7" name="availability_to[3]" class="form-control time_to" data-dtp="" placeholder="Select Time" value="{{ !in_array(3,$working_days) ? old('availability_to.3') : $avail[3]['availability_to'] }}" disabled>
                          <!-- <label for="time" class="control-label">Available To</label> -->
                               @if ($errors->has('availability_to.3'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('availability_to.3') }}</strong>
                                    </span>
                                @endif
                            </div>
                            </div>
                      </div>
                  </div>
                </div>
                    <hr>
                    <div class="availclass">
                    <div class="row justify-content-between">
                    <div class="col-4">
                      <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input daycheck" id="materialChecked5" name="availability[4]" value="4" @if(old('availability.4') == 4 || in_array(4,$working_days)) checked @endif>
                        <label class="form-check-label" for="materialChecked5">Thursday</label>
                      </div>
                    </div>
                    <div class="col-4">
                      <div class="form-group">
                        <input type="number" min="0" step="1" class="form-control fees" name="fees[4]"  placeholder="Fees" @if(in_array(4,$working_days)) value="{{old('fees.4',$avail[4]['fees'])}}" @else value="{{old('fees.4')}}" @endif disabled>
                      </div>
                    </div>
                  </div>
                  <div class="row justify-content-between">
                      <div class="col-6">
                        <div class="form-group label-floating">
                            <h3 class="pb-0 h3-responsive">
                            <strong class="h6"> Available From</strong></h3>
                            <div class="{{ $errors->has('availability_from.4') ? ' has-error' : '' }}">
                            <input type="text" id="time8" name="availability_from[4]" class="form-control time_from" data-dtp="" placeholder="Select Time" value="{{ !in_array(4,$working_days) ? old('availability_from.4') : $avail[4]['availability_from'] }}" disabled>
                            <!-- <label for="time" class="control-label">Available From</label> -->
                                 @if ($errors->has('availability_from.4'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('availability_from.4') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                      </div>
                      <div class="col-6">
                        <div class="form-group label-floating">
                          <h3 class="pb-0 h3-responsive">
                          <strong class="h6"> Available To</strong></h3>
                          <div class="{{ $errors->has('availability_to.4') ? ' has-error' : '' }}">
                          <input type="text" id="time9" name="availability_to[4]" class="form-control time_to" data-dtp="" placeholder="Select Time" value="{{ !in_array(4,$working_days) ? old('availability_to.4') : $avail[4]['availability_to'] }}" disabled>
                          <!-- <label for="time" class="control-label">Available To</label> -->
                               @if ($errors->has('availability_to.4'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('availability_to.4') }}</strong>
                                    </span>
                                @endif
                            </div>
                            </div>
                      </div>
                  </div>
                </div>
                    <hr>
                    <div class="availclass">
                    <div class="row justify-content-between">
                    <div class="col-4">
                      <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input daycheck" id="materialChecked6" name="availability[5]" value="5" @if(old('availability.5') == 5 || in_array(5,$working_days)) checked @endif>
                        <label class="form-check-label" for="materialChecked6">Friday</label>
                      </div>
                    </div>
                    <div class="col-4">
                      <div class="form-group">
                        <input type="number" min="0" step="1" class="form-control fees" name="fees[5]"  placeholder="Fees" @if(in_array(5,$working_days)) value="{{old('fees.5',$avail[5]['fees'])}}" @else value="{{old('fees.5')}}" @endif disabled>
                      </div>
                    </div>
                  </div>
                  <div class="row justify-content-between">
                      <div class="col-6">
                        <div class="form-group label-floating">
                            <h3 class="pb-0 h3-responsive">
                            <strong class="h6"> Available From</strong></h3>
                            <div class="{{ $errors->has('availability_from.5') ? ' has-error' : '' }}">
                            <input type="text" id="time10" name="availability_from[5]" class="form-control time_from" data-dtp="" placeholder="Select Time" value="{{ !in_array(5,$working_days) ? old('availability_from.5') : $avail[5]['availability_from'] }}" disabled>
                            <!-- <label for="time" class="control-label">Available From</label> -->
                                 @if ($errors->has('availability_from.5'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('availability_from.5') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                      </div>
                      <div class="col-6">
                        <div class="form-group label-floating">
                          <h3 class="pb-0 h3-responsive">
                          <strong class="h6"> Available To</strong></h3>
                          <div class="{{ $errors->has('availability_to.5') ? ' has-error' : '' }}">
                          <input type="text" id="time11" name="availability_to[5]" class="form-control time_to" data-dtp="" placeholder="Select Time" value="{{ !in_array(5,$working_days) ? old('availability_to.5') : $avail[5]['availability_to'] }}" disabled>
                          <!-- <label for="time" class="control-label">Available To</label> -->
                               @if ($errors->has('availability_to.5'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('availability_to.5') }}</strong>
                                    </span>
                                @endif
                            </div>
                            </div>
                      </div>
                  </div>
                </div>
                    <hr>
                    <div class="availclass">
                    <div class="row justify-content-between">
                    <div class="col-4">
                      <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input daycheck" id="materialChecked7" name="availability[6]" value="6" @if(old('availability.6') == 6 || in_array(6,$working_days)) checked @endif>
                        <label class="form-check-label" for="materialChecked7">Saturday</label>
                      </div>
                    </div>
                    <div class="col-4">
                      <div class="form-group">
                        <input type="number" min="0" step="1" class="form-control fees" name="fees[6]"  placeholder="Fees" @if(in_array(6,$working_days)) value="{{old('fees.6',$avail[6]['fees'])}}" @else value="{{old('fees.6')}}" @endif disabled>
                      </div>
                    </div>
                  </div>
                  <div class="row justify-content-between">
                      <div class="col-6">
                        <div class="form-group label-floating">
                            <h3 class="pb-0 h3-responsive">
                            <strong class="h6"> Available From</strong></h3>
                            <div class="{{ $errors->has('availability_from.6') ? ' has-error' : '' }}">
                            <input type="text" id="time12" name="availability_from[6]" class="form-control time_from" data-dtp="" placeholder="Select Time" value="{{ !in_array(6,$working_days) ? old('availability_from.6') : $avail[6]['availability_from'] }}" disabled>
                            <!-- <label for="time" class="control-label">Available From</label> -->
                                 @if ($errors->has('availability_from.6'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('availability_from.6') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                      </div>
                      <div class="col-6">
                        <div class="form-group label-floating">
                          <h3 class="pb-0 h3-responsive">
                          <strong class="h6"> Available To</strong></h3>
                          <div class="{{ $errors->has('availability_to.6') ? ' has-error' : '' }}">
                          <input type="text" id="time13" name="availability_to[6]" class="form-control time_to" data-dtp="" placeholder="Select Time" value="{{ !in_array(6,$working_days) ? old('availability_to.6') : $avail[6]['availability_to'] }}" disabled>
                          <!-- <label for="time" class="control-label">Available To</label> -->
                               @if ($errors->has('availability_to.6'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('availability_to.6') }}</strong>
                                    </span>
                                @endif
                            </div>
                            </div>
                      </div>
                  </div>
                </div>
                    <hr>
                    <div class="availclass">
                    <div class="row justify-content-between">
                    <div class="col-4">
                      <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input daycheck" id="materialChecked8" name="availability[7]" value="7" @if(old('availability.7') == 7 || in_array(7,$working_days)) checked @endif>
                        <label class="form-check-label" for="materialChecked8">Sunday</label>
                      </div>
                    </div>
                    <div class="col-4">
                      <div class="form-group">
                        <input type="number" min="0" step="1" class="form-control fees" name="fees[7]"  placeholder="Fees" @if(in_array(7,$working_days)) value="{{old('fees.7',$avail[7]['fees'])}}" @else value="{{old('fees.7')}}" @endif disabled>
                      </div>
                    </div>
                  </div>
                  <div class="row justify-content-between">
                      <div class="col-6">
                        <div class="form-group label-floating">
                            <h3 class="pb-0 h3-responsive">
                            <strong class="h6"> Available From</strong></h3>
                            <div class="{{ $errors->has('availability_from.7') ? ' has-error' : '' }}">
                            <input type="text" id="time14" name="availability_from[7]" class="form-control time_from" data-dtp="" placeholder="Select Time" value="{{ !in_array(7,$working_days) ? old('availability_from.7') : $avail[7]['availability_from'] }}" disabled>
                            <!-- <label for="time" class="control-label">Available From</label> -->
                                 @if ($errors->has('availability_from.7'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('availability_from.7') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                      </div>
                      <div class="col-6">
                        <div class="form-group label-floating">
                          <h3 class="pb-0 h3-responsive">
                          <strong class="h6"> Available To</strong></h3>
                          <div class="{{ $errors->has('availability_to.7') ? ' has-error' : '' }}">
                          <input type="text" id="time16" name="availability_to[7]" class="form-control time_to" data-dtp="" placeholder="Select Time" value="{{ !in_array(7,$working_days) ? old('availability_to.7') : $avail[7]['availability_to'] }}" disabled>
                          <!-- <label for="time" class="control-label">Available To</label> -->
                               @if ($errors->has('availability_to.7'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('availability_to.7') }}</strong>
                                    </span>
                                @endif
                            </div>
                            </div>
                      </div>
                  </div>
                </div>
                    
                       <h6 class="pb-0">About Doctor</strong><span class="required">*</span></h6>
                       <div class="{{ $errors->has('fees') ? ' has-error' : '' }}">
                         <textarea type="text" id="textareaBasic" class="form-control md-textarea" name="about_doc" placeholder="Enter about Doctor in brief.">{{ isset($item->doctor->about_doc) ? $item->doctor->about_doc : old('about_doc')}}</textarea>
                         <!-- <label for="textareaBasic">Enter about Doctor in brief.</label> -->
                          <div class="{{ $errors->has('about_doc') ? ' has-error' : '' }}">
                            @if ($errors->has('about_doc'))
                                 <span class="help-block">
                                     <strong>{{ $errors->first('about_doc') }}</strong>
                                 </span>
                             @endif
                          </div>
                     </div>
                   
                         <!-- <h6 class="pb-0">Your Signature<span class="required">*</span></h6>
                            <div class="file-field mt-2">
                               <a class="btn-floating blue-gradient mt-0 float-left upload-btn">
                               <i class="fa fa-paperclip" aria-hidden="true"></i>
                               <input type="file" id="select_sign" name="esign">
                               </a>
                               <div class="file-path-wrapper">
                                  <input class="file-path validate" type="text" placeholder="Upload Your Signature Picture" disabled>
                               </div> 
                               <img id="signpreview" height="150" style="display:none"/>
                            </div>
                            <div class="{{ $errors->has('esign') ? ' has-error' : '' }}">
                              @if ($errors->has('esign'))
                                   <span class="help-block">
                                       <strong>{{ $errors->first('esign') }}</strong>
                                   </span>
                               @endif
                            </div> -->
                    
                           <h6 class="pb-0" style="margin-top:20px">Profile Picture<span class="required">*</span></h6>
                            <div class="file-field mt-2">
                               <a class="btn-floating blue-gradient mt-0 float-left upload-btn">
                               <i class="fa fa-paperclip" aria-hidden="true"></i>
                               <input type="file" id="select_file" name="profile_image">
                               </a>
                               <!-- <div class="file-path-wrapper">
                                  <input class="file-path validate" type="text" placeholder="Upload Profile Picture" disabled>
                               </div> -->
                               <img id="preview" height="150" style="display:none"/>
                            </div>
                            <div class="{{ $errors->has('profile_image') ? ' has-error' : '' }}">
                              @if ($errors->has('profile_image'))
                                   <span class="help-block">
                                       <strong>{{ $errors->first('profile_image') }}</strong>
                                   </span>
                               @endif
                            </div>
                            <div class="mt-5" style="padding-top:20px">
                              <a href="{{ route('nursing-doctor.index') }}"><button class="btn btn-primary text-white"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a></button>
                              <button type="submit" class="btn btn-primary text-white">Save</button>
                            </div>
                    
              </div>
          </div>
		  </div>
        </form>
        </div>
    </section>
   <!-- Section: Blog v.3 -->


  </div>
  </div>
  </div>
  </div>
  <style>
      .availclass{ padding:0 0px 0 15px;}
     .help-block strong {font-size: 13px;  font-weight: normal;    color: red;}
     .text-white a{ color:#fff !important;}
     
      #select_file{    font-size: 0;    width: 200px;}
     
     #select_file::-webkit-file-upload-button {
  visibility: hidden;
}
#select_file::before {
  content: 'Select  files';
  display: inline-block;
  background:#676869;
  color:#fff;
  border-radius: 3px;
  padding: 8px 12px;
  outline: none;
  white-space: nowrap;
  -webkit-user-select: none;
 
  
  font-size: 10pt;
}

      
  </style>
<!--container end here-->
@endsection

@section('script')

    <script type="text/javascript">
    function textNumeric(inputtext, alertMsg){
        //alert(alertMsg)
          var numericExpression =/^[0-9]+$/;
          if(inputtext.match(numericExpression)){
            document.getElementById('p6').innerText = "";
            if(inputtext.length < 10){
              document.getElementById('p6').innerText = "Please enter correct mobile number";
            }
          return true;
          }else{
          document.getElementById('p6').innerText = alertMsg;
        /*   inputtext.focus();
          return false; */
          }
      }

      //Auto Complete Address
      var searchInput = 'addr_input_location';  
      $(document).ready(function () {  
          var autocomplete;
          autocomplete = new google.maps.places.Autocomplete((document.getElementById(searchInput)), {
              types: ["geocode"],
          });
          google.maps.event.addListener(autocomplete, 'place_changed', function () {
              var near_place = autocomplete.getPlace();
              //assign lat long only for user location value to 
              document.getElementById('user_lat').value = near_place.geometry.location.lat();
              document.getElementById('user_long').value = near_place.geometry.location.lng();
                          
          });   
      }); 
    
    $(document).ready(function()
    {
      $('#time').timepicki();
			// ({
			// 	date: false,
			// 	//shortTime: false,
			// 	format: 'HH:mm'
			// });
      $('#time3').timepicki();
     

      $('#time2').timepicki();
			 
      $('#time4').timepicki();
			 
      $('#time5').timepicki();
      
      $('#time6').timepicki();
      
      $('#time7').timepicki();
      $('#time8').timepicki();
      
      $('#time9').timepicki();
       
      $('#time10').timepicki();
      
      $('#time11').timepicki();
     
      $('#time12').timepicki();
     
      $('#time14').timepicki();
      $('#time15').timepicki();
      $('#time16').timepicki();

      //Enable time selection
      $('.daycheck').change(function() {
       
        if(this.checked) {
            $(this).closest('.availclass').find('.time_to').prop('disabled', false);
            $(this).closest('.availclass').find('.time_from').prop('disabled', false);
            $(this).closest('.availclass').find('.fees').prop('disabled', false);
        }else{
            $(this).closest('.availclass').find('.time_to').prop('disabled', true);
            $(this).closest('.availclass').find('.time_from').prop('disabled', true);
            $(this).closest('.availclass').find('.fees').prop('disabled', true);
        }
    });

    $('input[type=checkbox]').each(function () {
        if(this.checked){
          $(this).closest('.availclass').find('.time_to').prop('disabled', false);
          $(this).closest('.availclass').find('.time_from').prop('disabled', false);
          $(this).closest('.availclass').find('.fees').prop('disabled', false);
        }
    });

    });
//preview profile sk img 
$(function() {
  $("#select_sign").on('change', function() {
    $("#signpreview").removeAttr("style");
    // Display image on the page for viewing
    readURL(this, "signpreview");

  });
});  


//end profile preview

//preview profile sk img 
$(function() {
  $("#select_file").on('change', function() {
    $("#preview").removeAttr("style");
    // Display image on the page for viewing
    readURL(this, "preview");

  });
});  

function readURL(input, tar) {
  if (input.files && input.files[0]) { // got sth

    // Clear image container
    $("#" + tar).removeAttr('src');

    $.each(input.files, function(index, ff) // loop each image 
      {

        var reader = new FileReader();

        // Put image in created image tags
        reader.onload = function(e) {
          $('#' + tar).attr('src', e.target.result);
        }

        reader.readAsDataURL(ff);

      });
  }
} 
//end profile preview

</script>
    
<!--container end here-->
@endsection
