@extends('clinics.layouts.nursing_home_main')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid m-t-15">
        @include('clinics.partials.messages')
            <div class="row">
                <div class="col-lg-8 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>Hospital Dashboard</h1>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
                
                <!-- /# column -->
            </div>
            <!-- /# row -->
            <section id="main-content">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="stat-widget-two">
                            <img src="{{ asset('core/assets/images/icon7.png') }}">
                                <div class="stat-content">
                                <div class="stat-digit">Doctors</div>
                                    <div class="stat-text">Total number of Doctors {{ isset($doctors) && ($doctors <= 1) ? 'is':'are'}} {{ isset($doctors) ? $doctors:'not available'}}</div>
                                    <a href="{{ route('nursing-doctor.index') }}" style="float:right; color:#06C; font-size:12px">view Doctors</a>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="stat-widget-two">
                            <img src="{{ asset('core/assets/images/icon8.png') }}">
                                <div class="stat-content">
                                <div class="stat-digit">Patients</div>
                                    <div class="stat-text">Total number of Patients {{ isset($patients) && ($patients <= 1) ? 'is':'are'}} {{ isset($patients) ? $patients:'not available'}}</div>
                                    <a href="{{ route('nursing.patients') }}" style="float:right; color:#06C; font-size:12px">view Patients</a>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="stat-widget-two">
                            <img src="{{ asset('core/assets/images/icon9.png') }}">
                                <div class="stat-content">
                                <div class="stat-digit">Appointment</div>
                                    <div class="stat-text">Total number of Appointments {{ isset($appointments) && ($appointments <= 1) ? 'is':'are'}} {{ isset($appointments) ? $appointments:'not available'}}</div>
                                    <a href="{{ route('nursing_all.appointments') }}" style="float:right; color:#06C; font-size:12px">view Appointments</a>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="stat-widget-two">
                            <img src="{{ asset('core/assets/images/icon10.png') }}">
                                <div class="stat-content">
                                <div class="stat-digit">Messages</div>
                                    <div class="stat-text">Total number of Messages {{ isset($messages) && ($messages <= 1) ? 'is':'are'}} {{ isset($messages) ? $messages:'not available'}} </div>
                                    <a href="#" style="float:right; color:#06C; font-size:12px">view Messages</a>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    
                    <!-- /# column -->
                </div>
                <!-- /# row -->


                <div class="row">
                    <!-- column -->
                    <div class="col-lg-6">
                        <div class="card sales">
                            <div class="card-body">
                                <h2>Overall Appointments <span style="float: right;  color: #9999; font-size:26px;">60000</span></h2>
                                
                            </div>
                            <div class="graph_div">
                            <div class="graph">
                            <span>2000</span>
                            <div class="graph_height">
                            </div>
                            <span>Mar</span>
                            </div><div class="graph">
                            <span>4000</span>
                            <div class="graph_height heightdiv">
                            </div>
                            <span>A</span>
                            </div><div class="graph">
                            <span>2000</span>
                            <div class="graph_height">
                            </div>
                            <span>Mar</span>
                            </div><div class="graph">
                            <span>4000</span>
                            <div class="graph_height heightdiv">
                            </div>
                            <span>A</span>
                            </div><div class="graph">
                            <span>2000</span>
                            <div class="graph_height">
                            </div>
                            <span>Mar</span>
                            </div><div class="graph">
                            <span>4000</span>
                            <div class="graph_height heightdiv">
                            </div>
                            <span>A</span>
                            </div>
                            </div>
                        </div>
                    </div>
                    <!-- column -->
                    <div class="col-lg-6">
                    
                        <div class="card sales ">
                            <div class="card-body">
                                <h2>Department Wise Data <span style="float: right;  color: #9999; font-size:26px;">60000</span></h2>
                                
                            </div>
                            <div class="graph_div department">
                            <div class="graph">
                            <span>2000</span>
                            <div class="graph_height">
                            </div>
                            <span>Dep1</span>
                            </div><div class="graph">
                            <span>4000</span>
                            <div class="graph_height heightdiv">
                            </div>
                            <span>Dep2</span>
                            </div><div class="graph">
                            <span>2000</span>
                            <div class="graph_height">
                            </div>
                            <span>Dep3</span>
                            </div><div class="graph">
                            <span>4000</span>
                            <div class="graph_height heightdiv">
                            </div>
                            <span>Dep4</span>
                            </div><div class="graph">
                            <span>2000</span>
                            <div class="graph_height">
                            </div>
                            <span>Dep5</span>
                            </div>
                            </div>
                        </div>
                    </div>
                    <!-- column -->
                    <div class="col-lg-6">
                        <div class="card sales">
                            <div class="card-body">
                                <h2>Overall Appointments <span style="float: right;  color: #9999; font-size:26px;">60000</span></h2>
                                
                            </div>
                            <div class="graph_div">
                            <div class="graph">
                            <span>2000</span>
                            <div class="graph_height">
                            </div>
                            <span>Mar</span>
                            </div><div class="graph">
                            <span>4000</span>
                            <div class="graph_height heightdiv">
                            </div>
                            <span>A</span>
                            </div><div class="graph">
                            <span>2000</span>
                            <div class="graph_height">
                            </div>
                            <span>Mar</span>
                            </div><div class="graph">
                            <span>4000</span>
                            <div class="graph_height heightdiv">
                            </div>
                            <span>A</span>
                            </div><div class="graph">
                            <span>2000</span>
                            <div class="graph_height">
                            </div>
                            <span>Mar</span>
                            </div><div class="graph">
                            <span>4000</span>
                            <div class="graph_height heightdiv">
                            </div>
                            <span>A</span>
                            </div>
                            </div>
                        </div>
                    </div>                   
                </div>
                <!-- column -->
                <div class="card cardapp">
                    <div class="row mt-20">
                        <div class="col-lg-8">
                            <h2>Today’s Upcoming Appointments</h2>
                        </div>        
                        <div class="col-lg-4">
                            <label>Filter <select class="selectbtn"><option>Today's Appointment</option></select>
                            <!--<span class="fa fa-bars adddiv">
                            </span>-->
                            </label>
                        </div>
                    </div>
                    <div class="row mt-20">  
                        <div class="col-lg-12">                      
                        @forelse($items as $item)
                            <div class="doctor_apointment card today_appointment">
                                <!-- <h3 class="pull-left">{{ isset($item->nursingHome->organization_name) ? ucfirst($item->nursingHome->organization_name) : ''}}</h3> -->
                                <div>
                                    <label><i class="fa fa-male"></i>{{ isset($item->user) ? $item->user->full_name : ''}}</label>
                                    <label><i class="fa fa-map-marker"></i>{{ isset($item->user->user_country) ? ucfirst($item->user->user_country->name).', ' : ''}} {{ isset($item->user->city) ? ucfirst($item->user->city) : ''}}</label>
                                    <label><i class="fa fa-calendar-minus-o"></i>{{ isset($item->created_at) ? date('d M y', strtotime($item->created_at)) : '' }}</label>
                                </div>
                                    
                                <p>{{ isset($item->desease) ? $item->desease : ''}}</p>
                                <div>
                                    <label>Scheduled on</label>
                                    <label><i class="fa fa-calendar"></i>{{ isset($item->appointment_day) ? getMyWorkingDay($item->appointment_day)[$item->appointment_day] : ''}}</label>
                                    <label><i class="fa fa-calendar-minus-o"></i>{{ isset($item->appointment_time) ? $item->appointment_time : ''}}</label>
                                </div>
                                <button class="pull-right btn btn-primary">Contact Now</button>  
                            </div>
                        
                            @empty
                            <div class="col-lg-12">
                            <div class="doctor_apointment card" style="text-align: center;">
                                <p>Appointment not found</p>
                            </div>
                            </div>
                            @endforelse
                        </div>
                    </div>  
                </div>
                <!-- <div class="row mt-20">
                    <div class="col-lg-8">
                        <h2>Today’s Upcoming Appointment</h2>
                            </div>
                            
                            <div class="col-lg-4">
                        <label>Filter <select class="selectbtn"><option>Today's Appointment</option></select></label>
                            </div>
                            
                        </div>
                        
                <div class="row mt-20">
                <div class="col-lg-12">
                <div class="doctor_apointment">
                <h3 class="pull-left">Mr. Abhiraj sirohi</h3>
                <div class="pull-right">
                <label><i class="fa fa-male"></i>Mr. Amit Kumar</label>
                <label><i class="fa fa-map-marker"></i>California, Uk</label>
                <label><i class="fa fa-clock-o"></i>11.00am</label>
                </div>
                
                <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."Lorem ipsum dolor sit amet,</p>
                
                <h6 class="pull-right">Contact Now</h6>  
                </div>
                <div class="doctor_apointment">
                <h3 class="pull-left">Mr. Abhiraj sirohi</h3>
                <div class="pull-right">
                <label><i class="fa fa-male"></i>Mr. Amit Kumar</label>
                <label><i class="fa fa-map-marker"></i>California, Uk</label>
                <label><i class="fa fa-clock-o"></i>11.00am</label>
                </div>
                
                <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."Lorem ipsum dolor sit amet,</p>
                
                <h6 class="pull-right">Contact Now</h6>  
                </div>
                <div class="doctor_apointment">
                <h3 class="pull-left">Mr. Abhiraj sirohi</h3>
                <div class="pull-right">
                <label><i class="fa fa-male"></i>Mr. Amit Kumar</label>
                <label><i class="fa fa-map-marker"></i>California, Uk</label>
                <label><i class="fa fa-clock-o"></i>11.00am</label>
                </div>
                
                <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."Lorem ipsum dolor sit amet,</p>
                
                <h6 class="pull-right">Contact Now</h6>  
                </div>
                <div class="doctor_apointment">
                <h3 class="pull-left">Mr. Abhiraj sirohi</h3>
                <div class="pull-right">
                <label><i class="fa fa-male"></i>Mr. Amit Kumar</label>
                <label><i class="fa fa-map-marker"></i>California, Uk</label>
                <label><i class="fa fa-clock-o"></i>11.00am</label>
                </div>
                
                <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."Lorem ipsum dolor sit amet,</p>
                
                <h6 class="pull-right">Contact Now</h6>  
                </div>
                </div>
                </div>   
                        
                    </div> -->
            </section>
        </div>
    </div>
</div>
@endsection        
@section('script')
<script>
    $(document).ready(function(){
        // $(".adddiv").click(function(){
        
        $(".today_appointment").toggleClass("divwidth");
        
        // });
    });
    
</script>
@endsection
