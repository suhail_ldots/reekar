   
        @extends('clinics.layouts.nursing_home_main')
        @section('content')
       
        <div class="content-wrap">
            <div class="main">
                <div class="container-fluid m-t-15">
                @include('clinics.partials.messages')
                    <!-- /# row -->
                    <section id="main-content">
                        <div class="row mt-20">
                            <div class="col-lg-7 col-sm-6 col-md-5 p-r-0 ">
                            <div class="page-header">
                                <div class="page-title">
                                    <h1>Appointments</h1>
                                </div>
                            </div>
                        </div>
								  
				        <div class="col-lg-5 col-sm-6 col-md-7 labeldiv">
                             <label>Filter <select class="selectbtn"><option>Today's Appointment</option></select>
							 <span class="fa fa-bars adddiv">
                             </span>
                            </label>
                        </div>
                                    
                    </div>
                    <div class="row mt-20">
                        <div class="col-lg-12">
                        @forelse($items as $item)
                            <div class="doctor_apointment card today_appointment">
                                <!-- <h3 class="pull-left">{{ isset($item->nursingHome->organization_name) ? ucfirst($item->nursingHome->organization_name) : ''}}</h3> -->
                                <div>
                                    <label><i class="fa fa-male"></i>{{ isset($item->user) ? $item->user->full_name : ''}}</label>
                                    <label><i class="fa fa-map-marker"></i>{{ isset($item->user->user_country) ? ucfirst($item->user->user_country->name).', ' : ''}} {{ isset($item->user->city) ? ucfirst($item->user->city) : ''}}</label>
                                    <label><i class="fa fa-calendar-minus-o"></i>{{ isset($item->created_at) ? date('d M y', strtotime($item->created_at)) : '' }}</label>
                                </div>
                                    
                                <p>{{ isset($item->desease) ? $item->desease : ''}}</p>
                                <div>
                                    <label>Scheduled on</label>
                                    <label><i class="fa fa-calendar"></i>{{ isset($item->appointment_day) ? getMyWorkingDay($item->appointment_day)[$item->appointment_day] : ''}}</label>
                                    <label><i class="fa fa-calendar-minus-o"></i>{{ isset($item->appointment_time) ? $item->appointment_time : ''}}</label>
                                </div>
                                <button class="pull-right btn btn-primary">Contact Now</button>  
                            </div>
                            @empty
                            <div class="doctor_apointment card today_appointment" style="text-align: center;">
                                <p>Appointment not found</p>
                            </div>
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div>           
        </section>
    </div>
</div>
</div>
@endsection
@section('script')
<script>
    $(document).ready(function(){
        $(".adddiv").click(function(){
        
        $(".today_appointment").toggleClass("divwidth");
        
        });
    });
    
</script>
@endsection
