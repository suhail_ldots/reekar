<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 no-padding">
@forelse($nursing_home as $nursing)
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding gappiinngg">
        <a href="{{ route('nursing.list.details', $nursing->id) }}">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 no-padding">
            <div class="iamgeee-sectionnss clearfix">
                <div class="image--left">
                    <img src="{{ @fopen(\Url('storage/app/images/doctor/profile/').'/'.$nursing->profile_pic, r) ? \Url('storage/app/images/doctor/profile/').'/'.$nursing->profile_pic : asset('core/images/doctorsLoop_placeholder.jpg') }}" alt="">
                    <!-- <img src="{{ @fopen(\Url('storage/app/images/doctor/profile/').'/'.$nursing->profile_pic, r) ? \Url('storage/app/images/doctor/profile/').'/'.$nursing->profile_pic : asset('core/images/right-dr-image.jpg') }}" alt=""> -->
                </div>
                <!-- <div class="image--right">
                    <img src="{{ asset('core/images/width--fulll.jpg') }}" alt="" class="margin-bott">
                    <img src="{{ asset('core/images/width--fulll.jpg') }}" alt="">
                </div> -->
            </div>
        </div>
        </a>
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 no-padding its-color-back">
            <!--Main div Name and rating starts-->
            <div class="name-and-rating ">
                <!--Name and rating start-->
                <div class="top-section-name clearfix">
                    <ul class="ineer-name-and-rating clearfix">
                        <li class="head-namee">{{$nursing->role_id == 3 ? 'Dr. ':''}}{{$nursing->first_name.' '.$nursing->last_name }} {{ $nursing->role_id == 4 ? "Hospital" :'('.$nursing->doctor->degree.')' }}</li>
                        <!-- <li class="head-namee">Mr.Amit Kundu (MD, MBBS)</li> -->
                        <!-- <li class="ratingss">
                            <ul class="inner-for-ratings">
                                <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li class="ratingg-text">Rating</li>
                            </ul>
                        </li> -->
                    </ul>
                </div>
                <!--Name and rating Ends-->
                @if($nursing->role_id == 3)
                <!--categroy-loction-name starts-->
                <div class="categroy-loction-name clearfix">
                    <ul class="inner-name-loca">
                        <li class="cateeggory">
                            <span class="cat-inner-icon"><img src="{{ asset('core/images/category.png') }}" alt=""></span>{{ isset($nursing->doctor->doctorSpeciality->name) ? $nursing->doctor->doctorSpeciality->name :'' }}
                        </li>                                            
                    </ul>
                </div>
                @endif
                <!--categroy-loction-name ends-->
                <!--description starts-->
                <div class="description--">
                    <p>
                    {{ ($nursing->role_id == 3  && isset($nursing->doctor)) ? shortString($nursing->doctor->about_doc): shortString($nursing->nursinghome->about_nursing_home) }}
                    </p>

                </div>
                <!--description starts-->
                <!--Date time starts-->
                @if($nursing->role_id == 3 )
                <div class="categroy-loction-name clearfix date-time">
                    <ul class="inner-name-loca">
                        <li class="cateeggory avaliaabb">
                            @lang('home.availability') 
                        </li>
                        <li class="cateeggory">
                            <span class="cat-inner-icon"><img src="{{ asset('core/images/calendr.png') }}" alt=""></span>{{ ($nursing->role_id == 3  && isset($nursing->doctor->working_days)) ? getWorkingDay($nursing->doctor->working_days):'' }}
                        </li>
                        <li class="cateeggory">
                            <span class="cat-inner-icon"><img src="{{ asset('core/images/watch.png') }}" alt=""></span>{{ (isset($nursing->doctor->availability)) ? getDoctorTiming($nursing->doctor->availability) : '' }}
                        </li>
                    </ul>
                </div>
                @endif
                <!--Date time ends-->
                <!--Book-appointment-button starts-->
                <div class="my-appoint">
                    <ul class="bokkk-appo">
                        <li><a href="{{ route('nursing.list.details', $nursing->id) }}">@lang('home.book_appointment')</a></li>
                    </ul>
                </div>
                <!--Book-appointment-button ends-->

            </div>
            <!--Main div Name and rating ends-->

        </div>

    </div>
@empty 
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding gappiinngg" style="text-align:center; ">

    @if($nursing_home->currentPage() == $nursing_home->lastPage())
    <span> Record {{ __('l.not_found') }}</span>
    @else
    <span> {{ __('l.no_more_record') }}</span>
    @endif
</div>   
@endforelse    
</div>