@extends('site_view.layouts.main')
@section('head')
<style>
.required{
    color:"red";
}

input {
    position: relative;
    width: 150px; height: 20px;
    color: white;
}

input:before {
    position: absolute;
    top: 3px; left: 3px;
    content: attr(data-date);
    display: inline-block;
    color: black;
}

input::-webkit-datetime-edit, input::-webkit-inner-spin-button, input::-webkit-clear-button {
    display: none;
}

input::-webkit-calendar-picker-indicator {
    position: absolute;
    top: 3px;
    right: 0;
    color: black;
    opacity: 1;
}
</style>
@endsection
@section('content')
<div class="subscriptionplan med_toppadder40">
  <div class="container">
    <div class="row">
        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2">
            <div class="about_heading_wraper  text-center med_bottompadder50">
                <h1 class="med_bottompadder20">{{ $plans->name }}</h1>
                <img src="{{ asset('core/images/dental_line_center.png') }}" alt="line" class="med_bottompadder20">
                <p>{{ __('featured_plan.plan_tag') }}</p>
            </div>
        </div>
                            
            <!-- <div class="listli med_toppadder0 med_bottompadder100">
                <ul style="clear:both">
                    @foreach($plans->features as $feature)
                    @php $item = getFeature_byid($feature->feature_id); @endphp
                    <li><i class="fa fa-arrow-circle-right"></i>{{ $item ? $item->count.' '. $item->name : ''}} </li>
                    @endforeach
                </ul>
          
            </div>                 -->
                            
            <div class="booking_wrapper book_section med_toppadder100 featuredplan registermember med_bottompadder100">
                <div class="container clearfix">
                    <div class="booking_box">
                        <div class="row">
                            <form action="{{ route('payment.store') }}" method="POST" autocomplete="off" class="require-validation"
                             data-cc-on-file="false" data-stripe-publishable-key="{{ $public_key }}" id="payment-form">
                            <!-- <form action="{{ route('payment.store') }}" method="POST" id="upload_form" autocomplete="off"> -->
                            @csrf
                            <div class="box_side_icon clearfix">
                            <!--   <div class="left--img">
                                    <img src="images/Icon_bk.png" alt="img">
                                </div> -->
                                
                                <div class="about_heading_wraper  text-center med_bottompadder10">
                                    <h1 class="med_bottompadder20">{{ __('featured_plan.form_fill_info') }}</h1>
                                
                                </div>
                                <input type="hidden" name="package_id" value="{{ $plans->id }}">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="contect_form1 form-group">
                                       
                                        <span class="reqsymbal">*</span>
                                        <select name="user_type" class="classic" value="{{ old('user_type') }}">
                                        <option>{{ __('featured_plan.account_type') }} *</option>
                                        <option value="3" {{ old('user_type') == 3 ? 'selected' :'' }}>{{ __('featured_plan.single_doctor_clinic') }}</option>
                                        <option value="4" {{ old('user_type') == 4 ? 'selected' :'' }}>{{ __('featured_plan.multispeciality_hospital') }}</option>
                                        </select>
                                        <div class="help-block"></div>
                                         @error('user_type')
                                            <span class="help-block required" role="alert">
                                                <li>{{ $message }}</li>
                                            </span>
                                            @enderror	
                                    </div>
                                </div>
                                <h6>Account Owner Information</h6>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="contect_form1 form-group">
                                        <span class="reqsymbal">*</span>
                                        <input type="text"  id="fname" name="fname" class="classic" value="{{ old('fname') }}" placeholder="{{ __('featured_plan.first_name') }}*">
                                        <div class="help-block"></div>
                                       
                                        @error('fname')
                                            <span class="help-block required" role="alert">
                                                <li>{{ $message }}</li>
                                            </span>
                                            @enderror
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="contect_form1 form-group">
                                        <span class="reqsymbal">*</span>
                                        <input type="text"  id="lname" name="lname" class="classic" value="{{ old('lname') }}" placeholder="{{ __('featured_plan.last_name') }}*">
                                        <div class="help-block"></div>
                                        @error('lname')
                                            <span class="help-block required" role="alert">
                                                <li>{{ $message }}</li>
                                            </span>
                                            @enderror
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="contect_form1 form-group">
                                        <span class="reqsymbal">*</span>
                                        <input type="text" id="email" name="email" class="classic" value="{{ old('email') }}" placeholder="{{ __('featured_plan.email') }}*">
                                        <div class="help-block"></div>
                                        @error('email')
                                            <span class="help-block required" role="alert">
                                                <li>{{ $message }}</li>
                                            </span>
                                            @enderror
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="contect_form1 form-group">
                                        
                                        <input type="text" name="mobile" class="classic" value="{{ old('mobile') }}" placeholder="{{ __('featured_plan.phone') }}">
                                        <div class="help-block"></div>
                                        @error('mobile')
                                            <span class="help-block required" role="alert">
                                                <li>{{ $message }}</li>
                                            </span>
                                            @enderror
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="contect_form1 form-group">
                                        <span class="reqsymbal">*</span>
                                        <input type="text" name="address" id="addr_input_location" class="classic" autocomplete="off" placeholder="{{ __('featured_plan.address') }}*">
                                        <div class="help-block"></div>
                                        @error('address')
                                            <span class="help-block required " role="alert">
                                                <li>{{ $message }}</li>
                                            </span>
                                            @enderror
                                        <input type="hidden" name="user_lat" value="{{old('user_lat')}}" id="user_lat" >
                                        <input type="hidden" name="user_long" value="{{old('user_long')}}" id="user_long" >
                                       
                                    </div>
                                </div>
                                <!-- <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="contect_form1 divhide">
                                        <label for="cars" style="visibility:hidden">Full Name</label>
                                        <input type="buton" value="Save Information" id="cars" class="btn btn-primary">
                                        <div class="help-block"></div>                                       
                                    </div>
                                </div> -->
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="contect_form1 form-group">
                                        
                                        <select name="country" class="classic" value="{{ old('country') }}" placeholder="{{ __('featured_plan.country') }}">

                                        @foreach(country() as $country)
                                         <option value="{{ $country->id }}">{{ $country->name }}</option>
                                        @endforeach 
                                        </select>
                                        @error('country')
                                        <span class="help-block required" role="alert">
                                            <li>{{ $message }}</li>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="clear:both">
                                    <div class="contect_form1 form-group">
                                        <input type="text"  name="city" class="classic" value="{{ old('city') }}" placeholder="{{ __('featured_plan.city') }}">
                                        <div class="help-block"></div>
                                        @error('city')
                                            <span class="help-block required" role="alert">
                                                <li>{{ $message }}</li>
                                            </span>
                                            @enderror
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="contect_form1 form-group">
                                        <label>{{ __('featured_plan.zipcode') }}</label>
                                        <input type="text"  name="zipcode" class="classic" value="{{ old('zipcode') }}" placeholder="{{ __('featured_plan.zipcode') }}">
                                        <div class="help-block"></div>
                                        @error('zipcode')
                                            <span class="help-block required" role="alert">
                                                <li>{{ $message }}</li>
                                            </span>
                                            @enderror
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="contect_form1 form-group">
                                        <span class="reqsymbal">*</span>
                                        <textarea class="aboutnursing classic"  name="about_nursing" id="" placeholder="Enter about your nursing home*" >{{ old('about_nursing') }}</textarea>
                                        <div class="help-block"></div>
                                        @error('about_nursing')
                                        <br><br><br>
                                            <span class="help-block required div2pos" role="alert">
                                            
                                                <li>{{ $message }}</li>
                                            </span>
                                            @enderror
                                    </div>
                                </div>
                            </div> 
                            <input type="hidden" name="amount" value ="{{ $plans->price }}">
                                <div class="col-lg-12 col-md-12 med_toppadder10 " @if($plans->price != '' && $plans->price != 0)  style="display: none;" @endif>
                                    <div class="contect_form1 form-group">
                                        <input type="submit" value="submit" id="cars" style="margin: 0 auto; display: table;width: 22%;"  class="btn btn-primary saveBtn">
                                    </div>
                                    <div id="ajaxloader" style="display: none;"><img
                                        src="{{ asset('public/admin/images/ajax-loader.gif')}}" />{{ __('l.processing') }}
                                    </div>
                                   
                                
                                </div>                      
                                                     
                        </div>
                    </div>
                    <!-- payment card start -->
                    <!-- payment card start -->
                    <div class="chat_box chat_box_clr"  @if($plans->price == '' || $plans->price == 0)  style="display: none;" @endif >
                        <div class="row">
                            <div class="booking_box_2 payment_div">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="about_heading_wraper  text-center med_bottompadder10">
                                        <h1 class="med_bottompadd0">Payment</h1>                                
                                    </div>    

                                    <!-- <h3>Card Type</h3>                                    
                                    <label><input type="radio" name="card_type" value="cc">Credit Card</label>&nbsp      
                                    <label><input type="radio" name="card_type" value="dc">Debit Card </label>
                                    <div class="help-block"></div>
                                    @error('card_type')
                                    <span class="help-block required" role="alert">
                                        <li>{{ $message }}</li>
                                    </span>
                                    @enderror -->

                                    <div class='col-lg-12 col-sm-6  error form-group hide'>
                                        <div class='alert-danger alert'>Please correct the errors and try
                                            again.</div>
                                    </div>

                            <div class="row">
                                    <div class="col-lg-3 col-md-3 col-xs-12 med_toppadder10" >
                                    <div class="contect_form1 form-group">
                                       
                                        <input type="text" name="name_on_card" class="classic" value="{{ old('name_on_card') }}" placeholder="Name On card">
                                        <div class="help-block"></div>
                                        @error('name_on_card')
                                            <span class="help-block required" role="alert">
                                                <li>{{ $message }}</li>
                                            </span>
                                            @enderror

                                    </div>
                                </div>
                                
                                <div class="col-lg-3 col-md-3 col-xs-12 med_toppadder10">
                                    <div class="contect_form1 form-group required">
                                        
                                        <input type="text"  name="card_no" class="classic card-number" value="{{ old('card_no') }}" placeholder="Card Number" size='20'>
                                        <div class="help-block"></div>
                                        @error('card_no')
                                            <span class="help-block required" role="alert">
                                                <li>{{ $message }}</li>
                                            </span>
                                            @enderror
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-xs-12 med_toppadder10 ">
                                    <div class="col-lg-4 col-sm-6 p-5">
                                    <div class="contect_form1 form-group cvc required">
                                        
                                        <input type="text" name="cvv_no" class="classic card-cvc" value="{{ old('cvv_no') }}" placeholder="CVV">
                                        <div class="help-block"></div>
                                        @error('cvv')
                                        <span class="help-block required" role="alert">
                                            <li>{{ $message }}</li>
                                        </span>
                                        @enderror
                                        
                                    </div>
                                    </div>
                                    <div class='col-lg-4 col-sm-6 form-group expiration required'>
                                        <!-- <label class='control-label'>Expiration Month</label> -->
                                         <input name="card_exp_month" class='form-control  card-expiry-month' placeholder='MM' size='2'
                                            type='text'>
                                            @error('card_exp_month')
                                            <span class="help-block required" role="alert">
                                                <li>{{ $message }}</li>
                                            </span>
                                            @enderror
                                    </div>
                                    <div class='col-lg-4 col-sm-6 form-group expiration required'>
                                        <!-- <label class='control-label'>Expiration Year</label>  -->
                                        <input name="card_exp_year" class='form-control card-expiry-year' placeholder='YYYY' size='4'
                                            type='text'>
                                            @error('card_exp_year')
                                            <span class="help-block required" role="alert">
                                                <li>{{ $message }}</li>
                                            </span>
                                            @enderror
                                    </div>
                                    <!-- <div class="col-lg-6 col-sm-6 p-5">
                                        <div class="contect_form1 form-group">
                                            <input type="date" data-date="" data-date-format="MMMM YYYY" name="expiry_date" value="{{ date('Y-m-d') }}" placeholder="Expiry">
                                            <div class="help-block"></div>
                                            @error('expiry_date')
                                            <span class="help-block required" role="alert">
                                                <li>{{ $message }}</li>
                                            </span>
                                            @enderror	
                                        
                                        </div>
                                    </div> -->
                                    
                                </div>
                                <!-- <div class="col-lg-12 col-xs-12 med_toppadder10">
                                    <input type="submit" name="" value="save" id="">
                                </div> -->
                                
                                <input type="hidden" name="amount" value ="{{ $plans->price }}">
                                <div class="col-lg-2 col-md-2 med_toppadder10 ">
                                    <div class="contect_form1 form-group">
                                        <input type="submit" value="PAY ( {{ '$'.$plans->price }} )" id="cars" class="btn btn-primary saveBtn">
                                    </div>
                                    <div id="ajaxloader" style="display: none;"><img
                                        src="{{ asset('public/admin/images/ajax-loader.gif')}}" />{{ __('l.processing') }}
                                    </div>                                 
                                
                                </div>
                                
                                   
                                
                            </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- payment card end -->
                </div>
            </div>            
            </form>
        
        
        </div>     

        <div class="listli med_toppadder0 med_bottompadder100">
            <div class="about_heading_wraper  text-center med_bottompadder50">
                <h6  class="med_bottompadder20">plan features</h6>
                <img src="http://103.25.128.66/~doctorzayet/core/images/dental_line_center.png" alt="line" class="med_bottompadder20">
                </div>
                <ul style="clear:both">  
                             
                    @foreach($plans->features as $feature)
                    
                    @php $item = getFeature_byid($feature->feature_id); @endphp
                    <li><i class="fa fa-arrow-circle-right"></i>{{ ucfirst($item->name) == 'Gallery Image' ? $plans->image_count : ''}}  {{ $item ? $item->count.' '. $item->name : ''}} </li>
                    @endforeach
                </ul>          
            </div>                             
    </div>
    </div>
</div>

@endsection
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script>
     //auto complete car location 

     var searchInput = 'addr_input_location';  
        $(document).ready(function () {  
            var autocomplete;
            autocomplete = new google.maps.places.Autocomplete((document.getElementById(searchInput)), {
                types: ["geocode"],
            });
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var near_place = autocomplete.getPlace();
                //assign lat long only for user location value to 
                document.getElementById('user_lat').value = near_place.geometry.location.lat();
                document.getElementById('user_long').value = near_place.geometry.location.lng();
                           
            });   
        }); 
        $(document).ready(function () { 
         $("input").on("change", function() {
           
            this.setAttribute(
                "data-date",
                moment(this.value, "YYYY-MM")
                .format( this.getAttribute("data-date-format") )
            )
            }).trigger("change") 
        });   
</script>

@if($plans->price != '' && $plans->price != 0)  
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
  
<script type="text/javascript">
$(function() {
    var $form = $(".require-validation");
  $('form.require-validation').bind('submit', function(e) {
    var $form         = $(".require-validation"),
        inputSelector = ['input[type=email]', 'input[type=password]',
                         'input[type=text]', 'input[type=file]',
                         'textarea'].join(', '),
        $inputs       = $form.find('.required').find(inputSelector),
        $errorMessage = $form.find('div.error'),
        valid         = true;
        $errorMessage.addClass('hide');
 
        $('.has-error').removeClass('has-error');
    $inputs.each(function(i, el) {
      var $input = $(el);
      if ($input.val() === '') {
        $input.parent().addClass('has-error');
        $errorMessage.removeClass('hide');
        e.preventDefault();
      }
    });
  
    if (!$form.data('cc-on-file')) {
      e.preventDefault();
      Stripe.setPublishableKey($form.data('stripe-publishable-key'));
      Stripe.createToken({
        number: $('.card-number').val(),
        cvc: $('.card-cvc').val(),
        exp_month: $('.card-expiry-month').val(),
        exp_year: $('.card-expiry-year').val()
      }, stripeResponseHandler);
    }
  
  });
  
  function stripeResponseHandler(status, response) {
        if (response.error) {
            $('.error')
                .removeClass('hide')
                .find('.alert')
                .text(response.error.message);
                // .fadeOut(3000, function(){
                //     $('.error').addClass('hide');
                // });
        } else {
            // token contains id, last4, and card type
            var token = response['id'];
            // insert the token into the form so it gets submitted to the server
            $form.find('input[type=text]').empty();
            $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
            $form.get(0).submit();
        }
    }
  
});
</script>
@endif
@endsection