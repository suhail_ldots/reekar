<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
    <meta charset="utf-8" />
    <title>HTML Template</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <meta name="MobileOptimized" content="320" />
    <!--start theme style -->
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="css/magnific-popup.css">
    <link rel="stylesheet" type="text/css" href="css/owl.theme.default.css">
    <link rel="stylesheet" type="text/css" href="css/flaticon_3.css">
    <link rel="stylesheet" type="text/css" href="css/fonts.css" />
    <link rel="stylesheet" type="text/css" href="css/before_after.css" />
    <link rel="stylesheet" type="text/css" href="css/doctor-css.css" />
    <link rel="stylesheet" type="text/css" href="css/listing-page.css" />
    <link rel="stylesheet" type="text/css" href="css/responsive.css" />
    <link href='../../../../api.mapbox.com/mapbox-gl-js/v0.47.0/mapbox-gl.css' rel='stylesheet' />
    <!-- favicon link-->
    <link rel="shortcut icon" type="image/icon" href="images/favicon_4.png" />
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,300;0,400;0,700;0,900;1,400;1,700;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style_4.css" />
</head>

<body>
    <!-- preloader Start -->
    <div id="preloader">
        <div id="status">
            <img src="images/preloader.gif" id="preloader_image" alt="loader">
        </div>
    </div>
    <!--top header start-->
    <div class="md_header_wrapper">
        <div class="top_header_section hidden-xs">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                        <div class="top_header_add">
                            <ul>
                                <li><i class="fa fa-map-marker" aria-hidden="true"></i><span>Address :</span> -12/california,us</li>
                                <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="#"><span>Email :</span> medical@example.com</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                        <div class="right_side_main_warpper">
                            <div class="md_right_side_warpper">
                                <ul>
                                    <li><a href="#">Login</a></li>
                                    <li><a href="#">Sign Up</a></li>
                                    <li><a href="#"><span class="Multi-language"><img src="images/English-multi.png"></span>English</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--top header end-->
    <!--header menu wrapper-->
    <div class="menu_wrapper header-area hidden-menu-bar stick">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <nav class="navbar hidden-xs">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse nav_response" id="bs-example-navbar-collapse-1">
                            <ul class="logo-header">
                                <li>
                                    <a href="#"><img src="images/index_4_logo.png"></a>
                                </li>
                            </ul>
                            <ul class="nav navbar-nav" id="nav_filter">
                                <li class="dropdown"> <a href="#" class="dropdown-toggle first_menu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Home</a>
                                </li>
                                <li class="dropdown"><a href="#" class="first_menu">about us</a>
                                </li>
                                <li class="dropdown"> <a href="#" class="dropdown-toggle first_menu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">services</a>

                                </li>
                                <li class="dropdown"> <a href="#" class="dropdown-toggle first_menu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Become a Patner</a></li>
                                <li class="dropdown"> <a href="#" class="dropdown-toggle first_menu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Doctors</a>
                                </li>
                                <li class="dropdown"> <a href="#" class="dropdown-toggle first_menu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Contact</a>
                                </li>
                                <li class="dropdown"> <a href="#" class="dropdown-toggle first_menu header-buttonn" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Register as Member</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /.navbar-collapse -->
                    </nav>
                    <div class="rp_mobail_menu_main_wrapper visible-xs">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="gc_logo logo_hidn">
                                    <img src="images/response_logo.png" class="img-responsive" alt="logo">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div id="toggle">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 31.177 31.177" style="enable-background:new 0 0 31.177 31.177;" xml:space="preserve" width="25px" height="25px">
                                        <g>
                                            <g>
                                                <path class="menubar" d="M30.23,1.775H0.946c-0.489,0-0.887-0.398-0.887-0.888S0.457,0,0.946,0H30.23    c0.49,0,0.888,0.398,0.888,0.888S30.72,1.775,30.23,1.775z" fill="white" />
                                            </g>
                                            <g>
                                                <path class="menubar" d="M30.23,9.126H12.069c-0.49,0-0.888-0.398-0.888-0.888c0-0.49,0.398-0.888,0.888-0.888H30.23    c0.49,0,0.888,0.397,0.888,0.888C31.118,8.729,30.72,9.126,30.23,9.126z" fill="white" />
                                            </g>
                                            <g>
                                                <path class="menubar" d="M30.23,16.477H0.946c-0.489,0-0.887-0.398-0.887-0.888c0-0.49,0.398-0.888,0.887-0.888H30.23    c0.49,0,0.888,0.397,0.888,0.888C31.118,16.079,30.72,16.477,30.23,16.477z" fill="white" />
                                            </g>
                                            <g>
                                                <path class="menubar" d="M30.23,23.826H12.069c-0.49,0-0.888-0.396-0.888-0.887c0-0.49,0.398-0.888,0.888-0.888H30.23    c0.49,0,0.888,0.397,0.888,0.888C31.118,23.43,30.72,23.826,30.23,23.826z" fill="white" />
                                            </g>
                                            <g>
                                                <path class="menubar" d="M30.23,31.177H0.946c-0.489,0-0.887-0.396-0.887-0.887c0-0.49,0.398-0.888,0.887-0.888H30.23    c0.49,0,0.888,0.398,0.888,0.888C31.118,30.78,30.72,31.177,30.23,31.177z" fill="white" />
                                            </g>
                                        </g>
                                    </svg>
                                </div>
                            </div>
                        </div>
                        <div id="sidebar">
                            <h1><a href="#">LOGO<span>Company</span></a></h1>
                            <div id="toggle_close">&times;</div>
                            <div id='cssmenu' class="wd_single_index_menu">
                                <ul>
                                    <li class='has-sub'><a href='#'>Home</a>
                                    </li>
                                    <li><a href="#">about us</a>
                                    </li>
                                    <li class='has-sub'><a href='#'>services</a>
                                        <ul>
                                            <li><a href="#">Services1</a>
                                            </li>
                                            <li><a href="#">Services2</a>
                                            </li>
                                            <li><a href="#">Services3</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class='has-sub'><a href='#'>Become a Doctor</a>
                                    </li>
                                    <li class='has-sub'><a href='#'>Doctors</a>
                                        <ul>
                                            <li><a href="gallery_2.html">Doctor 1</a>
                                            </li>
                                            <li><a href="gallery_3.html">Doctor 2/a>
                                            </li>
                                            <li><a href="gallery_4.html">Doctor 3</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class='has-sub'><a href='#'>contact</a> </li>
                                    <li class="dropdown"> <a href="#" class="dropdown-toggle first_menu header-buttonn" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Register as Member</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--header wrapper end-->

    <!--bio section start-->
    <div class="med_doctor_info">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                </div>
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 no-padding">
                        <div class="doctor_info">
                            <h1>Denilia D’cuz ( MBBS )</h1>
                            <p>( Cancer Specialist )</p>
                        </div>
                        <div class="doctor_info">
                            <p class="lic-no">Licence no : lic4832682244 </p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 no-padding">
                        <div class="doctor_info2">
                            <ul class="inner-for-ratings">
                                <li class="ratingg-text">Rating</li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                            </ul>
                        </div>
                        <div class="doctor_info3">
                            <ul class="inner-for-ratings22">
                                <li class="ratingg-text"><a href="#">Diploma pdf</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!--bio section end-->
    <!--single doc info start-->
    <div class="bio_section_doc">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="team_about doc_img_top fix-my-height">
                        <div class="team_img bio_team_img">
                            <img src="images/single_doc.jpg" alt="img" class="img-responsive">
                        </div>

                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <div class="choose_heading_wrapper about_heading_wraper fonnt-color">
                        <h1 class="med_bottompadder20 med_toppadder50">My BioGraphy</h1>
                        <img src="images/dental_line_blue-single.png" alt="line" class="med_bottompadder20">
                        <p>Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibht henhn id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat thr auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis.</p>
                        <p class="hidden-sm">Aenean sollicitudin, lorem quis bibendum auc tor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit.</p>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ant_txt">
                    <p>lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet yunibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed nonmauris vitae erat consequaiut auctor eu in elit. Class aptent taciti so- ciosqu ad litora torquent per conubia nostra, per inceptos himenaeos. </p>
                </div>
            </div>
        </div>
    </div>
    <!--single doc info end-->
    <!--our team wrapper start-->
    <div class="team_wrapper our-doctor-pagee">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2">
                    <div class="about_heading_wraper  text-center med_bottompadder50">
                        <h1 class="med_bottompadder20">Available Timings</h1>
                        <img src="images/dental_line_center.png" alt="line" class="med_bottompadder20">
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="timiee">
                        <div class="ull-1">
                            <ul class="ull-1-inner">
                                <li>Monday</li>
                                <li>Tuesday</li>
                                <li>Wednesday</li>
                                <li>Thursday</li>
                                <li>Friday</li>
                            </ul>

                        </div>
                        <div class="ull-2">
                            <ul class="ull-1-inner">
                                <li>10:00 am - 2:00 pm</li>
                                <li>10:00 am - 2:00 pm</li>
                                <li>10:00 am - 2:00 pm</li>
                                <li>10:00 am - 2:00 pm</li>
                                <li>10:00 am - 2:00 pm</li>
                            </ul>
                        </div>

                        <div class="ull-2">
                            <ul class="ull-1-inner">
                                <li>10:00 am - 2:00 pm</li>
                                <li>10:00 am - 2:00 pm</li>
                                <li>10:00 am - 2:00 pm</li>
                                <li>10:00 am - 2:00 pm</li>
                                <li>10:00 am - 2:00 pm</li>
                            </ul>

                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

    <!--team wrapper end-->
        <!--our team wrapper start-->
    <div class="team_wrapper appoint-book">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2">
                    <div class="about_heading_wraper  text-center med_bottompadder50">
                        <h1 class="med_bottompadder20">Book Appointment</h1>
                        <img src="images/dental_line_center.png" alt="line" class="med_bottompadder20">
                    </div>
                </div>
                    <!-- appoint_section start -->
    <div class="booking_wrapper book_section med_toppadder100">
        <div class="container clearfix">
            <div class="booking_box">
                <div class="row">
                    <form>

                    <div class="box_side_icon clearfix">
                      <!--   <div class="left--img">
                            <img src="images/Icon_bk.png" alt="img">
                        </div> -->
                        
                        <div class="about_heading_wraper  text-center med_bottompadder50">
                            <h1 class="med_bottompadder20">Fill Information</h1>
                            <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum.</p>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="contect_form1">
                                <label for="cars">Treatment</label>

                                <select id="cars" class="classic">
                                  <option>Choose your treatment</option>
                                  <option>Treatment category 1</option>
                                  <option>Treatment category 2</option>
                                  <option>Treatment category 3</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="contect_form1">
                                <label for="cars">Appointment Day</label>
                                <select id="cars" class="classic">
                                  <option>Choose your appointment day</option>
                                  <option>Saab (Swedish Aeroplane AB)</option>
                                  <option>Mercedes (Mercedes-Benz)</option>
                                  <option>Audi (Auto Union Deutschland Ingolstadt)</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-ke-beech">
                            <h2 class="seleectt-my-time">Select Your Time Slot</h2>
                            <div class="ull-2">
                                <ul class="ull-1-inner">
                                    <li>10:00 am - 2:00 pm</li>
                                    <li>10:00 am - 2:00 pm</li>
                                    <li>10:00 am - 2:00 pm</li>
                                    <li>10:00 am - 2:00 pm</li>
                                    <li class="no-spac">10:00 am - 2:00 pm</li>
                                </ul>
                            </div>
                            <div class="ull-2">
                                <ul class="ull-1-inner less-space">
                                    <li>10:00 am - 2:00 pm</li>
                                    <li>10:00 am - 2:00 pm</li>
                                    <li>10:00 am - 2:00 pm</li>
                                    <li>10:00 am - 2:00 pm</li>
                                    <li class="no-spac">10:00 am - 2:00 pm</li>
                                </ul>
                            </div>

                        </div>


                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                            <div class="submiiit">
                                <a href="#">Book Now</a>
                                
                            </div>
                           
                        </div>
                    </div>
                </form>
                </div>
            </div>
            <div class="chat_box chat_box_clr">
                <div class="row">
                    <div class="booking_box_2">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="booking_box_img">
                                <img src="images/booking_call.png" alt="img" class="img-circle">
                            </div>
                            <div class="booking_chat bottom-gap">
                                <h1>Call Us at</h1>
                                <p>1800-149-123</p>
                            </div>
                            <div class="booking_chat second--text bottom-gap">
                                <h1>Address</h1>
                                <p>Cecilia Chapman</p>
                                <p>711-2880 Nulla St.</p>
                                <p>Mankato Mississippi 96522</p>
                            </div>
                            <div class="booking_chat second--text bottom-gap">
                                <h1>Email</h1>
                                <p>Examplemail@gamil.com</p>
                                
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--appoint_section end-->
                
            </div>
        </div>
    </div>

    <!--team wrapper end-->
        <!-- portfolio-section start -->
    <section class="portfolio-area med_toppadder100">
        <div class="container">
                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2">
                    <div class="about_heading_wraper  text-center med_bottompadder50">
                        <h1 class="med_bottompadder20">Gallery</h1>
                        <img src="images/dental_line_center.png" alt="line" class="med_bottompadder20">
                    </div>
                </div>


            <div class="row three-column">
                <div id="gridWrapper" class="clearfix">
                    <div class="col-xs-12 col-sm-6 col-md-6 portfolio-wrapper III_column" data-groups='["all", "website", "photoshop"]'>
                        <div class="portfolio-thumb">
                            <div class="port_img_wrapper">
                                <img src="images/dc_galry_img_1.jpg" alt="filter_img">
                            </div>
                            <div class="portfolio_icon_wrapper zoom_popup">
                                <a class="img-link" href="images/dc_galry_img_1.jpg"> <i class="fa fa-search-plus"></i>
                                </a>
                            </div>

                            <div class="portfolio-content">
                                <div class="portfolio-info">
                                    <h3>Satisfied Patients</h3>
                                    <p class="hidden-xs">Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh elit. Duis sed amet nibh vulputate cursus a sit amet mauris erat.</p>
                                </div>
                                <!-- portfolio-info -->


                            </div>
                            <!-- portfolio-content -->

                        </div>
                        <!-- /.portfolio-thumb -->
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 portfolio-wrapper III_column" data-groups='["all", "business", "website", "photoshop"]'>
                        <div class="portfolio-thumb">
                            <div class="port_img_wrapper">
                                <img src="images/dc_galry_img_2.jpg" alt="filter_img">
                            </div>
                            <div class="portfolio_icon_wrapper zoom_popup">
                                <a class="img-link" href="images/dc_galry_img_2.jpg"> <i class="fa fa-search-plus"></i>
                                </a>
                            </div>

                            <div class="portfolio-content">
                                <div class="portfolio-info">
                                    <h3>Satisfied Patients</h3>
                                    <p class="hidden-xs">Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh elit. Duis sed amet nibh vulputate cursus a sit amet mauris erat.</p>
                                </div>
                                <!-- portfolio-info -->


                            </div>
                            <!-- portfolio-content -->

                        </div>
                        <!-- /.portfolio-thumb -->
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 portfolio-wrapper III_column" data-groups='["all", "website", "business"]'>
                        <div class="portfolio-thumb">
                            <div class="port_img_wrapper">
                                <img src="images/dc_galry_img_3.jpg" alt="filter_img">
                            </div>
                            <div class="portfolio_icon_wrapper zoom_popup">
                                <a class="img-link" href="images/dc_galry_img_3.jpg"> <i class="fa fa-search-plus"></i>
                                </a>
                            </div>

                            <div class="portfolio-content">
                                <div class="portfolio-info">
                                    <h3>Satisfied Patients</h3>
                                    <p class="hidden-xs">Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh elit. Duis sed amet nibh vulputate cursus a sit amet mauris erat.</p>
                                </div>
                                <!-- portfolio-info -->


                            </div>
                            <!-- portfolio-content -->

                        </div>
                        <!-- /.portfolio-thumb -->
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 portfolio-wrapper III_column" data-groups='["all", "business", "website", "photoshop"]'>
                        <div class="portfolio-thumb">
                            <div class="port_img_wrapper">
                                <img src="images/dc_galry_img_4.jpg" alt="filter_img">
                            </div>
                            <div class="portfolio_icon_wrapper zoom_popup">
                                <a class="img-link" href="images/dc_galry_img_4.jpg"> <i class="fa fa-search-plus"></i>
                                </a>
                            </div>

                            <div class="portfolio-content">
                                <div class="portfolio-info">
                                    <h3>Satisfied Patients</h3>
                                    <p class="hidden-xs">Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh elit. Duis sed amet nibh vulputate cursus a sit amet mauris erat.</p>
                                </div>
                                <!-- portfolio-info -->


                            </div>
                            <!-- portfolio-content -->

                        </div>
                        <!-- /.portfolio-thumb -->
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 portfolio-wrapper III_column" data-groups='["all", "photoshop", "website", "business"]'>
                        <div class="portfolio-thumb">
                            <div class="port_img_wrapper">
                                <img src="images/dc_galry_img_3.jpg" alt="filter_img">
                            </div>
                            <div class="portfolio_icon_wrapper zoom_popup">
                                <a class="img-link" href="images/dc_galry_img_3.jpg"> <i class="fa fa-search-plus"></i>
                                </a>
                            </div>

                            <div class="portfolio-content">
                                <div class="portfolio-info">
                                    <h3>Satisfied Patients</h3>
                                    <p class="hidden-xs">Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh elit. Duis sed amet nibh vulputate cursus a sit amet mauris erat.</p>
                                </div>
                                <!-- portfolio-info -->


                            </div>
                            <!-- portfolio-content -->

                        </div>
                        <!-- /.portfolio-thumb -->
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 portfolio-wrapper III_column" data-groups='["all", "business", "website", "all"]'>
                        <div class="portfolio-thumb">
                            <div class="port_img_wrapper">
                                <img src="images/dc_galry_img_6.jpg" alt="filter_img">
                            </div>
                            <div class="portfolio_icon_wrapper zoom_popup">
                                <a class="img-link" href="images/dc_galry_img_6.jpg"> <i class="fa fa-search-plus"></i>
                                </a>
                            </div>

                            <div class="portfolio-content">
                                <div class="portfolio-info">
                                    <h3>Satisfied Patients</h3>
                                    <p class="hidden-xs">Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh elit. Duis sed amet nibh vulputate cursus a sit amet mauris erat.</p>
                                </div>
                                <!-- portfolio-info -->


                            </div>
                            <!-- portfolio-content -->

                        </div>
                        <!-- /.portfolio-thumb -->
                    </div>
                </div>
                <!--/#gridWrapper-->
            </div>
            <!-- /.row -->

        </div>
        <!-- /.container -->
    </section>
    <!--/.portfolio-area-->

    <!-- footer wrapper start-->
    <div class="footer_wrapper">
        <div class="container">
            <div class="box_1_wrapper">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="address_main">
                            <div class="footer_widget_add">
                                <a href="#"><img src="images/index_4_logo.png" class="img-responsive" alt="footer_logo" /></a>
                                <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis
                                    <br>bibendum auctor.</p>
                                <a href="#">Read More <i class="fa fa-long-arrow-right"></i></a>
                            </div>
                            <div class="footer_box_add">
                                <ul>
                                    <li><i class="fa fa-map-marker" aria-hidden="true"></i><span>Address : </span>-512/fonia,canada</li>
                                    <li><i class="fa fa-phone" aria-hidden="true"></i><span>Call us : </span>+61 5001444-122</li>
                                    <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="#"><span>Email :</span> dummy@example.com</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--footer_1-->
            <div class="booking_box_div">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="footer_main_wrapper">

                            <!--footer_2-->
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 respons_footer_nav hidden-xs">
                                <div class="footer_heading footer_menu">
                                    <h1 class="med_bottompadder10">Useful Links</h1>
                                    <img src="images/dental_line_white.png" class="img-responsive" alt="img" />
                                </div>
                                <div class="footer_ul_wrapper">
                                    <ul class="Side-shit">
                                        <li><i class="fa fa-caret-right" aria-hidden="true"></i> <a href="#">home</a></li>
                                        <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="#">about us</a></li>
                                        <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="#">services</a></li>
                                        <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="#">doctors</a></li>
                                        <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="#">blog</a></li>
                                        <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="#">contact</a></li>
                                    </ul>
                                </div>
                            </div>
                            <!--footer_3-->
                            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-6 contact_last_div">
                                <div class="footer_heading">
                                    <h1 class="med_bottompadder10">Opening Hours</h1>
                                    <img src="images/dental_line_white.png" class="img-responsive" alt="img" />
                                </div>
                                <div class="footer_cnct">
                                    <p>Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum.Aenean sollicitudin, lorem </p>
                                </div>
                                <!--footer_4-->
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="footer_botm_wrapper">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="bottom_footer_copy_wrapper grey-">
                                        <span>Copyright © 2018- <a href="#">IMEDICO</a>Design by <a href="#">Webstrot.</a></span>
                                    </div>
                                    <div class="footer_btm_icon">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--footer_5-->
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="up_wrapper">
                    <a href="javascript:" id="return-to-top"><i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
        <!--footer wrapper end-->
        <!--main js files-->
        <script src="js/jquery_min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.shuffle.min.js"></script>
        <script src="js/jquery.inview.min.js"></script>
        <script src="js/jquery.countTo.js"></script>
        <script src="js/wow.min.js"></script>
        <script src="js/owl.carousel.js"></script>
        <script src="js/jquery.magnific-popup.js"></script>
        <script src="js/custom_4.js"></script>
        <script src='../../../../api.mapbox.com/mapbox-gl-js/v0.47.0/mapbox-gl.js'></script>
        <!--js code-->
        <script>
            var divisor = document.getElementById("divisor"),
                slider = document.getElementById("slider");

            function moveDivisor() {
                divisor.style.width = slider.value + "%";
            }
        </script>

        <script>
            function colorize() {
                var hue = Math.random() * 360;
                return "HSL(" + hue + ",100%,50%)";
            }
        </script>
        <script>
            mapboxgl.accessToken = 'pk.eyJ1IjoiYWtzaGF5aGFuZGdlIiwiYSI6InI2bzhEcUUifQ.8r-lNnjAuIZUk8pjhtxlFw';
            var map = new mapboxgl.Map({
                container: 'map', // container id
                style: 'mapbox://styles/mapbox/streets-v9', // stylesheet location
                center: [-74.50, 40], // starting position [lng, lat]
                zoom: 10 // starting zoom

            });
            map.scrollZoom.disable();
            map.dragPan.disable();
        </script>

        <!-- map Script-->
</body>

</html>