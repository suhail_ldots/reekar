
    <!--bio section start-->
@extends('site_view.layouts.main')
@section('content')

<div class="subscriptionplan med_toppadder100 thankyou">
  <div class="container">
  <div class="row">
  <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2">
  <div class="about_heading_wraper  text-center med_bottompadder50">
                        <h1 class="med_bottompadder20"><img src="{{ asset('core/images/thank-you-page.png') }}">Thank You<br>
Your Payment had been Received</h1>
                        
                        <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate.</p>
               <p>Your login Credential is <b>Email : {{ $user->email }} <br> Random Password (Copy this password for use) : {{ $password }} </b></p>
                <a href="{{ route('login') }}">Please login to continue </a>
                    
                    </div>
                    </div>
                    
<div class="booking_wrapper book_section med_toppadder100 featuredplan med_bottompadder100">
        <div class="container clearfix">
            <div class="book_appointment">
            <div class="about_heading_wraper  text-center med_bottompadder10">
                            
                          
                        </div>
                <div class="row">
                    <form>
<div class="col-lg-12 med_bottompadder20"><h2>Shipping & Billing Info</h2></div>
                    <div class="box_side_icon clearfix">
                     
<!--                         
                        
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="contect_form1">
                                
                                <input type="text" placeholder="I am a Oncologist "   class="classic " disabled>
                                  
                               
                            </div>
                        </div> -->
                        <!-- <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="contect_form1">
                               
                                <input type="text" placeholder=""   value="{{ $user->first_name.' '.$user->last_name }}" class="classic" disabled>
                                  
                                
                            </div>
                        </div> -->
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="contect_form1">
                               
                                <input type="text"  placeholder="{{ $user->email }}"   class="classic " disabled>
                                  
                                
                            </div>
                        </div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="contect_form1">
                               
                                <input type="text"  placeholder="{{ $user->mobile }}"   class="classic" disabled>
                                  
                                
                            </div>
                        </div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="contect_form1">
                       <input type="text"  placeholder="{{ $user->address }}"  class="classic" disabled>
                               
                            </div>
                        </div>
                        
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="contect_form1">
                                <input type="text"  placeholder="{{ $user->zipcode }}"  class="classic" disabled>
                            </div>
                        </div>
                        
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="contect_form1">
                               
                                <input type="text" placeholder="{{ $user->first_name.' '.$user->last_name }}" class="classic" disabled>
                               
                            </div>
                        </div>
                      
                      <div class="col-lg-12 med_bottompadder20 med_toppadder20">
                       <h2>Card Type</h2>
                      </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="contect_form1">                               
                                <input type="text" placeholder="{{ $plan->card_type }}"   class="classic visa" disabled>                               
                            </div>
                        </div>
                        <div class="col-lg-12 med_bottompadder20 med_toppadder20">
                       <h2>Subscribed Package</h2>
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                            <div class="contect_form1">
                               
                                <input type="text" placeholder="{{ $plan->name }} Plan"   class="classic" disabled>
                               
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="contect_form1">
                               
                                <input value="Total Amt :    {{ $plan->amount }}"   class="classic toatalamount" disabled>
                               
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="contect_form1">
                               
                                <input type="text" placeholder="Reference ID : {{ $plan->txn_id }}"   class="classic" disabled>
                               
                            </div>
                        </div>
                        
                        <div class="col-lg-12  col-xs-12">
                            <div class="contect_form1 ">
                                
                                <a type="buton" href="{{ $plan->stripe_invoice }}" class="btn btn-primary" download>Print Receipt</a>
                                  
                                
                            </div>
                        </div>


                        
                    </div>
                </form>
                </div>
            </div>
            
        </div>
    </div>
              
         
       
                                   
                    </div>
  </div>
  
  </div>                  
  </div>
  </div>
  
  </div>   
 @endsection 