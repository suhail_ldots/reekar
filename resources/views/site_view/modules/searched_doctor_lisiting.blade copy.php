<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
    <meta charset="utf-8" />
    <title>HTML Template</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <meta name="MobileOptimized" content="320" />
    <!--start theme style -->
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="css/magnific-popup.css">
    <link rel="stylesheet" type="text/css" href="css/owl.theme.default.css">
    <link rel="stylesheet" type="text/css" href="css/flaticon_3.css">
    <link rel="stylesheet" type="text/css" href="css/fonts.css" />
    <link rel="stylesheet" type="text/css" href="css/before_after.css" />
    <link rel="stylesheet" type="text/css" href="css/style_4.css" />
    <link rel="stylesheet" type="text/css" href="css/listing-page.css" />
    <link rel="stylesheet" type="text/css" href="css/responsive.css" />
    <link href='../../../../api.mapbox.com/mapbox-gl-js/v0.47.0/mapbox-gl.css' rel='stylesheet' />
    <!-- favicon link-->
    <link rel="shortcut icon" type="image/icon" href="images/favicon_4.png" />
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,300;0,400;0,700;0,900;1,400;1,700;1,900&display=swap" rel="stylesheet">
</head>

<body>
    <!-- preloader Start -->
    <div id="preloader">
        <div id="status">
            <img src="images/preloader.gif" id="preloader_image" alt="loader">
        </div>
    </div>
    <!--top header start-->
    <div class="md_header_wrapper">
        <div class="top_header_section hidden-xs">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                        <div class="top_header_add">
                            <ul>
                                <li><i class="fa fa-map-marker" aria-hidden="true"></i><span>Address :</span> -12/california,us</li>
                                <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="#"><span>Email :</span> medical@example.com</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                        <div class="right_side_main_warpper">
                            <div class="md_right_side_warpper">
                                <ul>
                                    <li><a href="#">Login</a></li>
                                    <li><a href="#">Sign Up</a></li>
                                    <li><a href="#"><span class="Multi-language"><img src="images/English-multi.png"></span>English</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--top header end-->
    <!--header menu wrapper-->
    <div class="menu_wrapper header-area hidden-menu-bar stick">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <nav class="navbar hidden-xs">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse nav_response" id="bs-example-navbar-collapse-1">
                            <ul class="logo-header">
                                <li>
                                    <a href="#"><img src="images/index_4_logo.png"></a>
                                </li>
                            </ul>
                            <ul class="nav navbar-nav" id="nav_filter">
                                <li class="dropdown"> <a href="#" class="dropdown-toggle first_menu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Home</a>
                                </li>
                                <li class="dropdown"><a href="#" class="first_menu">about us</a>
                                </li>
                                <li class="dropdown"> <a href="#" class="dropdown-toggle first_menu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">services</a>
                                    <!-- <ul class="dropdown-menu hovr_nav_tab">
                                        <li><a href="#">services</a>
                                        </li>
                                        <li><a href="#">events</a>
                                        </li>
                                        <li><a href="#">pricing</a>
                                        </li>
                                    </ul> -->
                                </li>
                                <li class="dropdown"> <a href="#" class="dropdown-toggle first_menu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Become a Patner</a></li>
                                <li class="dropdown"> <a href="#" class="dropdown-toggle first_menu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Doctors</a>
                                </li>
                                <li class="dropdown"> <a href="#" class="dropdown-toggle first_menu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Contact</a>
                                </li>
                                <li class="dropdown"> <a href="#" class="dropdown-toggle first_menu header-buttonn" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Register as Member</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /.navbar-collapse -->
                    </nav>
                    <div class="rp_mobail_menu_main_wrapper visible-xs">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="gc_logo logo_hidn">
                                    <img src="images/response_logo.png" class="img-responsive" alt="logo">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div id="toggle">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 31.177 31.177" style="enable-background:new 0 0 31.177 31.177;" xml:space="preserve" width="25px" height="25px">
                                        <g>
                                            <g>
                                                <path class="menubar" d="M30.23,1.775H0.946c-0.489,0-0.887-0.398-0.887-0.888S0.457,0,0.946,0H30.23    c0.49,0,0.888,0.398,0.888,0.888S30.72,1.775,30.23,1.775z" fill="white" />
                                            </g>
                                            <g>
                                                <path class="menubar" d="M30.23,9.126H12.069c-0.49,0-0.888-0.398-0.888-0.888c0-0.49,0.398-0.888,0.888-0.888H30.23    c0.49,0,0.888,0.397,0.888,0.888C31.118,8.729,30.72,9.126,30.23,9.126z" fill="white" />
                                            </g>
                                            <g>
                                                <path class="menubar" d="M30.23,16.477H0.946c-0.489,0-0.887-0.398-0.887-0.888c0-0.49,0.398-0.888,0.887-0.888H30.23    c0.49,0,0.888,0.397,0.888,0.888C31.118,16.079,30.72,16.477,30.23,16.477z" fill="white" />
                                            </g>
                                            <g>
                                                <path class="menubar" d="M30.23,23.826H12.069c-0.49,0-0.888-0.396-0.888-0.887c0-0.49,0.398-0.888,0.888-0.888H30.23    c0.49,0,0.888,0.397,0.888,0.888C31.118,23.43,30.72,23.826,30.23,23.826z" fill="white" />
                                            </g>
                                            <g>
                                                <path class="menubar" d="M30.23,31.177H0.946c-0.489,0-0.887-0.396-0.887-0.887c0-0.49,0.398-0.888,0.887-0.888H30.23    c0.49,0,0.888,0.398,0.888,0.888C31.118,30.78,30.72,31.177,30.23,31.177z" fill="white" />
                                            </g>
                                        </g>
                                    </svg>
                                </div>
                            </div>
                        </div>
                        <div id="sidebar">
                            <h1><a href="#">LOGO<span>Company</span></a></h1>
                            <div id="toggle_close">&times;</div>
                            <div id='cssmenu' class="wd_single_index_menu">
                                <ul>
                                    <li class='has-sub'><a href='#'>Home</a>
                                    </li>
                                    <li><a href="#">about us</a>
                                    </li>
                                    <li class='has-sub'><a href='#'>services</a>
                                        <ul>
                                            <li><a href="#">Services1</a>
                                            </li>
                                            <li><a href="#">Services2</a>
                                            </li>
                                            <li><a href="#">Services3</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class='has-sub'><a href='#'>Become a Doctor</a>
                                    </li>
                                    <li class='has-sub'><a href='#'>Doctors</a>
                                        <ul>
                                            <li><a href="gallery_2.html">Doctor 1</a>
                                            </li>
                                            <li><a href="gallery_3.html">Doctor 2/a>
                                            </li>
                                            <li><a href="gallery_4.html">Doctor 3</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class='has-sub'><a href='#'>contact</a> </li>
                                    <li class="dropdown"> <a href="#" class="dropdown-toggle first_menu header-buttonn" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Register as Member</a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--header wrapper end-->
    <!--TOp search start-->
    <div class="search-for-listing-page">
        <div class="container new-full-cont">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding padding-for-mobile">
                <ul class="search-for-listing--">
                    <li class="sellecctt">
                        <select id="category">
                            <option value="volvo">Choose your search category </option>
                            <option value="saab">1</option>
                            <option value="opel">2</option>
                            <option value="audi">3</option>
                        </select>
                    </li>
                    <li class="location2">
                        <input type="search" id="mySearch" placeholder="location">
                    </li>
                    <li class="namme-search2">
                        <input type="search2" id="mySearch" placeholder="search by name">
                    </li>
                    <li class="search----">
                        <a href="#"> Search</a>
                    </li>

                </ul>

            </div>
        </div>

    </div>
    <!--Search filter start-->
    <div class="filter-for-listing">
        <div class="container new-full-cont">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding padding-for-mobile">
                <div class="row clearfix">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 push-towards-left ipad-width1">
                        <div class="sort--"> 
                            <h4>Sort by</h4>
                        </div>

                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 push-towards-left ipad-width">
                        <div class="optionn-for-sort">
                            <ul>
                                <li>Availability</li>
                                <li>Sort by Distance </li>
                                <li>Visitor Experience</li>
                                <li>suggestion</li>
                            </ul>
                        </div>

                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mobillee-displayy">
                        <div class="just-for-mob">
                            <ul>
                                <li class="filter----for-mob">
                                    <select id="category">
                                        <option value="volvo">Choose your filter</option>
                                        <option value="saab">Availability</option>
                                        <option value="opel">Sort by Distance</option>
                                        <option value="audi">Visitor Experience</option>
                                        <option value="audi">suggestion</option>
                                    </select>
                                </li>
                            </ul>
                            
                        </div>

                    </div>

                </div>

            </div>
        </div>

    </div>

    <!--map and listing start-->
    <div class="map-and">
        <div class="container new-full-cont">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding padding-for-mobile">
                <div class="row clearfix">
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 no-padding">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding gappiinngg">

                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 no-padding">
                                <div class="iamgeee-sectionnss clearfix">
                                    <div class="image--left">
                                        <img src="images/right-dr-image.jpg" alt="">
                                    </div>
                                    <div class="image--right">
                                        <img src="images/width--fulll.jpg" alt="" class="margin-bott">
                                        <img src="images/width--fulll.jpg" alt="">
                                    </div>

                                </div>

                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 no-padding its-color-back">
                                <!--Main div Name and rating starts-->
                                <div class="name-and-rating ">
                                    <!--Name and rating start-->
                                    <div class="top-section-name clearfix">
                                        <ul class="ineer-name-and-rating clearfix">
                                            <li class="head-namee">Mr.Amit Kundu (MD, MBBS)</li>
                                            <li class="ratingss">
                                                <ul class="inner-for-ratings">
                                                    <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li class="ratingg-text">Rating</li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                    <!--Name and rating Ends-->
                                    <!--categroy-loction-name starts-->
                                    <div class="categroy-loction-name clearfix">
                                        <ul class="inner-name-loca">
                                            <li class="cateeggory">
                                                <span class="cat-inner-icon"><img src="images/category.png" alt=""></span>Dermatologist
                                            </li>
                                            <li class="cateeggory">
                                                <span class="cat-inner-icon"><img src="images/category.png" alt=""></span>Dermatologist
                                            </li>
                                            <li class="cateeggory">
                                                <span class="cat-inner-icon"><img src="images/category.png" alt=""></span>Dermatologist
                                            </li>
                                        </ul>
                                    </div>
                                    <!--categroy-loction-name ends-->
                                    <!--description starts-->
                                    <div class="description--">
                                        <p>
                                            Proin gravida nibh vel velit auctor aliuet. Aenean aks lorem sollicitudin, aks lorem quis aks bibendum auctor. sollicitudin, aks lorem quis.Proin gravida nibh vel velit auctor aliuet. Aenean aks lorem sollicitudin, aks lorem quis.
                                        </p>

                                    </div>
                                    <!--description starts-->
                                    <!--Date time starts-->
                                    <div class="categroy-loction-name clearfix date-time">
                                        <ul class="inner-name-loca">
                                            <li class="cateeggory avaliaabb">
                                                Availability
                                            </li>
                                            <li class="cateeggory">
                                                <span class="cat-inner-icon"><img src="images/calendr.png" alt=""></span>Mon - Fri
                                            </li>
                                            <li class="cateeggory">
                                                <span class="cat-inner-icon"><img src="images/watch.png" alt=""></span>10:00 am - 05:00 pm
                                            </li>
                                        </ul>
                                    </div>
                                    <!--Date time ends-->
                                    <!--Book-appointment-button starts-->
                                    <div class="my-appoint">
                                        <ul class="bokkk-appo">
                                            <li><a href="#">Book Appointment</a></li>
                                        </ul>
                                    </div>
                                    <!--Book-appointment-button ends-->

                                </div>
                                <!--Main div Name and rating ends-->

                            </div>

                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding gappiinngg">

                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 no-padding">
                                <div class="iamgeee-sectionnss clearfix">
                                    <div class="image--left">
                                        <img src="images/right-dr-image.jpg" alt="">
                                    </div>
                                    <div class="image--right">
                                        <img src="images/width--fulll.jpg" alt="" class="margin-bott">
                                        <img src="images/width--fulll.jpg" alt="">
                                    </div>

                                </div>

                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 no-padding its-color-back">
                                <!--Main div Name and rating starts-->
                                <div class="name-and-rating ">
                                    <!--Name and rating start-->
                                    <div class="top-section-name clearfix">
                                        <ul class="ineer-name-and-rating clearfix">
                                            <li class="head-namee">Mr.Amit Kundu (MD, MBBS)</li>
                                            <li class="ratingss">
                                                <ul class="inner-for-ratings">
                                                    <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li class="ratingg-text">Rating</li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                    <!--Name and rating Ends-->
                                    <!--categroy-loction-name starts-->
                                    <div class="categroy-loction-name clearfix">
                                        <ul class="inner-name-loca">
                                            <li class="cateeggory">
                                                <span class="cat-inner-icon"><img src="images/category.png" alt=""></span>Dermatologist
                                            </li>
                                            <li class="cateeggory">
                                                <span class="cat-inner-icon"><img src="images/category.png" alt=""></span>Dermatologist
                                            </li>
                                            <li class="cateeggory">
                                                <span class="cat-inner-icon"><img src="images/category.png" alt=""></span>Dermatologist
                                            </li>
                                        </ul>
                                    </div>
                                    <!--categroy-loction-name ends-->
                                    <!--description starts-->
                                    <div class="description--">
                                        <p>
                                            Proin gravida nibh vel velit auctor aliuet. Aenean aks lorem sollicitudin, aks lorem quis aks bibendum auctor. sollicitudin, aks lorem quis.Proin gravida nibh vel velit auctor aliuet. Aenean aks lorem sollicitudin, aks lorem quis.
                                        </p>

                                    </div>
                                    <!--description starts-->
                                    <!--Date time starts-->
                                    <div class="categroy-loction-name clearfix date-time">
                                        <ul class="inner-name-loca">
                                            <li class="cateeggory avaliaabb">
                                                Availability
                                            </li>
                                            <li class="cateeggory">
                                                <span class="cat-inner-icon"><img src="images/calendr.png" alt=""></span>Mon - Fri
                                            </li>
                                            <li class="cateeggory">
                                                <span class="cat-inner-icon"><img src="images/watch.png" alt=""></span>10:00 am - 05:00 pm
                                            </li>
                                        </ul>
                                    </div>
                                    <!--Date time ends-->
                                    <!--Book-appointment-button starts-->
                                    <div class="my-appoint">
                                        <ul class="bokkk-appo">
                                            <li><a href="#">Book Appointment</a></li>
                                        </ul>
                                    </div>
                                    <!--Book-appointment-button ends-->

                                </div>
                                <!--Main div Name and rating ends-->

                            </div>

                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding gappiinngg">

                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 no-padding">
                                <div class="iamgeee-sectionnss clearfix">
                                    <div class="image--left">
                                        <img src="images/right-dr-image.jpg" alt="">
                                    </div>
                                    <div class="image--right">
                                        <img src="images/width--fulll.jpg" alt="" class="margin-bott">
                                        <img src="images/width--fulll.jpg" alt="">
                                    </div>

                                </div>

                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 no-padding its-color-back">
                                <!--Main div Name and rating starts-->
                                <div class="name-and-rating ">
                                    <!--Name and rating start-->
                                    <div class="top-section-name clearfix">
                                        <ul class="ineer-name-and-rating clearfix">
                                            <li class="head-namee">Mr.Amit Kundu (MD, MBBS)</li>
                                            <li class="ratingss">
                                                <ul class="inner-for-ratings">
                                                    <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li class="ratingg-text">Rating</li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                    <!--Name and rating Ends-->
                                    <!--categroy-loction-name starts-->
                                    <div class="categroy-loction-name clearfix">
                                        <ul class="inner-name-loca">
                                            <li class="cateeggory">
                                                <span class="cat-inner-icon"><img src="images/category.png" alt=""></span>Dermatologist
                                            </li>
                                            <li class="cateeggory">
                                                <span class="cat-inner-icon"><img src="images/category.png" alt=""></span>Dermatologist
                                            </li>
                                            <li class="cateeggory">
                                                <span class="cat-inner-icon"><img src="images/category.png" alt=""></span>Dermatologist
                                            </li>
                                        </ul>
                                    </div>
                                    <!--categroy-loction-name ends-->
                                    <!--description starts-->
                                    <div class="description--">
                                        <p>
                                            Proin gravida nibh vel velit auctor aliuet. Aenean aks lorem sollicitudin, aks lorem quis aks bibendum auctor. sollicitudin, aks lorem quis.Proin gravida nibh vel velit auctor aliuet. Aenean aks lorem sollicitudin, aks lorem quis.
                                        </p>

                                    </div>
                                    <!--description starts-->
                                    <!--Date time starts-->
                                    <div class="categroy-loction-name clearfix date-time">
                                        <ul class="inner-name-loca">
                                            <li class="cateeggory avaliaabb">
                                                Availability
                                            </li>
                                            <li class="cateeggory">
                                                <span class="cat-inner-icon"><img src="images/calendr.png" alt=""></span>Mon - Fri
                                            </li>
                                            <li class="cateeggory">
                                                <span class="cat-inner-icon"><img src="images/watch.png" alt=""></span>10:00 am - 05:00 pm
                                            </li>
                                        </ul>
                                    </div>
                                    <!--Date time ends-->
                                    <!--Book-appointment-button starts-->
                                    <div class="my-appoint">
                                        <ul class="bokkk-appo">
                                            <li><a href="#">Book Appointment</a></li>
                                        </ul>
                                    </div>
                                    <!--Book-appointment-button ends-->

                                </div>
                                <!--Main div Name and rating ends-->

                            </div>

                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding gappiinngg">

                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 no-padding">
                                <div class="iamgeee-sectionnss clearfix">
                                    <div class="image--left">
                                        <img src="images/right-dr-image.jpg" alt="">
                                    </div>
                                    <div class="image--right">
                                        <img src="images/width--fulll.jpg" alt="" class="margin-bott">
                                        <img src="images/width--fulll.jpg" alt="">
                                    </div>

                                </div>

                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 no-padding its-color-back">
                                <!--Main div Name and rating starts-->
                                <div class="name-and-rating ">
                                    <!--Name and rating start-->
                                    <div class="top-section-name clearfix">
                                        <ul class="ineer-name-and-rating clearfix">
                                            <li class="head-namee">Mr.Amit Kundu (MD, MBBS)</li>
                                            <li class="ratingss">
                                                <ul class="inner-for-ratings">
                                                    <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li class="ratingg-text">Rating</li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                    <!--Name and rating Ends-->
                                    <!--categroy-loction-name starts-->
                                    <div class="categroy-loction-name clearfix">
                                        <ul class="inner-name-loca">
                                            <li class="cateeggory">
                                                <span class="cat-inner-icon"><img src="images/category.png" alt=""></span>Dermatologist
                                            </li>
                                            <li class="cateeggory">
                                                <span class="cat-inner-icon"><img src="images/category.png" alt=""></span>Dermatologist
                                            </li>
                                            <li class="cateeggory">
                                                <span class="cat-inner-icon"><img src="images/category.png" alt=""></span>Dermatologist
                                            </li>
                                        </ul>
                                    </div>
                                    <!--categroy-loction-name ends-->
                                    <!--description starts-->
                                    <div class="description--">
                                        <p>
                                            Proin gravida nibh vel velit auctor aliuet. Aenean aks lorem sollicitudin, aks lorem quis aks bibendum auctor. sollicitudin, aks lorem quis.Proin gravida nibh vel velit auctor aliuet. Aenean aks lorem sollicitudin, aks lorem quis.
                                        </p>

                                    </div>
                                    <!--description starts-->
                                    <!--Date time starts-->
                                    <div class="categroy-loction-name clearfix date-time">
                                        <ul class="inner-name-loca">
                                            <li class="cateeggory avaliaabb">
                                                Availability
                                            </li>
                                            <li class="cateeggory">
                                                <span class="cat-inner-icon"><img src="images/calendr.png" alt=""></span>Mon - Fri
                                            </li>
                                            <li class="cateeggory">
                                                <span class="cat-inner-icon"><img src="images/watch.png" alt=""></span>10:00 am - 05:00 pm
                                            </li>
                                        </ul>
                                    </div>
                                    <!--Date time ends-->
                                    <!--Book-appointment-button starts-->
                                    <div class="my-appoint">
                                        <ul class="bokkk-appo">
                                            <li><a href="#">Book Appointment</a></li>
                                        </ul>
                                    </div>
                                    <!--Book-appointment-button ends-->

                                </div>
                                <!--Main div Name and rating ends-->

                            </div>

                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding gappiinngg">

                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 no-padding">
                                <div class="iamgeee-sectionnss clearfix">
                                    <div class="image--left">
                                        <img src="images/right-dr-image.jpg" alt="">
                                    </div>
                                    <div class="image--right">
                                        <img src="images/width--fulll.jpg" alt="" class="margin-bott">
                                        <img src="images/width--fulll.jpg" alt="">
                                    </div>

                                </div>

                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 no-padding its-color-back">
                                <!--Main div Name and rating starts-->
                                <div class="name-and-rating ">
                                    <!--Name and rating start-->
                                    <div class="top-section-name clearfix">
                                        <ul class="ineer-name-and-rating clearfix">
                                            <li class="head-namee">Mr.Amit Kundu (MD, MBBS)</li>
                                            <li class="ratingss">
                                                <ul class="inner-for-ratings">
                                                    <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li class="ratingg-text">Rating</li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                    <!--Name and rating Ends-->
                                    <!--categroy-loction-name starts-->
                                    <div class="categroy-loction-name clearfix">
                                        <ul class="inner-name-loca">
                                            <li class="cateeggory">
                                                <span class="cat-inner-icon"><img src="images/category.png" alt=""></span>Dermatologist
                                            </li>
                                            <li class="cateeggory">
                                                <span class="cat-inner-icon"><img src="images/category.png" alt=""></span>Dermatologist
                                            </li>
                                            <li class="cateeggory">
                                                <span class="cat-inner-icon"><img src="images/category.png" alt=""></span>Dermatologist
                                            </li>
                                        </ul>
                                    </div>
                                    <!--categroy-loction-name ends-->
                                    <!--description starts-->
                                    <div class="description--">
                                        <p>
                                            Proin gravida nibh vel velit auctor aliuet. Aenean aks lorem sollicitudin, aks lorem quis aks bibendum auctor. sollicitudin, aks lorem quis.Proin gravida nibh vel velit auctor aliuet. Aenean aks lorem sollicitudin, aks lorem quis.
                                        </p>

                                    </div>
                                    <!--description starts-->
                                    <!--Date time starts-->
                                    <div class="categroy-loction-name clearfix date-time">
                                        <ul class="inner-name-loca">
                                            <li class="cateeggory avaliaabb">
                                                Availability
                                            </li>
                                            <li class="cateeggory">
                                                <span class="cat-inner-icon"><img src="images/calendr.png" alt=""></span>Mon - Fri
                                            </li>
                                            <li class="cateeggory">
                                                <span class="cat-inner-icon"><img src="images/watch.png" alt=""></span>10:00 am - 05:00 pm
                                            </li>
                                        </ul>
                                    </div>
                                    <!--Date time ends-->
                                    <!--Book-appointment-button starts-->
                                    <div class="my-appoint">
                                        <ul class="bokkk-appo">
                                            <li><a href="#">Book Appointment</a></li>
                                        </ul>
                                    </div>
                                    <!--Book-appointment-button ends-->

                                </div>
                                <!--Main div Name and rating ends-->

                            </div>

                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding gappiinngg">

                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 no-padding">
                                <div class="iamgeee-sectionnss clearfix">
                                    <div class="image--left">
                                        <img src="images/right-dr-image.jpg" alt="">
                                    </div>
                                    <div class="image--right">
                                        <img src="images/width--fulll.jpg" alt="" class="margin-bott">
                                        <img src="images/width--fulll.jpg" alt="">
                                    </div>

                                </div>

                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 no-padding its-color-back">
                                <!--Main div Name and rating starts-->
                                <div class="name-and-rating ">
                                    <!--Name and rating start-->
                                    <div class="top-section-name clearfix">
                                        <ul class="ineer-name-and-rating clearfix">
                                            <li class="head-namee">Mr.Amit Kundu (MD, MBBS)</li>
                                            <li class="ratingss">
                                                <ul class="inner-for-ratings">
                                                    <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li class="ratingg-text">Rating</li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                    <!--Name and rating Ends-->
                                    <!--categroy-loction-name starts-->
                                    <div class="categroy-loction-name clearfix">
                                        <ul class="inner-name-loca">
                                            <li class="cateeggory">
                                                <span class="cat-inner-icon"><img src="images/category.png" alt=""></span>Dermatologist
                                            </li>
                                            <li class="cateeggory">
                                                <span class="cat-inner-icon"><img src="images/category.png" alt=""></span>Dermatologist
                                            </li>
                                            <li class="cateeggory">
                                                <span class="cat-inner-icon"><img src="images/category.png" alt=""></span>Dermatologist
                                            </li>
                                        </ul>
                                    </div>
                                    <!--categroy-loction-name ends-->
                                    <!--description starts-->
                                    <div class="description--">
                                        <p>
                                            Proin gravida nibh vel velit auctor aliuet. Aenean aks lorem sollicitudin, aks lorem quis aks bibendum auctor. sollicitudin, aks lorem quis.Proin gravida nibh vel velit auctor aliuet. Aenean aks lorem sollicitudin, aks lorem quis.
                                        </p>

                                    </div>
                                    <!--description starts-->
                                    <!--Date time starts-->
                                    <div class="categroy-loction-name clearfix date-time">
                                        <ul class="inner-name-loca">
                                            <li class="cateeggory avaliaabb">
                                                Availability
                                            </li>
                                            <li class="cateeggory">
                                                <span class="cat-inner-icon"><img src="images/calendr.png" alt=""></span>Mon - Fri
                                            </li>
                                            <li class="cateeggory">
                                                <span class="cat-inner-icon"><img src="images/watch.png" alt=""></span>10:00 am - 05:00 pm
                                            </li>
                                        </ul>
                                    </div>
                                    <!--Date time ends-->
                                    <!--Book-appointment-button starts-->
                                    <div class="my-appoint">
                                        <ul class="bokkk-appo">
                                            <li><a href="#">Book Appointment</a></li>
                                        </ul>
                                    </div>
                                    <!--Book-appointment-button ends-->

                                </div>
                                <!--Main div Name and rating ends-->

                            </div>

                        </div>

                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="mapp-ssection">
                            <div class="mapouter">
                                <div class="gmap_canvas">
                                    <iframe width="100%" height="100%" id="gmap_canvas" src="https://maps.google.com/maps?q=university%20of%20san%20francisco&t=&z=9&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.embedgooglemap.org">embedgooglemap.org</a></div>
                                <style>
                                    .mapouter {
                                        position: relative;
                                        text-align: right;
                                        height: 850px;
                                        width: 100%;
                                    }
                                    
                                    .gmap_canvas {
                                        overflow: hidden;
                                        background: none!important;
                                        height: 850px;
                                        width: 100%;
                                    }
                                </style>
                            </div>

                        </div>
                        <div class="add-number-1">
                            <img src="images/addss-number-1.jpg" alt="">

                        </div>
                        <div class="add-number-1">
                            <img src="images/addss-number-2.jpg" alt="">

                        </div>

                    </div>

                </div>
            </div>
        </div>

    </div>

    <!-- footer wrapper start-->
    <div class="footer_wrapper">
        <div class="container">
            <div class="box_1_wrapper">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="address_main">
                            <div class="footer_widget_add">
                                <a href="#"><img src="images/index_4_logo.png" class="img-responsive" alt="footer_logo" /></a>
                                <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis
                                    <br>bibendum auctor.</p>
                                <a href="#">Read More <i class="fa fa-long-arrow-right"></i></a>
                            </div>
                            <div class="footer_box_add">
                                <ul>
                                    <li><i class="fa fa-map-marker" aria-hidden="true"></i><span>Address : </span>-512/fonia,canada</li>
                                    <li><i class="fa fa-phone" aria-hidden="true"></i><span>Call us : </span>+61 5001444-122</li>
                                    <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="#"><span>Email :</span> dummy@example.com</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--footer_1-->
            <div class="booking_box_div">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="footer_main_wrapper">

                            <!--footer_2-->
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 respons_footer_nav hidden-xs">
                                <div class="footer_heading footer_menu">
                                    <h1 class="med_bottompadder10">Useful Links</h1>
                                    <img src="images/dental_line_white.png" class="img-responsive" alt="img" />
                                </div>
                                <div class="footer_ul_wrapper">
                                    <ul class="Side-shit">
                                        <li><i class="fa fa-caret-right" aria-hidden="true"></i> <a href="#">home</a></li>
                                        <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="#">about us</a></li>
                                        <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="#">services</a></li>
                                        <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="#">doctors</a></li>
                                        <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="#">blog</a></li>
                                        <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="#">contact</a></li>
                                    </ul>
                                </div>
                            </div>
                            <!--footer_3-->
                            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-6 contact_last_div">
                                <div class="footer_heading">
                                    <h1 class="med_bottompadder10">Opening Hours</h1>
                                    <img src="images/dental_line_white.png" class="img-responsive" alt="img" />
                                </div>
                                <div class="footer_cnct">
                                    <p>Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum.Aenean sollicitudin, lorem </p>
                                </div>
                                <!--footer_4-->
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="footer_botm_wrapper">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="bottom_footer_copy_wrapper grey-">
                                        <span>Copyright © 2018- <a href="#">IMEDICO</a>Design by <a href="#">Webstrot.</a></span>
                                    </div>
                                    <div class="footer_btm_icon">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--footer_5-->
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="up_wrapper">
                    <a href="javascript:" id="return-to-top"><i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
        <!--footer wrapper end-->
        <!--main js files-->
        <script src="js/jquery_min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.shuffle.min.js"></script>
        <script src="js/jquery.inview.min.js"></script>
        <script src="js/jquery.countTo.js"></script>
        <script src="js/wow.min.js"></script>
        <script src="js/owl.carousel.js"></script>
        <script src="js/jquery.magnific-popup.js"></script>
        <script src="js/custom_4.js"></script>
        <script src='../../../../api.mapbox.com/mapbox-gl-js/v0.47.0/mapbox-gl.js'></script>
        <!--js code-->
        <script>
            var divisor = document.getElementById("divisor"),
                slider = document.getElementById("slider");

            function moveDivisor() {
                divisor.style.width = slider.value + "%";
            }
        </script>

        <script>
            function colorize() {
                var hue = Math.random() * 360;
                return "HSL(" + hue + ",100%,50%)";
            }
        </script>
        <script>
            mapboxgl.accessToken = 'pk.eyJ1IjoiYWtzaGF5aGFuZGdlIiwiYSI6InI2bzhEcUUifQ.8r-lNnjAuIZUk8pjhtxlFw';
            var map = new mapboxgl.Map({
                container: 'map', // container id
                style: 'mapbox://styles/mapbox/streets-v9', // stylesheet location
                center: [-74.50, 40], // starting position [lng, lat]
                zoom: 10 // starting zoom

            });
            map.scrollZoom.disable();
            map.dragPan.disable();
        </script>

        <!-- map Script-->
</body>

</html>