<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-ke-beech">
    <h2 class="seleectt-my-time">Select Your Time Slot</h2>
    <div class="ull-1">
        <ul class="ull-1-inner">
      
        
            @php //$workingday = getMyWorkingDay($item->doctor->working_days != '' ? $item->doctor->working_days : '0' ); @endphp
            @php $workingday = getWorkingDay_withDate($item->doctor->working_days != '' ? $item->doctor->working_days : '0' ); @endphp
            @forelse($workingday as $key => $value)                                        
                <input type="radio" name="day" id="" value="{{ $key }}" @if( old("day") == $key) selected @endif ><li>{{ $value }}</li>
                
            @empty
            <li>Not available</li>
            @endforelse                                
        </ul>
    </div>
    <div class="ull-2">
        @php
        $working_days = explode(",", isset($item->doctor->working_days) ? $item->doctor->working_days :'' );
        if($item->doctor->availability){
            // dd($item->doctor->availability); 
            $avail = optional($item->doctor)->availability;
        }else{
            $avail = false;
        }
        @endphp
        <ul class="ull-1-inner">
            @if($avail)
            @foreach( $working_days as $working_day)                                           
                <li>{{ $avail[$working_day]['availability_from'] }} - {{ $avail[$working_day]['availability_to'] }}</li>
            @endforeach
            @endif                                       
        </ul>
    </div>
    <div class="help-block"></div>
    @error('day')
    <span class="help-block required" role="alert">
        {{ $message }}
    </span>
    @enderror
    
</div>