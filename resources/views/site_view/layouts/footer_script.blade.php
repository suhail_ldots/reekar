    <!--main js files-->
    <script src="{{ asset('core/js/jquery_min.js') }}"></script>
    <script src="{{ asset('core/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('core/js/jquery.shuffle.min.js') }}"></script>
    <script src="{{ asset('core/js/jquery.inview.min.js') }}"></script>
    <script src="{{ asset('core/js/jquery.countTo.js') }}"></script>
    <script src="{{ asset('core/js/wow.min.js') }}"></script>
    <script src="{{ asset('core/js/owl.carousel.js') }}"></script>
    <script src="{{ asset('core/js/jquery.magnific-popup.js') }}"></script>
    
    <script src="{{ asset('core/js/custom_4.js') }}"></script>
    
    <!-- ajax post -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/axios/0.16.2/axios.js"></script>

    <!-- <script src="{{ asset('core/jobjs/app.js') }}"></script> -->
    <script src="{{ asset('core/jobjs/post-jobs.js') }}"></script>
    <!-- to get nearby places -->
    <!-- https://maps.googleapis.com/maps/api/place/nearbysearch/json
  ?location=29.9680035,77.55520659999999
  &radius=500
  &types=food
  &name=harbour
  &key=YOUR_API_KEY -->
    <!-- https://maps.googleapis.com/maps/api/place/nearbysearch/json
  ?location=29.9680035,77.55520659999999
  &radius=500
  &types=food
  &name=harbour
  &key=YOUR_API_KEY -->
    <!-- <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyCbjwwNzm99H7BeeZBpUPX34ANpPIHNy00"></script> -->
    <!-- below key from rekr -->


    <!--js code-->
    
    <script>
        var divisor = document.getElementById("divisor"),
            slider = document.getElementById("slider");

        function moveDivisor() {
            divisor.style.width = slider.value + "%";
        }
    </script>

    <script>
        function colorize() {
            var hue = Math.random() * 360;
            return "HSL(" + hue + ",100%,50%)";
        }
    </script>
    <!-- <script>
        mapboxgl.accessToken = 'pk.eyJ1IjoiYWtzaGF5aGFuZGdlIiwiYSI6InI2bzhEcUUifQ.8r-lNnjAuIZUk8pjhtxlFw';
        var map = new mapboxgl.Map({
            container: 'map', // container id
            style: 'mapbox://styles/mapbox/streets-v9', // stylesheet location
            center: [-74.50, 40], // starting position [lng, lat]
            zoom: 10 // starting zoom

        });
        map.scrollZoom.disable();
        map.dragPan.disable();
    </script> -->
    <!-- map Script-->
    @yield('js')