<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" >

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
    <meta charset="utf-8" />
    @yield('title')
    <title>Doctors Loop - Patient access to hope</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <meta name="MobileOptimized" content="320" />
    <!--start theme style -->
    <link rel="stylesheet" type="text/css" href="{{ asset('core/css/owl.carousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core/css/fonts.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('core/css/listing-page.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('core/css/doctor-css.css') }} " />   
    <link rel="stylesheet" type="text/css" href="{{ asset('core/css/hospital-css.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('core/css/pricing.css') }}" />

    <link rel="stylesheet" type="text/css" href="{{ asset('core/css/animate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core/css/owl.carousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core/css/magnific-popup.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core/css/owl.theme.default.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core/css/flaticon_3.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core/css/fonts.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('core/css/before_after.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('core/css/style_4.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('core/css/responsive.css') }}" />
    <!-- <link href='../../../../api.mapbox.com/mapbox-gl-js/v0.47.0/mapbox-gl.css' rel='stylesheet' /> -->
    <!-- favicon link-->
    <link rel="shortcut icon" type="image/icon" href="{{ asset('core/images/favicon.png') }}" />
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,300;0,400;0,700;0,900;1,400;1,700;1,900&display=swap" rel="stylesheet">
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyA2X23zOQ4hIRy3ZoHPWxSqnPRzjAdOoCc"></script>

    @yield('head')

</head>
    @yield('div')
    <body>
        <div class="wrapper">
            @include('site_view.layouts.header')
            {{--@include('site_view.partials.messages')--}}
            @yield('content')
            @include('site_view.layouts.footer') 
        </div>
    </body>
</html>