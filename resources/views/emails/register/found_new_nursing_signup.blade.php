@component('mail::message')

@php
$url = route('main');
@endphp

# Dear Admin

You have a new {{$item}} on your site. <br>

@component('mail::button', ['url' => $url])
Go to Site
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent