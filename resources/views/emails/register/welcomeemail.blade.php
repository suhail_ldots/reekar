@component('mail::message')
@php
$url = route('main');

$username = $item['first_name'].' '.$item['last_name'];

@endphp
# Dear {{$username}},


Welcome to Doctors Loop we are Happy to see you Here.<br>
You have successfully Subscribed <b>{{ ucfirst($item['plan_name']) }} Plan.</b><br>
and now you can enjoy the following benefits->
@php $i = 1; @endphp
@foreach($item['plan_feature_name'] as $feature)
({{ $i }}) {{ $feature }}
@php $i++; @endphp
@endforeach

Please login by Using given credentials- <br>
Email Address - {{$item['email']}}.<br>
Password - {{ $item['temp_password'] }}
@component('mail::button', ['url' => $url])
Go to Site
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent