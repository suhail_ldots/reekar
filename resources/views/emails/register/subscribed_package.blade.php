@component('mail::message')
@php
$url = route('mail.payment', $item['uid']);

$username = $item['organization_name'];

@endphp
# Dear {{$username}},

You have {{$item['name']}}. Please Pay $-{{$item['price']}} by using given link Or by login at Dashboard to activate
your account.
@component('mail::button', ['url' => $url])
Pay Now
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent