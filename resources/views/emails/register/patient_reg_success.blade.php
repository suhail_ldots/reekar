@component('mail::message')
@php
$url = route('main');
@endphp
#Hi, {{ucfirst($user->first_name)}} {{ ucfirst($user->last_name)}}<br/>


Thank you for choosing Doctors Loop as your personal treatment plateform.<br/>


Your account have been successfully created on Doctors Loop platform. Now you can get benefits from most qualified doctors in your area.<br/>

<br/>

@component('mail::button', ['url' => $url])
Go to Site
@endcomponent

Best Regards, <br/>
-{{ config('app.name') }}
@endcomponent