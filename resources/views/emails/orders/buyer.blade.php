@component('mail::message')

#Hi, {{$order->user->first_name}}<br/>



Your order with booking id <b>{{$order->booking_id}}</b> have been placed successfully.<br/>

Find below all the details<br/>
@component('mail::table')
|                                       |                       |                                                               |
| :-----------------------              |:----------------------|:--------------------------                                    |
|**Booking Id**                        |                       |{!!$order->booking_id!!}                           |
|**Booking Date**                        |                       |{!!date("jS M Y g:i A", strtotime($order->updated_at))!!}                           |
|**Rent Start Date**                        |                       |{!!date("jS M Y g:i A", strtotime($order->booking_from_date))!!}                           |
|**Rent End Date**                        |                       |{!!date("jS M Y g:i A", strtotime($order->booking_to_date))!!}                           |
|**Car**                                  |                       |{!!$order->car->brand->name.' '.$order->car->model->name!!}                           |
|**Car Owner Name**                    |                       |{!!$order->car->owner->first_name.' '.$order->car->owner->last_name!!}                           |
|**Car Contact no.**                    |                       |{!!$order->car->owner->country_code.' '.$order->car->owner->mobile!!}                           |
|**Billing Name**                   |                       |{!!$order->billing_first_name.' '.$order->billing_last_name!!}                          |
|**Billing Email**                   |                       |{!!$order->billing_email!!}                          |
|**Billing Mobile no.**                   |                       |{!!$order->billing_mobile!!}                          |
|**Billing Alternate Mobile no.**       |                       |{!!$order->billing_alternate_mobile!!}                          |
|**Billing Address**                      |                       |{!!$order->billing_address!!}                          |
|**Pickup Address**                      |                       |{!!$order->pickup_location!!}                          |
|**Total Amount**                      |                       |{!!($order->currency == 'USD' ? '$' : '&#x20AC;').$order->total_amount!!}       |
@if($order->security_amount)                        
|**Security Amount**                      |                       |{!!($order->currency == 'USD' ? '$' : '&#x20AC;').$order->security_amount!!}         |
@endif                         
|**Tax Amount**                      |                       |{!!($order->currency == 'USD' ? '$' : '&#x20AC;').$order->tax_amount!!}                          |
|**Grand Total**                      |                       |{!!($order->currency == 'USD' ? '$' : '&#x20AC;').$order->grand_total!!}                          |
|**Paid Amount**                      |                       |{!!($order->currency == 'USD' ? '$' : '&#x20AC;').$order->paid_amount!!}                          |
@endcomponent 

Please find below your invoice for this order.<br/>

<br/>




Best Regards, <br/>
-{{ config('app.name') }}
@endcomponent