@component('mail::message')
@php
$url = route('main');
@endphp

# Dear <b> Dr. {{ $doctor['user']['full_name'] }} </b>

You have new Appointment with the patient <b> {{ $item['full_name'] }} </b>
on {{ $item['appointment_date'] }} at {{ $item['appointment_time'] }}  <br>


@component('mail::button', ['url' => $url])
Go to site
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
