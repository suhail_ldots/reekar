@component('mail::message')
@php
$url = route('main');
@endphp
# Dear  <b>{{ $item['full_name'] }}</b>

your appointment has been created successfully with the 

<b> Dr. {{ $doctor['user']['full_name'] }} </b>

on {{ $item['appointment_date'] }} at {{ $item['appointment_time'] }}  <br>

@component('mail::button', ['url' => $url])
Go to site
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
