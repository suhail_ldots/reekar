@extends('admin.layouts.master')
@section('css')
<!--Chartist Chart CSS -->
<link rel="stylesheet" href="{{ URL::asset('assets/public/plugins/chartist/css/chartist.min.css') }}">
@endsection

@section('content')
<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <h4 class="page-title">Dashboard
            </h4>
        </div>
    </div>
</div>
<div style="padding-top:17px;">
    @include('admin.partials.messages')
</div>
<div class="row">
    <div class="col-xl-4 col-md-6">
        <div class="card mini-stat bg-primary text-white proo--dduct">
            <div class="card-body">
                <div class="mb-4">
                    <div class="float-left mini-stat-img mr-4">
                        <i class="fa fa-hospital"></i>
                    </div>
                    <h5 class="font-16 text-uppercase mt-0 text-white-50">Nursing Homes</h5>
                   
                    <h4 class="font-500">{{ isset($clinics) ? $clinics : 'Not Available' }}</h4>
                </div>
                <a href="{{ route('nursinghome.index') }}" class="text-white-50">
                    <div class="pt-2">
                        <div class="float-right">
                            <i class="mdi mdi-arrow-right h5"></i>
                        </div>
                        <p class="text-white-50 mb-0">@lang("l.view")</p>
                    </div>
                </a>
            </div>
        </div>
    </div>
    
    <div class="col-xl-4 col-md-6">
        <div class="card mini-stat bg-primary text-white us---eer">
            <div class="card-body">
                <div class="mb-4">
                    <div class="float-left mini-stat-img mr-4">
                        <i class="fa fa-user-md"></i>
                    </div>
                    <h5 class="font-16 text-uppercase mt-0 text-white-50">Doctors</h5>                    
                    <h4 class="font-500">{{ isset($doctors) ? $doctors : 'Not Available' }}</h4>
                </div>
                <a href="{{ route('doctor.index') }} " class="text-white-50">
                    <div class="pt-2">
                        <div class="float-right">
                            <i class="mdi mdi-arrow-right h5"></i>
                        </div>
                        <p class="text-white-50 mb-0">@lang("l.view")</p>
                    </div>
                </a>
            </div>
        </div>
    </div>
   
    <div class="col-xl-4 col-md-6">
        <div class="card mini-stat bg-primary text-white suppliers">
            <div class="card-body">
                <div class="mb-4">
                    <div class="float-left mini-stat-img mr-4">
                        <i class="fas fa-users"></i>
                    </div>
                    <h5 class="font-16 text-uppercase mt-0 text-white-50">Patients</h5>
                    <h4 class="font-500">{{ isset($patients) ? $patients : 'Not Available' }}</h4>
                </div>
                <a href="{{ route('patient.index') }}" class="text-white-50">
                    <div class="pt-2">
                        <div class="float-right">
                            <i class="mdi mdi-arrow-right h5"></i>
                        </div>
                        <p class="text-white-50 mb-0">@lang("l.view")</p>
                    </div>
                </a>
            </div>
        </div>
    </div>
     
    {{-- <div class="col-xl-3 col-md-6">
        <div class="card mini-stat bg-primary text-white">
            <div class="card-body">
                <div class="mb-4">
                    <div class="float-left mini-stat-img mr-4">
                        <i class="fas fa-calendar-check"></i>
                    </div>
                    <h5 class="font-16 text-uppercase mt-0 text-white-50">Experiences</h5>
                    <h4 class="font-500">300 </h4>
                </div>
                <a href="#" class="text-white-50">
                <div class="pt-2">
                    <div class="float-right">
                        <i class="mdi mdi-arrow-right h5"></i>
                    </div>
                    <p class="text-white-50 mb-0">@lang("l.view")</p>
                </div>
                </a>
            </div>
        </div>
    </div> --}}
</div>


<!-- end row -->


<!-- end row -->
@endsection

@section('script')
<!--Chartist Chart-->
<script src="{{ URL::asset('assets/public/plugins/chartist/js/chartist.min.js') }}"></script>
<script src="{{ URL::asset('assets/public/plugins/chartist/js/chartist-plugin-tooltip.min.js') }}"></script>
<!-- peity JS -->
<script src="{{ URL::asset('assets/public/plugins/peity-chart/jquery.peity.min.js') }}"></script>
<script src="{{ URL::asset('assets/public/pages/dashboard.js') }}"></script>
@endsection