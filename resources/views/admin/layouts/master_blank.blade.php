<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Doctors Loop- Patient access to hope</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="Doctors Loop" name="author" />
        <link rel="shortcut icon" href="public/admin/images/favicon.ico">
        @include('admin.layouts.head')
        <style type="text/css">
           .help-block {color: red;}    

           .form-control.is-invalid, .was-validated .form-control:invalid {
                border-color: #dc3545;
                padding-right: calc(1.5em + .75rem);
                background-image: none;
                background-repeat: no-repeat;
                background-position: center right calc(.375em + .1875rem);
                background-size: calc(.75em + .375rem) calc(.75em + .375rem);
            }   
        </style>        
  </head>
    <body class="pb-0">
        @yield('content')
        @include('admin.layouts.footer-script')   
    </body>
</html>