<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">
    <div class="slimscroll-menu" id="remove-scroll">
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu" id="side-menu">
                    @php $user = Auth::user(); @endphp
                    {{-- <li class="menu-title" style="text-align:left;">
                        <span style="padding:12.5px 42px;">{{  $user->title.' '.$user->first_name.' '.$user->last_name}}</span>
                        <a href="#" style="font-size:12px;text-align:left;"><i class="fa fa-circle text-success"></i> {{ $user->role->name }}</a>
                    </li>--}}
                {{-- @if(!canRead(3) || Auth::user()->role_id == 2 || Auth::user()->role_id == 3 || Auth::user()->role_id == 1) --}}
                
                    <li ><a href="{{ URL::to('/home') }}" class="waves-effect"><i class="ti-home"></i> <span>Dashboard</span></a></li>
                     <li ><a href="{{ route('nursinghome.index') }}" class="waves-effect"><i class="fa fa-plus-square"></i> <span>Nursing Homes</span></a></li>
                     <li ><a href="{{ route('doctor.index') }}" class="waves-effect"><i class="fa fa-user-md"></i> <span>Doctors</span></a></li>
                     <li ><a href="{{ route('patient.index') }}" class="waves-effect"><i class="fa fa-users"></i> <span>Patients</span></a></li>
                     {{--<!--<li><a href="{{ route('car.index')}}" class="waves-effect"><i class="fa fa-car"></i> <span>Cars </span></a></li> 
                     <li><a href="{{ route('order.index')}}" class="waves-effect"><i class="fa fa-shopping-cart"></i><span>Orders</span></a></li> 
                     <li><a href="{{ route('account.index')}}" class="waves-effect"><i class="fas fa-money-check-alt"></i> <span>Account</span></a></li> 
                     
                     <li class="{{ isActiveRoute('settings.index','active') }}">                     
                     <a  href="javascript:void(0);" class="waves-effect"><i class="fas fa-cogs"></i> <span>Site Setting</span></a>
                     
                     <ul class="submenu">
                        <li><a  href="{{ route('settings.index')}}" class="waves-effect"><i class="fas fa-cogs"></i> <span>General Info</span></a></li>
                        <li><a href="{{ URL::to('/translations') }}" class="waves-effect"><i class="fas fa-cogs"></i> <span>Contents Setting</span></a></li>
                     </ul>
                     </li>  -->--}}
                 
                    <li class="{{ isActiveRoute('speciality.index','active') }}">
                        <a href="javascript:void(0);" class="waves-effect"><i class="ti-book"></i> <span> Masters <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span> </a>
                        <ul class="submenu">
                            <li><a href="{{ route('speciality.index')}}"><i class="fa fa-circle"></i> Speciality</a></li>
                            <li><a href="{{ route('departments.index')}}"><i class="fa fa-circle"></i>Department</a></li>
                            <li><a href="{{ route('equipments.index')}}"><i class="fa fa-circle"></i>Equipments</a></li>
                            <li><a href="{{ route('countries.index')}}"><i class="fa fa-circle"></i>Countries</a></li>
                            <li class="{{ isActiveRoute('plans.index','active') }}">
                                <a href="{{ route('plans.index')}}" class="waves-effect"><i class="fa fa-circle"></i> <span> Plans </span> </span> </a>
                            </li>

                           {{--<!-- <li><a href="{{ route('colors.index')}}"><i class="fa fa-circle"></i> Colors</a></li>
                            <li><a href="{{ route('engine-types.index')}}"><i class="fa fa-circle"></i>Engine Types</a></li>
                            <li><a href="{{ route('vehicle-types.index')}}"><i class="fa fa-circle"></i>Vehicle Types</a></li>
                            <li><a href="{{ route('transmission.index')}}"><i class="fa fa-circle"></i>Transmissions</a></li>
                            <li><a href="{{ route('model.index')}}"><i class="fa fa-circle"></i>Models</a></li>
                            <li><a href="{{ route('import-brands-models.index')}}"><i class="fa fa-circle"></i> {{ __('l.import_master_brand') }} </a></li> -->--}}
                            
                        </ul>
                        
                    </li>
                    <li class="{{ isActiveRoute('settings.index','active') }}">                     
                     <a  href="javascript:void(0);" class="waves-effect"><i class="fas fa-cogs"></i> <span>Site Setting</span></a>
                     
                     <ul class="submenu">
                        <li><a  href="{{ route('settings.index')}}" class="waves-effect"><i class="fas fa-cog"></i> <span>General Info</span></a></li>
                        <li><a href="{{ route('contents.index') }}" class="waves-effect"><i class="fa fa-language"></i> <span>Contents Setting</span></a></li>
                        <!-- <li><a href="{{ URL::to('/translations') }}" class="waves-effect"><i class="fa fa-language"></i> <span>Contents Setting</span></a></li> -->
                     </ul>
                     </li>

                     
                    <li>  
                        <a onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();" class="collapsible-header waves-effect arrow-r">
                        <i class="fas fa-power-off"></i><span> Logout</span></a>
                    </li>
            </ul>
        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>
    </div>
    <!-- Sidebar -left -->
</div>
<!-- Left Sidebar End -->
