@extends('admin.layouts.master')
@push('styles')
<link rel="stylesheet" href="{{ asset('public/admin/assets/js/trumbowyg/dist/ui/trumbowyg.min.css') }}">
@endpush
@section('content')
<section class="content-header">
    <h5>
        Profile
    </h5>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            @include('admin.partials.messages')
        </div>
    </div>
    <div class="box box-success">
        <div class="box-header with-border">
            <div class="row">
                <div class="col-md-10">
                </div>
            </div>
        </div>
        <div class="box-body">

            <div class="row">
                {{-- @if(canRead(3) && Auth::user()->role_id != 2 && Auth::user()->role_id != 3 && Auth::user()->role_id != 1)
<h2>Hurray you are real user. You are assigned as {{Auth::user()->role->name}} for
                {{Auth::user()->company->organization_name}}. You administrator name is
                {{Auth::user()->company->admin->full_name}}.</h2>
                @else
                <h2>You are assigned as {{Auth::user()->role->name}} for {{Auth::user()->company->organization_name}}.
                    You administrator name is {{Auth::user()->company->admin->full_name}}.</h2>
                @endif --}}

                @php $user = Auth::user(); @endphp

                <div class="col-md-12">
                    <div class="form-group">
                        <label>Name</label>
                        <input value="{{$user->full_name}}" type="text" class="form-control border-input" disabled>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label>Email</label>
                        <input value="{{$user->email}}" type="text" class="form-control border-input" disabled>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label>Mobile</label>
                        <input value="{{$user->mobile}}" type="text" class="form-control border-input" disabled>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label>Alternate Mobile</label>
                        <input value="{{$user->alternate_mobile}}" type="text" class="form-control border-input"
                            disabled>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label>Address</label>
                        <input value="{{$user->detail->address}}" type="text" class="form-control border-input"
                            disabled>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label>City</label>
                        <input value="{{$user->detail->city}}" type="text" class="form-control border-input" disabled>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label>State</label>
                        <input value="{{$user->detail->state}}" type="text" class="form-control border-input" disabled>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label>Country</label>
                        <input value="{{$user->detail->country}}" type="text" class="form-control border-input"
                            disabled>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label>Postcode</label>
                        <input value="{{$user->detail->postcode}}" type="text" class="form-control border-input"
                            disabled>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label>Organization Name</label>
                        <input value="{{$user->company ? $user->company->organization_name : ''}}" type="text"
                            class="form-control border-input" disabled>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label>Registration No.</label>
                        <input value="{{$user->company ? $user->company->registration_no : ''}}" type="text"
                            class="form-control border-input" disabled>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label>Package name.</label>
                        <input
                            value="{{$user->company && $user->company->package ? Auth::user()->company->package->name : ''}}"
                            type="text" class="form-control border-input" disabled>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label>Role.</label>
                        <input value="{{$user->role->name}}" type="text" class="form-control border-input" disabled>
                    </div>
                </div>


            </div>

            <div class="clearfix"></div>

        </div>
    </div>
</section>
@endsection
@push('appendJs')
<script type="text/javascript" src="{{ asset('public/admin/assets/js/post-jobs.js')}}"></script>
@endpush