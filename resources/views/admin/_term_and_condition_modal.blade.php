<!--container end here-->
<div class="modal fade" id="condition-model" tabindex="-1" role="dialog" aria-labelledby="edit-modal-label"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title mt-0">Privacy Policy</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body text-justify" style="height: 100%;"> 
                <p>The Privacy Policy of Immerse digital marketing detailed herein below governs the collection, 
                    Possession, storage, handling and dealing of personal identifiable information/data and sensitive 
                    Personal data of the users of the site. 
                    All the users must read and understand the Privacy Policy. This Privacy Policy outlines the ways the 
                    users can ensure protection of their personal identifiable information. 
                    This Privacy Policy detailed herein is also applicable to user of the site or mobile application through 
                    Mobile or any other similar device.
                 </p><br>
                <h5>COLLECTION OF INFORMATION</h5><br>
                <p>We confirm that we collect the information from you which is required to extend the services available 
                    On the site.
                </p>
                <p>At the time of signing up and registration with the site, we collect user information including name, 
                    Company name, email address, phone/mobile number.  
                    Once a user registers, the user is no longer anonymous to us and thus all the information 
                    Provided by you shall be stored, possessed in order to provide you with the requested services. 
                    User’s registration with us and providing information is intended for facilitating the users in its 
                    Business.
                </p>
                <p>We retain user provided Information for as long as the Information is required for the purpose of 
                    Providing services to you or where the same is required for any purpose for which the Information 
                    can be lawfully processed or retained as required under any statutory enactments or applicable 
                    laws.
                </p><br>
                <p>User may update, correct, or confirm provided information by logging on to their accounts on the site.  
                    Users may also choose to deactivate their accounts on the site. We will evaluate such requests on a 
                    case-to-case basis and take the requisite action as per applicable law. 
                </p><br>
                <h5>PURPOSE AND USAGE OF INFORMATION</h5><br>
                <p>The following are the purposes of collecting the Information: </p>
                    <ul>
                        <li> For the verification of your identity, eligibility, registration and to provide customized services. </li>
                        <li> For facilitating the services offered/available on the site. </li>
                        <li> For enabling communication with the users of the site, so that the users may fetch maximum 
                        business opportunities.</li>
                        <li> For generating business enquires and trade leads. </li>
                        <li> For sending notifications etc. </li>
                    </ul><br>
                <h5>REASONABLE PROTECTION OF INFORMATION </h5><br>
                <p>We employ commercially reasonable and industry-standard security measures to prevent 
                    unauthorized access, maintain data accuracy and ensure proper use of information we receive. 
                    These security measures are both electronic as well as physical but at the same time no data 
                    transmission over the Internet can be guaranteed to be 100% secure.
                </p>
                <p>We recommend you not to disclose password of your email address, online bank transaction and 
                    other important credentials to our employees / agents / affiliates/ personnel, as we do not ask for the 
                    same.
                </p>
                <p>We recommend that registered users not to share their site’s account password and also to sign out 
                    of their account when they have completed their work. This is to ensure that others cannot access 
                    Information of the users and correspondence, if the user shares the computer with someone else or is 
                    using a computer in a public place.
                </p>
                <p>We recommend you not to disclose password of your email address, online bank transaction and 
                    other important credentials to our employees / agents / affiliates/ personnel, as we do not ask for the 
                    same. 
                </p><br>
                <h5>COOKIES</h5><br>
                <p>We use cookies to recognize your browser software and to provide features such as 
                    recommendations and personalization. 
                </p>
                <p>Third parties whose products or services are accessible or advertised through the site, including 
                    social media sites, may also use cookies or similar tools, and we advise you to check their privacy 
                    policies for information about their cookies and the practices followed by them. We do not control the 
                    practices of third parties and their privacy policies govern their interactions with you. 
                </p><br>
                <h5>DATA TRANSFERS</h5><br>
                <p>User Information that we collect may be transferred to, and stored at, any of our affiliates, partners or 
                    service providers which may be inside or outside the country you reside in. By submitting your 
                    personal data, you agree to such transfers.
                </p>
                <p>When we transfer or disclose your Personal Information to other countries, we will protect that 
                    information as described in this Privacy Policy. Relevant, we will ensure appropriate contractual 
                    safeguards to ensure that your information is processed with the highest standards of transparency 
                    and fairness. 
                </p>
        <div class="modal-footer"><button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button></div>
        
        </div>
        
    </div>
    
</div>
</div>
