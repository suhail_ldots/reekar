<?php use App\User; ?>
@extends('admin.layouts.master')
@section('content')
<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <h4 class="page-title">&nbsp</h4>
        </div>
        <div class="col-sm-6">
            <div class="float-right d-md-block">
               
                <a href="{{ route('nursinghome.edit', $item->id)}}">
                    <button class="btn btn-primary arrow-none waves-effect waves-light" type="button">
                        <i class="far fa-edit"></i> {{ __('l.edit') }}
                    </button>
                </a>
                
                <a href="{{ route('nursinghome.index')}}">
                    <button class="btn btn-primary arrow-none waves-effect waves-light" type="button">
                        <i class="fa fa-arrow-left"></i> @lang("l.back")
                    </button>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mt-0 header-title">{{ __('l.nursing_home') }} Details</h4>
                <!-- <small class="form-text text-muted m-b-30" style="color: #9ca8b3 !important;  font-size: 15px;">(Your's user details)</small> -->
                @include('admin.partials.messages')
                <div class="row">
                    <div class="col-md-4">                        
                        <img alt="User Pic" src="{{ @fopen(\Url('storage/app/images/doctor/profile').'/'.$item->profile_pic, 'r') ? \Url('storage/app/images/doctor/profile/').'/'.$item->profile_pic : asset('public/nobody_user.jpg') }}"
                               id="profile_pic" class="" width="150" height="150">                         
                    
                    </div>
                    <div class="col-md-8">
                        <div class="table-responsive">
                            <table class="table mb-0">
                                <thead class="thead-light">
                                    <tr>
                                        <th colspan=2>User Details</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($item->first_name != '')
                                    <tr>
                                        <td><b>Name :</b></td>
                                        <td> {{ isset($item->first_name) ? $item->getFullNameAttribute() : '' }}</td>
                                    </tr>
                                    @endif
                                    <tr>
                                        <td><b>Contact No. :</b></td>
                                        <td>{{ isset($item->country_code) ? $item->country_code : '' }} {{ isset($item->mobile) ? $item->mobile : '' }}</td>
                                    </tr>
                                    
                                    <tr>
                                        <td><b>Email :</b></td>
                                        <td>{{ isset($item->email) ? $item->email : '' }}</td>
                                    </tr>

                                    <tr>
                                        <td><b>Designation :</b></td>
                                        <td>{{ isset($item->role->name) ? $item->role->name : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Subscribed Plan :</b></td>
                                        <td>{!! isset($item->nursingHome) ? $item->nursingHome->package->name : '' !!}<br>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>Plan Expiry date:</b></td>
                                        <td> @php $order = isset($item->nursinghome->subscription_order) ? $item->nursinghome->subscription_order->where('order_status',1)->first() :'' @endphp
                                        {!! isset($order->plan_exp) && $order->plan_exp >= date('Y-m-d') ? date('M d, Y', strtotime($order->plan_exp)) : '<span class="badge-danger badge mr-2">Expired<span>' !!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>Address :</b></td>
                                        <td>{!! isset($item->address) ? $item->address : '' !!}<br>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div id="document-dropzone">
                    
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive table-full-width">
                            <div style="overflow: auto;">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>@lang('l.#')</th>
                                            <th>@lang('l.name')</th>
                                            <th>@lang('l.email')</th>
                                            <th>@lang('l.mobile')</th>  
                                            <th>@lang('l.status')</th> 
                                            <th>Action</th>                                            
                                        </tr>
                                    </thead>
                                    <tbody id="tbody">
                                        @include('admin.modules.nursinghomes.view_tbody')
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
        load_images();
        function load_images()
        {
            $.ajax({
            method :'Get'    ,
            url:"{{ route('dropzone.fetch', $item ? $item->nursing_home_id :'' ) }}",            
            success:function(data)
            {
                // $('#uploaded_image').html(data);
                if(data)
                $('#document-dropzone').append('<h3>Gallery Images</h3>');
                $('#document-dropzone').append(data);
                // $('#uploaded_image').append(data);
            }
            });
        }

        $(document).on('click', '.remove_image', function(){
            var name = $(this).attr('id');
            $.ajax({
            url:"{{ route('dropzone.delete') }}",
            data:{name : name},
            success:function(data){
                var row = $('#document-dropzone').find('.row');
                $('#document-dropzone').find('h3').remove();
                row.empty();
                load_images();
            }
            })
        });
    </script>
@endsection
@if(count($items) > 0)
    @section('script')
    @include('lazyloading.loading')
    @endsection
@endif
