@php 
use App\Models\Cars;
$jsoncurrency = currencyConverter();
@endphp
@if(!empty($items))

@forelse($items as $item)

    @php if($item->currency == 'USD'){
            $icon =  '$ ' ;
        }elseif($item->currency == 'EURO'){
            $icon = '&euro; '; 
        }else{
            $icon = $item->currency.' '; 
        } 
    @endphp  
<tr>
    <td>{{(($items->currentPage() * $items->perPage()) + $loop->iteration) - $items->perPage()}}</td>
    {{--<td ><a href="{{ route('car.show',  $item->id)}}" class="link" title="See Details">{{$item->full_name}}</a></td>--}}
    <td>{{isset($item->user->full_name ) ? $item->user->full_name : ''}} </td>
    <td>{{ isset($item->brand->name ) ? $item->brand->name :''}}</td>
    <td>{{isset($item->model->name) ? $item->model->name : ''}}</td>
    <td>{{isset($item->color->name) ? $item->color->name : ''}}</td>
    <td>{{isset($item->year) ? $item->year : ''}}</td>
    <!-- <td>{{isset($item->available_from_date) ? $item->available_from_date : ''}}</td> -->
    <td>{{isset($item->available_from_date) ? getCurrentTimezoneDate($item->available_from_date) : ''}}</td>
    <!-- <td>{{isset($item->available_from_date) ? date('d-M-Y ', strtotime($item->available_from_date)) : ''}}</td> -->
    <td>{{isset($item->available_from_time) ? getCurrentTimezoneTime($item->available_from_time) : ''}}</td>
    <!-- <td>{{isset($item->available_from_time) ? date('H:m:i', strtotime($item->available_from_time)) : ''}}</td> -->
    <td>{{isset($item->available_to_date) ? getCurrentTimezoneDate($item->available_to_date) : ''}}</td>
    <td>{{isset($item->available_to_time) ? getCurrentTimezoneTime($item->available_to_time) : ''}}</td>
    <td>{!! isset($item->price_per_day) ? '$'.getCurrencyFromUser('USD', 'USD', $item->currency, $item->price_per_day, $jsoncurrency) :"" !!}</td>
    <!-- <td>{!! isset($item->price_per_day) ? $icon.$item->price_per_day :"" !!}</td> -->
     {{-- <!-- <td>{!! isset($item->total_price) ? $icon.$item->total_price :"" !!}</td> -->--}}
      

    <td>
    <a href="javascript:void(0)" id= "{{$item->id}}" data-list-status = "{{$item->listing_status}}" @if($item->listing_status != '1') onclick="change_status(this.id)" @endif >
    <?php if($item->listing_status == 0) { echo '<span class="badge-warning badge mr-2">Pending</span>'; } else if($item->listing_status == 1) { echo '<span class="badge-success badge mr-2">Approved</span>'; }
     elseif($item->listing_status == 2){  echo '<span class="badge-danger badge mr-2">Rejected</span>'; } ?></a>
    
    </td>     
    <td>
        <div class="btn-group" role="group" aria-label="...">
            @if($item->listing_status != 1)
            <a href="{{ route('car.edit', $item->id)}}" class="btn btn-default" title="Edit"><i
                    class="far fa-edit"></i></a>
            @endif

            @if($item->status == 1)
            <a href="{{ route('car.status',[ 'id' => $item->id,'status' => $item->status])}}"
                class="btn btn-default" title="Active"><i class="fas fa-check text-success"></i></a>
            @else
            <a href="{{ route('car.status',[ 'id' => $item->id,'status' => $item->status])}}"
                class="btn btn-default" title="Inactive"><i class="fas fa-times text-danger"></i></a>
            @endif
            @if(!$item->is_rented)
            <a href="{{ route('car.delete',$item->id)}}" class="btn btn-default" title="Delete"><i
                    class="far fa-trash-alt"></i></a>
            @endif
            <a href="{{ route('car.show',$item->id)}}" class="btn btn-default link" title="See Details">
            <i class="fas fa-eye"></i></a>
        </div>
    </td>
    
</tr>
@empty
<tr>
    @if($items->currentPage() == $items->lastPage())
    <td colspan="14" align="center"> {{ __('l.car_not_found') }}</td>
    @else
        <td colspan="14" align="center"> {{ __('l.no_more_record') }}</td>
    @endif
</tr>
@endforelse
@else
<tr>
    <td colspan="14" align="center"> {{ __('l.car_not_found') }}</td>
</tr>
@endif