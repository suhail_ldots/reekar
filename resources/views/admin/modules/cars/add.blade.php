@extends('admin.layouts.master')
@section('css')
<style>
    .required {
        color: red;
    }

    .pad_right {
        padding-right: 0px !important;
    }

    .padd_rl {
        padding-left: 0px !important;
        padding-right: 0px !important;
    }

    @media screen and (max-width: 992px) and (min-width: 768px) {
        .padd_rl {
            font-size: 11px !important;
        }
    }

    @media(max-width:767px) {
        .pad_right {
            padding-right: 15px !important;
        }
    }
</style>

<link href="{{ URL::asset('public/admin/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('content')

<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <h4 class="page-title"> &nbsp</h4>
            <!-- <h4 class="page-title">@lang('l.users')</h4> -->
        </div>         
        <div class="col-sm-6">
            <div class="float-right d-md-block">
                @if($item)
                <a href="{{ route('car.show', $item->id)}}">
                    <button class="btn btn-primary arrow-none waves-effect waves-light" type="button">
                        <i class="fas fa-eye"></i> @lang("l.car") Details
                    </button>
                </a>
                @endif
                <a href="{{ route('car.index')}}">
                    <button class="btn btn-primary arrow-none waves-effect waves-light" type="button">
                        <i class="fas fa-arrow-left"></i> @lang('l.back')
                    </button>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">            
                <h4 class="mb-0 mt-0 header-title"> @if($item){{ __('l.edit') }} @else @lang('l.add') @endif @lang('l.car') </h4>
                <small class="form-text text-muted mt-0" style="color: #9ca8b3 !important;  font-size: 15px;">@if(!$item) (New Car)@endif</small>
                @include('admin.partials.messages')
                <form action="{{route('car.store')}}" method="POST" id="upload_form" autocomplete="off" enctype="multipart/form-data">
                <!-- <form action="{{route('car.store')}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                    @csrf -->
                     
                    @if($item)
                    <input type="hidden" name="id" id="id" value="{{ $item->id }}">
                    @endif
                    <div class="p-20">
                        <h6>Car Details</h6>
                        <div class="row"> 
                            <div class="col-md-6">
                                <div class="form-group">
                                     <label>Model Year</label><span class="required">*</span>
                                    <input value="{{$item ? $item->year : old('year')}}" type="text"
                                        name="year" maxlength="4" id="inputYear" class="form-control"
                                        placeholder="Model Year">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Car Color</label><span class="required">*</span>
                                    <select name="color" id="color" class="form-control">
                                        <option value="">Select Color*</option>
                                        @if(count($ms_colors) > 0)
                                            @foreach($ms_colors as $color)
                                            <option value="{{$color->id}}"  {{isset($item->color_id) && ($item->color_id == $color->id) ? 'selected' :''}}> {{$color->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            
                        </div> 
                       <!--  <h6>Basic Details</h6> -->
                        <div class="row"> 
                            <div class="col-md-6">
                                <div class="form-group">
                                <label>Vehicle Type</label><span class="required">*</span>                               
                                    <select name="vehicle_type" id="vehicle_type" class="form-control">
                                        <option value="">Select Vehicle Type*</option>
                                        @if(count($ms_vehicle_types) > 0)
                                            @foreach($ms_vehicle_types as $vehicle_type)
                                            <option value="{{$vehicle_type->id}}"  {{isset($item->vehicle_type_id) && ($item->vehicle_type_id == $vehicle_type->id) ? 'selected' :''}}> {{$vehicle_type->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Engine Type</label><span class="required">*</span>
                                    <select name="engine_type" id="engine_type" class="form-control">
                                        <option value="">Select Engine Type*</option>
                                        @if(count($ms_engine_types) > 0)
                                            @foreach($ms_engine_types as $engine_type)
                                            <option value="{{$engine_type->id}}"  {{isset($item->engine_type_id) && ($item->engine_type_id == $engine_type->id) ? 'selected' :''}}> {{$engine_type->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                <label>Brand</label><span class="required">*</span>
                                    <select name="brand" id="" class="form-control brand" onchange="getModelByBrand(this.value);">
                                        <option value="">Select Brand*</option>
                                        @if(count($ms_brands) > 0)
                                            @foreach($ms_brands as $brand)
                                            <option value="{{$brand->id}}"  {{(isset($item->brand_id) && $item->brand_id == $brand->id) ? 'selected' :''}}> {{$brand->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                <label >Model</label><span class="required">*</span>
                                    <select name="model" id="model" class="form-control">
                                        <option value="">Select Model*</option>
                                        @if(isset($item->model_id))                                            
                                            <option value="{{$item->model_id}}"  selected> {{$item->model->name}}</option>                                             
                                        @endif 
                                    </select>
                                    <div class="help-block"></div>
                                </div>
                            </div> 
                            
                        </div>
                        <div class="row">
                            <div class="col-md-6" >
                                <div class="form-group">
                                    <label>Transmission</label><span class="required">*</span>
                                    <select name="transmission" id="transmission" class="form-control">
                                        <option value="">Select transmission*</option>
                                        @if(count($ms_transmissions) > 0)
                                            @foreach($ms_transmissions as $transmission)
                                            <option value="{{$transmission->id}}"  {{isset($item->transmission_id) && ($item->transmission_id == $transmission->id) ? 'selected' :''}}> {{$transmission->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                <label>Seat Available</label><span class="required">*</span>
                                    <select name="seat" id="" class="form-control seat">
                                        <option value="">Select Seat*</option>
                                        @if(count($ms_seats) > 0)
                                            @foreach($ms_seats as $seat)
                                            <option value="{{$seat->id}}"  {{(isset($item->seat_id) && $item->seat_id == $seat->id) ? 'selected' :''}}> {{$seat->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    <div class="help-block"></div>
                                </div>
                            </div> 
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Vehicle Chassis Number</label>
                                    <input value="{{$item ? $item->chassis_number : old('chassis_number')}}" type="text" name="chassis_number" maxlength="100"
                                        id="chassis_number" class="form-control" placeholder="Chassis Number">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Mileage (KM)</label>
                                    <input value="{{$item ? $item->mileage : old('mileage')}}" type="text" name="mileage" maxlength="200"
                                        id="inputSecurityAmount" class="form-control" placeholder="Mileage (KM)">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>    

                        @if(!$item)
                        <div class="row">                        
                            <div class="col-md-6">
                                <div class="form-group">
                                <!-- <div class="form-group" style="margin-top:17px;"> -->
                                    <!-- <label>Car Pic</label><span class="required">*</span> --> 
                                    <small id="imageHelp" class="form-text text-muted" >(<b>Car Image:</b> Only jpeg, jpg, png are allowed.)</small>
                                    <input type="file" id="inputGroupFile01" name="car_img[]"
                                        class="imgInp filestyle custom-file-input car_img"
                                        aria-describedby="inputGroupFileAddon01"
                                        data-buttonname="btn-secondary" multiple>
                                        <div class="help-block"></div>
                                </div>                               
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group"> 
                                    <small id="imageHelp" class="form-text text-muted" >(<b>Car Docs:</b> Only jpeg, jpg, png, pdf, doc, docx are allowed.)</small>
                                    <input type="file" id="inputGroupFile02" name="car_docs[]"
                                        class="imgInp filestyle custom-file-input car_docs"
                                        aria-describedby="inputGroupFileAddon02"
                                        data-buttonname="btn-secondary" multiple>
                                        <div class="help-block"></div>
                                </div>                               
                            </div>
                        </div>
                        @endif
                        <h6>Car Availability</h6>
                        <div class="row"> 
                            <div class="col-md-6">
                                <div class="form-group">
                                <label>Available From Date</label><span class="required">*</span> 
                                    <input value="{{$item ? date('Y-m-d', strtotime(getCurrentTimezoneDate($item->available_from_date))) : old('available_from_date')}}" type="date" name="available_from_date" 
                                        id="inputFromDate" class="form-control" placeholder="Available From Date*">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                <label>Available From Time</label><span class="required">*</span>
                                    <input value="{{$item ? date('H:i', strtotime(getCurrentTimezoneTime($item->available_from_time))) : old('available_from_time')}}" type="time" name="available_from_time" 
                                        id="inputFromTime" class="form-control" placeholder="Available From Time*">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div> 
                        <div class="row"> 
                            <div class="col-md-6">
                                <div class="form-group">
                                <label>Available To Date </label><span class="required">*</span>
                                    <input value="{{$item ? date('Y-m-d', strtotime(getCurrentTimezoneDate($item->available_to_date))) : old('available_to_date')}}" type="date" name="available_to_date"  
                                        id="inputToDate" class="form-control" placeholder="Available To Date*">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                <label>Available To Time</label><span class="required">*</span>
                                    <input value="{{$item ? date('H:i', strtotime(getCurrentTimezoneTime($item->available_to_time))) : old('available_to_time')}}" type="time" name="available_to_time"   
                                        id="inputToTime" class="form-control" placeholder="Available To Time*">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                <label>Car Location</label><span class="required">*</span>
                                    <input  value="{{$item ? $item->car_location : old('car_location')}}" type="text" name="car_location" maxlength="255"
                                        id="car_input_location" class="form-control" placeholder="Car Location">

                                    <div class="help-block"></div>
                                    <input type="hidden" name="pickup_lat" value="{{$item ? $item->car_location_lat : old('pickup_lat')}}" id="pickup_lat" >
                                    <input type="hidden" name="pickup_long" value="{{$item ? $item->car_location_long : old('pickup_long')}}" id="pickup_long" >
                                </div>
                            </div>
                        </div>
                        <h6>Rental Cost</h6>
                        <div class="row"> 
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Select Currency</label><span class="required">*</span>
                                    <select name="currency" id="currency" class="form-control" style="padding-left: 3px;">
                                        <option value="">Select Currency*</option>
                                        @php $data =  availCurrency();@endphp
                                        @foreach($data as $availcurrency)
                                        <option value="{{ $availcurrency }}" {{ isset($item->currency) && ($item->currency == $availcurrency ) ? 'selected' :''}}>{{ $availcurrency }}</option>
                                        @endforeach
                                    </select>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                <label>Price Per Day</label><span class="required">*</span>
                                    <input value="{{$item ? $item->price_per_day : old('price_per_day')}}" type="int" name="price_per_day" maxlength="5"
                                        id="inputAmount/Day" class="form-control" placeholder="Amount/Day*">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            {{-- <!-- <div class="col-md-6">
                                <div class="form-group">
                                    <label>Total Amount</label><span class="required">*</span>
                                    <input value="{{$item ? $item->total_price : old('total_amount')}}" type="int" name="total_amount" maxlength="6"
                                        id="inputSecurityAmount" class="form-control" placeholder="Total Price">
                                    <div class="help-block"></div>
                                </div>
                            </div> --> --}}
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Security Amount</label>
                                    <input value="{{$item ? $item->security_amount : old('security_amount')}}" type="int" name="security_amount" maxlength="6"
                                        id="inputSecurityAmount" class="form-control" placeholder="Security Amount">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>
                      
                        <h6>Others</h6>
                        <div class="row">
                            <div class="col-md-6"  >
                                <div class="form-group">
                                    <label>User Name</label><span class="required">*</span>
                                    <select name="user" id="user" class="form-control">
                                        <option value="">Select User*</option>
                                        @if(count($ms_users) > 0)
                                            @foreach($ms_users as $user)
                                            <option value="{{$user->id}}"  {{isset($item->user_id) && ($item->user_id == $user->id) ? 'selected' :''}}> {{$user->full_name}}</option>
                                            @endforeach
                                        @endif
                                    </select> 
                                    <div class="help-block"></div>
                                </div>
                            </div> 
                        </div> 
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                     <input type="checkbox"  name="ac" value="yes" {!! isset($item->ac) && ($item->ac == '1') ? 'checked' :'' !!} >&nbsp AC
                                    <div class="help-block"></div>
                                </div>
                                <div class="form-group">
                                     <input type="checkbox"  name="gps" value="yes" {{isset($item->gps) && ($item->gps == '1') ? 'checked' :''}} >&nbsp GPS
                                    <div class="help-block"></div>
                                </div>
                                <div class="form-group">
                                     <input type="checkbox"  name="ipod_interface" value="yes" {{isset($item->ipod_interface) && ($item->ipod_interface == '1') ? 'checked' :''}}>&nbsp Ipod Interface
                                    <div class="help-block"></div>
                                </div>
                                <div class="form-group">
                                     <input type="checkbox"  name="sunroof" value="yes" {{isset($item->sunroof) && ($item->sunroof == '1') ? 'checked' :''}}>&nbsp Sunroof
                                    <div class="help-block"></div>
                                </div>
                                <div class="form-group">
                                     <input type="checkbox"  name="cruise_control" value="yes" {{isset($item->cruise_control) && ($item->cruise_control == '1') ? 'checked' :''}}>&nbsp Cruise Control
                                    <div class="help-block"></div>
                                </div>
                               
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                     <input type="checkbox"  name="electric_windows" value="yes" {{isset($item->electric_windows) && ($item->electric_windows == '1') ? 'checked' :''}}>&nbsp Electric Windows
                                    <div class="help-block"></div>
                                </div>
                                <div class="form-group">
                                     <input type="checkbox"  name="heated_seat" value="yes" {{isset($item->heated_seat) && ($item->heated_seat == '1') ? 'checked' :''}}>&nbsp Heated Seat
                                    <div class="help-block"></div>
                                </div>
                                <div class="form-group">
                                     <input type="checkbox"  name="panorma_roof" value="yes" {{isset($item->panorma_roof) && ($item->panorma_roof == '1') ? 'checked' :''}}>&nbsp Panorma Roof
                                    <div class="help-block"></div>
                                </div>
                                <div class="form-group">
                                     <input type="checkbox"  name="prm_gauge" value="yes" {{isset($item->prm_gauge) && ($item->prm_gauge == '1') ? 'checked' :''}}>&nbsp Prm Gauge
                                    <div class="help-block"></div>
                                </div>
                                <div class="form-group">
                                     <input type="checkbox"  name="licensed" value="yes" {{isset($item->is_licensed) && ($item->is_licensed == '1') ? 'checked' :''}}>&nbsp is thr Car licensed?
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-4">
                            <div class="form-group">
                                     <input type="checkbox"  name="child_seat" value="yes" {{isset($item->child_seat) && ($item->child_seat == '1') ? 'checked' :''}}>&nbsp Child Seat
                                    <div class="help-block"></div>
                                </div>
                                <div class="form-group">
                                     <input type="checkbox"  name="abs" value="yes" {{isset($item->abs) && ($item->abs == '1') ? 'checked' :''}}>&nbsp ABS
                                    <div class="help-block"></div>
                                </div>
                                <div class="form-group">
                                     <input type="checkbox"  name="traction_control" value="yes" {{isset($item->traction_control) && ($item->traction_control == '1') ? 'checked' :''}}>&nbsp Traction Control
                                    <div class="help-block"></div>
                                </div>
                                <div class="form-group">
                                     <input type="checkbox"  name="audio_system" value="yes" {{isset($item->audio_system) && ($item->audio_system == '1') ? 'checked' :''}}>&nbsp Audio System
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div> 
                        
                        <div class="clearfix"></div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="p-20">
                                <button type="submit"
                                    class="btn btn-primary waves-effect waves-light saveBtn">@lang('l.save')</button>
                                <a href="{{ route('car.index') }}">
                                    <button type="button"
                                        class="btn btn-secondary waves-effect m-l-5">@lang('l.cancel')</button>
                                </a>
                                <div id="ajaxloader" style="display: none;"><img
                                        src="{{ asset('public/admin/images/ajax-loader.gif')}}" /> Processing...</div>

                            </div>
                        </div>
                    </div>
            </div>
        </div>
        </form>
    </div>

    @endsection
    @push('appendJs')
    <!-- <script type="text/javascript" src="{{ asset('public/admin/js/post-jobs.js')}}"></script> -->
    <script src="{{ URL::asset('public/admin/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}"></script>    
    <script src="{{ URL::asset('public/admin/plugins/select2/js/select2.min.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyCbjwwNzm99H7BeeZBpUPX34ANpPIHNy00"></script>

    <script>    
        function getModelByBrand(brand) {
            //alert(brand)
            var toAppend = '';

           // $('#model').html('<option value="" selected >Select Model</option>');
            if (brand !== '') {

                $.ajax({
                    type: 'GET',
                    url: "{{route('get-model-by-brand-ajax')}}?id=" + brand,
                    dataType: 'json',
                    success: function (data) {
                        var obj = data.data;
                        //console.log(obj);
                        for (var i = 0; i < obj.length; i++) {   
                            toAppend += '<option value=' + obj[i]['id'] + '>' + obj[i]['name'] +
                                '</option>';
                        }
                        if(toAppend !== ''){
                            $('#model').html('');
                            $('#model').append(toAppend);
                            toAppend = '';
                        }else{
                            $('#model').html('<option value="" selected >Select Model</option>');
                            alert("Model not found for selected Brand")
                        }
                    }
                });
            } else {
                alert("please choose brand");
            }

        }

        //auto complete car location 

        var searchInput = 'car_input_location';  
        $(document).ready(function () {  
            var autocomplete;
            autocomplete = new google.maps.places.Autocomplete((document.getElementById(searchInput)), {
                types: ["geocode"],
            });
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var near_place = autocomplete.getPlace();
                //assign lat long only for pickup location value to 
                document.getElementById('pickup_lat').value = near_place.geometry.location.lat();
                document.getElementById('pickup_long').value = near_place.geometry.location.lng();
                           
            });   
        });   
        
    </script>

    <script>
        $(document).ready(function () {
            $('#user').select2();
            $('.brand').select2();
            $('.seat').select2();
            $('#model').select2();
            $('#vehicle_type').select2();
            $('#engine_type').select2();
            $('#color').select2(); 
            $('#transmission').select2();
            $('#currency').select2(); 
        });
    </script>
    @endpush