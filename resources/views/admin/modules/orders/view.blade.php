<?php 
use App\User; 
$jsoncurrency = currencyConverter();

?>
@extends('admin.layouts.master')
@section('content')
<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <h4 class="page-title">&nbsp</h4>
        </div>
        <div class="col-sm-6">
            <div class="float-right d-md-block"> 
                <a href="{{ url()->previous() }}">
                    <button class="btn btn-primary arrow-none waves-effect waves-light" type="button">
                        <i class="fa fa-arrow-left"></i> @lang("l.back")
                    </button>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">{{ __('l.order') }} Details</h4>
                <!-- <small class="form-text text-muted m-b-30" style="color: #9ca8b3 !important;  font-size: 15px;">(Your user's order details)</small> -->
                @include('admin.partials.messages')
                <div class="row">                   
                    <div class="col-md-10">
                        <div class="table-responsive">
                            <table class="table mb-0">
                                <thead class="thead-light">
                                    <tr>
                                        <th colspan=4>Order Details</th>
                                    </tr>
                                </thead>
                                @php 
                                    if($item->currency == 'USD'){
                                        $icon =  '$ ' ;
                                    }elseif($item->currency == 'EURO'){
                                        $icon = '&euro; '; 
                                    }else{
                                        $icon = $item->currency.' '; 
                                    } 
                                @endphp  
                                <tbody>
                                    <tr>
                                        <td><b>Hired By:</b></td>
                                        <td> {{ isset($item->user->full_name) ? $item->user->full_name : '' }}</td>
                                        <td><b>Contact No. :</b></td>
                                        <td>{{ isset($item->user->country_code) ? $item->user->country_code : '' }} {{ isset($item->user->mobile) ? $item->user->mobile : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Owner Name:</b></td>
                                        <td> {{ isset($item->car->user->full_name) ? $item->car->user->full_name : '' }}</td>
                                        <td><b>Owner Contact No. :</b></td>
                                        <td>{{ isset($item->car->user->country_code) ? $item->car->user->country_code : '' }} {{ isset($item->car->user->mobile) ? $item->car->user->mobile : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Car Name:</b></td>
                                        <td>{{ isset($item->car->brand->name) ? $item->car->brand->name : '' }} {{ isset($item->car->model->name) ? $item->car->model->name : '' }}</td>
                                        <td><b>Model Year :</b></td>
                                        <td>{{ isset($item->car->year) ? $item->car->year : '' }}</td>
                                    </tr> 
                                    <tr>
                                        <td><b>Booking Id :</b></td>
                                        <td>{{ isset($item->booking_id) ? $item->booking_id : '' }}</td>
                                        <td><b>Bank Referencer Id :</b></td>
                                        <td>{{ isset($item->bank_reference_id) ? $item->bank_reference_id : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Booking From :</b></td>
                                        <td>{{ isset($item->booking_from_date) ? getCurrentTimezoneDateTime($item->booking_from_date) : '' }}</td>
                                        <td><b>Booking To :</b></td>
                                        <td>{{ isset($item->booking_to_date) ? getCurrentTimezoneDateTime($item->booking_to_date) : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Total Amount  :</b></td>
                                        <!-- <td>{!! isset($item->total_amount) ? $icon.$item->total_amount : '' !!}</td> -->
                                        <td>{!! isset($item->total_amount) ? '$'.getCurrencyFromUser('USD', 'USD', $item->currency, $item->total_amount, $jsoncurrency) :"" !!}</td>
                                        <td><b>Security Amount :</b></td>
                                        <!-- <td>{!! isset($item->security_amount) ? $icon.$item->security_amount : '' !!}</td> -->
                                        <td>{!! isset($item->security_amount) ? '$'.getCurrencyFromUser('USD', 'USD', $item->currency, $item->security_amount, $jsoncurrency) :"" !!}</td>
                                    </tr>                                   
                                    <tr>
                                        <td><b>Tax Amount :</b></td>
                                        <!-- <td>{!! isset($item->tax_amount) ? $icon.$item->tax_amount : '' !!}</td> -->
                                        <td>{!! isset($item->tax_amount) ? '$'.getCurrencyFromUser('USD', 'USD', $item->currency, $item->tax_amount, $jsoncurrency) :"" !!}</td>
                                        <td><b>Payment Method :</b></td>
                                        <td>{{ isset($item->payment_method) ? $item->payment_method : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Paid Amount  :</b></td>
                                        <!-- <td>{!! isset($item->paid_amount) ? $icon.$item->paid_amount : '' !!}</td> -->
                                        <td>{!! isset($item->paid_amount) ? '$'.getCurrencyFromUser('USD', 'USD', $item->currency, $item->paid_amount, $jsoncurrency) :"" !!}</td>
                                        <td><b>Grand Total :</b></td>
                                        <!-- <td>{!! isset($item->grand_total) ? $icon.$item->grand_total : '' !!}</td> -->
                                        <td>{!! isset($item->grand_total) ? '$'.getCurrencyFromUser('USD', 'USD', $item->currency, $item->grand_total, $jsoncurrency) :"" !!}</td>
                                        
                                    </tr>
                                    <tr>
                                        <td><b>Transaction Id :</b></td>
                                        <td>{{ isset($item->transaction_id) ? $item->transaction_id : '' }}</td>
                                      <td></td>
                                      <td></td>
                                    </tr>
                                    <tr>
                                        <td><b>Billing First Name :</b></td>
                                        <td>{{ isset($item->billing_first_name) ? $item->billing_first_name : '' }}</td>                                    
                                        <td><b>Billing Last Name :</b></td>
                                        <td>{{ isset($item->billing_last_name) ? $item->billing_last_name : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Billing Email :</b></td>
                                        <td>{{ isset($item->billing_email) ? $item->billing_email : '' }}</td>                                    
                                        <td><b>Billing Mobile :</b>
                                        {{ isset($item->user->country_code) ? $item->user->country_code : '' }} {{ isset($item->billing_mobile) ? $item->billing_mobile : '' }}</td> 
                                        <td><b>Billing Alternate Mobile :</b>
                                        {{ isset($item->user->country_code) ? $item->user->country_code : '' }} {{ isset($item->billing_alternate_mobile) ? $item->billing_alternate_mobile : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Billing Address :</b></td>
                                        <td>{{ isset($item->billing_address) ? $item->billing_address : '' }}</td>                                    
                                        <td><b>Pickup Location :</b></td>
                                         <td>{{ isset($item->pickup_location) ? $item->pickup_location : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Order Status :</b></td>
                                        <td><?php if($item->order_status == 1) { echo '<span class="badge-warning badge mr-2">Pending</span>'; }
                                            else if($item->order_status == 2)
                                            { echo '<span class="badge-success badge mr-2">Success</span>'; }
                                            else if($item->order_status == 3) {echo '<span class="badge-danger badge mr-2">Failed</span>';}
                                            else if($item->order_status == 4){echo '<span class="badge-danger badge mr-2">Canceled</span>';} ?>
                                        </td>                                   
                                        <td><b>Order Date :</b></td>
                                        <td>{{ isset($item->updated_at) ? getCurrentTimezoneDateTime($item->updated_at) : '' }}</td>
                                    </tr>
                                    <tr>
                                    @if($item->doc != NULL) 
                                       
                                        @php
                                        $docs = json_decode($item->doc);
                                        foreach($docs as $doc){
                                            $exist1 = \Url('storage/app/images/orders/docs/').'/'.$doc ;
                                            $exist  =  @fopen($exist1, 'r');
                                            }
                                            @endphp
                                   
                                        <td><b>Identity Image: </b></td>
                                        <td>
                                        @foreach($docs as $doc)
                                        @if($exist) 
                                        
                                            <a href="{{ $exist1 ??''}}" target="blank">
                                            @php $extension = explode('.', $exist1); @endphp
                                            @if( $extension[1] == 'png' || $extension[1] == 'jpeg' || $extension[1] == 'jpg')    
                                            <img src="{{ $exist1 }}" id="user_id"  width="150" height="150"></a>
                                            @else
                                            <iframe src="{{ $exist1 }}" frameborder="0" width="150" height="120" scrolling="no"></iframe>See</a>
                                            @endif 
                                         
                                        @endif
                                    @endforeach
                                    </td> 
                                    @endif
                                    @if($item->order_invoice != NULL)

                                    @php   
                                           $invoice1 = \Url('storage/invoice/').'/'.$item->order_invoice ;
                                           $invoice  =  @fopen($invoice1, 'r'); @endphp
                                    
                                    @if($invoice)
                                    
                                    <td><b>Order Invoice: </b></td> 
                                    <td>
                                        <a href="{{ $invoice1 ?? '' }}" target="blank">
                                        @php $ext = explode('.', $invoice1); @endphp
                                        @if( $ext[1] == 'png' || $ext[1] == 'jpeg' || $ext[1] == 'jpg')
                                        <img src="{{ $invoice1 }}" id="invoice"  width="150" height="150"></a>
                                        @else    
                                        <embed type="application/pdf" src="{{ $invoice1 }}" frameborder="0" width="150" height="120" scrolling="no"></iframe>See</a>
                                        @endif
                                    </td> 
                                @endif



                                    @endif
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                   
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection