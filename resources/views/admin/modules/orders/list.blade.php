@extends('admin.layouts.master')
@section('content')
<div class="page-title-box">
    {{-- <div class="row align-items-center">
        <div class="col-sm-6">
            <h4 class="page-title"> &nbsp</h4>
            <!-- <h4 class="page-title"> {{ __('l.members') }}</h4> -->
        </div>
        <div class="col-sm-6">
            <div class="float-right d-md-block">
                 
                <a href="{{ route('order.create')}}">
                    <button class="btn btn-primary arrow-none waves-effect waves-light" type="button">
                        <i class="fas fa-plus"></i> @lang('l.add') @lang('l.order')
                    </button>
                </a>
                 
            </div>
        </div>
    </div> --}}
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mt-0 header-title m-b-30">{{ __('l.order') }} {{ __('l.list') }}</h4>
                @include('admin.partials.messages')
                <div class="table-responsive table-full-width">
                    <div style="overflow: auto;">
                        <form action="{{ route('order.search')}}" method="GET">
                            <div class="row"> 
                                <div class="col-md-12">
                                    <div class="col-md-4" style="float:left;">
                                        <div class="form-group">
                                            <input type="text" name="hired_by" maxlength="250" class="form-control" placeholder="Hired By First Name"
                                                @if(isset($name)) value="{{ $name }}" @endif autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="float:left;">
                                        <div class="form-group">
                                            <input type="text" name="brand" maxlength="250" class="form-control" placeholder="Brand Name"
                                                @if(isset($brand)) value="{{ $brand }}" @endif autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="float:left;">
                                        <div class="form-group">
                                            <input type="text" name="booking_id" maxlength="250" class="form-control" placeholder="Booking Id"
                                                @if(isset($booking_id)) value="{{ $booking_id }}" @endif autocomplete="off">
                                        </div>
                                    </div>
                                   <!--  <div class="col-md-3" style="float:left;">
                                        <div class="form-group">
                                            <input type="text" name="order_status" maxlength="250" class="form-control" placeholder="Order Status"
                                                @if(isset($order_status)) value="{{ $order_status }}" @endif autocomplete="off">
                                        </div>
                                    </div> -->
                                     
                                    <div class="col-md-4" style="float:left;">
                                        <div class="form-group">
                                        <small class="form-text text-muted mt-0" style="color: #9ca8b3 !important;  font-size: 13px;">Order Date :</small>
                                            <input type="date" name="order_date" class="form-control" placeholder="Order Date"
                                                @if(isset($order_date)) value="{{ $order_date }}" @endif autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="float:left;">
                                        <div class="form-group"><br>
                                            <button class="btn btn-primary dropdown-toggle arrow-none waves-effect waves-light"
                                                type="submit"><i class="fa fa-search"></i> Search</button>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </form>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>@lang('l.#')</th>
                                    <th>Hired By</th>
                                    <th>@lang('l.car') @lang('l.name')</th>
                                    <th>Booking Id</th>
                                    <th>Paid Amount</th> 
                                    <th>Order Status</th>
                                    <th>Order Date</th>
                                    <th>Action</th>
                                    
                                </tr>
                            </thead>
                            <tbody id="tbody">
                                @include('admin.modules.orders.tbody')
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@if(count($items) > 0)
    @section('script')
    @include('lazyloading.loading')
    @endsection
@endif
