@forelse($items as $item)
<tr>
    <td>{{(($items->currentPage() * $items->perPage()) + $loop->iteration) - $items->perPage()}}</td>
    <td>{{$item->name}}</td>
    <td>{{$item->time_duration}}</td>
    <td>{{$item->price}}</td>
    <td>
        @if($item->status == 1) <span class="badge badge-success">@lang('active')</span>
        @else <span class="badge badge-danger">@lang('inactive')</span>
        @endif
    </td>
    
    <td>
        <div class="btn-group" role="group" aria-label="...">
            
            <a href="{{ route('plans.edit',$item->id)}}" class="btn btn-default"><i
                    class="far fa-edit"></i></a>
           
            {{-- @if(canDelete(2))
            <a href="{{ route('plans.delete',$item->id)}}" class="btn btn-default"><i
                class="far fa-trash-alt"></i></a>
            @endif --}}
        </div>
    </td>
   
</tr>

@empty
<tr>
    @if($items->currentPage() == $items->lastPage())
        <td colspan="11" align="center">Plan Not Found</td>
    @else
        <td colspan="10" align="center"> {{ __('l.no_more_record') }}</td>
    @endif
</tr>
@endforelse