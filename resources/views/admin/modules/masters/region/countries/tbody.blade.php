@forelse($items as $item)
<tr>
    <td>{{(($items->currentPage() * $items->perPage()) + $loop->iteration) - $items->perPage()}}</td>
    <td>{{$item->name}}</td>
   {{-- <td><a class="image-popup-vertical-fit"
            href="{{ $item->brand_logo ? $item->brand_logo : asset('assets/images/default-brand.png') }}"><img
                src="{{ $item->brand_logo ? $item->brand_logo : asset('assets/images/default-brand.png') }}"
                class="rsposive-product" alt="Brand logo"></a></td> --}}
    
    <td>
        <div class="btn-group" role="group" aria-label="...">
           
            <a href="{{ route('countries.edit',$item->id)}}" class="btn btn-default"><i class="far fa-edit"></i></a>
            <a href="{{ route('countries.delete',$item->id)}}" class="btn btn-default"><i class="far fa-trash-alt"></i></a>
        </div>
    </td>     
</tr>

@empty
<tr>
    @if($items->currentPage() == $items->lastPage())
    <td colspan="11" align="center"> {{ __('No Country Found.') }}</td>
    @else
        <td colspan="10" align="center"> {{ __('No More Record.') }}</td>
    @endif
</tr>
@endforelse