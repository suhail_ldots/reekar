@extends('admin.layouts.master')
@push('styles')
@endpush

@section('content')
<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <h4 class="page-title"> &nbsp</h4>
            <!-- <h4 class="page-title"> {{ __('l.sub_sub_region') }}</h4> -->
        </div>
        <div class="col-sm-6">
            <div class="float-right d-md-block">
                <a href="{{ route('sub_sub_region.index')}}">
                    <button class="btn btn-primary arrow-none waves-effect waves-light" type="button">
                        <i class="fas fa-arrow-left"></i> @lang('l.back')
                    </button>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                @if($item)
                <h4 class="mt-0 header-title m-b-30">{{ __('l.edit_sub_sub_region') }}</h4>
                @else
                <h4 class="mt-0 header-title m-b-30">{{ __('l.add_sub_sub_region') }}</h4>
                @endif
                @include('admin.partials.messages')
                <form action="{{route('sub_sub_region.store')}}" method="POST" onsubmit="return saveData(this)"
                    autocomplete="off">
                    {{ csrf_field() }}
                    @if($item)
                    <input type="hidden" name="id" id="id" value="{{ $item->id }}">
                    @endif
                    <div class="row">
                        <div class="col-md-6">
                            <div class="p-20">
                                <div class="form-group">
                                    <label>{{ __('l.country') }}</label>
                                    <select name="country_id" id="country" class="form-control dynamic"
                                        data-dependent="state_id" onchange="getStateByCountry(this.value);">
                                        <option>{{ __('l.select_country') }}</option>
                                        @isset($selected_country)
                                        <option value="{{$selected_country->id}}" selected>{{$selected_country->name}}
                                        </option>
                                        @endisset
                                        @foreach($countries as $country)
                                        <option value="{{$country->id}}">{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                    <div class="help-block"></div>
                                </div>
                                <div class="form-group">
                                    <label>{{ __('l.state') }}</label>
                                    <select name="state_id" id="state" class="form-control"
                                        onchange="getCityByState(this.value);">
                                        <option>Select State</option>
                                        @isset($selected_state)
                                        <option value="{{$selected_state->id}}" selected>{{$selected_state->name}}
                                        </option>
                                        @endisset
                                        @php $currentState = isset($selected_country->id) ? state($selected_country->id)
                                        : array(); @endphp
                                        @if(count($currentState) > 0)
                                        @foreach($currentState as $state)
                                        <option value="{{  $state->id }}">{{$state->name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                    <div class="help-block"></div>
                                </div>
                                <div class="form-group">
                                    <label>{{ __('l.city_name') }}</label>
                                    <select name="city_id" id="city" class="form-control select2">
                                        <option value="">Select City *</option>

                                        @isset($selected_city)
                                        <option value="{{$selected_city->id}}" selected>{{$selected_city->name}}
                                        </option>
                                        @endisset

                                        @php $currentCity = isset($selected_state->id) ? city($selected_state->id) :
                                        array(); @endphp
                                        @foreach($currentCity as $city)
                                        <option value="{{ $city->id }}">{{$city->name}}</option>
                                        @endforeach
                                    </select>
                                    <div class="help-block"></div>
                                </div>
                                <div class="form-group">
                                    <label>{{ __('l.sub_sub_region_name') }}</label>
                                    <input value="{{$item ? $item->name : ''}}" type="text" name="name" maxlength="250" id="inputEmail"
                                        class="form-control" placeholder="{{ __('l.sub_sub_region_name') }}"
                                        autocomplete="off">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="p-20">
                                <button type="submit"
                                    class="btn btn-primary waves-effect waves-light saveBtn">{{ __('l.save') }}</button>
                                <a href="{{ route('sub_sub_region.index') }}">
                                    <button type="button"
                                        class="btn btn-secondary waves-effect m-l-5">{{ __('l.cancel') }}</button>
                                </a>
                                <div id="ajaxloader" style="display: none;"><img
                                        src="{{ asset('assets/images/ajax-loader.gif')}}" />{{ __('l.processing') }}
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
            </div>
        </div>
        </form>
    </div>
</div>
@endsection
@push('appendJs')
<script type="text/javascript" src="{{ asset('public/admin/js/post-jobs.js')}}"></script>
@endpush

<script>
    function getStateByCountry(country) {
        var toAppend = '';

        $('#state').html('<option value="" selected >Select State*</option>');
        if (country !== '') {

            $.ajax({
                type: 'GET',
                url: "{{route('get-state-by-country-ajax')}}?id=" + country,
                dataType: 'json',
                success: function (data) {
                    for (var i = 0; i < data.length; i++) {
                        toAppend += '<option value=' + data[i]['id'] + '>' + data[i]['name'] + '</option>';
                    }
                    $('#state').append(toAppend);
                    toAppend = '';
                }
            });
        } else {
            alert("please choose country");
        }

    }

    function getCityByState(city) {
        var toAppend = '';
        //alert(city);
        $('#city').html('<option value="" selected >Select Region*</option>');
        if (city !== '') {
            $.ajax({
                type: 'GET',
                url: "{{route('get-city-by-state-ajax')}}?id=" + city,
                dataType: 'json',
                success: function (data) {
                    for (var i = 0; i < data.length; i++) {
                        toAppend += '<option value=' + data[i]['id'] + '>' + data[i]['name'] + '</option>';
                    }
                    $('#city').append(toAppend);
                    toAppend = '';
                }
            });
        } else {
            alert("please choose state");
        }

    }
    //get sub region by city 
</script>