@extends('admin.layouts.master')
@section('content')
@if(canWrite(2))
<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <h4 class="page-title"> &nbsp</h4>
            <!-- <h4 class="page-title"> {{ __('l.states') }}</h4> -->
        </div>
        <div class="col-sm-6">
            <div class="float-right d-md-block">
                <a href="{{ route('states.create')}}">
                    <button class="btn btn-primary arrow-none waves-effect waves-light" type="button">
                        <i class="fas fa-plus"></i> {{ __('l.add') }} {{__('l.state')}}
                    </button>
                </a>
            </div>
        </div>
    </div>
</div>
@endif
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mt-0 header-title m-b-30">{{ __('l.states_list') }}</h4>
                @include('admin.partials.messages')
                <div class="table-responsive table-full-width">
                    <div style="overflow: auto;">
                    <form action="{{route('admin.state_search')}}" method="GET">                        
                        <!-- <div class="row"> -->
                            <div class="col-md-12">
                                <div class="col-md-3" style="float:left;">
                                    <div class="form-group">
                                        <input type="text" name="name" maxlength="250" class="form-control" placeholder="Name"
                                            @if(isset($name)) value="{{ $name }}" @endif autocomplete="off">
                                    </div>
                                </div>                                
                            </div>
                            <div class="col-md-2" style="float:left;">
                                <div class="form-group">
                                    <select name="selected_country" id="selected_country" class="select2 form-control">
                                        <option value="">Select Country</option>
                                        @foreach(country() as $thiscountry)
                                            <option value="{{$thiscountry->id}}" @if(isset($selected_country)) dd("dsfdsf"); @if($selected_country == $thiscountry->id) selected @endif @endif>{{$thiscountry->name}}</option>
                                        @endforeach
                                        
                                        
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2" style="float:left;">
                                <div class="form-group">
                                    <button class="btn btn-primary dropdown-toggle arrow-none waves-effect waves-light"
                                        type="submit"><i class="fa fa-search"></i> Search</button>
                                </div>
                            </div>
                        </form>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>{{ __('l.#') }}</th>
                                    <th>{{ __('l.name') }}</th>
                                    <th>{{ __('l.country') }}</th>
                                    @if(canUpdate(2) || canDelete(2))
                                    <th>{{ __('l.actions') }}</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody id="tbody">
                                @include('admin.modules.masters.region.states.tbody')
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
@include('lazyloading.loading')
@endsection