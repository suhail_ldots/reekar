@extends('admin.layouts.master')
@section('content')

<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <h4 class="page-title"> &nbsp</h4>
            <!-- <h4 class="page-title"> {{ __('l.cities') }}</h4> -->
        </div>
        <div class="col-sm-6">
            @if(canWrite(9))
            <div class="float-right d-md-block">
                <a href="{{ route('cities.create')}}">
                    <button class="btn btn-primary arrow-none waves-effect waves-light" type="button">
                        <i class="fas fa-plus"></i> {{ __('l.add') }} {{ __('l.city')}}
                    </button>
                </a>
            </div>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mt-0 header-title m-b-30">{{ __('l.cities_list') }}</h4>
                @include('admin.partials.messages')
                <div class="table-responsive table-full-width">
                    <div style="overflow: auto;">
                    <form action="{{route('admin.city_search')}}" method="GET">                        
                        <!-- <div class="row"> -->
                            <div class="col-md-12">
                                <div class="col-md-3" style="float:left;">
                                    <div class="form-group">
                                        <input type="text" name="name" maxlength="250" class="form-control" placeholder="Name"
                                            @if(isset($name)) value="{{ $name }}" @endif autocomplete="off">
                                    </div>
                                </div>                                
                            </div>
                            <div class="col-md-3" style="float:left;">
                                <div class="form-group">
                                    <select name="selected_country" id="selected_country" class="select2 form-control" onchange="getStateByCountry(this.value)">
                                        <option value="">Select Country</option>
                                        @foreach(country() as $thiscountry)
                                            <option value="{{$thiscountry->id}}" @if(isset($selected_country)) @if($selected_country == $thiscountry->id) selected @endif @endif>{{$thiscountry->name}}</option>
                                        @endforeach
                                        
                                        
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3" style="float:left;">
                                <div class="form-group">
                                    <select name="selected_state" id="state" class="select2 form-control">
                                        <option value="">Select State</option>
                                        @php $currentState = isset($selected_country) ? state($selected_country):array(); @endphp
                                        
                                        @if(count($currentState) > 0)
                                            @foreach($currentState as $state)
                                            <option value="{{$state->id}}" @if($state->id == $selected_state) selected
                                                @endif>{{$state->name}}</option>
                                            @endforeach
                                        @endif
                                        
                                        
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2" style="float:left;">
                                <div class="form-group">
                                    <button class="btn btn-primary dropdown-toggle arrow-none waves-effect waves-light"
                                        type="submit"><i class="fa fa-search"></i> Search</button>
                                </div>
                            </div>
                        </form>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>{{ __('l.#') }}</th>
                                    <th>{{ __('l.name') }}</th>
                                    <th>{{ __('l.country') }}</th>
                                    <th>{{ __('l.state') }}</th>
                                   <!--  <th>{{ __('l.zipcode') }}</th> -->
                                    @if(canUpdate(2) || canDelete(2))
                                    <th>{{ __('l.actions') }}</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody id="tbody">
                                @include('admin.modules.masters.region.cities.tbody')
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
@include('lazyloading.loading')
<script>
    function getStateByCountry(country) {
        var toAppend = '';
        $('#state').html('<option value="" selected >Select State</option>');
        if (country !== '') {

            $.ajax({
                type: 'GET',
                url: "{{route('get-state-by-country-ajax')}}?id=" + country,
                dataType: 'json',
                success: function (data) {
                    for (var i = 0; i < data.length; i++) {
                        toAppend += '<option value=' + data[i]['id'] + '>' + data[i]['name'] + '</option>';
                    }
                    $('#state').append(toAppend);
                    //$('#state').html(toAppend);
                    toAppend = '';
                }
            });
        }/*  else {
            alert("please choose country");
        }
 */
    }
</script>
@endsection