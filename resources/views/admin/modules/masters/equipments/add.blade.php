@extends('admin.layouts.master')
@push('styles')

@endpush
@section('content')
<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <h4 class="page-title"> &nbsp</h4>
        </div>
        <div class="col-sm-6">
            <div class="float-right d-none d-md-block">
                <a href="{{ route('equipments.index')}}">
                    <button class="btn btn-primary arrow-none waves-effect waves-light" type="button">
                     <i class="fas fa-arrow-left"></i> @lang('l.back')
                    </button>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mt-0 header-title m-b-30">@if($item) @lang('l.edit') @else @lang('l.add') @endif
                   Equipment</h4>
                @include('admin.partials.messages')
                <form action="{{route('equipments.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                    @if($item)
                    <input type="hidden" name="id" id="id" value="{{ $item->id }}">
                    @endif
                    <div class="row">
                        <div class="col-md-6">
                            <div class="p-20">
                            <div class="form-group">
                                    <label>  Equipment  @lang('l.name')</label>
                                    <input value="{{$item ? $item->name : old('name')}}" type="text" name="name" maxlength="250" id="inputEmail"
                                        class="form-control" placeholder="Equipments {{ __('l.name') }}">
                                        @if($errors->has('name'))
                                            <div class="help-block required">{{ $errors->first('name') }}</div>
                                        @endif
                                </div>
                            </div>
                        </div>
                        </div>
                        
                    <div class="row">
                        <div class="col-md-12">
                            <div class="p-20">
                                <button type="submit"
                                    class="btn btn-primary waves-effect waves-light saveBtn">{{ __('l.save') }}</button>
                                <a href="{{ route('equipments.index') }}">
                                    <button type="button"
                                        class="btn btn-secondary waves-effect m-l-5">{{ __('l.cancel') }}</button>
                                </a>
                                <div id="ajaxloader" style="display: none;"><img
                                        src="{{ asset('public/admin/images/ajax-loader.gif')}}" />{{ __('l.processing') }}
                                </div>
                            </div>
                        </div>
                    </div>
                        
                   
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
@push('appendJs')
<script src="{{ URL::asset('public/admin/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/admin/js/post-jobs.js')}}"></script>
@endpush