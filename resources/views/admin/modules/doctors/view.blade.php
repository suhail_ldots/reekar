@extends('admin.layouts.master')
@section('content')
<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <h4 class="page-title">&nbsp</h4>
        </div>
        <div class="col-sm-6">
            <div class="float-right d-md-block">
               
                <a href="{{ route('doctor.edit', $item->id)}}">
                    <button class="btn btn-primary arrow-none waves-effect waves-light" type="button">
                        <i class="far fa-edit"></i> {{ __('l.edit') }}
                    </button>
                </a>
                
                <a href="{{ route('doctor.index')}}">
                    <button class="btn btn-primary arrow-none waves-effect waves-light" type="button">
                        <i class="fa fa-arrow-left"></i> @lang("l.back")
                    </button>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mt-0 header-title">{{ __('l.doctor') }} Details</h4>
                <!-- <small class="form-text text-muted m-b-30" style="color: #9ca8b3 !important;  font-size: 15px;">(Your's user details)</small> -->
                @include('admin.partials.messages')
                <div class="row">
                    <div class="col-md-4">                        
                    <img alt="User Pic" src="{{ @fopen(\Url('storage/app/images/doctor/profile').'/'.$item->profile_pic, 'r') ? \Url('storage/app/images/doctor/profile/').'/'.$item->profile_pic : asset('public/nobody_user.jpg') }}"
                               id="profile_pic" class="" width="150" height="150">
                    </div>
                    <div class="col-md-8">
                        <div class="table-responsive">
                            <table class="table mb-0">
                                <thead class="thead-light">
                                    <tr>
                                        <th colspan=2>Details</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($item->first_name != '')
                                    <tr>
                                        <td><b>Name :</b></td>
                                        <td> {{ isset($item->first_name) ? $item->getFullNameAttribute() : '' }}</td>
                                    </tr>
                                    @endif
                                    @if($item->doctor != '')
                                    <tr>
                                        <td><b>Qualification :</b></td>
                                        <td> {{ isset($item->doctor) ? $item->doctor->degree : '' }}</td>
                                    </tr>
                                    @endif
                                    <tr>
                                        <td><b>Contact No. :</b></td>
                                        <td>{{ isset($item->country_code) ? $item->country_code : '' }} {{ isset($item->mobile) ? $item->mobile : '' }}</td>
                                    </tr>
                                    
                                    <tr>
                                        <td><b>Email :</b></td>
                                        <td>{{ isset($item->email) ? $item->email : '' }}</td>
                                    </tr>

                                    <tr>
                                        <td><b>Designation :</b></td>
                                        <td>{{ isset($item->role->name) ? $item->role->name : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Speciality :</b></td>
                                        <td>{{ isset($item->doctor->doctorSpeciality) ? $item->doctor->doctorSpeciality->name : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Department :</b></td>
                                        <td>{{ isset($item->doctor->department) ? $item->doctor->department->name : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Registration No. :</b></td>
                                        <td>{{ isset($item->doctor) ? $item->doctor->reg_no : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Availability :</b></td>
                                        <td>{{ isset($item->doctor) ? getWorkingDay($item->doctor->working_days) : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Timing :</b></td>
                                        <td>{{ isset($item->doctor->availability) ? getDoctorTiming($item->doctor->availability) : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Address :</b></td>
                                        <td>{!! isset($item->address) ? $item->address : '' !!}<br>
                                        </td>
                                    </tr>
                                    
                                   
                                   
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection