@extends('admin.layouts.master')
@section('css')
<style>
    .required {
        color: red;
    }

    .pad_right {
        padding-right: 0px !important;
    }

    .padd_rl {
        padding-left: 0px !important;
        padding-right: 0px !important;
    }

    @media screen and (max-width: 992px) and (min-width: 768px) {
        .padd_rl {
            font-size: 11px !important;
        }
    }

    @media(max-width:767px) {
        .pad_right {
            padding-right: 15px !important;
        }
    }
</style>
<link href="{{ URL::asset('public/admin/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('content')

<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <h4 class="page-title"> &nbsp</h4>
            <!-- <h4 class="page-title">@lang('l.doctors')</h4> -->
        </div>
         
        <div class="col-sm-6">
            <div class="float-right d-md-block">
                @if($item)
                <a href="{{ route('doctor.show', $item->id)}}">
                    <button class="btn btn-primary arrow-none waves-effect waves-light" type="button">
                        <i class="fas fa-eye"></i> @lang("l.doctor") Details
                    </button>
                </a>
                @endif
                <a href="{{ route('doctor.index')}}">
                    <button class="btn btn-primary arrow-none waves-effect waves-light" type="button">
                        <i class="fas fa-arrow-left"></i> @lang('l.back')
                    </button>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">            
                <h4 class="mb-0 mt-0 header-title"> @if($item){{ __('l.edit') }} @else @lang('l.add') @endif @lang('l.doctor') </h4>
                <small class="form-text text-muted mt-0" style="color: #9ca8b3 !important;  font-size: 15px;">@if(!$item) (New Doctor)@endif</small>
                @include('admin.partials.messages')
                <form action="{{route('doctor.store')}}" method="POST" id="upload_form" autocomplete="off" enctype="multipart/form-data" >
                   @csrf
                    @if($item)
                    <input type="hidden" name="id" id="id" value="{{ $item->id }}">
                    @endif

                    <div class="p-20">
                        <h6>Basic Details</h6>
                        <div class="row"> 
                            <div class="col-md-6">
                                <div class="form-group">
                                    <!--  <label>@lang('l.first_name')</label><span class="required">*</span> -->
                                    <input value="{{$item ? $item->first_name : old('first_name')}}" type="text"
                                        name="first_name" maxlength="100" id="inputEmail" class="form-control"
                                        placeholder="@lang('l.first_name')*">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <!--  <label>@lang('l.last_name')</label><span class="required">*</span> -->
                                    <input value="{{$item ? $item->last_name : old('last_name')}}" type="text"
                                        name="last_name" maxlength="100" id="inputEmail" class="form-control"
                                        placeholder="@lang('l.last_name')">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row"> 
                            <div class="col-md-6">
                                <div class="form-group">
                                    <!-- <label style="margin-bottom: -8px;">Nursing Home</label>
                                    <small id="imageHelp" class="form-text text-muted">(Note :- You can select only multi-speciality hospitals from here.)</small> -->
                                    <select name="speciality" id="speciality" class="form-control">
                                        <option value="">Select Doctor Speciality* </option>                                      
                                       
                                        @foreach(speciality() as $speciality)
                                            <option value="{{ $speciality->id }}" {{isset($item->doctor->speciality) && ($item->doctor->speciality == $speciality->id) ? "selected" :''}}>{{ $speciality->name }}</option>
                                        @endforeach
                                       
                                    </select>
                                    
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <!-- <label style="margin-bottom: -8px;">Nursing Home</label>
                                    <small id="imageHelp" class="form-text text-muted">(Note :- You can select only multi-speciality hospitals from here.)</small> -->
                                    <select name="department" id="department" class="form-control">
                                        <option value="">Select Doctor Department*</option>
                                        @foreach(Departments() as $department)
                                            <option value="{{ $department->id }}" {{isset($item->doctor->department_id) && ($item->doctor->department_id == $department->id) ? "selected" :''}}>{{ $department->name }}</option>
                                        @endforeach
                                    </select>
                                    
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row"> 
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input value="{{$item && $item->doctor  ? $item->doctor->reg_no : old('reg_no')}}" type="text" name="reg_no" maxlength="200"
                                        id="inputEmail" class="form-control" placeholder="Enter your license or registration no.*">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input value="{{$item && $item->doctor  ? $item->doctor->degree : old('degree')}}" type="text" name="degree" maxlength="200"
                                        id="inputEmail" class="form-control" placeholder="Doctor qualifications ">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="row"> 
                        @if(($item && $item->nursinghome == '') || !$item )
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label style="margin-bottom: -8px;">Nursing Home</label>
                                    <small id="imageHelp" class="form-text text-muted">(Note :- You can select only multi-speciality hospitals from here.)</small>
                                    <select name="nursing_home" id="usertype" class="form-control">
                                        <option value="">Select Nursing Home* </option>                                      
                                        @if(isset($nursinghome))   
                                            @foreach($nursinghome as $nursing)
                                                <option value="{{ $nursing->id }}" {{isset($item->nursing_home_id) && ($item->nursing_home_id == $nursing->id) ? "selected" :''}}>{{ $nursing->organization_name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        @endif
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label style="margin-bottom: -8px;">Profile Pic</label>
                                    <small id="imageHelp" class="form-text text-muted">(Max Upload 2MB, Only jpeg, png, jpg are allowed.)</small>
                                    <input type="file" id="inputGroupFile01" name="profile_img"
                                        class="imgInp filestyle custom-file-input"
                                        aria-describedby="inputGroupFileAddon01"
                                        data-buttonname="btn-secondary">
                                        <div class="help-block"></div>
                                </div>                               
                            </div>
                        </div>
                        <h6>Contact Info</h6>
                        <div class="row"> 
                            <div class="col-md-6">
                                <div class="form-group">
                                    <!-- <label>@lang('l.country_code')</label> -->
                                    <select name="country_code" id="country_code" class="form-control">
                                        <option value="">Select Country Code</option>
                                        @if(CountryCodes())
                                            @foreach(CountryCodes() as $code)
                                            <option value="{{$code->dial_code}}" {{isset($item->country_code) && ($item->country_code == $code->dial_code) ? "selected" :''}}>{{$code->dial_code}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input value="{{$item ? $item->mobile : old('mobile')}}" type="text" name="mobile" maxlength="25"
                                        id="inputEmail" class="form-control" placeholder="@lang('l.mobile') No.*">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div> 
                        <div class="row"> 
                            <div class="col-md-6">
                                <div class="form-group">
                                    <!-- <label>@lang('l.country_code')</label> -->
                                    <select name="country" id="country" class="form-control">
                                        <option value="">Select Country*</option>
                                        @if(country())
                                            @foreach(country() as $code)
                                            <option value="{{$code->id}}" {{isset($item->country) && ($item->country == $code->id) ? "selected" :''}}>{{$code->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input value="{{$item ? $item->city : old('city')}}" type="text" name="city" maxlength="200"
                                        id="inputEmail" class="form-control" placeholder="city">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input value="{{$item && $item->address ? $item->address : old('address')}}"
                                        type="text" name="address" maxlength="250" id="inputEmail" class="form-control"
                                        placeholder="Address">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>
                        <h6>Account Details</h6>
                        <div class="row"> 
                            <div class="col-md-12">
                                <div class="form-group">
                                    <!--  <label>@lang('l.email_add')</label><span class="required">*</span> -->
                                    <input value="{{$item ? $item->email : old('email')}}" {{ $item ? "readonly" : ''}}
                                        type="email" name="email" maxlength="70" id="inputEmail" class="form-control"
                                        placeholder="@lang('l.email_add') *">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>
                        @if(!$item)
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                <small class="form-text text-muted">(<b>Hint: </b> Your password must be equal or more than 8 characters.)</small>                                
                                    <input value="" type="password" name="password" maxlength="250" id="inputPassword"
                                        class="form-control showpass" placeholder="@lang('l.password')*"
                                        autocomplete="off">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <!-- <label for="password_confirmation" >Confirm Password</label><span class="required">*</span> -->
                                    <small class="form-text text-muted">&nbsp</small>                                
                                    <input type="password" name="password_confirmation" maxlength="250" id="password_confirmation"
                                        class="form-control showpass @error('password_confirmation') is-invalid @enderror"
                                        value="{{old('password_confirmation')}}" placeholder="Confirm Password*">
                                    <span toggle=".showpass" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>
                        @endif
                        <h6>About Doctor</h6>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea name="about_nursing" id="" cols="30" rows="6"  
                                    placeholder="Enter about doctor*"  class="form-control">{{ $item && $item->doctor ? $item->doctor->about_doc : old('about_nursing') }}</textarea>
                                    
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="p-20">
                                <button type="submit"
                                    class="btn btn-primary waves-effect waves-light saveBtn">@lang('l.save')</button>
                                <a href="{{ route('doctor.index') }}">
                                    <button type="button"
                                        class="btn btn-secondary waves-effect m-l-5">@lang('l.cancel')</button>
                                </a>
                                <div id="ajaxloader" style="display: none;"><img
                                        src="{{ asset('public/admin/images/ajax-loader.gif')}}" /> Processing...</div>

                            </div>
                        </div>
                    </div>
            </div>
        </div>
        </form>
    </div>

    @endsection
    @push('appendJs')
    <!-- <script type="text/javascript" src="{{ asset('public/admin/js/post-jobs.js')}}"></script> -->
    <script src="{{ URL::asset('public/admin/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}"></script>    
    <script src="{{ URL::asset('public/admin/plugins/select2/js/select2.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#department').select2();
            $('#usertype').select2();
            $('#country').select2();
            $('#speciality').select2();
            $('#vintage').select2();
            $('#size').select2();
            $('#brand').select2();
            $('#state').select2();
            $('#city').select2();
            $('#country_code').select2();
            $('#rating').select2();
            $('#subregion').select2();
        });
    </script>
    @endpush