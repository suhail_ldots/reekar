@extends('admin.layouts.master')
@section('css')
<!-- <style>
    #myloading { color: green; }
</style> -->
@endsection
@section('content')
@php use App\Models\ErrorLogs; @endphp
@if(canWrite(2))
<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <h4 class="page-title"> &nbsp</h4>
        </div>
        
        <!-- <div class="col-sm-6">
            <div class="float-right d-none d-md-block">
                    <a href="{{ route('cities.create')}}">
                        <button class="btn btn-primary arrow-none waves-effect waves-light" type="button">
                            <i class="fas fa-plus"></i> {{ __('l.add_new') }}
                        </button>
                    </a>
            </div>
        </div> -->
    </div>
</div>
@endif
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="mt-0 header-title">@lang('l.logs_list')</h4>
                        <small class="form-text text-muted m-b-30" style="color: #9ca8b3 !important;  font-size: 15px;">(Check the User Activity Log.)</small>    
                        {{-- <div class="float-right">
                        <label for="filter">Search by date:-</label>
                            <input type="Date" name="filter" placeholder="Enter date" id="filter">
                        </div>    --}}
                        <div class="table-responsive table-full-width">
                                <form action="{{ route('adminlogs.search')}}" method="GET">
                                <div class="col-md-2" style="float:left;">
                                    <div class="form-group">
                                        <select name="user" id="type" class="form-control">
                                            <option value="">Select User</option>
                                            @if(loguser())
                                                @foreach(loguser() as $newtype)
                                                    @if(isset($newtype->users['first_name']))
                                                    <option value="{{ $newtype->users['id'] }}" @if(isset($user)) @php if($newtype->users['id'] ==
                                                        $user){ echo 'selected'; } @endphp @endif>{{ $newtype->users['first_name'].' '.$newtype->users['last_name'] }}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2" style="float:left;">
                                    <div class="form-group">
                                        <select name="company" id="Company" class="form-control select2">
                                            <option value="">Select Company</option>
                                            @if(logcompany())
                                                @foreach(logcompany() as $newstyle)
                                                    @if(isset($newstyle->company['id']))
                                                    <option value="{{ $newstyle->company['id'] }}" @if(isset($company)) @php if($newstyle->company['id'] == $company){ echo 'selected'; } @endphp @endif>{{ $newstyle->company['organization_name'] }}
                                                    </option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2" style="float:left;">
                                    <div class="form-group">
                                        <select name="event" id="event" class="form-control select2">
                                            <option value="">Select Event</option>
                                            @if(logevent())
                                                @foreach(logevent() as $newstyle)
                                                    <option value="{{ $newstyle->event }}" @if(isset($event)) @php if($newstyle->event == $event){ echo 'selected'; } @endphp @endif>{{ $newstyle->event }}
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2" style="float:left;">
                                    <div class="form-group">
                                        <select name="action" id="action" class="form-control select2">
                                            <option value="">Select Action</option>
                                            @if(logaction())
                                                @foreach(logaction() as $newstyle)
                                                    <option value="{{ $newstyle->action }}" @if(isset($action)) @php if($newstyle->action == $action){ echo 'selected'; } @endphp @endif>{{ $newstyle->action }}
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                        <div class="col-md-3" style="float:left;">
                                            <div class="form-group">
                                                <button class="btn btn-primary dropdown-toggle arrow-none waves-effect waves-light" type="submit"><i class="fa fa-search"></i> Search</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <table class="table table-striped" id="dtBasicExample">
                                    <thead>
                                        <tr>
                                            <th>@lang('l.#')</th>
                                            <th>@lang('l.logs_created_by')</th>
                                            <th>@lang('l.log_comapny')</th>
                                            <th>@lang('l.log_ip')</th>
                                            <th>@lang('l.log_event')</th>
                                            <th>@lang('l.actions')</th>
                                            <th>@lang('l.created_at')</th>                                        
                                        </tr>
                                    </thead>
                                    <tbody id="tbody">                                         
                                        @include('admin.logs.logs_list')
                                    </tbody>

                                </table>
                        </div>
                    </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
@include('lazyloading.loading')
@endsection

