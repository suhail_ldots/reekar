@extends('front.layouts.main')
@section('title')
<title>Doctors Loop - Patient access to hope</title>
@endsection

@section('css') 
    
@endsection

@section('div') 
    
@endsection
@section('content')
      <div class="content-area">
        <div class="container">
          <br>
          <div id="wcfm-main-contentainer">
            <div id="wcfm-content">
              <div class="wcfm-membership-wrapper">
                <h2>Registration</h2>
                <div id="wcfm_membership_container">
                @foreach($errors->all() as $error)
                <li class="required">{{ $error }}</li><br>
                 @endforeach 
                  
                    <div class="wcfm-container">
                      <div id="wcfm_membership_registration_form_expander" class="wcfm-content">
                       
                        <form action="{{ route('user.verify') }}" method="post" id="verifyEmail" >
                          @csrf
                          <p class="user_email wcfm_ele wcfm_title"><strong>Email<span class="required">*</span></strong></p>
                          <label class="screen-reader-text" for="user_email">Email<span class="required">*</span></label><input type="text" id="user_email" name="email" class="wcfm-text wcfm_ele " value="{{ \Session::has("email") ? \Session::get("email") : old('email') }}" placeholder="" data-required="1" data-required_message="Email: This field is required."  />
                           <!--  @error('email')
                              <span class="help-block required" role="alert">
                                 <li>{{ $message }}</li>
                             </span>
                            @enderror	 -->						
                          <input type="submit" name="wcfm_email_verified_button" class="wcfm-text wcfm_submit_button wcfm_email_verified_button" value="Re-send Code" />
                        </form>
                        <form action="{{ route('register') }}" method ="post" class="wcfm">
                        @csrf
                        <input type="hidden" name="email" value={{ \Session::has("email") ? \Session::get("email") : ''}} />
                        <div class="wcfm_email_verified">                        
                          <input type="number" name="otp" data-required="1" data-required_message="Email Verification Code: This field is required." class="wcfm-text wcfm_email_verified_input" placeholder="Verification Code" value="{{ old('otp') }}" />                         
                          <!-- @error('otp')
                          <span class="help-block required" role="alert">
                              <li>{{ $message }}</li>
                          </span>
                        @enderror -->
                        </div>
                        <div class="wcfm-message email_verification_message" tabindex="-1"></div>
                        <div class="wcfm_clearfix"></div>
                        <p class="first_name wcfm_ele wcfm_title"><strong>First Name  <span class="required">*</span> </strong></p>
                        <label class="screen-reader-text" for="first_name">First Name <span class="required">*</span> </label><input type="text" id="first_name" name="first_name" class="wcfm-text wcfm_ele " value="{{ old('first_name') }}" placeholder=""   />
                       <!--  @error('first_name')
                          <span class="help-block required" role="alert">
                              <li>{{ $message }}</li>
                          </span>
                        @enderror	 -->
                        
                        <p class="last_name wcfm_ele wcfm_title"><strong>Last Name  <span class="required">*</span></strong></p>
                        <label class="screen-reader-text" for="last_name">Last Name <span class="required">*</span></label><input type="text" id="last_name" name="last_name" class="wcfm-text wcfm_ele " value="{{ old('last_name') }}" placeholder=""   />
                       <!--  @error('last_name')
                          <span class="help-block required" role="alert">
                              <li>{{ $message }}</li>
                          </span>
                        @enderror  --> 
                      
                        <p class="country_code wcfm_ele wcfm_title"><strong>Country Code  <span class="required">*</span></strong></p>
                        <label class="screen-reader-text" for="country_code">Country Code <span class="required">*</span></label>
                        <select name="country_code"
                              class="selectpicker input-price wcfm-text wcfm_ele" data-live-search="true"
                              data-width="60%"
                              data-toggle="tooltip" title="Select"> 
                                <option value="">Select Country Code</option>
                                @if(CountryCodes())
                                    @foreach(CountryCodes() as $code)
                                    <option value="{{$code->dial_code}}" {{isset($item->country_code) && ($item->country_code == $code->dial_code) ? "selected" :''}}>{{$code->dial_code}}</option>
                                    @endforeach
                                @endif
                            </select><br>
                            <!-- @error('country_code')
                          <span class="danger required" role="alert">
                              <li>{{ $message }}</li>
                          </span>
                        @enderror -->
                                 
                            
                        <p class="dc5a97b5eb1c5fd1f5b2282795e4c8f0 wcfm_title"><strong>Phone <span class="required">*</span></strong></p>
                        <label class="screen-reader-text" for="dc5a97b5eb1c5fd1f5b2282795e4c8f0">Phone <span class="required">*</span></label><input type="text" id="dc5a97b5eb1c5fd1f5b2282795e4c8f0" name="mobile" class="wcfm-text" value="{{ old('mobile') }}" placeholder=""   />
                        <!-- @error('mobile')
                          <span class="help-block required" role="alert">
                              <li>{{ $message }}</li>
                          </span>
                        @enderror -->


                        <p class="address wcfm_ele wcfm_title"><strong> Address <span class="required"></span></strong></p>
                        <label class="screen-reader-text" for="address"> Address <span class="required"></span></label><input type="text" id="address" name="address" class="wcfm-text wcfm_ele" value="{{ old('address') }}" placeholder="" data-required="1" data-required_message=" Address : This field is required."  />
                        <!-- @error('address')
                          <span class="help-block required" role="alert">
                              <li>{{ $message }}</li>
                          </span>
                        @enderror -->

                        <p class="password wcfm_ele wcfm_title"><strong>Password<span class="required">*</span></strong></p>
                        <label class="screen-reader-text" for="password">Password<span class="required">*</span></label><input type="password" id="password" name="password" class="wcfm-text wcfm_ele " value="{{ old('password') }}" placeholder="" data-required="1" data-required_message="Password: This field is required." data-mismatch_message="Password and Confirm-password are not same."  />
                        <!-- @error('password')
                          <span class="help-block required" role="alert">
                              <li>{{ $message }}</li>
                          </span>
                        @enderror -->
                        <div id="passowrd_strength" name="passowrd_strength" class=""  >
                          <div id="password-strength-status"></div>
                        </div>
                        <p class="password_confirmation wcfm_ele wcfm_title"><strong>Confirm Password<span class="required">*</span></strong></p>
                        <label class="screen-reader-text" for="password_confirmation">Confirm Password<span class="required">*</span></label><input type="password" id="password_confirmation" name="password_confirmation" class="wcfm-text wcfm_ele " value="{{ old('password_confirmation') }}" placeholder="" data-required="1" data-required_message="Confirm Password: This field is required."  />			
                        <!-- @error('password_confirmation')
                          <span class="help-block required" role="alert">
                              <li>{{ $message }}</li>
                          </span>
                        @enderror -->
                      </div>
                      <div class="wcfm-clearfix"></div>
                    </div>
                    <div class="wcfm-clearfix"></div>
                    <div class="wcfm-message" tabindex="-1"></div>
                    <div id="wcfm_membership_registration_submit" class="wcfm_form_simple_submit_wrapper">
                      <input type="submit" value="Register" id="wcfm_membership_register_button" class="wcfm_submit_button" />
                    </div>
                    <div class="wcfm-clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <figure class="wp-block-image"><img alt=""/></figure>
        </div>
      </div>
      
    <div id="um_upload_single" style="display:none">
    </div>
    <div id="um_view_photo" style="display:none">
      <a href="#" data-action="um_remove_modal" class="um-modal-close"><i class="um-faicon-times"></i></a>
      <div class="um-modal-body photo">
        <div class="um-modal-photo">
        </div>
      </div>
    </div>
    @endsection