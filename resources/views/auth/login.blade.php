@extends('site_view.layouts.main')
@section('head')
<style>
    #login_form{     padding: 50px 30px;  background: #f6f6f6;  width: 100%; display: table;margin: 50px auto;  box-shadow: 1px 2px 5px #0000004d;}
     #login_form .form-control{ border: 1px solid #e0e0e0;   border-radius: 30px; height: 49px;}
     #login_form .has-error{}
     #login_form .form-group{ margin-bottom:15px !important; position: relative; min-height: 90px; }
     #login_form .help-block {  margin-top: 0;  margin-bottom: 0;    position: absolute;   bottom: 0;}
     .fixed-mar-top-inner{ clear:both;}
     
     @media only screen and (min-width:320px) and (max-width:991px) {
         #login_form{ margin:110px auto 100px;}
        }
</style>
@endsection
@section('content')
<div class="container fixed-mar-top-inner"> 
      <div class="row">
        <div class="col-12 col-lg-12"> 
      <h2 style="text-align:center;margin-top: 50px;color: #000;font-size: 35px;margin-bottom: -10px;">Sign in</h2>
          <form class="formbox" action="{{ route('login') }}" method="post" id="login_form" enctype="multipart/form-data">
          @csrf

          <div class="col-12 col-md-12 mx-auto white mt-2">
          @if(Session::has('message'))
            <div class="alert alert-dismissible alert-danger">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <a href="#" class="alert-link">{{Session::get('message')}}</a>
            </div>
            @endif
            @if(Session::has('login_error'))
            <div class="alert alert-dismissible alert-danger">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <a href="#" class="alert-link">{{Session::get('login_error')}}</a>
            </div>
            @endif

            @if(Session::has('success'))
            <div class="alert alert-dismissible alert-success">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <a href="#" class="alert-link">{{Session::get('success')}}</a>
            </div>
            @endif

            @if(Session::has('error'))
            <div class="alert alert-dismissible alert-danger">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <a href="#" class="alert-link">{{Session::get('error')}}</a>
            </div>
            @endif
            @if (count($errors) > 0)
            <div class="alert alert-dismissible alert-danger">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                @foreach ($errors->all() as $error)
                {{ $error }}
                @endforeach
            </div>
            @endif
           <div class="row">
                <div class="col-md-12">
                  <div class="md-form mb-0 form-group">
                    <label for="email" class="">Email</label>
                    <input type="text" name="email" value="" class="form-control">
                    <div class="help-block"></div>
                  </div>
              </div>
            </div>
           <div class="row">
              <div class="col-md-12">
                  <div class="md-form mb-0 form-group">
                  <label for="email" class="">Password</label>
                       <div class="{{ $errors->has('password') ? ' has-error' : '' }}">
                          <input type="password" name="password" class="form-control" value="{{old('password')}}" placeholder="">
                          <div class="help-block"></div>
                            @error('password')
                                <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                 </div>
            </div>
            <div class="row" style="margin:0">  
                    <div class="col-md-8">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="remember" id="customControlInline"
                                {{ old('remember') ? 'checked' : '' }}>
                            <label class="custom-control-label" for="customControlInline">Remember me</label>
                        </div>
                    </div> 
                    <div class="col-md-4">
                    <!-- <div class="col-12 col-lg-12 text-center my-4" style="margin:20px auto"> -->
                            <button class="btn btn-primary w-100 p-0 m-0 py-3 saveBtn" type="submit" style="padding:10px 20px; font-size:15px">{{ __('login') }}</button>
                        
                            <div id="ajaxloader" style="display: none;"><img
                                    src="{{ asset('public/admin/images/ajax-loader.gif')}}" /> Processing...</div>

                    <!-- </div> -->
                    </div>
                    @if (Route::has('password.request'))
                    <div class=" m-t-10 mb-0 col-md-12" >
                        <div class="col-12 m-t-20">
                            <a href="{{route('password.request') }}"><i class="mdi mdi-lock"></i> Forgot your password?</a>
                        </div>
                    </div>
                    @endif
            </div><hr>
            <div class="row">   
                <div class="col-md-12"> 
                    <div class="text-center py-1 my-3">
                        
                        Don't have Account?<a href="{{route('register')}}"> {{ __('register') }}</a>
                        
                    </div>
                </div>
            </div>
              </div>
             </div>
          </div>
        </form>
      </div>
  </div>
</div>
<!--container end here-->
</div>
@endsection
