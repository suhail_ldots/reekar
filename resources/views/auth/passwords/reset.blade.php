@extends('admin.layouts.master_blank')

@section('content')
<div class="wrapper-page">
    <div class="card overflow-hidden account-card mx-3">
        <div class="bg-primary p-4 text-white text-center position-relative">
            <h4 class="font-20 m-b-5">Forgot Password </h4>
            <p class="text-white-50 mb-4">&nbsp;</p>
            <a href="{{ URL::to('/') }}" class="logo logo-admin"><img
                    src="{{ URL::asset('public/admin/images/favicon.png') }}" height="40" alt="logo"></a>
        </div>
        <div class="account-card-content">
            <form method="POST" action="{{ route('password.update') }}">
                @csrf
                <input type="hidden" name="tz" id="tz">
                <input type="hidden" name="token" value="{{ $token }}">

                <div class="form-group row">
                    <label for="email" class="col-form-label text-md-right">Email</label>

                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                        name="email" maxlength="70" value="{{ $email ?? old('email') }}" required autocomplete="email" readonly>

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group row">
                    <label for="userpassword" class="col-form-label text-md-right">{{ __('Password') }}</label>

                    <input id="userpassword" type="password" class="form-control showpass @error('password') is-invalid @enderror"
                        name="password" maxlength="250" required autocomplete="new-password"
                        placeholder="Password must have 8 characters or number" autofocus>

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group row">
                    <label for="password-confirm"
                        class="col-form-label text-md-right">{{ __('Confirm Password') }}</label>
                    <input id="password_confirm" type="password" class="form-control showpass" name="password_confirmation" maxlength="250"
                        required autocomplete="new-password" placeholder="Password must have 8 characters or number">
                        <span toggle=".showpass" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                        @if($errors->has('password_confirmation'))
                        <span class="help-block">
                            {{ $errors->first('password_confirmation') }}
                        </span>
                        @endif
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-6 text-right">
                        <button type="submit" class="btn btn-primary" id="resetPassword">
                            {{ __('Reset Password') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="m-t-40 text-center">
        <p>Already have an account ? <a href="{{route('login')}}" class="font-500 text-primary"> Login </a> </p>
        <p>© {{date('Y')}} Doctors Loop</p>
    </div>

</div>
<!-- end wrapper-page -->
@endsection
@section('script')
<script>
    $( document ).ready(function() {
       // alert("zfvds")
        $('input').attr('autocomplete', 'off');

        var email = $("#email").val().length;
        var pass  = $("#userpassword").val().length;
        var pass_conf  = $("#password_confirm").val().length;              
        if(email == 0 || pass == 0 || pass_conf == 0){
            $('#resetPassword').prop( "disabled", true );
        }
        $("#userpassword").keyup(function(){           
            var email1 = $("#email").val().length;
            var pass1  = $("#userpassword").val().length; 
            var pass_conf1 = $("#password_confirm").val().length; 
            
            if(email1 != 0 && pass1 != 0 && pass_conf1 != 0){
                $('#resetPassword').prop( "disabled", false);
            }else{
                $('#resetPassword').prop( "disabled", true );
            }
        });
        $("#email").keyup(function(){            
            var email2 = $("#email").val().length;
            var pass2  = $("#userpassword").val().length;  
            var pass_conf2  = $("#password_confirm").val().length;  
           
            if(email2 != 0 && pass2 != 0 && pass_conf2 != 0){
                $('#resetPassword').prop( "disabled", false);
            }else{
                $('#resetPassword').prop( "disabled", true );
            }
        });

        $("#password_confirm").keyup(function(){            
            var email3 = $("#email").val().length;
            var pass3  = $("#userpassword").val().length;  
            var pass_conf3  = $("#password_confirm").val().length;  
           
            if(email3 != 0 && pass3 != 0 && pass_conf3 != 0){
                $('#resetPassword').prop( "disabled", false);
            }else{
                $('#resetPassword').prop( "disabled", true );
            }
        });

    });
</script>
<script>
   
   $(".toggle-password").click(function () {
       $(this).toggleClass("fa-eye fa-eye-slash");
       var input = $($(this).attr("toggle"));
       if (input.attr("type") == "password") {
           input.attr("type", "password");
       } else {
           input.attr("type", "text");
       }
   });
</script>
@endsection