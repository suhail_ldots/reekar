@extends('site_view.layouts.main')
@section('head')
<style>
    #login_form{     padding: 50px 30px;  background: #f6f6f6;  width: 100%; display: table;margin: 50px auto;  box-shadow: 1px 2px 5px #0000004d;}
     #login_form .form-control{ border: 1px solid #e0e0e0;   border-radius: 30px; height: 49px;}
     #login_form .has-error{}
     #login_form .form-group{ margin-bottom:15px !important; position: relative; min-height: 90px; }
     #login_form .help-block {  margin-top: 0;  margin-bottom: 0;    position: absolute;   bottom: 0;}
     .fixed-mar-top-inner{ clear:both;}
     
     @media only screen and (min-width:320px) and (max-width:991px) {
         #login_form{ margin:110px auto 100px;}
        }
</style>
@endsection
@section('content')
<!--  <div class="home-btn d-none d-sm-block">
            <a href="index" class="text-dark"><i class="fas fa-home h2"></i></a>
        </div>
 -->
<div class="wrapper-page">
    <div class="card overflow-hidden account-card mx-3">
        <!-- <div class="bg-primary p-4 text-white text-center position-relative">
            <h4 class="font-20 m-b-5">Forgot Password </h4>
            <p class="text-white-50 mb-4">&nbsp;</p>
            <a href="{{ URL::to('/') }}" class="logo logo-admin"><img
                    src="{{ URL::asset('public/admin/images/favicon.png') }}" height="40" alt="logo"></a>
        </div> -->
        <div class="account-card-content">

            @if(Session::has('login_error'))
            <div class="alert alert-dismissible alert-danger">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <a href="#" class="alert-link">{{Session::get('login_error')}}</a>
            </div>
            @endif

            @if(Session::has('success'))
            <div class="alert alert-dismissible alert-success">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <a href="#" class="alert-link">{{Session::get('success')}}</a>
            </div>
            @endif

            @if(Session::has('error'))
            <div class="alert alert-dismissible alert-danger">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <a href="#" class="alert-link">{{Session::get('error')}}</a>
            </div>
            @endif
            @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
            @endif

            <form class="form-horizontal m-t-30" method="POST" id="login_form" action="{{ route('password.email') }}">
                @csrf
                <input type="hidden" name="tz" id="tz">
                <div class="form-group">
                    <label for="username">Email</label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                        name="email" maxlength="70" value="{{ old('email') }}" required autocomplete="email" placeholder="Enter email"
                        autofocus>
                </div>
                @error('email')
                <span class="help-block" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
                <div class="form-group row m-t-20">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6 text-right">
                        <button class="btn btn-primary w-md waves-effect waves-light"
                            type="submit" id="resetBtn">{{ __('Reset Password') }}</button>
                    </div>

                </div>
            </form>
        </div>
    </div>

    <div class="m-t-0 text-center">
        <p>Already have an account ? <a href="{{route('login')}}" class="font-500 text-primary"> Login </a> </p>
       
    </div>

</div>
<!-- end wrapper-page -->
@endsection

@section('script')
<script>
    $( document ).ready(function() {
        var email = $("#email").val().length;
        
        if(email == 0){
            $("#resetBtn").prop( "disabled", true );
        }
        
        $("#email").keyup(function(){            
            var email2 = $("#email").val().length;             
           
            if(email2 != 0){
                $("#resetBtn").prop( "disabled", false);
            }else{
                $("#resetBtn").prop( "disabled", true );
            }
        });

    });
</script>

@endsection