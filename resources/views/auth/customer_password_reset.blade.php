<!DOCTYPE html>
<html lang="en-US">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Password Reset &#8211; Reekar</title>
    <link rel='dns-prefetch' href='http://code.jquery.com/' />
    <link rel='dns-prefetch' href='http://maps.googleapis.com/' />
    <link rel='dns-prefetch' href='http://s.w.org/' />
    <link rel="alternate" type="application/rss+xml" title="Reekar &raquo; Feed" href="feed/index.html" />
    <link rel="alternate" type="application/rss+xml" title="Reekar &raquo; Comments Feed" href="comments/feed/index.html" />
    <link rel='stylesheet' id='wp-block-library-css' href='wp-includes/css/dist/block-library/style.min9dff.css?ver=5.3.2' type='text/css' media='all' />
    <link rel='stylesheet' id='wc-block-style-css' href='wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/stylea1ec.css?ver=2.3.0' type='text/css' media='all' />
    <link rel='stylesheet' id='contact-form-7-css' href='wp-content/plugins/contact-form-7/includes/css/styles58e0.css?ver=5.1.4' type='text/css' media='all' />
    <link rel='stylesheet' id='woocommerce-layout-css' href='wp-content/plugins/woocommerce/assets/css/woocommerce-layout3088.css?ver=3.7.0' type='text/css' media='all' />
    <link rel='stylesheet' id='woocommerce-smallscreen-css' href='wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen3088.css?ver=3.7.0' type='text/css' media='only screen and (max-width: 768px)' />
    <link rel='stylesheet' id='woocommerce-general-css' href='wp-content/plugins/woocommerce/assets/css/woocommerce3088.css?ver=3.7.0' type='text/css' media='all' />
    <link rel='stylesheet' id='spu-public-css-css' href='wp-content/plugins/popups/public/assets/css/public4bfd.css?ver=1.9.3.6' type='text/css' media='all' />
    <link rel='stylesheet' id='wcfm_enquiry_button_css-css' href='wp-content/plugins/wc-frontend-manager/assets/css/min/enquiry/wcfm-style-enquiry-button311f.css?ver=6.2.7' type='text/css' media='all' />
    <link rel='stylesheet' id='wcfm_subscribe_button_css-css' href='wp-content/plugins/wc-multivendor-membership/assets/css/min/wcfmvm-style-subscribe-button6770.css?ver=2.6.5' type='text/css' media='all' />
    <link rel='stylesheet' id='wcfm_qtip_css-css' href='wp-content/plugins/wc-frontend-manager/includes/libs/qtip/qtip311f.css?ver=6.2.7' type='text/css' media='all' />
    <link rel='stylesheet' id='wcfm_colorbox_css-css' href='wp-content/plugins/wc-frontend-manager/includes/libs/jquery-colorbox/colorbox311f.css?ver=6.2.7' type='text/css' media='all' />
    <link rel='stylesheet' id='jquery-ui-style-css' href='wp-content/plugins/woocommerce/assets/css/jquery-ui/jquery-ui.min3088.css?ver=3.7.0' type='text/css' media='all' />
    <link rel='stylesheet' id='wcfm_wc_icon_css-css' href='wp-content/plugins/wc-frontend-manager/assets/css/min/wcfm-style-icon311f.css?ver=6.2.7' type='text/css' media='all' />
    <link rel='stylesheet' id='wcfm_fa_icon_css-css' href='wp-content/plugins/wc-frontend-manager/assets/fonts/font-awesome/css/wcfmicon.min311f.css?ver=6.2.7' type='text/css' media='all' />
    <link rel='stylesheet' id='wcfm_admin_bar_css-css' href='wp-content/plugins/wc-frontend-manager/assets/css/min/wcfm-style-adminbar311f.css?ver=6.2.7' type='text/css' media='all' />
    <link rel='stylesheet' id='wcfm_core_css-css' href='wp-content/plugins/wc-frontend-manager/assets/css/min/wcfm-style-core311f.css?ver=6.2.7' type='text/css' media='all' />
    <link rel='stylesheet' id='um_fonticons_ii-css' href='wp-content/plugins/ultimate-member/assets/css/um-fonticons-ii7ab2.css?ver=2.0.55' type='text/css' media='all' />
    <link rel='stylesheet' id='um_fonticons_fa-css' href='wp-content/plugins/ultimate-member/assets/css/um-fonticons-fa7ab2.css?ver=2.0.55' type='text/css' media='all' />
    <link rel='stylesheet' id='select2-css' href='wp-content/plugins/ultimate-member/assets/css/select2/select2.min7ab2.css?ver=2.0.55' type='text/css' media='all' />
    <link rel='stylesheet' id='um_crop-css' href='wp-content/plugins/ultimate-member/assets/css/um-crop7ab2.css?ver=2.0.55' type='text/css' media='all' />
    <link rel='stylesheet' id='um_modal-css' href='wp-content/plugins/ultimate-member/assets/css/um-modal7ab2.css?ver=2.0.55' type='text/css' media='all' />
    <link rel='stylesheet' id='um_styles-css' href='wp-content/plugins/ultimate-member/assets/css/um-styles7ab2.css?ver=2.0.55' type='text/css' media='all' />
    <link rel='stylesheet' id='um_members-css' href='wp-content/plugins/ultimate-member/assets/css/um-members7ab2.css?ver=2.0.55' type='text/css' media='all' />
    <link rel='stylesheet' id='um_profile-css' href='wp-content/plugins/ultimate-member/assets/css/um-profile7ab2.css?ver=2.0.55' type='text/css' media='all' />
    <link rel='stylesheet' id='um_account-css' href='wp-content/plugins/ultimate-member/assets/css/um-account7ab2.css?ver=2.0.55' type='text/css' media='all' />
    <link rel='stylesheet' id='um_misc-css' href='wp-content/plugins/ultimate-member/assets/css/um-misc7ab2.css?ver=2.0.55' type='text/css' media='all' />
    <link rel='stylesheet' id='um_fileupload-css' href='wp-content/plugins/ultimate-member/assets/css/um-fileupload7ab2.css?ver=2.0.55' type='text/css' media='all' />
    <link rel='stylesheet' id='um_datetime-css' href='wp-content/plugins/ultimate-member/assets/css/pickadate/default7ab2.css?ver=2.0.55' type='text/css' media='all' />
    <link rel='stylesheet' id='um_datetime_date-css' href='wp-content/plugins/ultimate-member/assets/css/pickadate/default.date7ab2.css?ver=2.0.55' type='text/css' media='all' />
    <link rel='stylesheet' id='um_datetime_time-css' href='wp-content/plugins/ultimate-member/assets/css/pickadate/default.time7ab2.css?ver=2.0.55' type='text/css' media='all' />
    <link rel='stylesheet' id='um_raty-css' href='wp-content/plugins/ultimate-member/assets/css/um-raty7ab2.css?ver=2.0.55' type='text/css' media='all' />
    <link rel='stylesheet' id='um_scrollbar-css' href='wp-content/plugins/ultimate-member/assets/css/simplebar7ab2.css?ver=2.0.55' type='text/css' media='all' />
    <link rel='stylesheet' id='um_tipsy-css' href='wp-content/plugins/ultimate-member/assets/css/um-tipsy7ab2.css?ver=2.0.55' type='text/css' media='all' />
    <link rel='stylesheet' id='um_responsive-css' href='wp-content/plugins/ultimate-member/assets/css/um-responsive7ab2.css?ver=2.0.55' type='text/css' media='all' />
    <link rel='stylesheet' id='um_default_css-css' href='wp-content/plugins/ultimate-member/assets/css/um-old-default7ab2.css?ver=2.0.55' type='text/css' media='all' />
    <link rel='stylesheet' id='renita_bootstrap.min-css' href='wp-content/themes/rentit/js/bootstrap/css/bootstrap.min9dff.css?ver=5.3.2' type='text/css' media='all' />
    <link rel='stylesheet' id='renita_bootstrap-select-css' href='wp-content/themes/rentit/js/bootstrap-select/css/bootstrap-select.min9dff.css?ver=5.3.2' type='text/css' media='all' />
    <link rel='stylesheet' id='renita_font-awesome-css' href='wp-content/themes/rentit/js/fontawesome/css/font-awesome.min9dff.css?ver=5.3.2' type='text/css' media='all' />
    <link rel='stylesheet' id='renita_prettyPhoto-css' href='wp-content/themes/rentit/js/prettyphoto/css/prettyPhoto9dff.css?ver=5.3.2' type='text/css' media='all' />
    <link rel='stylesheet' id='renita_owl.carousel.min-css' href='wp-content/themes/rentit/js/owl-carousel2/assets/owl.carousel.min9dff.css?ver=5.3.2' type='text/css' media='all' />
    <link rel='stylesheet' id='renita_owl.theme.default.min-css' href='wp-content/themes/rentit/js/owl-carousel2/assets/owl.theme.default.min9dff.css?ver=5.3.2' type='text/css' media='all' />
    <link rel='stylesheet' id='renita_animate.min-css' href='wp-content/themes/rentit/js/animate/animate.min9dff.css?ver=5.3.2' type='text/css' media='all' />
    <link rel='stylesheet' id='renita_swiper.min-css' href='wp-content/themes/rentit/js/swiper/css/swiper.min9dff.css?ver=5.3.2' type='text/css' media='all' />
    <link rel='stylesheet' id='renita_bootstrap-datetimepicker-css' href='wp-content/themes/rentit/js/datetimepicker/css/bootstrap-datetimepicker.min9dff.css?ver=5.3.2' type='text/css' media='all' />
    <link rel='stylesheet' id='renita_theme1-css' href='wp-content/themes/rentit/css/theme9dff.css?ver=5.3.2' type='text/css' media='all' />
    <link rel='stylesheet' id='renita_theme2-css' href='wp-content/themes/rentit/css/theme-red-19dff.css?ver=5.3.2' type='text/css' media='all' />
    <link rel='stylesheet' id='chld_thm_cfg_parent-css' href='wp-content/themes/rentit/style9dff.css?ver=5.3.2' type='text/css' media='all' />
    <link rel='stylesheet' id='renita_wp-css' href='wp-content/themes/rentit-child/style4eb1.css?ver=1.7.3.1566665367' type='text/css' media='all' />
    </style>
    <link rel='stylesheet' id='renita_jquery-style-css' href='wp-content/themes/rentit/css/jquery-ui9dff.css?ver=5.3.2' type='text/css' media='all' />
    <script type='text/javascript' src='code.jquery.com/jquery-1.11.3.minc1d8.js?ver=1.11.3'></script>
    <script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min44fd.js?ver=2.70'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var wc_add_to_cart_params = {
            "ajax_url": "\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
            "i18n_view_cart": "View cart",
            "cart_url": "http:\/\/reekar.com\/cart\/",
            "is_cart": "",
            "cart_redirect_after_add": "yes"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min3088.js?ver=3.7.0'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var wpgmza_google_api_status = {
            "message": "Remove API checked in settings",
            "code": "REMOVE_API_CHECKED"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/plugins/wp-google-maps/wpgmza_data9dff.js?ver=5.3.2'></script>
    <script src="wp-includes/js/jquery/jquery.js"></script>
        <script src="wp-content/themes/rentit-child/common.js"></script>
    <script type='text/javascript' src='wp-content/plugins/js_composer/assets/js/vendors/woocommerce-add-to-cart52c7.js?ver=6.0.5'></script>
    <script type='text/javascript' src='wp-content/plugins/ultimate-member/assets/js/um-gdpr.min7ab2.js?ver=2.0.55'></script>
    <link rel='https://api.w.org/' href='wp-json/index.html' />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc0db0.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="wp-includes/wlwmanifest.xml" />
    <meta name="generator" content="WordPress 5.3.2" />
    <meta name="generator" content="WooCommerce 3.7.0" />
    <link rel='shortlink' href='index3256.html?p=11092' />
    <link rel="alternate" type="application/json+oembed" href="wp-json/oembed/1.0/embede888.json?url=http%3A%2F%2Freekar.com%2Fpassword-reset%2F" />
    <link rel="alternate" type="text/xml+oembed" href="wp-json/oembed/1.0/embedb19c?url=http%3A%2F%2Freekar.com%2Fpassword-reset%2F&amp;format=xml" />
    <meta name="referrer" content="always" />
    <noscript>
    </noscript>
    <meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress." />
    <link rel="icon" href="wp-content/uploads/2019/08/cropped-imageedit_8_2407516188-3-32x32.png" sizes="32x32" />
    <link rel="icon" href="wp-content/uploads/2019/08/cropped-imageedit_8_2407516188-3-192x192.png" sizes="192x192" />
    <link rel="apple-touch-icon-precomposed" href="wp-content/uploads/2019/08/cropped-imageedit_8_2407516188-3-180x180.png" />
    <link rel="stylesheet" type="text/css" href="index.css">
    <script type="text/javascript" src="custom.js"></script>
    <meta name="msapplication-TileImage" content="http://reekar.com/wp-content/uploads/2019/08/cropped-imageedit_8_2407516188-3-270x270.png" />
    <script type="text/javascript">
        var global_map_style = [];
    </script>
    <noscript>
    </noscript>
</head>

<body id="home" class="page-template page-template-template-fullwidth-breadcrumb page-template-template-fullwidth-breadcrumb-php page page-id-11092 wp-embed-responsive wide um-page-password-reset um-page-loggedout theme-rentit woocommerce-no-js page-password-reset wcfm-theme-rentit wpb-js-composer js-comp-ver-6.0.5 vc_responsive">
    <!-- PRELOADER -->
    <div id="preloader">
        <div id="preloader-status">
            <div class="spinner">
                <div class="rect1"></div>
                <div class="rect2"></div>
                <div class="rect3"></div>
                <div class="rect4"></div>
                <div class="rect5"></div>
            </div>
            <div id="preloader-title"> Loading </div>
        </div>
    </div>

    <div class="wrapper">
        <header class="header fixed">
            <div class="header-wrapper">
                <div class="container">
                    <!-- Logo -->
                    <div class="logo">
                        <a href="index.html"><img src="wp-content/uploads/2019/12/imgonline-com-ua-remJpegArtifacts-klxDUKE0IaXu.png" alt="Reekar" /></a>
                    </div>
                    <a href="#" class="menu-toggle btn ripple-effect btn-theme-transparent"><i class="fa fa-bars"></i></a>
                    <nav class="navigation closed clearfix">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <a href="#" class="menu-toggle-close btn"><i class="fa fa-times"></i></a>
                                <div class="menu-reekar-menu-container">
                                    <ul id="menu-reekar-menu" class="rentit_topmenu nav sf-menu">
                                        <li data-menuflag="" data-parent="0" id="menu-item-10986" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-10986 detph0"><a href="index.html">HOME</a></li>
                                        <li data-menuflag="" data-parent="0" id="menu-item-12411" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12411 detph0"><a href="search/index.html">BOOK A CAR</a></li>
                                        <li data-menuflag="" data-parent="0" id="menu-item-11551" class="listmycar-menuitem menu-item menu-item-type-post_type menu-item-object-page menu-item-11551 detph0"><a href="your-car/index.html">LIST MY CAR</a></li>
                                        <li data-menuflag="" data-parent="0" id="menu-item-11925" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-11925 detph0"><a href="sample-page/index.html">HOW IT WORKS</a></li>
                                        <li data-menuflag="" data-parent="0" id="menu-item-12673" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-12673 detph0"><a href="privacy-policy-2/index.html">TERMS OF SERVICE</a>
                                            <ul data-menuflaglvl1='' data-depth='0' class="bbbb sub-menu">
                                                <li data-menuflag="" data-parent="12673" id="menu-item-11918" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-11918 detph1"><a href="privacy-policy/index.html">PRIVACY POLICY</a></li>
                                            </ul>
                                        </li>
                                        <li data-menuflag="" data-parent="0" id="menu-item-11812" class="logout-menuitem menu-item menu-item-type-post_type menu-item-object-page menu-item-11812 detph0"><a href="login-2/index.html">login</a></li>
                                        <li>
                                            <ul class="social-icons">
                                                <li>
                                                    <a target="_blank" href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a>
                                                </li>

                                                <li>
                                                    <a target="_blank" href="https://twitter.com/"><i class="fa fa-twitter"></i></a>
                                                </li>

                                                <li>
                                                    <a target="_blank" href="https://www.instagram.com/"><i class="fa fa-instagram"></i></a>
                                                </li>

                                                <li>
                                                    <a target="_blank" href="https://linkedin.com/"><i class="fa fa-linkedin"></i></a>
                                                </li>

                                                <li>
                                                    <a target="_blank" href="https://youtube.com/"><i class="fa fa-youtube"></i></a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <div>
                                </div>
                            </div>

                        </div>
                        <div class="swiper-scrollbar"></div>
                    </nav>
                </div>
            </div>
        </header>


        
        <div class="content-area">
            <!-- BREADCRUMBS -->
            <section class="page-section breadcrumbs text-center">
                <div class="container">
                    <div class="page-header">
                        <h1>Password Reset</h1>
                    </div>
                    <ul class="breadcrumb">
                        <li><a href="index.html">
                            Home                        </a></li>
                        <li class="active"> Password Reset</li>
                    </ul>
                </div>
            </section>

            <div class="um um-password um-um_password_id">
                <div class="um-form">
                    <form method="post" action="#">

                        <input type="hidden" name="_um_password_reset" id="_um_password_reset" value="1" />

                        <div class="um-field um-field-block um-field-type_block">
                            <div class="um-field-block">
                                <div style="text-align:center;">
                                    To reset your password, please enter your email address or username below </div>
                            </div>
                        </div>
                        <div class="um-field um-field-username_b um-field-text um-field-type_text" data-key="username_b">
                            <div class="um-field-area">
                                <input autocomplete="off" class="um-form-field valid " type="text" name="username_b" id="username_b" value="" placeholder="Enter your username or email" data-validate="" data-key="username_b" />

                            </div>
                        </div>
                        <div class="um-col-alt um-col-alt-b">

                            <div class="um-center">
                                <input type="submit" value="Reset my password" class="um-button" id="um-submit-btn" />
                            </div>

                            <div class="um-clear"></div>

                        </div>

                        <input type="hidden" name="form_id" id="form_id_um_password_id" value="um_password_id" />

                        <input type="hidden" name="timestamp" class="um_timestamp" value="1580887327" />

                        <p class="request_name">
                            <label for="request_um_password_id">Only fill in if you are not human</label>
                            <input type="text" name="request" id="request_um_password_id" class="input" value="" size="25" autocomplete="off" />
                        </p>

                    </form>
                </div>
            </div>
            <style type="text/css">
                .um-um_password_id.um {
                    max-width: 450px;
                }
            </style>

            <p></p>
        </div>
		






        <div id="to-top" class="to-top"><i class="fa fa-angle-up"></i></div>
    </div>

    <script>
        jQuery(document).ready(function($) {

            $('#tabs1 li a').click(function() {
                jQuery(window).trigger('resize');
            });
        });
    </script>
    <div id="um_upload_single" style="display:none">

    </div>
    <div id="um_view_photo" style="display:none">

        <a href="#" data-action="um_remove_modal" class="um-modal-close"><i class="um-faicon-times"></i></a>

        <div class="um-modal-body photo">

            <div class="um-modal-photo">

            </div>

        </div>

    </div>
    <script type="text/javascript">
        var c = document.body.className;
        c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
        document.body.className = c;
    </script>
    <script type="text/javascript">
        var wc_product_block_data = JSON.parse(decodeURIComponent('%7B%22min_columns%22%3A1%2C%22max_columns%22%3A6%2C%22default_columns%22%3A3%2C%22min_rows%22%3A1%2C%22max_rows%22%3A6%2C%22default_rows%22%3A1%2C%22thumbnail_size%22%3A300%2C%22placeholderImgSrc%22%3A%22http%3A%5C%2F%5C%2Freekar.com%5C%2Fwp-content%5C%2Fuploads%5C%2Fwoocommerce-placeholder-300x300.png%22%2C%22min_height%22%3A500%2C%22default_height%22%3A500%2C%22isLargeCatalog%22%3Afalse%2C%22limitTags%22%3Afalse%2C%22hasTags%22%3Atrue%2C%22productCategories%22%3A%5B%7B%22term_id%22%3A159%2C%22name%22%3A%22Uncategorized%22%2C%22slug%22%3A%22uncategorized%22%2C%22term_group%22%3A0%2C%22term_taxonomy_id%22%3A159%2C%22taxonomy%22%3A%22product_cat%22%2C%22description%22%3A%22%22%2C%22parent%22%3A0%2C%22count%22%3A1%2C%22filter%22%3A%22raw%22%2C%22link%22%3A%22http%3A%5C%2F%5C%2Freekar.com%5C%2Fproduct-category%5C%2Funcategorized%5C%2F%22%7D%2C%7B%22term_id%22%3A131%2C%22name%22%3A%22Best%20Offers%22%2C%22slug%22%3A%22best-offers%22%2C%22term_group%22%3A0%2C%22term_taxonomy_id%22%3A131%2C%22taxonomy%22%3A%22product_cat%22%2C%22description%22%3A%22%22%2C%22parent%22%3A0%2C%22count%22%3A2%2C%22filter%22%3A%22raw%22%2C%22link%22%3A%22http%3A%5C%2F%5C%2Freekar.com%5C%2Fproduct-category%5C%2Fbest-offers%5C%2F%22%7D%2C%7B%22term_id%22%3A147%2C%22name%22%3A%22Economic%20Cars%22%2C%22slug%22%3A%22economic-cars%22%2C%22term_group%22%3A0%2C%22term_taxonomy_id%22%3A147%2C%22taxonomy%22%3A%22product_cat%22%2C%22description%22%3A%22%22%2C%22parent%22%3A0%2C%22count%22%3A0%2C%22filter%22%3A%22raw%22%2C%22link%22%3A%22http%3A%5C%2F%5C%2Freekar.com%5C%2Fproduct-category%5C%2Feconomic-cars%5C%2F%22%7D%2C%7B%22term_id%22%3A132%2C%22name%22%3A%22Popular%20Cars%22%2C%22slug%22%3A%22popular-cars%22%2C%22term_group%22%3A0%2C%22term_taxonomy_id%22%3A132%2C%22taxonomy%22%3A%22product_cat%22%2C%22description%22%3A%22%22%2C%22parent%22%3A0%2C%22count%22%3A0%2C%22filter%22%3A%22raw%22%2C%22link%22%3A%22http%3A%5C%2F%5C%2Freekar.com%5C%2Fproduct-category%5C%2Fpopular-cars%5C%2F%22%7D%2C%7B%22term_id%22%3A149%2C%22name%22%3A%22Subcompact%22%2C%22slug%22%3A%22subcompact%22%2C%22term_group%22%3A0%2C%22term_taxonomy_id%22%3A149%2C%22taxonomy%22%3A%22product_cat%22%2C%22description%22%3A%22%22%2C%22parent%22%3A0%2C%22count%22%3A0%2C%22filter%22%3A%22raw%22%2C%22link%22%3A%22http%3A%5C%2F%5C%2Freekar.com%5C%2Fproduct-category%5C%2Fsubcompact%5C%2F%22%7D%5D%2C%22homeUrl%22%3A%22http%3A%5C%2F%5C%2Freekar.com%5C%2F%22%7D'));
    </script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var wpcf7 = {
            "apiSettings": {
                "root": "http:\/\/reekar.com\/wp-json\/contact-form-7\/v1",
                "namespace": "contact-form-7\/v1"
            }
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/plugins/contact-form-7/includes/js/scripts58e0.js?ver=5.1.4'></script>
    <script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min6b25.js?ver=2.1.4'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var woocommerce_params = {
            "ajax_url": "\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/?wc-ajax=%%endpoint%%"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min3088.js?ver=3.7.0'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var wc_cart_fragments_params = {
            "ajax_url": "\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
            "cart_hash_key": "wc_cart_hash_91575ff9a5d21710d3a3c236909b6508",
            "fragment_name": "wc_fragments_91575ff9a5d21710d3a3c236909b6508",
            "request_timeout": "5000"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min3088.js?ver=3.7.0'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var spuvar = {
            "is_admin": "",
            "disable_style": "",
            "ajax_mode": "",
            "ajax_url": "http:\/\/reekar.com\/wp-admin\/admin-ajax.php",
            "ajax_mode_url": "http:\/\/reekar.com\/?spu_action=spu_load",
            "pid": "11092",
            "is_front_page": "",
            "is_category": "",
            "site_url": "http:\/\/reekar.com",
            "is_archive": "",
            "is_search": "",
            "is_preview": "",
            "seconds_confirmation_close": "5"
        };
        var spuvar_social = [];
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/plugins/popups/public/assets/js/public4bfd.js?ver=1.9.3.6'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var mailchimp_public_data = {
            "site_url": "http:\/\/reekar.com",
            "ajax_url": "http:\/\/reekar.com\/wp-admin\/admin-ajax.php",
            "queue_url": "http:\/\/reekar.com\/wp-json\/mailchimp-for-woocommerce\/v1\/queue\/work",
            "queue_should_fire": ""
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/plugins/mailchimp-for-woocommerce/public/js/mailchimp-woocommerce-public.min6472.js?ver=2.1.17'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var wcfm_enquiry_manage_messages = {
            "no_name": "Name is required.",
            "no_email": "Email is required.",
            "no_enquiry": "Please insert your Inquiry before submit.",
            "no_reply": "Please insert your reply before submit.",
            "enquiry_saved": "Your inquiry successfully sent.",
            "enquiry_published": "Inquiry reply successfully published.",
            "enquiry_reply_saved": "Your reply successfully sent."
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/plugins/wc-frontend-manager/assets/js/min/enquiry/wcfm-script-enquiry-tab311f.js?ver=6.2.7'></script>
    <script type='text/javascript' src='wp-content/plugins/wc-multivendor-membership/assets/js/min/wcfmvm-script-membership-subscribe6770.js?ver=2.6.5'></script>
    <script type='text/javascript' src='wp-content/plugins/wc-frontend-manager/includes/libs/qtip/qtip311f.js?ver=6.2.7'></script>
    <script type='text/javascript' src='wp-content/plugins/wc-frontend-manager/includes/libs/jquery-blockui/jquery.blockUI.min311f.js?ver=6.2.7'></script>
    <script type='text/javascript' src='wp-content/plugins/wc-frontend-manager/includes/libs/jquery-colorbox/jquery.colorbox311f.js?ver=6.2.7'></script>
    <script type='text/javascript' src='wp-includes/js/jquery/ui/core.mine899.js?ver=1.11.4'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var wcfm_datepicker_params = {
            "closeText": "Done",
            "currentText": "Today",
            "monthNames": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            "monthNamesShort": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            "monthStatus": "Show a different month",
            "dayNames": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            "dayNamesShort": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
            "dayNamesMin": ["S", "M", "T", "W", "T", "F", "S"],
            "dateFormat": "MM dd, yy",
            "firstDay": "1",
            "isRTL": ""
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-includes/js/jquery/ui/datepicker.mine899.js?ver=1.11.4'></script>
    <script type='text/javascript'>
        jQuery(document).ready(function(jQuery) {
            jQuery.datepicker.setDefaults({
                "closeText": "Close",
                "currentText": "Today",
                "monthNames": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                "monthNamesShort": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                "nextText": "Next",
                "prevText": "Previous",
                "dayNames": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                "dayNamesShort": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                "dayNamesMin": ["S", "M", "T", "W", "T", "F", "S"],
                "dateFormat": "MM d, yy",
                "firstDay": 1,
                "isRTL": false
            });
        });
    </script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var wcfm_notification_sound = "index.html\/\/reekar.com\/wp-content\/plugins\/wc-frontend-manager\/assets\/sounds\/notification.mp3";
        var wcfm_desktop_notification_sound = "index.html\/\/reekar.com\/wp-content\/plugins\/wc-frontend-manager\/assets\/sounds\/desktop_notification.mp3";
        var wcfm_core_dashboard_messages = {
            "product_approve_confirm": "Are you sure and want to approve \/ publish this 'Product'?",
            "product_archive_confirm": "Are you sure and want to archive this 'Product'?",
            "multiblock_delete_confirm": "Are you sure and want to delete this 'Block'?\nYou can't undo this action ...",
            "article_delete_confirm": "Are you sure and want to delete this 'Article'?\nYou can't undo this action ...",
            "product_delete_confirm": "Are you sure and want to delete this 'Product'?\nYou can't undo this action ...",
            "message_delete_confirm": "Are you sure and want to delete this 'Message'?\nYou can't undo this action ...",
            "order_delete_confirm": "Are you sure and want to delete this 'Order'?\nYou can't undo this action ...",
            "enquiry_delete_confirm": "Are you sure and want to delete this 'Enquiry'?\nYou can't undo this action ...",
            "support_delete_confirm": "Are you sure and want to delete this 'Support Ticket'?\nYou can't undo this action ...",
            "follower_delete_confirm": "Are you sure and want to delete this 'Follower'?\nYou can't undo this action ...",
            "following_delete_confirm": "Are you sure and want to delete this 'Following'?\nYou can't undo this action ...",
            "order_mark_complete_confirm": "Are you sure and want to 'Mark as Complete' this Order?",
            "booking_mark_complete_confirm": "Are you sure and want to 'Mark as Confirmed' this Booking?",
            "appointment_mark_complete_confirm": "Are you sure and want to 'Mark as Complete' this Appointment?",
            "add_new": "Add New",
            "select_all": "Select all",
            "select_none": "Select none",
            "any_attribute": "Any",
            "add_attribute_term": "Enter a name for the new attribute term:",
            "wcfmu_upgrade_notice": "Please upgrade your WC Frontend Manager to Ultimate version and avail this feature.",
            "pdf_invoice_upgrade_notice": "Install WC Frontend Manager Ultimate and WooCommerce PDF Invoices & Packing Slips to avail this feature.",
            "wcfm_bulk_action_no_option": "Please select some element first!!",
            "wcfm_bulk_action_confirm": "Are you sure and want to do this?\nYou can't undo this action ...",
            "review_status_update_confirm": "Are you sure and want to do this?",
            "everywhere": "Everywhere Else",
            "required_message": "This field is required.",
            "choose_select2": "Choose ",
            "category_attribute_mapping": "All Attributes",
            "search_attribute_select2": "Search for a attribute ...",
            "search_product_select2": "Search for a product ...",
            "search_taxonomy_select2": "Search for a category ...",
            "choose_category_select2": "Choose Categories ...",
            "choose_listings_select2": "Choose Listings ...",
            "choose_vendor_select2": "Choose Vendor ...",
            "no_category_select2": "No categories",
            "select2_searching": "Searching ...",
            "select2_no_result": "No matching result found.",
            "select2_loading_more": "Loading ...",
            "select2_minimum_input": "Minimum input character ",
            "wcfm_product_popup_next": "Next",
            "wcfm_product_popup_previous": "Previous",
            "wcfm_multiblick_addnew_help": "Add New Block",
            "wcfm_multiblick_remove_help": "Remove Block",
            "wcfm_multiblick_collapse_help": "Toggle Block",
            "wcfm_multiblick_sortable_help": "Drag to re-arrange blocks",
            "sell_this_item_confirm": "Do you want to add this item(s) to your store?",
            "bulk_no_itm_selected": "Please select some product first!",
            "user_non_logged_in": "Please login to the site first!",
            "shiping_method_not_selected": "Please select a shipping method",
            "shiping_method_not_found": "Shipping method not found",
            "shiping_zone_not_found": "Shipping zone not found",
            "shipping_method_del_confirm": "Are you sure you want to delete this 'Shipping Method'?\nYou can't undo this action ..."
        };
        var wcfm_params = {
            "ajax_url": "\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/wp-admin\/admin-ajax.php",
            "shop_url": "http:\/\/reekar.com\/shop\/",
            "wcfm_is_allow_wcfm": "",
            "wcfm_is_vendor": "",
            "is_user_logged_in": "",
            "wcfm_allow_tinymce_options": "undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify |  bullist numlist outdent indent | link image | ltr rtl",
            "unread_message": "0",
            "unread_enquiry": "0",
            "wcfm_is_desktop_notification": "1",
            "wcfm_is_allow_external_product_analytics": ""
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/plugins/wc-frontend-manager/assets/js/min/wcfm-script-core311f.js?ver=6.2.7'></script>
    <script type='text/javascript' src='wp-includes/js/imagesloaded.min55a0.js?ver=3.2.0'></script>
    <script type='text/javascript' src='wp-includes/js/masonry.mind617.js?ver=3.3.2'></script>
    <script type='text/javascript' src='wp-includes/js/jquery/jquery.masonry.minef70.js?ver=3.1.2b'></script>
    <script type='text/javascript' src='wp-content/plugins/ultimate-member/assets/js/select2/select2.full.min7ab2.js?ver=2.0.55'></script>
    <script type='text/javascript' src='wp-includes/js/underscore.min4511.js?ver=1.8.3'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var _wpUtilSettings = {
            "ajax": {
                "url": "\/wp-admin\/admin-ajax.php"
            }
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-includes/js/wp-util.min9dff.js?ver=5.3.2'></script>
    <script type='text/javascript' src='wp-content/plugins/ultimate-member/assets/js/um-crop.min7ab2.js?ver=2.0.55'></script>
    <script type='text/javascript' src='wp-content/plugins/ultimate-member/assets/js/um-modal.min7ab2.js?ver=2.0.55'></script>
    <script type='text/javascript' src='wp-content/plugins/ultimate-member/assets/js/um-jquery-form.min7ab2.js?ver=2.0.55'></script>
    <script type='text/javascript' src='wp-content/plugins/ultimate-member/assets/js/um-fileupload.min7ab2.js?ver=2.0.55'></script>
    <script type='text/javascript' src='wp-content/plugins/ultimate-member/assets/js/pickadate/picker7ab2.js?ver=2.0.55'></script>
    <script type='text/javascript' src='wp-content/plugins/ultimate-member/assets/js/pickadate/picker.date7ab2.js?ver=2.0.55'></script>
    <script type='text/javascript' src='wp-content/plugins/ultimate-member/assets/js/pickadate/picker.time7ab2.js?ver=2.0.55'></script>
    <script type='text/javascript' src='wp-content/plugins/ultimate-member/assets/js/pickadate/legacy7ab2.js?ver=2.0.55'></script>
    <script type='text/javascript' src='wp-content/plugins/ultimate-member/assets/js/um-raty.min7ab2.js?ver=2.0.55'></script>
    <script type='text/javascript' src='wp-content/plugins/ultimate-member/assets/js/um-tipsy.min7ab2.js?ver=2.0.55'></script>
    <script type='text/javascript' src='wp-content/plugins/ultimate-member/assets/js/simplebar.min7ab2.js?ver=2.0.55'></script>
    <script type='text/javascript' src='wp-content/plugins/ultimate-member/assets/js/um-functions.min7ab2.js?ver=2.0.55'></script>
    <script type='text/javascript' src='wp-content/plugins/ultimate-member/assets/js/um-responsive.min7ab2.js?ver=2.0.55'></script>
    <script type='text/javascript' src='wp-content/plugins/ultimate-member/assets/js/um-conditional.min7ab2.js?ver=2.0.55'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var um_scripts = {
            "nonce": "ba83493fd2"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/plugins/ultimate-member/assets/js/um-scripts.min7ab2.js?ver=2.0.55'></script>
    <script type='text/javascript' src='wp-content/plugins/ultimate-member/assets/js/um-members.min7ab2.js?ver=2.0.55'></script>
    <script type='text/javascript' src='wp-content/plugins/ultimate-member/assets/js/um-profile.min7ab2.js?ver=2.0.55'></script>
    <script type='text/javascript' src='wp-content/plugins/ultimate-member/assets/js/um-account.min7ab2.js?ver=2.0.55'></script>
    <script type='text/javascript' src='wp-includes/js/jquery/ui/widget.mine899.js?ver=1.11.4'></script>
    <script type='text/javascript' src='wp-includes/js/jquery/ui/mouse.mine899.js?ver=1.11.4'></script>
    <script type='text/javascript' src='wp-includes/js/jquery/ui/slider.mine899.js?ver=1.11.4'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var rentit_obj = {
            "theme_url": "http:\/\/reekar.com\/wp-content\/themes\/rentit",
            "date_format": "MM\/DD\/YYYY H:mm",
            "lang": "en",
            "plugin_activate": "1",
            "plugin_error_message": "Error: Activate rentit plugin!",
            "themeurl": "http:\/\/reekar.com\/wp-content\/themes\/rentit",
            "ajaxurl": "http:\/\/reekar.com\/wp-admin\/admin-ajax.php",
            "lat": "35.126413",
            "longu": "33.429858999999965",
            "global_map_style": "[]",
            "blocked_hours": [],
            "zum": "9",
            "location": ["Airport", "Famagusta", "Kyrenia", "Larnaca", "Limassol", "Nicosia", "Paphos"],
            "price_group": ["Business Cars", "Economic Cars", "Luxury Cars", "Premium Cars"],
            "reserved_date": [],
            "GET": [],
            "currency": "$",
            "la st_cat": "uncategorized",
            "rtl": "",
            "currency_pos": "left"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/themes/rentit/js/bootstrap/js/bootstrap68b3.js?ver=1'></script>
    <script type='text/javascript' src='wp-content/themes/rentit/js/bootstrap-select/js/bootstrap-select.min68b3.js?ver=1'></script>
    <script type='text/javascript' src='wp-content/themes/rentit/js/superfish/js/superfish.min68b3.js?ver=1'></script>
    <script type='text/javascript' src='wp-content/themes/rentit/js/prettyphoto/js/jquery.prettyPhoto68b3.js?ver=1'></script>
    <script type='text/javascript' src='wp-content/themes/rentit/js/owl-carousel2/owl.carousel.min68b3.js?ver=1'></script>
    <script type='text/javascript' src='wp-content/themes/rentit/js/jquery.sticky.min68b3.js?ver=1'></script>
    <script type='text/javascript' src='wp-content/themes/rentit/js/jquery.easing.min68b3.js?ver=1'></script>
    <script type='text/javascript' src='wp-content/themes/rentit/js/jquery.smoothscroll.min68b3.js?ver=1'></script>
    <script type='text/javascript' src='wp-content/themes/rentit/js/swiper/js/swiper.jquery.min68b3.js?ver=1'></script>
    <script type='text/javascript' src='wp-content/themes/rentit/js/datetimepicker/js/moment-with-locales.min68b3.js?ver=1'></script>
    <script type='text/javascript' src='wp-content/themes/rentit/js/datetimepicker/js/bootstrap-datetimepicker.min68b3.js?ver=1'></script>
    <script type='text/javascript' src='wp-content/themes/rentit/js/theme-ajax-mail68b3.js?ver=1'></script>
    <script type='text/javascript' src='wp-content/themes/rentit/js/theme68b3.js?ver=1'></script>
    <script type='text/javascript' src='wp-content/themes/rentit/js/main68b3.js?ver=1'></script>
    <script type='text/javascript' src='wp-content/themes/rentit/js/clustern68b3.js?ver=1'></script>
    <script type='text/javascript' src='wp-content/themes/rentit/js/map_init68b3.js?ver=1'></script>
    <script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key&amp;libraries=places&amp;callback=initialize_map&amp;ver=3'></script>
    <script type='text/javascript' src='wp-content/themes/rentit/js/bootstrap-typeahead68b3.js?ver=1'></script>
    <script type='text/javascript' src='wp-content/themes/rentit/js/countdown/jquery.plugin.min68b3.js?ver=1'></script>
    <script type='text/javascript' src='wp-content/themes/rentit/js/countdown/jquery.countdown.min68b3.js?ver=1'></script>
    <script type='text/javascript' src='wp-content/themes/rentit/js/jquery.isotope.min68b3.js?ver=1'></script>
    <script type='text/javascript' src='wp-includes/js/wp-embed.min9dff.js?ver=5.3.2'></script>
    <script type="text/javascript">
        jQuery('#request').val('');
    </script>
</body>

<!-- Mirrored from reekar.com/password-reset/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 05 Feb 2020 07:23:14 GMT -->

</html>