<?php

return [
  // api version
    // 'brainTree_merchant' => env('MY_MERCHANT_SANDBOX_ID'),
    // 'brainTree_private' => env('MY_PRIVATE_SANDBOX_ID'),
    // 'brainTree_public' => env('MY_PUBLIC_SANDBOX_ID'),
    #PYPAL 
    // 'sandbox_clientId' => env('SANDBOX_CLIENT_ID'),

    'api_version' => env('API_VERSION', 'v1'),
    'stripe_p_key' => env('STRIPE_P_KEY'),
    'stripe_secret_key' => env('STRIPE_SECRET_K'),

    // personal client
    'personal_client_id' => env('PERSONAL_CLIENT_ID', 1),
    'personal_client_key' => env('PERSONAL_CLIENT_SECRET', ''),

    // password client
    'password_client_id' => env('PASSWORD_CLIENT_ID', 2),
    'password_client_secret' => env('PASSWORD_CLIENT_SECRET', ''),
];